package com.recipeton.scrapper.service.ckd;

/*-
 * #%L
 * recipeton-scrapper
 * %%
 * Copyright (C) 2021 tincomisc
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.recipeton.shared.domain.RecipeDefinition;
import org.junit.jupiter.api.Test;

import java.time.Duration;

import static org.assertj.core.api.AssertionsForClassTypes.assertThat;

class CkdRecipeFormMapperTest {
    CkdRecipeFormMapper ckdRecipeFormMapper = new CkdRecipeFormMapper() {
        @Override
        public RecipeDefinition toRecipe(CkdRecipe ckdRecipe) {
            return null;
        }
    };

    @Test
    void testToDurationGivenDurationInMinutesThenReturnsDuration() {
        assertThat(ckdRecipeFormMapper.toDuration("PT1M")).isEqualTo(Duration.ofMinutes(1));
    }

    @Test
    void testToDurationGivenDurationInSecondsThenReturnsDuration() {
        assertThat(ckdRecipeFormMapper.toDuration("PT1S")).isEqualTo(Duration.ofSeconds(1));
    }

    @Test
    void testToDurationGivenStringDurationEmptyThenReturnsNull() {
        assertThat(ckdRecipeFormMapper.toDuration("")).isEqualTo(null);
    }

    @Test
    void testToDurationGivenStringDurationNullThenReturnsNull() {
        assertThat(ckdRecipeFormMapper.toDuration(null)).isEqualTo(null);
    }


    @Test
    void testToDurationGivenStringDurationWithMonthsAndDaysThenReturnsDuration() {
        assertThat(ckdRecipeFormMapper.toDuration("P1M11D")).isEqualTo(Duration.ofDays(41));
    }

    @Test
    void testToDurationGivenStringWithPeriodAndDurationThenReturnsDuration() {
        assertThat(ckdRecipeFormMapper.toDuration("P1WT6H")).isEqualTo(Duration.ofHours(174));
    }


}
