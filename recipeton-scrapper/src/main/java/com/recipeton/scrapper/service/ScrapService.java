package com.recipeton.scrapper.service;

/*-
 * #%L
 * recipeton-scrapper
 * %%
 * Copyright (C) 2021 tincomisc
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.recipeton.scrapper.config.ApplicationConfiguration;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.compress.utils.FileNameUtils;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.function.Function;

import static org.apache.commons.io.FilenameUtils.EXTENSION_SEPARATOR;

@Slf4j
@RequiredArgsConstructor
@Service
public class ScrapService {
    private final ApplicationConfiguration applicationConfiguration;
    private final Function<Path, Path> dataFilepathFunction = p -> getDataFilePath(getRecipeId(p), p.getParent());

    public String getDataFileName(String recipeId) {
        return recipeId + EXTENSION_SEPARATOR + applicationConfiguration.getExtractDataExtension();
    }

    public Path getDataFilePath(String recipeId, Path directoryOutputPath) {
        return directoryOutputPath.resolve(getDataFileName(recipeId));
    }

    public Function<Path, Path> getDataFilePathFunction() {
        return dataFilepathFunction;
    }

    public Path getHtmlFilePath(String recipeId, Path directoryOutputPath) {
        return directoryOutputPath.resolve(recipeId + EXTENSION_SEPARATOR + "html");
    }

    public String getPictureFileName(String recipeId) {
        return recipeId + EXTENSION_SEPARATOR + applicationConfiguration.getRecipePictureFormat();
    }

    public Path getPictureFilePath(String recipeId, Path directoryOutputPath) {
        return directoryOutputPath.resolve(getPictureFileName(recipeId));
    }

    public String getRecipeId(Path htmlFilePath) {
        return FileNameUtils.getBaseName(htmlFilePath.getFileName().toString());
    }

    public void moveToLegacy(Path directoryPath, String recipeId) throws IOException {
        Path legacyPath = Files.createDirectories(directoryPath.resolve(ApplicationConfiguration.SUBPATH_LEGACY));
        Path htmlFile = getHtmlFilePath(recipeId, directoryPath);
        if (Files.exists(htmlFile)) {
            Files.move(htmlFile, legacyPath.resolve(htmlFile.getFileName()));
        }
        Path pictureFile = htmlFile.resolveSibling(getPictureFileName(recipeId));
        if (Files.exists(pictureFile)) {
            Files.move(pictureFile, legacyPath.resolve(pictureFile.getFileName()));
        }
        Path dataFile = htmlFile.resolveSibling(getDataFileName(recipeId));
        if (Files.exists(dataFile)) {
            Files.move(dataFile, legacyPath.resolve(dataFile.getFileName()));
        }
    }

}
