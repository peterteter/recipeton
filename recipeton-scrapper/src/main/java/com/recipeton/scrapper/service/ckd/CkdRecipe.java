package com.recipeton.scrapper.service.ckd;

/*-
 * #%L
 * recipeton-scrapper
 * %%
 * Copyright (C) 2021 tincomisc
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonSubTypes;
import com.fasterxml.jackson.annotation.JsonTypeInfo;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;

import java.util.List;

@Data
public class CkdRecipe {
    public static final String TYPE_PROPERTY = "@type";

    @JsonProperty("@context")
    private String context;

    @JsonProperty(TYPE_PROPERTY)
    private String type;
    private String name;
    private String image;
    private String totalTime;
    private String cookTime;
    private String prepTime;
    private List<String> recipeCategory;
    private List<String> recipeIngredient;
    private List<Instruction> recipeInstructions;

    private Nutrition nutrition;
    private String recipeYield;

    private String keywords;

    private String inLanguage; // Really useless
    private Author author;
    private AggregatedRating aggregateRating;

    public String getKeywords() {
        return keywords;
    }

    @Data
    public static class Nutrition {
        @JsonProperty(TYPE_PROPERTY)
        private String type;

        private String calories;
        private String carbohydrateContent;
        private String fatContent;
        private String proteinContent;
    }

    @Data
    @JsonTypeInfo(use = JsonTypeInfo.Id.NAME, property = TYPE_PROPERTY, include = JsonTypeInfo.As.EXTERNAL_PROPERTY)
    @JsonSubTypes(value = {
            @JsonSubTypes.Type(value = Organization.class, name = Organization.TYPE),
    })
    public static class Author {
        @JsonProperty(TYPE_PROPERTY)
        private String type;

        private String name;
        private String address;
        private String url;
    }

    @Data
    @EqualsAndHashCode(callSuper = true)
    @ToString(callSuper = true)
    public static class Organization extends Author {
        public static final String TYPE = "Organization";
    }

    @Data
    @JsonTypeInfo(use = JsonTypeInfo.Id.NAME, property = TYPE_PROPERTY, include = JsonTypeInfo.As.EXTERNAL_PROPERTY)
    @JsonSubTypes(value = {
            @JsonSubTypes.Type(value = HowToStep.class, name = HowToStep.TYPE),
            @JsonSubTypes.Type(value = HowToSection.class, name = HowToSection.TYPE),
    })
    public static class Instruction {

        @JsonProperty(TYPE_PROPERTY)
        private String type;

    }

    @Data
    public static class HowToStep extends Instruction {
        public static final String TYPE = "HowToStep";

        private String text;
    }

    @Data
    public static class HowToSection extends Instruction {
        public static final String TYPE = "HowToSection";

        private String name;
        private List<HowToStep> itemListElement;
    }

    @Data
    public static class AggregatedRating {
        @JsonProperty("@id")
        private String id;
    }
}
