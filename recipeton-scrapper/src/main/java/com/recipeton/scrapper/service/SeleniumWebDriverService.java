package com.recipeton.scrapper.service;

/*-
 * #%L
 * recipeton-scrapper
 * %%
 * Copyright (C) 2021 tincomisc
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.common.collect.ImmutableMap;
import com.recipeton.scrapper.config.SeleniumConfiguration;
import io.github.bonigarcia.wdm.WebDriverManager;
import lombok.extern.slf4j.Slf4j;
import org.openqa.selenium.Dimension;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeDriverService;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.firefox.FirefoxOptions;
import org.openqa.selenium.remote.*;
import org.springframework.stereotype.Service;

import javax.annotation.PreDestroy;
import java.io.File;
import java.io.IOException;
import java.util.*;
import java.util.concurrent.TimeUnit;

@Slf4j
@Service
public class SeleniumWebDriverService {

    public static final String SYSTEM_PROPERTY_WEBDRIVER_OPTIONS_HEADLESS = "webdriver.options.headless";
    public static final String SYSTEM_PROPERTY_WEBDRIVER_CHROMEDRIVER = "webdriver.chrome.driver";
    public static final String SYSTEM_PROPERTY_WEBDRIVER_CHROME_OPTIONS_BINARY = "webdriver.chrome.options.binary";
    public static final String SYSTEM_PROPERTY_WEBDRIVER_GECKO_DRIVER = "webdriver.gecko.driver";
    public static final int OFFLINE_THROUGHPUT = 1024 * 1024;
    private static final int CHROME_FEATURE_DISABLED = 2;
    private final SeleniumConfiguration seleniumConfiguration;

    private final List<WebDriver> drivers = new ArrayList<>();

    private ChromeDriverService service;

    public SeleniumWebDriverService(SeleniumConfiguration seleniumConfiguration) {
        this.seleniumConfiguration = seleniumConfiguration;
    }

    public void closeWebDriver(WebDriver webDriver) {
        webDriver.close();
        try {
            webDriver.quit();
        } catch (Exception e) { // NOPMD
        }
    }

    private ChromeOptions createChromeOptions(String binaryAbsolutePath) {
        ChromeOptions chromeOptions = new ChromeOptions();

        if (binaryAbsolutePath != null) {
            chromeOptions.setBinary(binaryAbsolutePath); // Snap
        }
        if (seleniumConfiguration.getBrowser().isHeadless()) {
            chromeOptions.addArguments("headless");
        }
        if (seleniumConfiguration.getBrowser().isPicturesDisabled()) {
            chromeOptions.addArguments("--blink-settings=imagesEnabled=false");
        }

        if (seleniumConfiguration.getBrowser().isCrawlerMode()) {
            Map<String, Object> prefs = new HashMap<>();

//            prefs.put("profile.managed_default_content_settings.app_banner", disabledValue);
//            prefs.put("profile.managed_default_content_settings.auto_select_certificate", disabledValue);
//            prefs.put("profile.managed_default_content_settings.automatic_downloads", disabledValue);
            prefs.put("profile.managed_default_content_settings.cookies", CHROME_FEATURE_DISABLED);
//            prefs.put("profile.managed_default_content_settings.durable_storage", disabledValue);
//            prefs.put("profile.managed_default_content_settings.fullscreen", disabledValue);
            prefs.put("profile.managed_default_content_settings.geolocation", CHROME_FEATURE_DISABLED);
            prefs.put("profile.managed_default_content_settings.images", CHROME_FEATURE_DISABLED);
            prefs.put("profile.managed_default_content_settings.javascript", CHROME_FEATURE_DISABLED);
            prefs.put("profile.managed_default_content_settings.media_stream", CHROME_FEATURE_DISABLED);
//            prefs.put("profile.managed_default_content_settings.media_stream_camera", disabledValue);
//            prefs.put("profile.managed_default_content_settings.media_stream_mic", disabledValue);
//            prefs.put("profile.managed_default_content_settings.metro_switch_to_desktop", disabledValue);
//            prefs.put("profile.managed_default_content_settings.midi_sysex", disabledValue);
//            prefs.put("profile.managed_default_content_settings.mixed_script", disabledValue);
//            prefs.put("profile.managed_default_content_settings.mouselock", disabledValue);
            prefs.put("profile.managed_default_content_settings.notifications", CHROME_FEATURE_DISABLED);
            prefs.put("profile.managed_default_content_settings.plugins", CHROME_FEATURE_DISABLED);
            prefs.put("profile.managed_default_content_settings.popups", CHROME_FEATURE_DISABLED);
//            prefs.put("profile.managed_default_content_settings.ppapi_broker", disabledValue);
//            prefs.put("profile.managed_default_content_settings.protected_media_identifier", disabledValue);
//            prefs.put("profile.managed_default_content_settings.protocol_handlers", disabledValue);
//            prefs.put("profile.managed_default_content_settings.push_messaging", disabledValue);
//            prefs.put("profile.managed_default_content_settings.site_engagement", disabledValue);
//            prefs.put("profile.managed_default_content_settings.ssl_cert_decisions", disabledValue);
            prefs.put("profile.managed_default_content_settings.stylesheets", CHROME_FEATURE_DISABLED);

            chromeOptions.setExperimentalOption("prefs", prefs);
        }

        //              WebDriverManager.chromedriver().avoidAutoVersion().version(CHROME_VERSION).setup();

        chromeOptions.addArguments("start-maximized");
//         chromeOptions.addArguments("window-size=1024,768");
        if (seleniumConfiguration.getChromium().isSandboxed()) {
//        chromeOptions.addArguments("disable-infobars"); // disabling infobars
//        chromeOptions.addArguments("disable-extensions"); // disabling extensions
            chromeOptions.addArguments("no-sandbox"); // Needed to avoid error
            chromeOptions.addArguments("disable-dev-shm-usage"); // Needed to avoid error
//        chromeOptions.addArguments("remote-debugging-port=9222");
        }

        return chromeOptions;
    }

    private Optional<WebDriver> createChromeRemoteWebDriver(File chromeDriverFile) {
        System.setProperty(SYSTEM_PROPERTY_WEBDRIVER_CHROMEDRIVER, chromeDriverFile.getAbsolutePath());
        stopService();

        Map<String, String> environment = new HashMap<>();
        // environment.put("DISPLAY",":0.0"); // Explore if needed

        service = new ChromeDriverService.Builder() //
                .withEnvironment(environment) //
                // .withLogFile(File.createTempFile("xx_chromedriver","log")) // Explore if
                // needed
                .usingDriverExecutable(chromeDriverFile)
                .usingAnyFreePort()
                .build();

        try {
            service.start();
        } catch (IOException e) {
            log.error("Failure createChromeRemoteWebDriver", e);
        }

        ChromeOptions chromeOptions = createChromeOptions(getChromeBinaryPath());
        DesiredCapabilities desiredCapabilities = DesiredCapabilities.chrome();
        desiredCapabilities.setCapability(ChromeOptions.CAPABILITY, chromeOptions);

        RemoteWebDriver driver = new RemoteWebDriver(service.getUrl(), desiredCapabilities);
        driver.manage().timeouts().implicitlyWait(1, TimeUnit.SECONDS);
        return Optional.of(driver);
    }

    private Optional<WebDriver> createFirefoxDriver() {
        DesiredCapabilities desiredCapabilities = DesiredCapabilities.firefox();
        desiredCapabilities.setCapability("marionette", true);
        FirefoxOptions firefoxOptions = new FirefoxOptions(desiredCapabilities);
        if (seleniumConfiguration.getBrowser().isHeadless()) {
            firefoxOptions.addArguments("headless");
        }

        return Optional.of(new FirefoxDriver(firefoxOptions));
    }

    public Optional<WebDriver> createWebDriver(SeleniumConfiguration.SeleniumDriver seleniumDriver) {
        if (SeleniumConfiguration.SeleniumDriver.CHROME_REMOTE.equals(seleniumDriver)) {
            Optional.ofNullable(seleniumConfiguration.getChromeRemote().getDriverVersion()).ifPresent(s -> System.setProperty("wdm.chromeDriverVersion", s));
            WebDriverManager.chromedriver().setup();

            return seleniumConfiguration.getChromeRemote().getDriverPaths().stream()
                    .map(File::new).filter(File::exists).findFirst()
                    .map(this::createChromeRemoteWebDriver)
                    .orElseThrow(() -> new RuntimeException("Could not find chromedriver in paths" + seleniumConfiguration.getChromeRemote().getDriverPaths()));
        }

        if (SeleniumConfiguration.SeleniumDriver.CHROMIUM.equals(seleniumDriver)) {
            Optional<File> chromedriverFile = seleniumConfiguration.getChromium().getDriverPaths().stream().map(File::new).filter(File::exists).findFirst();
            boolean snap = chromedriverFile.filter(p -> p.getAbsolutePath().startsWith("/snap")).isPresent();

            if (seleniumConfiguration.getChromium().isAuto() && !snap) {
                Optional.ofNullable(seleniumConfiguration.getChromium().getDriverVersion()).ifPresent(s -> System.setProperty("wdm.chromeDriverVersion", s)); // alternatively set in setup
                WebDriverManager.chromiumdriver().setup();
                return Optional.of(chromedriverFile.map(p -> new ChromeDriver(createChromeOptions(null)))
                        .orElseThrow(() -> new RuntimeException("Could not find chromedriver in paths" + seleniumConfiguration.getChromium().getDriverPaths())));
            } else { // Ubuntu 19.10 snap workaround
                return Optional.of(chromedriverFile
                        .map(f -> {
                            System.setProperty(SYSTEM_PROPERTY_WEBDRIVER_CHROMEDRIVER, f.getAbsolutePath());

                            service = new ChromeDriverService.Builder() {
                                @Override
                                protected File findDefaultExecutable() {
                                    return new File(f.getAbsolutePath()) {
                                        @Override
                                        public String getCanonicalPath() {
                                            return this.getAbsolutePath();
                                        }
                                    };
                                }
                            }.build();

                            return new ChromeDriver(service, createChromeOptions(null));
                        })
                        .orElseThrow(() -> new RuntimeException("Could not find chromedriver in paths" + seleniumConfiguration.getChromium().getDriverPaths())));
            }
        }

        if (SeleniumConfiguration.SeleniumDriver.FIREFOX.equals(seleniumDriver)) {
            if (seleniumConfiguration.getFirefox().isAuto()) {
                WebDriverManager.firefoxdriver().setup();
                return createFirefoxDriver();
            } else {
                return seleniumConfiguration.getFirefox().getDriverPaths().stream()
                        .map(File::new).filter(File::exists).findFirst()
                        .map(file -> {
                            System.setProperty(SYSTEM_PROPERTY_WEBDRIVER_GECKO_DRIVER, file.getAbsolutePath());
                            return createFirefoxDriver();
                        })
                        .orElseThrow(() -> new RuntimeException("Could not find geckodriver in paths" + seleniumConfiguration.getFirefox().getDriverPaths()));
            }
        }

        return Optional.empty();
    }

    public WebDriver createWebDriver() {
        List<SeleniumConfiguration.SeleniumDriver> seleniumDrivers = seleniumConfiguration.getDriverPrecedence();
        for (SeleniumConfiguration.SeleniumDriver seleniumDriver : seleniumDrivers) {
            Optional<WebDriver> webDriverOption = createWebDriver(seleniumDriver);
            if (webDriverOption.isPresent()) {
                drivers.add(webDriverOption.get());
                return webDriverOption.get();
            }
        }

        throw new RuntimeException("Could not find valid webdriver of type=" + seleniumConfiguration.getDriverPrecedence()); // NOPMD
    }

    private String getChromeBinaryPath() {
        return seleniumConfiguration.getChromium().getBrowserPaths().stream()
                .map(File::new)
                .filter(File::exists)
                .findFirst()
                .orElseThrow(() -> new RuntimeException("Could not find chrome executable in paths" + seleniumConfiguration.getChromium().getBrowserPaths())).getAbsolutePath();
    }

    @PreDestroy
    public void preDestroy() {
        quitDrivers();
        stopService();
    }

    private void quitDrivers() {
        drivers.forEach(d -> {
            try {
                d.quit();
//                d.close();
            } catch (Exception e) { // NOPMD
                log.warn("Error quitting driver", e);
            }
        });
    }

    public void setOffline(WebDriver webDriver) throws IOException {
        if (!(webDriver instanceof ChromeDriver)) {
            log.warn("setOffline ignored. Not Chromedriver");
            return;
        }
        ChromeDriver chromeDriver = (ChromeDriver) webDriver;
        CommandExecutor executor = chromeDriver.getCommandExecutor();
        Map<String, Object> map = new HashMap<String, Object>();
        map.put("offline", true);
        map.put("latency", 0);
        map.put("download_throughput", OFFLINE_THROUGHPUT);
        map.put("upload_throughput", OFFLINE_THROUGHPUT);
        Response response = executor.execute(new Command(chromeDriver.getSessionId(), "setNetworkConditions", ImmutableMap.of("network_conditions", ImmutableMap.copyOf(map))));
        log.debug("setOffline executed {}", response);
    }

    public void setUpBrowser(WebDriver webDriver) {
        Dimension dimension = new Dimension(seleniumConfiguration.getBrowser().getWidth(), seleniumConfiguration.getBrowser().getHeight()); // (400,800);
        webDriver.manage().window().setSize(dimension);
        if (seleniumConfiguration.getImplicitWait() >= 0) {
            webDriver.manage().timeouts().implicitlyWait(seleniumConfiguration.getImplicitWait(), TimeUnit.MILLISECONDS);
        }
        if (seleniumConfiguration.getPageLoadTimeout() >= 0) {
            webDriver.manage().timeouts().pageLoadTimeout(seleniumConfiguration.getPageLoadTimeout(), TimeUnit.MILLISECONDS);
        }
    }

    private void stopService() {
        if (service != null && service.isRunning()) {
            service.stop();
        }
    }

}
