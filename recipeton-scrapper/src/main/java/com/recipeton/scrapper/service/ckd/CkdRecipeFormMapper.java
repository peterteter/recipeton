package com.recipeton.scrapper.service.ckd;

/*-
 * #%L
 * recipeton-scrapper
 * %%
 * Copyright (C) 2021 tincomisc
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.recipeton.shared.domain.RecipeDefinition;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.text.StringEscapeUtils;
import org.mapstruct.Builder;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Named;

import java.time.Duration;
import java.time.Period;
import java.time.format.DateTimeParseException;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@Mapper(componentModel = "spring", builder = @Builder(disableBuilder = true))
public interface CkdRecipeFormMapper {

    String STRING_WITH_COMMAS_TO_STRING_LIST = "StringToStringList";
    String UNESCAPED = "Unescaped";

    default Duration toDuration(String value) {
        if (StringUtils.isBlank(value)) {
            return null;
        }

        try {
            return Duration.parse(value);
        } catch (DateTimeParseException e) { // NOPMD
            // cant parse as duration only
        }

        try {
            Period period = Period.parse(value);
            long days = period.toTotalMonths() * 30 + period.getDays();
            return Duration.ofDays(days);
        } catch (DateTimeParseException e) { // NOPMD
            // cant parse as period only
        }

        int periodIndex = value.indexOf('P');
        int timeIndex = value.indexOf('T');
        Period period = Period.parse(value.substring(periodIndex, timeIndex));
        long days = period.toTotalMonths() * 30 + period.getDays();
        Duration durationDays = Duration.ofDays(days);
        Duration durationTime = Duration.parse("P" + value.substring(timeIndex));
        return durationDays.plus(durationTime);

    }

    @Mapping(target = "locale", source = "inLanguage")
    @Mapping(target = "keywords", source = "keywords", qualifiedByName = STRING_WITH_COMMAS_TO_STRING_LIST)
    @Mapping(target = "recipeSections", source = "recipeInstructions")
    RecipeDefinition toRecipe(CkdRecipe ckdRecipe);

    default List<RecipeDefinition.RecipeSection> toRecipeSection(List<CkdRecipe.Instruction> instructions) {
        if (instructions == null) {
            return null;
        }
        RecipeDefinition.RecipeSection currentSection = null;

        List<RecipeDefinition.RecipeSection> list = new ArrayList<>();
        for (CkdRecipe.Instruction instruction : instructions) {
            if (instruction instanceof CkdRecipe.HowToSection) {
                CkdRecipe.HowToSection howToSection = (CkdRecipe.HowToSection) instruction;
                currentSection = null;
                list.add(new RecipeDefinition.RecipeSection() // NOPMD
                        .setName(StringUtils.trimToNull(howToSection.getName()))
                        .setInstructions(howToSection.getItemListElement().stream()
                                .map(CkdRecipe.HowToStep::getText)
                                .map(StringEscapeUtils::unescapeHtml4)
                                .map(StringUtils::trimToNull)
                                .filter(Objects::nonNull)
                                .collect(Collectors.toList())));
            } else if (instruction instanceof CkdRecipe.HowToStep) {
                CkdRecipe.HowToStep howToStep = (CkdRecipe.HowToStep) instruction;
                if (currentSection == null) {
                    currentSection = new RecipeDefinition.RecipeSection(); // NOPMD
                    list.add(currentSection);
                }
                if (currentSection.getInstructions() == null) {
                    currentSection.setInstructions(new ArrayList<>()); // NOPMD
                }
                currentSection.getInstructions().add(StringEscapeUtils.unescapeHtml4(howToStep.getText()));
            }
        }
        return list;
    }

    default String toString(CkdRecipe.Author author) {
        if (author == null) {
            return null;
        }
        return author.getName() + Optional.ofNullable(author.getAddress()).map(s -> " " + s).orElse("");
    }

    @Named(STRING_WITH_COMMAS_TO_STRING_LIST)
    default List<String> toStringList(String string) {
        if (string == null) {
            return null;
        }

        return Stream.of(string.split(","))
                .map(StringUtils::trimToNull)
                .filter(Objects::nonNull)
                .collect(Collectors.toList());
    }

    @Named(UNESCAPED)
    default List<String> toStringList(List<String> strings) {
        if (strings == null) {
            return null;
        }

        return strings.stream()
                .map(StringEscapeUtils::unescapeHtml4)
                .map(StringUtils::trimToNull)
                .filter(Objects::nonNull)
                .collect(Collectors.toList());
    }

}
