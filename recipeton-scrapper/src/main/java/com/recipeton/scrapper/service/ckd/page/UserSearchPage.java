package com.recipeton.scrapper.service.ckd.page;

/*-
 * #%L
 * recipeton-scrapper
 * %%
 * Copyright (C) 2021 tincomisc
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.recipeton.scrapper.util.selenium.AbstractPage;
import com.recipeton.scrapper.util.selenium.SeleniumPage;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.interactions.MoveTargetOutOfBoundsException;
import org.openqa.selenium.support.FindBy;
import org.springframework.web.util.UriComponentsBuilder;

import java.net.URL;
import java.util.List;
import java.util.stream.Collectors;

@Slf4j
@SeleniumPage
public class UserSearchPage extends AbstractPage {
    public static final String FILTER_FACET_CATEGORY = "categories";
    public static final String FILTER_FACET_CATEGORY_ITEM_PREFIX = "category-";
    public static final String FILTER_FACET_LANGUAGE = "languages";
    public static final String FILTER_FACET_LANGUAGE_ITEM_PREFIX = "language-";

    public static final String FILTER_FACET_DIFFICULTY_EASY = "easy";
    public static final String FILTER_FACET_DIFFICULTY_MEDIUM = "medium";
    public static final String FILTER_FACET_DIFFICULTY_ADVANCED = "advanced";

    public static final String SORT_BY_TITLE = "title";
    public static final String SORT_BY_PUBLISHED_AT = "publishedAt";
    public static final String SEARCH_RESULT_ITEMS_XPATH = "//search-results-list/core-tiles-list/core-tile";


    @FindBy(id = "onetrust-accept-btn-handler")
    private List<WebElement> cookiesAccept;

    @FindBy(xpath = "//core-search-bar//button[@type='submit']")
    private WebElement search;

    @FindBy(xpath = "//sort-by[1]/core-dropdown-text/button")
    private WebElement displaySortBy;

    @FindBy(xpath = "//sort-by[1]//label[@for='title']")
    private WebElement sortByTitle;

    @FindBy(xpath = "//selected-badges/div")
    private List<WebElement> selectedBadges;

    @FindBy(xpath = "//filter-button")
    private WebElement displayFilter;

    @FindBy(xpath = "//filter-modal/core-modal/div/div/div[2]/button[1]")
    private WebElement closeFilter;

    @FindBy(xpath = "//filter-modal/core-modal/div/div/div[2]/button[2]")
    private WebElement resetFilter;

    @FindBy(xpath = "//filter-modal//button[contains(@class, 'core-modal__submit')]")
    private WebElement applyFilter;

    @FindBy(xpath = "//selected-badges/search-results-count/span")
    private WebElement searchResultCount;

    @FindBy(xpath = SEARCH_RESULT_ITEMS_XPATH)
    private List<WebElement> searchResultItems; // 20 per page

    @FindBy(id = "load-more-page")
    private WebElement loadMore;
    @FindBy(id = "easy")
    private WebElement difficultyEasy;
    @FindBy(id = "medium")
    private WebElement difficultyMedium;
    @FindBy(id = "advanced")
    private WebElement difficultyAdvanced;
    //*[@id='easy']")[0]
    @FindBy(xpath = "//*[contains(@class, 'search-no-results-for-query')]")
    private List<WebElement> errorNoResults;

    public UserSearchPage(WebDriver driver) {
        super(driver);
    }

    public void clearSelectedBadges() {
        for (WebElement selectedBadge : selectedBadges) {
            selectedBadge.click();
        }
    }

    public void doApplyFilter() {
        applyFilter.click();
    }

    public void doCloseFilter() {
        closeFilter.click();
    }

    public void doCookiesAccept() {
        try {
            if (!cookiesAccept.isEmpty() && cookiesAccept.get(0).isDisplayed()) {
                new Actions(getDriver()).moveToElement(cookiesAccept.get(0)).click().perform();
            }
        } catch (MoveTargetOutOfBoundsException e) {
            log.warn("doCookies accept failed. Not essential. msg={}", e.getMessage());
        }
    }

    public void doFilterDifficulty(String difficulty) {
        switch (difficulty) {
            case FILTER_FACET_DIFFICULTY_EASY:
                doFilterDifficultyEasy();
                break;
            case FILTER_FACET_DIFFICULTY_MEDIUM:
                doFilterDifficultyMedium();
                break;
            case FILTER_FACET_DIFFICULTY_ADVANCED:
                doFilterDifficultyAdvanced();
                break;
            default:
                throw new IllegalArgumentException("Unsupported difficulty " + difficulty);
        }
    }

    public void doFilterDifficultyAdvanced() {
        new Actions(getDriver()).moveToElement(difficultyAdvanced).click().perform();
    }

    public void doFilterDifficultyEasy() {
        new Actions(getDriver()).moveToElement(difficultyEasy).click().perform();
    }

    public void doFilterDifficultyMedium() {
        new Actions(getDriver()).moveToElement(difficultyMedium).click().perform();
    }

    public void doFilterDisplay() {
        displayFilter.click();
    }

    public void doFilterFacetExpand(String filterFacet) {
        WebElement categoryExpandElement = getDriver().findElement(getFilterExpandDoSelector(filterFacet));
        new Actions(getDriver()).moveToElement(categoryExpandElement).click().perform();
    }

    public void doFilterFacetLanguageClick(String languageId) {
        WebElement element = getFilterItem(FILTER_FACET_LANGUAGE, FILTER_FACET_LANGUAGE_ITEM_PREFIX, languageId);
        new Actions(getDriver()).moveToElement(element).click().perform();

    }

    public void doLoadMore() {
        loadMore.click();
    }

    public void doResetFilter() {
        resetFilter.click();
    }

    public void doSortByDisplay() {
        displaySortBy.click();
    }

    public void doSortbyTitle() {
        sortByTitle.click();
    }


    public List<Integer> getCategoryCounts() {
        List<WebElement> categoryCountElements = getFilterItemCounts(FILTER_FACET_CATEGORY);
        return categoryCountElements.stream()
                .peek(e -> new Actions(getDriver()).moveToElement(e).perform())
                .map(WebElement::getText)
                .map(s -> StringUtils.isEmpty(s) ? -1 : Integer.parseInt(s))
                .collect(Collectors.toList());
    }

    public List<String> getCategoryIdentifiers() {
        List<WebElement> categoryElements = getFilterItems(FILTER_FACET_CATEGORY);
        return categoryElements.stream()
                .map(c -> StringUtils.substringAfter(c.getAttribute("for"), FILTER_FACET_CATEGORY_ITEM_PREFIX))
                .collect(Collectors.toList());


    }

    public WebElement getFilterCollapseDo(String facetName) {
        return getDriver().findElement(By.xpath("//multi-select[@facet='" + facetName + "']/button/span[1]"));
    }

    public By getFilterExpandDoSelector(String facetName) {
        return By.xpath("//multi-select[@facet='" + facetName + "']/button/span[1]");
    }

    public WebElement getFilterItem(String facetName, String label) {
        return getDriver().findElement(By.xpath("//multi-select[@facet='" + facetName + "']//label[@for='" + label + "']"));
    }

    public WebElement getFilterItem(String facetName, String prefix, String value) {
        return getFilterItem(facetName, prefix + value);
    }

    public List<WebElement> getFilterItemCounts(String facetName) {
        return getDriver().findElements(By.xpath("//multi-select[@facet='" + facetName + "']//span[@class='multi-select__count']"));
    }

    public List<WebElement> getFilterItems(String facetName) {
        return getDriver().findElements(By.xpath("//multi-select[@facet='" + facetName + "']//label"));
    }

    public String getSearchResultCount() {
        return searchResultCount.getText();
    }

    public List<WebElement> getSearchResultItems() {
        return searchResultItems;
    }

    public int getSearchResultItemsCount() {
        return getSearchResultItems().size();
    }

    public boolean isErrorNoResults() {
        return !errorNoResults.isEmpty() && errorNoResults.get(0).isDisplayed();
    }

    public void loadByLanguageAndCategoryAndDifficultyAndPage(URL baseUrl, String languageId, String categoryId, String difficulty, int page) {
        String categoryPageUrl = UriComponentsBuilder.newInstance()
                .scheme(baseUrl.getProtocol())
                .host(baseUrl.getHost())
                .path(baseUrl.getPath())
                .queryParam("query", "")
                .queryParam("sortby", SORT_BY_PUBLISHED_AT)
                .queryParam("languages", languageId)
                .queryParam("categories", categoryId)
                .queryParam("difficulty", difficulty)
                .queryParam("page", page)
                .build().toUriString();
        log.debug("loading. languageId={}, categoryId={}, page={}", languageId, categoryId, page);
        load(categoryPageUrl);
    }

    public void loadByLanguageAndDifficulty(URL baseUrl, String languageId, String difficulty) {
        String categoryPageUrl = UriComponentsBuilder.newInstance()
                .scheme(baseUrl.getProtocol())
                .host(baseUrl.getHost())
                .path(baseUrl.getPath())
//            .queryParam("query", "")
                .queryParam("context", "recipes")
                .queryParam("languages", languageId)
                .queryParam("difficulty", difficulty)
                .build().toUriString();
        log.debug("loading. languageId={}, difficulty={}", languageId, difficulty);
        load(categoryPageUrl);
    }

    public void waitForVisibility() {
        waitForVisibility(search);
    }
}
