package com.recipeton.scrapper.service;

/*-
 * #%L
 * recipeton-scrapper
 * %%
 * Copyright (C) 2021 tincomisc
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.recipeton.scrapper.config.ApplicationConfiguration;
import com.recipeton.shared.domain.RecipeDefinition;
import com.recipeton.shared.service.RecipeDefinitionStorageService;
import com.recipeton.shared.util.Partition;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.Collections;
import java.util.List;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ForkJoinPool;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

import static org.apache.commons.io.FilenameUtils.EXTENSION_SEPARATOR;

@Slf4j
@RequiredArgsConstructor
@Service
public class ExportService {

    private static final int CHUNK_SIZE = 10;
    private static final int MAX_DEPTH = 3;

    private final ApplicationConfiguration applicationConfiguration;
    private final RecipeDefinitionStorageService recipeDefinitionStorageService;
    private final ScrapService scrapService;

    public void doExport() throws IOException, ExecutionException, InterruptedException {
        log.info("Start export");
        Path dataDirectoryPath = applicationConfiguration.getScrapPath();
        Path exportDirectoryPath = applicationConfiguration.getExportPath();
        List<Path> dataFilePaths = Files.exists(dataDirectoryPath) ? Files.walk(dataDirectoryPath, MAX_DEPTH)
                .filter(p -> p.toString().endsWith(EXTENSION_SEPARATOR + "yaml")).collect(Collectors.toList()) : Collections.emptyList();
        log.info("found files. count={}, path={}", dataFilePaths.size(), dataDirectoryPath);

        if (dataFilePaths.isEmpty()) {
            log.info("No files to export found, path={}", dataDirectoryPath);
        } else {
            Partition<Path> partition = Partition.ofSize(dataFilePaths, CHUNK_SIZE);
            AtomicInteger count = new AtomicInteger();

            int parallelism = applicationConfiguration.getReprocessParallelism();
            new ForkJoinPool(parallelism).submit(() -> {
                IntStream.range(0, partition.size())
                        .parallel()
                        .forEach(i -> {
                            count.incrementAndGet();
                            List<Path> dataFilePathsBatch = partition.get(i);
                            try {
                                for (Path dataFilePath : dataFilePathsBatch) {

                                    String recipeId = scrapService.getRecipeId(dataFilePath);
                                    Path pictureFilePath = scrapService.getPictureFilePath(recipeId, dataFilePath.getParent());
                                    if (!Files.exists(pictureFilePath)) {
                                        log.warn("missing picture. skip. path={}", dataFilePath);
                                        continue;
                                    }
                                    RecipeDefinition recipeDefinition = recipeDefinitionStorageService.read(dataFilePath);

                                    String locale = recipeDefinition.getLocale();
                                    String category = recipeDefinition.getRecipeCategory().isEmpty() ? "Unk" : recipeDefinition.getRecipeCategory().get(0);
                                    Path targetPath = exportDirectoryPath.resolve(locale).resolve(category);

                                    Path targetDataFilePath = targetPath.resolve(dataFilePath.getFileName());
                                    Path targetPictureFilePath = targetPath.resolve(pictureFilePath.getFileName());
                                    if (!Files.exists(targetPictureFilePath)) {
                                        Files.createDirectories(targetPath);
                                        Files.copy(dataFilePath, targetDataFilePath);
                                        Files.copy(pictureFilePath, targetPictureFilePath);
                                    }
                                }
                                log.info("processed. count={}/{}", count.addAndGet(dataFilePathsBatch.size()), dataFilePaths.size());
                            } catch (IOException e) {
                                log.error("Exception processing batch", e);
                            }
                        });
            }).get();
            log.info("Export completed succesfully, path={}", dataDirectoryPath);
        }
    }
}
