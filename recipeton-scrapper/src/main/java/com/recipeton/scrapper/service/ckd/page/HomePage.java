package com.recipeton.scrapper.service.ckd.page;

/*-
 * #%L
 * recipeton-scrapper
 * %%
 * Copyright (C) 2021 tincomisc
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.recipeton.scrapper.util.selenium.AbstractPage;
import com.recipeton.scrapper.util.selenium.SeleniumPage;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

@SeleniumPage
public class HomePage extends AbstractPage {

    @FindBy(xpath = "//*[@id=\"layout--default\"]/header/div/core-nav/nav/div[1]/ul[2]/li[2]/a")
    private WebElement doLogin;

    public HomePage(WebDriver driver) {
        super(driver);
    }

    public WebElement getDoLogin() {
        return doLogin;
    }

    public void waitForVisibility() {
        waitForVisibility(doLogin);
    }
}
