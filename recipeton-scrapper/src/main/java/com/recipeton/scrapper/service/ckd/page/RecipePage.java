package com.recipeton.scrapper.service.ckd.page;

/*-
 * #%L
 * recipeton-scrapper
 * %%
 * Copyright (C) 2021 tincomisc
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.recipeton.scrapper.util.selenium.AbstractPage;
import com.recipeton.scrapper.util.selenium.SeleniumPage;
import org.apache.commons.lang3.StringUtils;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

@SeleniumPage
public class RecipePage extends AbstractPage {

    public static final String IMAGE_XPATH = "//core-image-loader/img[@class='core-tile__image']";
    public static final String DIFFICULTY_XPATH = "//*[@id='rc-icon-difficulty-text']";
    public static final String UTENSILS_XPATH = "//*[@id='recipe-utensils']";
    public static final String HINTS_XPATH = "//*[@id='hints-and-tricks']/ul";

    //    @FindBy(xpath = "*[@id='recipe-card']")
    @FindBy(id = "recipe-card")
    private WebElement container;

    @FindBy(xpath = "//script[@type='application/ld+json']")
    private List<WebElement> jsonScripts;

    @FindBy(xpath = "//*[@id='in-collections']//a/div")
    private List<WebElement> inCollections;

    @FindBy(xpath = UTENSILS_XPATH)
    private List<WebElement> utensils; // 0..1 optional
    @FindBy(xpath = "//*[@id='useful-items-title']")
    private WebElement utensilsTitle;

    @FindBy(xpath = DIFFICULTY_XPATH)
    private WebElement difficulty;
    @FindBy(xpath = "//*[@id='rc-icon-difficulty-text']/span")
    private WebElement difficultyTitle;

    @FindBy(xpath = HINTS_XPATH)
    private List<WebElement> hints; // 0..1 optional


    @FindBy(xpath = "//div[@id='tm-versions-modal']/core-badge")
    private List<WebElement> devices;

    @FindBy(xpath = "//div[@id='ingredients']//core-list-section//ul")
    private List<WebElement> ingredientGroups;

    @FindBy(xpath = "//div[@id='ingredients']//core-list-section//*[@class='core-list-section__header']")
    private List<WebElement> ingredientGroupHeaders;

    @FindBy(xpath = IMAGE_XPATH)
    private WebElement image;

    public RecipePage(WebDriver driver) {
        super(driver);
    }

    public WebElement getContainer() {
        return container;
    }

    public String getDataJson() {
        return getInnerHtml(jsonScripts.get(0));
    }

    public List<String> getDevicesTexts() {
        return devices.stream()
                .map(WebElement::getText)
                .map(StringUtils::trimToNull)
                .filter(Objects::nonNull)
                .collect(Collectors.toList());
    }

    public String getDifficultyText() {
        if (!difficulty.isDisplayed()) {
            return null;
        }
        String text = difficulty.getText();
        if (difficultyTitle.isDisplayed()) {
            text = text.replace(difficultyTitle.getText(), "").trim();
        }

        return text;
    }

    public String getHintsText() {
        if (hints.isEmpty()) {
            return null;
        }

        return hints.get(0).getText();
    }

    public WebElement getImage() {
        return image;
    }

    public String getImageUrl() {
        return getImage().getAttribute("src");
    }

    public List<String> getInCollectionTexts() {
        return inCollections.stream()
                .map(WebElement::getText)
                .map(StringUtils::trimToNull)
                .filter(Objects::nonNull)
                .collect(Collectors.toList());
    }

    public int getIngredientGroupCount() {
        return ingredientGroups.size();
    }

    public List<String> getIngredientGroupItems(int index) {
        String xpathExpression = "//div[@id='ingredients']//core-list-section//ul[" + (index + 1) + "]//li";
        return getDriver().findElements(By.xpath(xpathExpression)).stream()
                .map(WebElement::getText)
                .map(StringUtils::trimToNull)
                .filter(Objects::nonNull)
                .collect(Collectors.toList());
    }

    public String getIngredientGroupName(int index) {
        String text = ingredientGroupHeaders.get(index).getText();
        return StringUtils.trimToNull(text);
    }

    public String getRatingJson() {
        boolean jsonScriptCountEnough = jsonScripts.size() >= 2;
        if (jsonScriptCountEnough) {
            return getInnerHtml(jsonScripts.get(1));
        }
        return null;
    }

    public String getUtensilsText() {
        if (utensils.isEmpty()) {
            return null;
        }
        String text = utensils.get(0).getText();
        if (utensilsTitle.isDisplayed()) {
            text = text.replace(utensilsTitle.getText(), "").trim();
        }
        return text;
    }

    public void waitForVisibility() {
        waitForVisibility(container);
    }
}
