package com.recipeton.scrapper.service.ckd;

/*-
 * #%L
 * recipeton-scrapper
 * %%
 * Copyright (C) 2021 tincomisc
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.recipeton.scrapper.config.ApplicationConfiguration;
import com.recipeton.scrapper.domain.ScrapItemResult;
import com.recipeton.scrapper.service.ScrapService;
import com.recipeton.scrapper.service.SeleniumWebDriverService;
import com.recipeton.scrapper.service.ckd.page.LoginPage;
import com.recipeton.scrapper.service.ckd.page.RecipePage;
import com.recipeton.scrapper.service.ckd.page.UserHomePage;
import com.recipeton.scrapper.service.ckd.page.UserSearchPage;
import com.recipeton.shared.domain.RecipeDefinition;
import com.recipeton.shared.service.RecipeDefinitionStorageService;
import com.recipeton.shared.util.Partition;
import lombok.Data;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.tuple.Pair;
import org.openqa.selenium.By;
import org.openqa.selenium.TimeoutException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.springframework.stereotype.Service;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.*;
import java.util.concurrent.ConcurrentLinkedDeque;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ForkJoinPool;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.function.Function;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

import static java.nio.file.StandardOpenOption.CREATE;
import static java.util.List.of;
import static org.apache.commons.io.FilenameUtils.EXTENSION_SEPARATOR;

@Slf4j
@RequiredArgsConstructor
@Service
public class CkdScrapperService {

    private static final int SLEEP_CATEGORY_FILTER = 1000; // NOTE: Find selector and remove this
    private static final int SLEEP_USER_SEARCH = 200; // NOTE: Find selector and remove this

    private static final int REPROCESS_MAX_DEPTH = 3;
    private static final int REPROCESS_CHUNK_SIZE = 10;

    private final ApplicationConfiguration applicationConfiguration;
    private final SeleniumWebDriverService seleniumWebDriverService;
    private final CkdRecipeFormMapper ckdRecipeFormMapper;
    private final RecipeDefinitionStorageService recipeDefinitionStorageService;
    private final ScrapService scrapService;

    private final ObjectMapper objectMapper = createObjectMapper();

    private ObjectMapper createObjectMapper() {
        return new ObjectMapper()
                .enable(SerializationFeature.INDENT_OUTPUT)
                .setSerializationInclusion(JsonInclude.Include.NON_NULL)
                .findAndRegisterModules();
    }

    private void downloadPicture(RecipePage recipePage, Path pictureFile) throws IOException {
        String imageUrl = recipePage.getImageUrl();
        BufferedImage saveImage = ImageIO.read(new URL(imageUrl));
        ImageIO.write(saveImage, applicationConfiguration.getRecipePictureFormat(), pictureFile.toFile());
    }

    public Path getCategoryIdTargetDirectoryPath(String languageId, String categoryId, String difficulty) {
        return applicationConfiguration.getScrapPath().resolve(languageId).resolve(categoryId + "_" + difficulty);
    }

    private List<CategoryScrapeRequest> getCategoryScrapeRequest(String languageId, String difficulty, UserSearchPage userSearchPage) throws MalformedURLException {
        userSearchPage.loadByLanguageAndDifficulty(new URL(applicationConfiguration.getUrlSearch()), languageId, difficulty);
//        userSearchPage.clearSelectedBadges();
        userSearchPage.doCookiesAccept();

        userSearchPage.doFilterDisplay();
        sleep(SLEEP_CATEGORY_FILTER);
//        userSearchPage.doFilterFacetExpand(FILTER_FACET_LANGUAGE);
//        userSearchPage.doFilterFacetLanguageClick(languageId);
//        userSearchPage.doFilterDifficulty(difficulty);
        userSearchPage.doFilterFacetExpand(UserSearchPage.FILTER_FACET_CATEGORY);

        List<CategoryScrapeRequest> categoryScrapeRequests = new ArrayList<>();

        List<String> categoryIds = userSearchPage.getCategoryIdentifiers();
        List<Integer> totalCounts = userSearchPage.getCategoryCounts();
        for (int i = 0; i < categoryIds.size(); i++) {
            String categoryId = categoryIds.get(i);
            int totalCount = totalCounts.get(i);
            if (totalCount > 0) {
                categoryScrapeRequests.add(new CategoryScrapeRequest(languageId, categoryId, difficulty, totalCount)); // NOPMD
            }
        }

        if (totalCounts.stream().anyMatch(i -> i < 0)) {
            log.error("Problem with script. some counts are less than zero counts={}", totalCounts);
        }

        return categoryScrapeRequests;
    }

    public void process() {
        if (!applicationConfiguration.getUrlHome().startsWith("http")) {
            log.error("Missing scrap url. Process aborted. set your urls and tld. Check readme for details");
            return;
        }
        WebDriver webDriver = seleniumWebDriverService.createWebDriver();
        try {
            processHome(webDriver);
        } catch (IOException e) {
            log.error("Script execution failed ", e);
        } finally {
            seleniumWebDriverService.closeWebDriver(webDriver);
        }
    }

    // Add option to force recheck and/or force reimport
    // Think that maybe better always sort by most recent and abort when 1st stored found and local storedcount >= category count
    private void processCategory(URL baseUrl, CategoryScrapeRequest request, int pageFrom, WebDriver webDriver) throws IOException, CategoryParseException { // NOPMD
        String languageId = request.getLanguageId();
        String categoryId = request.getCategoryId();
        String difficulty = request.getDifficulty();

        Path targetDirectoryPath = getCategoryIdTargetDirectoryPath(languageId, categoryId, difficulty);
        int totalRecipeCount = request.getTotalCount();

        List<Path> htmlFiles = Files.exists(targetDirectoryPath) ? Files.walk(targetDirectoryPath, 1).filter(p -> p.toString().endsWith(EXTENSION_SEPARATOR + "html")).collect(Collectors.toList()) : Collections.emptyList();

        log.info("processCategory categoryId={}, filesExisting={}, filesExpected={}, initialPage={}", categoryId, htmlFiles.size(), totalRecipeCount, pageFrom);

        if (totalRecipeCount == htmlFiles.size()) {
            log.info("processCategory already complete. skip. request={}", request);
            return;
        }

        int recipePageMax = applicationConfiguration.getRecipePageMax();
        int recipePageSize = applicationConfiguration.getRecipePageSize();

        int maxRetrievableRecipes = recipePageMax * recipePageSize;
        if (htmlFiles.size() >= maxRetrievableRecipes) {
            log.warn("processCategory already retrieved max files per filter. skip. max={}, request={}", maxRetrievableRecipes, request);
            return;
        }

        UserSearchPage userSearchPage = new UserSearchPage(webDriver);
        Set<String> processedRecipeIds = new HashSet<>();
        int processedRecipeCount = pageFrom * recipePageSize;
        int page = pageFrom;
        while (processedRecipeCount < totalRecipeCount) {
            if (page > recipePageMax) {
                log.warn("processCategory request={}, page={}, processed={}/{}. MAX PAGE EXCEEDED ({}). STOPPING. SCRIPT NEEDS TO PARTITION MORE!!", request, page, processedRecipeCount, totalRecipeCount, recipePageMax);
                return;
            }
            log.info("processCategory request={}, page={}, processed={}/{}", request, page, processedRecipeCount, totalRecipeCount);
            userSearchPage.loadByLanguageAndCategoryAndDifficultyAndPage(baseUrl, languageId, categoryId, difficulty, page);

            if (userSearchPage.isErrorNoResults()) {
                log.warn("processCategory. No results! Skip. request={}, page={}, processed={}/{}", request, page, processedRecipeCount, totalRecipeCount);
                return;
            }
            try {
                userSearchPage.waitForCondition(ExpectedConditions.numberOfElementsToBeMoreThan(By.xpath(UserSearchPage.SEARCH_RESULT_ITEMS_XPATH), 0));
            } catch (TimeoutException e) {
                log.error("Found empty result items! Force stop iteration. request={}, processed={}/{}", request, processedRecipeCount, totalRecipeCount);
            }

            if (userSearchPage.getSearchResultItemsCount() == 0) { // Not needed. Verify
                sleep(SLEEP_USER_SEARCH);
            }

            List<WebElement> searchResultItems = userSearchPage.getSearchResultItems();
            if (searchResultItems.isEmpty()) {
                log.error("Found empty result items! Force stop iteration. request={}, processed={}/{}", request, processedRecipeCount, totalRecipeCount);
                throw new CategoryParseException(categoryId, processedRecipeCount, totalRecipeCount, page, "Found empty result items! Force stop iteration. categoryId=" + categoryId);
            }

            List<Pair<String, String>> idUrlPairs = searchResultItems.stream()
                    .map(e -> {
                        String id = e.getAttribute("id");
                        WebElement recipeLink = e.findElements(By.xpath("a")).get(0);
                        String recipeUrl = recipeLink.getAttribute("href");

                        return Pair.of(id, recipeUrl);
                    }).collect(Collectors.toList());

            for (Pair<String, String> p : idUrlPairs) {
                String recipeId = p.getLeft();
                try {
                    String recipeUrl = p.getRight();
                    ScrapItemResult result = processRecipe(recipeId, recipeUrl, targetDirectoryPath, webDriver);
                    processedRecipeIds.add(recipeId);
                    if (applicationConfiguration.isUpdateMode() && ScrapItemResult.ALREADY_KNOWN.equals(result)) {
                        log.info("processCategory Found known recipe. Because sorted by most recent rest should be there already. Skipping. categoryId={}, page={}, processed={}/{}", categoryId, page, processedRecipeCount, totalRecipeCount);
                        return;
                    }
                } catch (RecipeProcessException e) {
                    throw new CategoryParseException(categoryId, processedRecipeCount, totalRecipeCount, page, "Recipe process failed recipeId=" + recipeId, e);
                }
                processedRecipeCount++;
            }
            page++;
        }

        // pageFrom == 0 Only if complete! If it comes from error then it wont have all data!
        if (pageFrom == 0 && htmlFiles.size() > processedRecipeIds.size()) {
            for (Path htmlFile : htmlFiles) {
                String htmlFileRecipeId = scrapService.getRecipeId(htmlFile);
                if (!processedRecipeIds.contains(htmlFileRecipeId)) {
                    scrapService.moveToLegacy(targetDirectoryPath, htmlFileRecipeId);
                }
            }
        }
    }

    private void processHome(WebDriver webDriver) throws IOException {
        LoginPage loginPage = new LoginPage(webDriver);
        UserHomePage userHomePage = new UserHomePage(webDriver);
        UserSearchPage userSearchPage = new UserSearchPage(webDriver);

        seleniumWebDriverService.setUpBrowser(webDriver);

        loginPage.load(applicationConfiguration.getUrlLogin());
        loginPage.doLogin(applicationConfiguration.getCredentials());
        userHomePage.waitForVisibility();

        String languageId = applicationConfiguration.getLanguageId();

        List<CategoryScrapeRequest> easyCategoryScrapeRequests = getCategoryScrapeRequest(languageId, UserSearchPage.FILTER_FACET_DIFFICULTY_EASY, userSearchPage);
        List<CategoryScrapeRequest> mediumCategoryScrapeRequests = getCategoryScrapeRequest(languageId, UserSearchPage.FILTER_FACET_DIFFICULTY_MEDIUM, userSearchPage);
        List<CategoryScrapeRequest> advancedCategoryScrapeRequests = getCategoryScrapeRequest(languageId, UserSearchPage.FILTER_FACET_DIFFICULTY_ADVANCED, userSearchPage);

        List<CategoryScrapeRequest> categoryScrapeRequests = new ArrayList<>();
        categoryScrapeRequests.addAll(easyCategoryScrapeRequests);
        categoryScrapeRequests.addAll(mediumCategoryScrapeRequests);
        categoryScrapeRequests.addAll(advancedCategoryScrapeRequests);

        if (applicationConfiguration.isScrapCategoryBeforeDifficulty()) {
            categoryScrapeRequests.sort(Comparator.comparing(c -> c.languageId + c.categoryId + c.difficulty));
        }

        URL currentUrl = new URL(webDriver.getCurrentUrl());

        // can parallelize this
        for (CategoryScrapeRequest categoryScrapeRequest : categoryScrapeRequests) {
            log.info("Extracting request={}", categoryScrapeRequest);

            boolean complete = false;
            int pageFrom = 0;
            int lastPageError = -1;
            while (!complete) {
                try {
                    processCategory(currentUrl, categoryScrapeRequest, pageFrom, webDriver);
                    complete = true;
                } catch (CategoryParseException e) {
                    if (lastPageError == e.getPage()) { // Second fail on same page quits
                        throw new IllegalStateException("Parse failed too many times. request=" + categoryScrapeRequest + ".page=" + e.getPage() + ".Aborting ", e);
                    }
                    log.warn("Extracting category failed retry! request={}, page={}", categoryScrapeRequest, e.getPage());
                    webDriver.get(applicationConfiguration.getUrlLogout());
                    loginPage.load(applicationConfiguration.getUrlLogin());
                    loginPage.doLogin(applicationConfiguration.getCredentials());

                    lastPageError = e.getPage();
                    pageFrom = e.getPage();
                }
            }
        }
    }

    // NOTE: Add option to force recheck and/or force reimport
    private ScrapItemResult processRecipe(String recipeId, String recipeUrl, Path targetDirectoryPath, WebDriver webDriver) throws IOException, RecipeProcessException {
        Path htmlFile = scrapService.getHtmlFilePath(recipeId, targetDirectoryPath);
        Path pictureFile = scrapService.getPictureFilePath(recipeId, targetDirectoryPath);
        Path dataFile = scrapService.getDataFilePath(recipeId, targetDirectoryPath);

        boolean missingHtml = !Files.exists(htmlFile);
        boolean missingPicture = !Files.exists(pictureFile);
        boolean missingData = !Files.exists(dataFile);

        if (!missingHtml && !missingPicture && !missingData) {
            log.debug("process recipe already done. skipping. recipeId={}, url={}", recipeId, recipeUrl);
            return ScrapItemResult.ALREADY_KNOWN;
        }

        RecipePage recipePage = new RecipePage(webDriver);
        if (missingHtml) {
            log.info("process new recipe. recipeId={}, url={}", recipeId, recipeUrl);
            recipePage.load(recipeUrl);
            recipePage.waitForVisibility();
            String source = webDriver.getPageSource();
            Files.createDirectories(targetDirectoryPath);
            Files.writeString(htmlFile, source, CREATE);
        } else {
            String fullUrl = "file://" + htmlFile.toString();
            log.info("process existing recipe page. recipeId={}, url={}", recipeId, fullUrl);
            recipePage.load(fullUrl);
            recipePage.waitForVisibility();
        }
        if (missingPicture) {
            downloadPicture(recipePage, pictureFile);
        }
        if (missingData) {
            try {
                recipeDefinitionStorageService.write(toRecipeDefinition(recipeId, recipeUrl, recipePage), dataFile);
            } catch (RecipeProcessException e) {
                Path errorPath = Files.createDirectories(targetDirectoryPath.resolve("000_ERROR"));
                Files.move(htmlFile, errorPath.resolve(htmlFile.getFileName()));
                throw e;
            }
        }
        return ScrapItemResult.SUCCESS;
    }

    /**
     * Generates data file for each html file that is missing it.
     *
     * @throws IOException
     * @throws ExecutionException
     * @throws InterruptedException
     */
    public void reprocess() throws IOException, ExecutionException, InterruptedException {
        Path scrapDirectoryPath = applicationConfiguration.getScrapPath();
        List<Path> htmlFilePaths = Files.exists(scrapDirectoryPath) ? Files.walk(scrapDirectoryPath, REPROCESS_MAX_DEPTH)
                .filter(p -> p.toString().endsWith(EXTENSION_SEPARATOR + "html")).collect(Collectors.toList()) : Collections.emptyList();
        log.info("found files. count={}, path={}", htmlFilePaths.size(), scrapDirectoryPath);

        Function<Path, Path> dataFilepathFunction = scrapService.getDataFilePathFunction();
        List<Path> unprocessedHtmlFilePaths = htmlFilePaths.stream().filter(
                p -> !Files.exists(dataFilepathFunction.apply(p)))
                .sorted()
                .collect(Collectors.toList());

        if (unprocessedHtmlFilePaths.isEmpty()) {
            log.info("no unprocessed files, path={}", scrapDirectoryPath);
        } else {
            Partition<Path> partition = Partition.ofSize(unprocessedHtmlFilePaths, REPROCESS_CHUNK_SIZE);
            AtomicInteger count = new AtomicInteger();

            ConcurrentLinkedDeque<WebDriver> c = new ConcurrentLinkedDeque<>();
            int parallelism = applicationConfiguration.getReprocessParallelism();

            for (int i = 0; i < parallelism; i++) {
                WebDriver webDriver = seleniumWebDriverService.createWebDriver();
                seleniumWebDriverService.setOffline(webDriver);
                c.add(webDriver);
            }

            new ForkJoinPool(parallelism).submit(() -> {
                IntStream.range(0, partition.size())
                        .parallel()
                        .forEach(i -> {
                            count.incrementAndGet();
                            List<Path> batch = partition.get(i);
                            WebDriver webDriver = c.pop();
                            try {
                                reprocess(batch, webDriver);
                                log.info("processed. count={}/{}", count.addAndGet(batch.size()), unprocessedHtmlFilePaths.size());
                            } catch (IOException e) {
                                log.error("Exception processing batch", e);
                            } finally {
                                c.push(webDriver);
                            }
                        });
            }).get();

            c.forEach(seleniumWebDriverService::closeWebDriver);

        }
    }

    public void reprocess(List<Path> unprocessedHtmlFilePaths, WebDriver webDriver) throws IOException {
        Function<Path, Path> dataFilePathFunction = scrapService.getDataFilePathFunction();

        for (Path htmlFile : unprocessedHtmlFilePaths) {
            try {
                String recipeId = scrapService.getRecipeId(htmlFile);

                RecipePage recipePage = new RecipePage(webDriver); // NOPMD
                recipePage.load("file://" + htmlFile.toString());
                recipePage.waitForVisibility();

                Path pictureFilePath = scrapService.getPictureFilePath(recipeId, htmlFile.getParent());
                if (!Files.exists(pictureFilePath)) {
                    downloadPicture(recipePage, pictureFilePath);
                }

                recipeDefinitionStorageService.write(toRecipeDefinition(recipeId, null, recipePage), dataFilePathFunction.apply(htmlFile));
            } catch (RecipeProcessException e) {
                log.error("reprocess failed. Ignoring.", e);
            }
        }
    }

    private void sleep(int sleepUserSearch) {
        try {
            Thread.sleep(sleepUserSearch);
        } catch (InterruptedException e) { // NOPMD
        }
    }

    private RecipeDefinition toRecipeDefinition(String recipeId, String recipeUrl, RecipePage recipePage) throws IOException, RecipeProcessException {
        String dataJson = recipePage.getDataJson();
//        String ratingJson = recipePage.getRatingJson();

        CkdRecipe ckdRecipe = objectMapper.readValue(dataJson, CkdRecipe.class);
        if (ckdRecipe.getRecipeInstructions() == null || ckdRecipe.getRecipeInstructions().isEmpty()) {
            throw new RecipeProcessException(recipeId, recipeUrl, "Empty instructions found. Aborting. No permissions?? url=" + recipeUrl);
        }

        // locale should come from context
        RecipeDefinition recipeDefinition = ckdRecipeFormMapper.toRecipe(ckdRecipe);

        Map<String, List<String>> ingredientsBySection = new HashMap<>();
        int ingredientGroupCount = recipePage.getIngredientGroupCount();
        for (int i = 0; i < ingredientGroupCount; i++) {
            String ingredientSectionName = StringUtils.trimToNull(recipePage.getIngredientGroupName(i));
            List<String> ingredients = recipePage.getIngredientGroupItems(i);
            ingredientsBySection.put(ingredientSectionName == null ? "" : ingredientSectionName, ingredients);
        }

        for (int i = 0; i < recipeDefinition.getRecipeSections().size(); i++) {
            RecipeDefinition.RecipeSection rs = recipeDefinition.getRecipeSections().get(i);
            String sectionName = rs.getName();
            List<String> ingredients = ingredientsBySection.get(sectionName == null ? "" : sectionName);
            rs.setIngredients(ingredients);
        }

        recipeDefinition.setCollections(recipePage.getInCollectionTexts());

        recipeDefinition.setUtensils(ckdRecipeFormMapper.toStringList(recipePage.getUtensilsText()));

        Optional.ofNullable(StringUtils.trimToNull(recipePage.getHintsText())).ifPresent(h -> {
            recipeDefinition.setHints(of(h));
        });

        recipeDefinition.setSource(Optional.ofNullable(recipeUrl).orElseGet(() -> ckdRecipe.getAuthor().getUrl() + "recipes/recipe/" + ckdRecipe.getInLanguage() + "/" + recipeId + ".html"));
        recipeDefinition.setDifficulty(recipePage.getDifficultyText());
        recipeDefinition.setDevices(recipePage.getDevicesTexts());

        recipeDefinition.setDocumentVersion(applicationConfiguration.getDataDocumentVersion());

        return recipeDefinition;
    }

    @Data
    private final class CategoryScrapeRequest {
        private final String languageId;
        private final String categoryId;
        private final String difficulty;
        private final int totalCount;

        private CategoryScrapeRequest(String languageId, String categoryId, String difficulty, int totalCount) {
            this.languageId = languageId;
            this.categoryId = categoryId;
            this.difficulty = difficulty;
            this.totalCount = totalCount;
        }
    }

    private final class RecipeProcessException extends RuntimeException {
        private final String recipeId;
        private final String recipeUrl;

        private RecipeProcessException(String recipeId, String recipeUrl, String message) {
            super(message);
            this.recipeId = recipeId;
            this.recipeUrl = recipeUrl;
        }

        public String getRecipeId() {
            return recipeId;
        }

        public String getRecipeUrl() {
            return recipeUrl;
        }
    }

    private final class CategoryParseException extends RuntimeException {
        private final String categoryId;
        private final int page;
        private final int processedCount;
        private final int totalCount;

        private CategoryParseException(String categoryId, int processedCount, int totalCount, int page, String message) {
            this(categoryId, processedCount, totalCount, page, message, null);
        }

        private CategoryParseException(String categoryId, int processedCount, int totalCount, int page, String message, Throwable cause) {
            super(message, cause);
            this.categoryId = categoryId;
            this.processedCount = processedCount;
            this.totalCount = totalCount;
            this.page = page;
        }

        public String getCategoryId() {
            return categoryId;
        }

        public int getPage() {
            return page;
        }

        public int getProcessedCount() {
            return processedCount;
        }

        public int getTotalCount() {
            return totalCount;
        }
    }
}
