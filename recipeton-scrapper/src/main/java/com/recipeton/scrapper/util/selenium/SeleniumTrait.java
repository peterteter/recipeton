package com.recipeton.scrapper.util.selenium;

/*-
 * #%L
 * recipeton-scrapper
 * %%
 * Copyright (C) 2021 tincomisc
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import org.apache.commons.compress.utils.FileNameUtils;
import org.apache.commons.lang3.StringUtils;
import org.openqa.selenium.*;
import org.openqa.selenium.logging.LogEntries;
import org.openqa.selenium.logging.LogType;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.nio.file.Path;
import java.util.List;
import java.util.Optional;
import java.util.concurrent.TimeUnit;
import java.util.function.Function;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public interface SeleniumTrait {

    int DEFAULT_TIMEOUT_SECS = 5;
    int WAIT_MIN = Optional.ofNullable(System.getProperty("ẗest.web.wait.min", null)).map(Integer::valueOf).orElse(0);
    int WAIT_IMPLICIT = Optional.ofNullable(System.getProperty("ẗest.web.wait.implicit", null)).map(Integer::valueOf).orElse(5);

    default ExpectedCondition<Boolean> expectInvisibilityOfElementsLocated(By... selectors) {
        return new ExpectedCondition<>() {

            @Override
            public Boolean apply(WebDriver driver) {
                if (Stream.of(selectors).allMatch(s -> ExpectedConditions.invisibilityOfElementLocated(s).apply(driver))) {
                    return true;
                }
                return null;
            }

            @Override
            public String toString() {
                return "invisibility of all " + Stream.of(selectors).collect(Collectors.toList());
            }
        };
    }

    WebDriver getDriver();

    default String getInnerHtml(WebElement webElement) {
        return webElement.getAttribute("innerHTML");
    }

    default LogEntries popBrowserLogs() {
        return getDriver().manage().logs().get(LogType.BROWSER);
    }

    default void resetImplicitWait() {
        setImplicityWait(WAIT_IMPLICIT);
    }

    default void scrollAndClick(WebElement element) {
        scrollTo(element);
        element.click();
    }

    default void scrollTo(int x, int y) {
        JavascriptExecutor jse = (JavascriptExecutor) getDriver();
        jse.executeScript("window.scrollBy(" + x + "," + y + ")", "");
    }

    default void scrollTo(WebElement element) {
// Does not work on Chrome
// Actions actions = new Actions(driver);
// actions.moveToElement(orderDefinitionEditPage.nextDo);
// actions.perform();
        Point location = element.getLocation();
        int x = location.x;
        int y = location.y;
        scrollTo(x, y);
    }

    default void setImplicityWait(int timeOutInSeconds) {
        try {
            RemoteWebDriver remoteWebDriver = (RemoteWebDriver) getDriver(); //IllegalAccessException
            remoteWebDriver.manage().timeouts().implicitlyWait(timeOutInSeconds, TimeUnit.SECONDS);
        } catch (Exception e) { // NOPMD
            throw new RuntimeException(e); // NOPMD
        }
    }

    default By toSelector(WebElement webElement) {
        String s = webElement.toString();
        if (s.contains("By.cssSelector:")) {
            return By.cssSelector(StringUtils.substringBetween(s, "By.cssSelector:", "'").trim());
        }
        if (s.contains("css selector:")) {
            return By.cssSelector(StringUtils.substringBetween(s, "css selector:", "]").trim());
        }

        throw new RuntimeException("Could not extract selector from element. Maybe improve toSelector method. Element toString:" + webElement); //NOPMD
    }

    default boolean waitForCondition(ExpectedCondition<?> expectedCondition, int timeOutInSeconds) {
        try {
            setImplicityWait(WAIT_MIN);
            Function<? super WebDriver, ?> x = expectedCondition;
            new WebDriverWait(getDriver(), timeOutInSeconds).until(x);
            return true;
        } finally {
            resetImplicitWait();
        }
    }

    default void waitForCondition(ExpectedCondition<?> expectedCondition) {
        waitForCondition(expectedCondition, DEFAULT_TIMEOUT_SECS);
    }

    default WebElement waitForElementText(WebElement element, String text, int timeoutSecs) throws Error {
        new WebDriverWait(getDriver(), timeoutSecs).until(ExpectedConditions.textToBePresentInElement(element, text));
        return element;
    }

    default boolean waitForInvisibility(WebElement... webElements) {
        return waitForInvisibility(WAIT_IMPLICIT, Stream.of(webElements).map(this::toSelector).toArray(By[]::new));
    }

    default boolean waitForInvisibility(int timeOutInSeconds, By... selectors) {
        return waitForCondition(expectInvisibilityOfElementsLocated(selectors), timeOutInSeconds);
    }

    default List<WebElement> waitForInvisibility(List<WebElement> elements) {
        new WebDriverWait(getDriver(), DEFAULT_TIMEOUT_SECS)
                .until(ExpectedConditions.invisibilityOfAllElements(elements));
        return elements;
    }

    default List<WebElement> waitForVisibility(List<WebElement> elements) {
        new WebDriverWait(getDriver(), DEFAULT_TIMEOUT_SECS)
                .until(ExpectedConditions.visibilityOfAllElements(elements));
        return elements;
    }

    default WebElement waitForVisibility(WebElement element) throws Error {
        waitForVisibility(element, DEFAULT_TIMEOUT_SECS);
        return element;
    }

    default WebElement waitForVisibility(By locator) throws Error {
        waitForCondition(ExpectedConditions.visibilityOfElementLocated(locator));
        return getDriver().findElement(locator);
    }

    default WebElement waitForVisibility(WebElement element, int timeoutSecs) throws Error {
        new WebDriverWait(getDriver(), timeoutSecs).until(ExpectedConditions.visibilityOf(element));
        return element;

    }

    // Too slow
    default void writeSnapshot(WebElement webElement, Path filePath) throws IOException {
        byte[] data = webElement.getScreenshotAs(OutputType.BYTES);
        ByteArrayInputStream memstream = new ByteArrayInputStream(data);
        BufferedImage saveImage = ImageIO.read(memstream);
        ImageIO.write(saveImage, FileNameUtils.getExtension(filePath.getFileName().toString().toLowerCase()), filePath.toFile());


    }
}
