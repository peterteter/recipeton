package com.recipeton.scrapper.config;

/*-
 * #%L
 * recipeton-scrapper
 * %%
 * Copyright (C) 2021 tincomisc
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import lombok.Data;
import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@Slf4j
@Data
@Configuration
@ConfigurationProperties(prefix = "selenium")
public class SeleniumConfiguration {

    private static final int DISABLED = -1;

    private long implicitWait = DISABLED;
    private long pageLoadTimeout = DISABLED;
    private List<SeleniumDriver> driverPrecedence = Stream.of(SeleniumDriver.CHROMIUM, SeleniumDriver.FIREFOX, SeleniumDriver.CHROME_REMOTE).collect(Collectors.toList());

    private BrowserConfiguration browser = new BrowserConfiguration();

    private ChromiumConfiguration chromium = new ChromiumConfiguration();
    private FirefoxConfiguration firefox = new FirefoxConfiguration();
    private ChromeRemoteConfiguration chromeRemote = new ChromeRemoteConfiguration();

    public enum SeleniumDriver {
        FIREFOX, CHROMIUM, CHROME_REMOTE
    }

    @Data
    public class ChromeRemoteConfiguration {
        private boolean auto = true;
        private List<String> browserPaths = List.of(
                "/snap/bin/chromium",
                "/usr/bin/chromium-browser",
                "/Applications/Google Chrome.app/Contents/MacOS/Google Chrome",
                "/opt/chrome/chrome",
                "/opt/opt/chrome/chrome");

        private List<String> driverPaths = List.of(
                "/snap/bin/chromium.chromedriver",
                "/usr/lib/chromium-browser/chromedriver",
                "/usr/local/bin/chromedriver");
        private String driverVersion;
    }

    @Data
    public class ChromiumConfiguration {
        private boolean auto = true;
        private boolean sandboxed;
        private List<String> browserPaths = List.of(
                "/snap/bin/chromium",
                "/usr/bin/chromium-browser",
                "/Applications/Google Chrome.app/Contents/MacOS/Google Chrome",
                "/opt/chrome/chrome",
                "/opt/opt/chrome/chrome");

        private List<String> driverPaths = List.of(
                "/snap/bin/chromium.chromedriver",
                "/usr/lib/chromium-browser/chromedriver",
                "/usr/local/bin/chromedriver");
        private String driverVersion;
    }

    @Data
    public class FirefoxConfiguration {
        private boolean auto = true;
        private List<String> driverPaths = List.of(
                "/usr/bin/geckodriver",
                "/usr/lib/geckodriver/geckodriver",
                "/usr/local/bin/geckodriver");
    }

    @Data
    public class BrowserConfiguration {
        private static final int DEFAULT_WIDTH = 1280;
        private static final int DEFAULT_HEIGHT = 1024;
        private boolean headless = true;
        private boolean picturesDisabled;
        private boolean crawlerMode;

        private int width = DEFAULT_WIDTH;
        private int height = DEFAULT_HEIGHT;
    }
}
