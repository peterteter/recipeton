package com.recipeton.scrapper.config;

/*-
 * #%L
 * recipeton-scrapper
 * %%
 * Copyright (C) 2021 tincomisc
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.recipeton.shared.service.RecipeDefinitionStorageService;
import com.recipeton.shared.util.ObjectMapperFactory;
import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.nio.file.Path;

@Data
@Configuration
@ConfigurationProperties(prefix = "recipeton")
public class ApplicationConfiguration {

    public static final String OPERATION_REPROCESS = "reprocess";
    public static final String OPERATION_EXPORT = "export";

    public static final String SUBPATH_LEGACY = "000_LEGACY";
    public static final int RECIPE_PAGE_SIZE_DEFAULT = 20;
    public static final int RECIPE_PAGE_MAX_DEFAULT = 49;
    public static final int PARALLELISM_DEFAULT = 8;

    private Path scrapPath;
    private Path exportPath;
    private String extractDataExtension = "yaml";

    private String operation;

    private int reprocessParallelism = PARALLELISM_DEFAULT;

    private Credentials credentials;

    private String urlHome;
    private String urlLogin;
    private String urlLogout;
    private String urlSearch;

    private String languageId;

    private boolean updateMode;
    private int recipePageSize = RECIPE_PAGE_SIZE_DEFAULT;
    private int recipePageMax = RECIPE_PAGE_MAX_DEFAULT;

    private String recipePictureFormat = "jpg";
    private boolean scrapCategoryBeforeDifficulty = true;

    private String dataDocumentVersion = "1.0";

    @Bean
    public RecipeDefinitionStorageService recipeStorageService() {
        return new RecipeDefinitionStorageService(new ObjectMapperFactory().createInstance(true));
    }

    @Data
    public static class Credentials {
        private String name;
        private String password;
    }
}
