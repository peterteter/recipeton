package com.recipeton.cookbook.config;

/*-
 * #%L
 * recipeton-tmdata-service
 * %%
 * Copyright (C) 2021 tincomisc
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

import java.nio.file.Path;

@Configuration
@ConfigurationProperties(prefix = "recipeton")
@Data
public class ApplicationConfiguration {

    private Path dataRootPath;

    private int dataMaxDepth = 1;
    private String[] dataExtensions = new String[]{"iso", "img"};
    private String dataContentDescriptionExtension = "lst";

    private String usbVendor;
    private String usbProduct;
    private String usbSerial;
    private String usbManufacturer;
    private String usbProductString;
    private String usbBcdDevice;
}
