package com.recipeton.cookbook.service;

/*-
 * #%L
 * recipeton-cookbook-service
 * %%
 * Copyright (C) 2021 tincomisc
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.recipeton.cookbook.config.ApplicationConfiguration;
import com.recipeton.cookbook.util.ProcessHelper;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.io.FilenameUtils;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.Collections;
import java.util.List;
import java.util.Optional;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.stream.Collectors;

@Slf4j
@RequiredArgsConstructor
@Service
public class RecipeCollectionService {

    private final ApplicationConfiguration applicationConfiguration;
    private final AtomicBoolean publishing = new AtomicBoolean();
    private Path currentPath;

    private int doMassStorageMount(Path path) throws IOException, InterruptedException {
        String mountCommand = String.format("sudo modprobe -v g_mass_storage file=%s stall=0 idVendor=%s idProduct=%s iManufacturer=\"%s\" iProduct=\"%s\" iSerialNumber=\"%s\" bcdDevice=%s cdrom=y ro=y\n", path, applicationConfiguration.getUsbVendor(), applicationConfiguration.getUsbProduct(), applicationConfiguration.getUsbManufacturer(), applicationConfiguration.getUsbProductString(), applicationConfiguration.getUsbSerial(), applicationConfiguration.getUsbBcdDevice());
        return ProcessHelper.exec(mountCommand, c -> log.info("exec:" + c));
    }

    private int doMassStorageUmount() throws IOException, InterruptedException {
        String umountCommand = "sudo rmmod -v g_mass_storage";
        return ProcessHelper.exec(umountCommand, c -> log.info("exec:" + c));
    }

    public void doPublish(Path path) throws RecipeCollectionPublishException {
        log.info("doPublish id={}", path);
        if (publishing.compareAndSet(false, true)) {
            try {
                if (currentPath != null && currentPath.equals(path)) {
                    log.warn("doPublish same path. Ignoring. path={}", path);
                    return;
                }

                int umountCode = doMassStorageUmount();
                log.debug("umount executed. code={}", umountCode);
                int mountResult = doMassStorageMount(path);
                log.debug("mount executed. code={}", mountResult);

                currentPath = path;
            } catch (InterruptedException | IOException e) {
                currentPath = null;
                throw new RecipeCollectionPublishException("Publish failed for id=" + path.getFileName() + " , reason=" + e.getMessage(), e);
            } finally {
                publishing.set(false);
            }
        } else {
            log.warn("Already publishing. Ignore. path={}", path);
        }
    }

    public Path getCurrentPath() {
        return currentPath;
    }

    public List<Path> getRecipeCollectionPaths() throws IOException {
        if (Files.exists(applicationConfiguration.getDataRootPath())) {
            return Files.walk(applicationConfiguration.getDataRootPath(), applicationConfiguration.getDataMaxDepth())
                    .filter(p -> FilenameUtils.isExtension(p.getFileName().toString().toLowerCase(), applicationConfiguration.getDataExtensions())).collect(Collectors.toList());
        }
        return Collections.emptyList();
    }

    public Optional<Path> getRecipeCollectionPathsByFileName(String fileName) {
        Path fullPath = applicationConfiguration.getDataRootPath().resolve(fileName);
        if (Files.exists(fullPath)) {
            return Optional.of(fullPath);
        } else {
            return Optional.empty();
        }
    }

    public Optional<List<String>> getRecipeCollectionTitlesByFileName(String fileName) throws IOException {
        Path fullPath = applicationConfiguration.getDataRootPath().resolve(fileName);
        if (Files.exists(fullPath)) {
            Path descriptionFilePath = fullPath.resolveSibling(FilenameUtils.getBaseName(fullPath.getFileName().toString()) + FilenameUtils.EXTENSION_SEPARATOR + applicationConfiguration.getDataContentDescriptionExtension());
            if (Files.exists(descriptionFilePath)) {
                // Raspberry pi zero default is NOT utf8!
                return Optional.of(Files.readAllLines(descriptionFilePath, StandardCharsets.UTF_8));
            } else {
                return Optional.of(Collections.emptyList());
            }
        }
        return Optional.empty();
    }

    @PostConstruct
    public void postConstruct() throws IOException {
        Files.createDirectories(applicationConfiguration.getDataRootPath());
    }
}
