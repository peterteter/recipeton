package com.recipeton.cookbook.server.rest;

/*-
 * #%L
 * recipeton-cookbook-service
 * %%
 * Copyright (C) 2021 tincomisc
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.recipeton.cookbook.server.rest.form.RecipeCollectionContentsForm;
import com.recipeton.cookbook.server.rest.form.RecipeCollectionForm;
import com.recipeton.cookbook.server.rest.form.RecipePublishRequestForm;
import com.recipeton.cookbook.service.RecipeCollectionPublishException;
import com.recipeton.cookbook.service.RecipeCollectionService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.RequiredArgsConstructor;
import org.apache.commons.io.FilenameUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.Base64;
import java.util.List;
import java.util.stream.Collectors;

@RestController
@RequiredArgsConstructor
@RequestMapping("/api/recipeCollection")
@Tag(name = "recipeCollection", description = "Recipe collections")
public class RecipeCollectionController {

    public static final String SPLITTER = "|";
    private final RecipeCollectionService recipeCollectionService;

    @GetMapping
    public ResponseEntity<List<RecipeCollectionForm>> getAll() throws IOException {
        List<RecipeCollectionForm> recipeCollectionForms = recipeCollectionService.getRecipeCollectionPaths().stream()
                .sorted()
                .map(this::toRecipeCollectionForm)
                .collect(Collectors.toList());
        if (recipeCollectionForms.isEmpty()) {
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        }
        return new ResponseEntity<>(recipeCollectionForms, HttpStatus.OK);
    }

    @Operation(summary = "Get recipe collection by id",
            description = "Returns recipe collection with matching id",
            tags = {"recipeCollection"})
    @ApiResponses(value = {
            @ApiResponse(
                    responseCode = "200",
                    description = "successful operation",
                    content = @Content(schema = @Schema(implementation = RecipeCollectionForm.class))),
            @ApiResponse(responseCode = "404", description = "recipe collection not found"),
            @ApiResponse(responseCode = "405", description = "Validation exception")
    })

    @GetMapping("/{id}")
    public RecipeCollectionForm getById(@Parameter(description = "identifier") @PathVariable String id) {
        Path path = recipeCollectionService.getRecipeCollectionPathsByFileName(toRecipeCollectionFileName(id)).orElseThrow(() -> new ResourceNotFoundException("Could not find resource with id=" + id));
        return toRecipeCollectionForm(path);
    }

    // better already store as json and stream
    @GetMapping("/{id}/content")
    public RecipeCollectionContentsForm getContentById(@Parameter(description = "identifier") @PathVariable String id) throws IOException {
        List<String> titles = recipeCollectionService.getRecipeCollectionTitlesByFileName(toRecipeCollectionFileName(id)).orElseThrow(() -> new ResourceNotFoundException("Could not find resource with id=" + id));
        return RecipeCollectionContentsForm.builder()
                .titles(titles)
                .build();
    }

    @Operation(summary = "Publish recipe collection",
            tags = {"recipeCollection"})
    @ApiResponses(value = {
            @ApiResponse(
                    responseCode = "200",
                    description = "successful operation",
                    content = @Content(schema = @Schema(implementation = RecipeCollectionForm.class))),
            @ApiResponse(responseCode = "500", description = "error performing operation")
    })
    @PostMapping("/{id}/publish")
    public ResponseEntity<?> postPublishRequest(@PathVariable("id") String id, @RequestBody RecipePublishRequestForm recipePublishRequestForm) {
        try {
            Path path = recipeCollectionService.getRecipeCollectionPathsByFileName(toRecipeCollectionFileName(id)).orElseThrow(() -> new ResourceNotFoundException("Could not find resource with id=" + id));
            recipeCollectionService.doPublish(path);
            return new ResponseEntity<>(HttpStatus.OK);
        } catch (RecipeCollectionPublishException e) {
            return new ResponseEntity<>(e.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    public String toRecipeCollectionFileName(String raw) {
        String decoded = new String(Base64.getUrlDecoder().decode(raw));
        return StringUtils.split(decoded, SPLITTER)[1];
    }

    private RecipeCollectionForm toRecipeCollectionForm(Path p) {
        try {
            String name = StringUtils.replace(FilenameUtils.getBaseName(p.getFileName().toString()), "_", " ");
            return RecipeCollectionForm.builder()
                    .id(toRecipeCollectionId(p))
                    .name(name)
                    .published(p.equals(recipeCollectionService.getCurrentPath()))
                    .build();
        } catch (IOException e) {
            throw new IllegalArgumentException(e);
        }

    }

    public String toRecipeCollectionId(Path path) throws IOException {
        String text = Files.getLastModifiedTime(path) + SPLITTER + path.getFileName().toString();
        return Base64.getUrlEncoder().withoutPadding().encodeToString(text.getBytes(StandardCharsets.UTF_8));
    }

}
