# Recipeton

This is a group of applications dedicated to recipe parsing and conversion into different systems.

The main goals are:

- Provide a relatively simple way to create and share recipes
- Convert recipes to be used by smart kitchen devices 
- Collect and analyze recipes from different sources
- Store and backup your recipes and avoid planned obsolescence

## Build and execute

## Prerequisites

### Software

- Java 11
- Maven

## How to build

These are maven projects

Go to project root and run

```
mvn package
```

You will find the built artifacts in the target folders

## Contribution

Ideas and, specially, pull requests are welcome.

## Components

## Scrapper

Application to extract recipes from online sources.

This is a java spring boot application

### How to execute

#### Setup
The application expects chromedriver or firefox equivalent. Although some automated download can happen. You may see
an error if you do not have it installed on your system.

#### Configuration file (optional)

You can set credentials and other parameters at: 

```
 ${user.home}/.config/recipeton/scrapper/application.yaml
```

You can skip this and use command line parameters. Or do a combination of both.

If you use the configuration file your password would be in clear text so be aware of that. 

Sample configuration file content
```
# One of es, en, de, it, etc...
recipeton:
    languageId: es
    siteLocale: es-ES
    site: http://cooksite.es

    credentials:
      name: <username>
      password: <password>
```


#### Execution

Open a terminal and run

```
java -jar recipeton-scrapper-0.0.2-SNAPSHOT.jar <Parameters>
```

Results of execution, by default are placed in 

```
${user.home}/recipeton/scrapper
```

#####  Command line credentials

You should set username and password (if you have not already set them in configuration file) with:
```
--recipeton.credentials.name=<username> 
--recipeton.credentials.password=<password>
```

#####  Site

You should set target site with

```
--recipeton.site=http://cooksite.es
```

or if you use a profile

```
--recipeton.siteBase=http://cooksite
```

#####  Profiles

There are some profiles for common configured locales. 

Activate profiles with parameter:
```
--spring.profiles.active=<profileName>
```

- For Spanish (lang_es)
```
java -jar recipeton-scrapper-0.0.2-SNAPSHOT.jar --spring.profiles.active=lang_es --recipeton.siteBase=http://cooksite --recipeton.credentials.name=<username> --recipeton.credentials.password=<password>
```

- For German (lang_de)
```
java -jar recipeton-scrapper-0.0.2-SNAPSHOT.jar --spring.profiles.active=lang_de --recipeton.siteBase=http://cooksite --recipeton.credentials.name=<username> --recipeton.credentials.password=<password>
```

- For English (lang_en)
```
java -jar recipeton-scrapper-0.0.2-SNAPSHOT.jar --spring.profiles.active=lang_en --recipeton.siteBase=http://cooksite --recipeton.credentials.name=<username> --recipeton.credentials.password=<password>
```

- For French (lang_fr)
```
java -jar recipeton-scrapper-0.0.2-SNAPSHOT.jar --spring.profiles.active=lang_fr --recipeton.siteBase=http://cooksite --recipeton.credentials.name=<username> --recipeton.credentials.password=<password>
```

- For Italian (lang_it)
```
java -jar recipeton-scrapper-0.0.2-SNAPSHOT.jar --spring.profiles.active=lang_it --recipeton.siteBase=http://cooksite --recipeton.credentials.name=<username> --recipeton.credentials.password=<password>
```

#####  Output directory

```
--recipeton.outputPath: ${user.home}/recipeton/scrapper
```

#####  Full parameter

If you do not use a profile you can use the locale parameters

```
--recipeton.site=http://cooksite.es 
--recipeton.siteLocale=es-ES 
--recipeton.languageId=es
```

**It is important that the site tld matches with the language**

Follow this table and replace 'cooksite' with the name of the site

Site                    | languageId    | siteLocale 
---                     | ---           | --- 
http://cooksite.es      | es            | es-ES 
http://cooksite.fr      | fr            | fr-FR 
http://cooksite.de      | de            | de-DE 
http://cooksite.co.uk   | en            | en-GB 
 


## TM Service

Converts recipes in import folders into a relational database that can be loaded by some smart kitchen appliances.

See:
https://www.mikrocontroller.net/topic/thermomix-rezeptchips?page=5

#### Execution

Open a terminal and run (replace bookName with your desired name)

```
java -jar recipeton-tmdata-service-0.0.2-SNAPSHOT.jar --recipeton.bookName=<bookName>
```

On first execution. Directories would be created automatically. You can use that fist execution to create directories and
then subsequent executions to add more data to your recipe book.

Your generated recipe book is created at:
```
    bookRootPath: ${user.home}/recipeton/tmdata/<bookName>
```

You create content in your book by placing recipe files (yaml+jpg) at
```
    bookResourceImportPath: ${user.home}/recipeton/tmdata/<bookName>/import
```


Successfully imported files are moved to:
```
    bookResourceRecipeDataPath: ${user.home}/recipeton/tmdata/<bookName>/recipe
```
If you remove a file from this directory the content will be removed from the book database.

Finally, failed and skipped import files are moved to:
```
    bookResourceImportErrorPath:${user.home}/recipeton/tmdata/<bookName>/000_error
    bookResourceImportErrorPath:${user.home}/recipeton/tmdata/<bookName>/000_skip
```
an extra file with ```.err``` extension is generated with some clue about the import error.


#### Source support status

Source                  | type          | languageId    | Status 
---                     | ---           | ---           | --- 
cooksite.es             | cooksite      | es            | (reasonable)
cooksite.co.uk          | cooksite      | en            | (getting close to reasonable)
cooksite.fr             | cooksite      | fr            | *WIP*  (need help to complete) 
cooksite.de             | cooksite      | de            | *WIP*  (need help to complete)
cooksite.it             | cooksite      | it            | Not supported yet (need help)
...                     |               |               | Not supported yet (need help)


## Shared recipe format

Recipe format is, in principle, yaml.

You should create a recipe file with a desired name and create a sibling file with recipe picture. 

Example:
```
cr1234.yaml
cr1234.jpg
```



Spanish example

```
name: Arroz con verduras
totalTime: PT20M
cookTime: PT50M
prepTime: PT10M
recipeYield: 5 servicios
recipeCategory:
  - Pasta y Arroz
recipeSections:
  - name: Verduras
    ingredients:
      - 30 g de aceite de oliva
      - 1 diente de ajo
      - 1 pellizco de sal
      - 3 - 4 pimientos (aprox. 250 g) en rodajas finas
      - 200 g de champiñones laminados
      - 300 g de ramilletes de brócoli
    instructions:
      - Ponga en el vaso el aceite y el ajo. Pique [2 seg/vel 7] y sofría [3 min/120°C/vel 1]
      - Incorpore los pimientos, los champiñones y la sal
      - Rehogue [10 min/120°C/reverse/vel 1]
      - Mientras tanto, ponga los ramilletes de brócoli en la vaporera
  - name: Arroz
    ingredients:
      - 300 g de arroz de grano redondo
      - 1 cucharadita de sal
      - 800 g de agua
    instructions:
      - Introduzca el cestillo en el vaso y vierta el arroz. Añada la sal y el agua y programe [10 min/100°C/vel 4]
      - Sitúe el recipiente vaporera en su posición y programe [10 min/Vapor/vel 2]
      - Sirva el arroz con la verduras y disfrute
hints:
  - Este plato es muy saludable :)
utensils:
  - cuchara
nutrition:
  calories: 11 kcal
  carbohydrateContent: 12.1 g
  fatContent: 13 g
  proteinContent: 14.75 g
keywords:
  - Personal
  - Arroz
collections:
  - Mis recetas
devices:
  - TM5
locale: es-ES
source: cooki
author: My name
difficulty: medio
```


### Content Server (recipeton-cookbook-service)
### Content Server Client (recipeton-cookbook-service-fe)

#### Setup

##### Raspberry Pi Zero

###### Java 11
Be careful! Raspbian Java 11 doesnot work on Raspberry pi Zero.  

To fix this follow this instructions (from https://webtechie.be/post/2020-08-27-azul-zulu-java-11-and-gluon-javafx-11-on-armv6-raspberry-pi/)

Download tar
```
$ sudo wget https://cdn.azul.com/zulu-embedded/bin/zulu11.41.75-ca-jdk11.0.8-linux_aarch32hf.tar.gz
```

Unpack to /usr/lib/jvm
```
$ sudo tar -xzvf zulu11.41.75-ca-jdk11.0.8-linux_aarch32hf.tar.gz -C /usr/lib/jvm
```

Update java alternatives
```
$ sudo update-alternatives --install /usr/bin/java java /usr/lib/jvm/zulu11.41.75-ca-jdk11.0.8-linux_aarch32hf/bin/java 1
$ sudo update-alternatives --config java
There are 2 choices for the alternative java (providing /usr/bin/java).

  Selection    Path                                                             Priority   Status
------------------------------------------------------------
* 0            /usr/lib/jvm/java-11-openjdk-armhf/bin/java                       1111      auto mode
  1            /usr/lib/jvm/java-11-openjdk-armhf/bin/java                       1111      manual mode
  2            /usr/lib/jvm/zulu11.41.75-ca-jdk11.0.8-linux_aarch32hf/bin/java   1         manual mode

Press <enter> to keep the current choice[*], or type selection number: 2
update-alternatives: using /usr/lib/jvm/zulu11.41.75-ca-jdk11.0.8-linux_aarch32hf/bin/java to provide /usr/bin/java (java) in manual mode
```

Update javac alternatives (optional)
```
$ sudo update-alternatives --install /usr/bin/javac javac /usr/lib/jvm/zulu11.41.75-ca-jdk11.0.8-linux_aarch32hf/bin/javac 1
$ sudo update-alternatives --config javac
There are 2 choices for the alternative javac (providing /usr/bin/javac).

  Selection    Path                                                              Priority   Status
------------------------------------------------------------
* 0            /usr/lib/jvm/java-11-openjdk-armhf/bin/javac                       1111      auto mode
  1            /usr/lib/jvm/java-11-openjdk-armhf/bin/javac                       1111      manual mode
  2            /usr/lib/jvm/zulu11.41.75-ca-jdk11.0.8-linux_aarch32hf/bin/javac   1         manual mode

Press <enter> to keep the current choice[*], or type selection number: 2
update-alternatives: using /usr/lib/jvm/zulu11.41.75-ca-jdk11.0.8-linux_aarch32hf/bin/javac to provide /usr/bin/javac (javac) in manual mode
```

If everything went well, we should be able to check the Java version now…

```
$ java -version
openjdk version "11.0.8" 2020-07-14 LTS
OpenJDK Runtime Environment Zulu11.41+75-CA (build 11.0.8+10-LTS)
OpenJDK Client VM Zulu11.41+75-CA (build 11.0.8+10-LTS, mixed mode)
```

##### Permissions

Probably your pi is not going to ask for passwords when sudo.

Just in case  this happens. you may need to avoid permission request.

run
```
visudo
```

Append your permissions

i.e:
```
YOUR_USER ALL= NOPASSWD: /usr/bin/dmesg
```


##### Keep job running

naturally create a service.

For quick tests 

```
nohup long-running-command &
```

or

```
ctrl+z
bg
disown -h
```


#### Development

TS/JS Frontend. 
Based on **Angular 11** 

It is served directly from recipeton-cookbook-service

Make sure Angular CLI is installed globally

```
sudo npm uninstall -g @angular/cli
sudo npm install -g @angular/cli --unsafe-perm=true --allow-root
```

##### Npm

If you want to execute manually json server
```
npm run json-server
```

```
npm run ng serve
```
##### Maven
There are a number of maven profiles to also facilitate development

To compile the project quicker when all dependencies are already present

```
cd recipeton-cookbook-service-fe
mvn install -Pquick
```

```
mvn install -P build_dev
```

To start server (ng serve)
Served content available at  http://localhost:4200/app

```                                                                        
cd recipeton-cookbook-service-fe
mvn package -Pbuild_dev
```                                                                        
which is essentially doing

```                                                                        
npm run build_dev
```                                                                        

If a light mock backend is needed using Json server could provide data.
Can be started by running

```
mvn package -P dev-server
```

