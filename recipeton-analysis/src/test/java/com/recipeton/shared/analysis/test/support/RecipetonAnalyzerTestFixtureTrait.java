package com.recipeton.shared.analysis.test.support;

/*-
 * #%L
 * recipeton-analysis
 * %%
 * Copyright (C) 2021 tincomisc
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.recipeton.shared.analysis.configuration.RecipeAnalyzerConfiguration;
import com.recipeton.shared.analysis.domain.RecipeIngredientAnalysis;
import com.recipeton.shared.analysis.domain.RecipeInstructionAnalysis;
import com.recipeton.shared.analysis.domain.RecipeInstructionTokenization;
import com.recipeton.shared.analysis.domain.RecipeUnitDefinition;
import com.recipeton.shared.analysis.service.RecipeIngredientAnalyzer;
import com.recipeton.shared.analysis.service.RecipeIngredientStandardAnalyzer;
import com.recipeton.shared.analysis.service.RecipeInstructionAnalyzer;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.tuple.Pair;

import java.util.Collections;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

import static com.recipeton.shared.analysis.configuration.RecipeAnalyzerConfiguration.TOKEN_LENGTH_MIN;
import static com.recipeton.shared.util.StreamUtil.toStream;

public interface RecipetonAnalyzerTestFixtureTrait {

    default RecipeAnalyzerConfiguration createRecipeAnalyzerConfiguration() {
        return new RecipeAnalyzerConfiguration("test", null)
                .setRecipeUnitDefinitions(
                        new RecipeUnitDefinition("someUnit")
                )
                .setStopWords(Set.of())
                .setInstructionMatchers(Collections.emptyMap());
    }

    default List<RecipeIngredientAnalysis> createRecipeIngredientAnalyses(RecipeAnalyzerConfiguration configuration, String... ingredients) {
        return toStream(ingredients).filter(StringUtils::isNotBlank).map((String notation) -> createRecipeIngredientAnalysis(notation, configuration)).collect(Collectors.toList());
    }

    default RecipeIngredientAnalysis createRecipeIngredientAnalysis(String source, String notation, RecipeAnalyzerConfiguration configuration) {
        return createRecipeIngredientAnalysis(notation, configuration).setSource(source);
    }

    default RecipeIngredientAnalysis createRecipeIngredientAnalysis(String notation, RecipeAnalyzerConfiguration configuration) {
        return new RecipeIngredientAnalysis()
                .setIngredientNotation(configuration.toUid(notation), notation, configuration.getTokens(notation, TOKEN_LENGTH_MIN));
    }

    default RecipeIngredientAnalyzer createRecipeIngredientAnalyzer(RecipeAnalyzerConfiguration configuration) {
        return new RecipeIngredientStandardAnalyzer(configuration);
    }

    default RecipeIngredientAnalyzer createRecipeIngredientAnalyzer() {
        return createRecipeIngredientAnalyzer(createRecipeAnalyzerConfiguration());
    }

    default RecipeInstructionAnalysis createRecipeInstructionAnalysis(String text, RecipeAnalyzerConfiguration configuration) {
        return new RecipeInstructionAnalysis(configuration.getTokenization(text, TOKEN_LENGTH_MIN));
    }

    default RecipeInstructionAnalysis createRecipeInstructionAnalysisWrapped(String instructionTextWithoutCommandTags, RecipeAnalyzerConfiguration configuration) {
        Pair<String, String> pair = configuration.getCommandDelimiters().get(0);
        return createRecipeInstructionAnalysis(pair.getLeft() + instructionTextWithoutCommandTags + pair.getRight(), configuration);
    }

    default RecipeInstructionAnalyzer createRecipeInstructionAnalyzer(RecipeAnalyzerConfiguration configuration) {
        return new RecipeInstructionAnalyzer(configuration);
    }

    default RecipeInstructionTokenization.Position createTokenizationPosition(int offset, int length) {
        return new RecipeInstructionTokenization.Position(offset, length);
    }


}
