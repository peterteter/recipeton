package com.recipeton.shared.analysis.cooki.en;

/*-
 * #%L
 * recipeton-shared
 * %%
 * Copyright (C) 2021 tincomisc
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.recipeton.shared.analysis.configuration.RecipeAnalyzerConfiguration;
import com.recipeton.shared.analysis.configuration.RecipeAnalyzerCookiEnConfiguration;
import com.recipeton.shared.analysis.domain.RecipeInstructionAnalysis;
import com.recipeton.shared.analysis.service.RecipeCommandAnalyzer;
import com.recipeton.shared.analysis.service.filter.RecipeCommandRecipeInstructionAnalysisFilter;
import com.recipeton.shared.analysis.test.support.RecipetonAnalyzerTestAssertTrait;
import org.junit.jupiter.api.Test;

import java.time.Duration;

import static org.assertj.core.api.Assertions.assertThat;

class RecipeCommandRecipeInstructionAnalysisFilterEnTest implements RecipetonAnalyzerTestAssertTrait {

    private final RecipeAnalyzerConfiguration configuration = new RecipeAnalyzerCookiEnConfiguration();
    private final RecipeCommandAnalyzer recipeCommandAnalyzer = new RecipeCommandAnalyzer(configuration);

    private final RecipeCommandRecipeInstructionAnalysisFilter recipeCommandAnalyzerFilter = new RecipeCommandRecipeInstructionAnalysisFilter(recipeCommandAnalyzer, configuration.getCommandDelimiters().get(0));

    private RecipeInstructionAnalysis apply(String s) {
        return recipeCommandAnalyzerFilter.apply(createRecipeInstructionAnalysisWrapped(s, configuration));
    }

    private RecipeInstructionAnalysis applyFilterWrapping(String command) {
        return apply(command);
    }

    @Test
    public void testAcceptGivenInstructionWithDurationHalfSecondsThenTermsContainsDuration() {
        assertThatRecipeCommandTerm(apply("0.5 sec"), c -> assertThat(c.getDuration()).isEqualTo(Duration.ofMillis(500)));
    }

    @Test
    public void testAcceptGivenInstructionWithDurationHoursThenTermsContainsDuration() {
        assertThatRecipeCommandTerm(apply("5 hour"), c -> assertThat(c.getDuration()).isEqualTo(Duration.ofHours(5)));
    }

    @Test
    public void testAcceptGivenInstructionWithDurationMinutesThenTermsContainsDuration() {
        assertThatRecipeCommandTerm(apply("5 min"), c -> assertThat(c.getDuration()).isEqualTo(Duration.ofMinutes(5)));
    }

    @Test
    public void testAcceptGivenInstructionWithDurationSecondsThenTermsContainsDuration() {
        assertThatRecipeCommandTerm(apply("5 sec"), c -> assertThat(c.getDuration()).isEqualTo(Duration.ofSeconds(5)));
    }

    @Test
    public void testAcceptGivenInstructionWithNoRecognizedContentThenTermsEmpty() {
        assertThat(applyFilterWrapping("otherjunk").getTerms()).isEmpty();
    }
}
