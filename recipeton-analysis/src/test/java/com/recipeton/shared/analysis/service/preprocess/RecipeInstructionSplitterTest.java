package com.recipeton.shared.analysis.service.preprocess;

/*-
 * #%L
 * recipeton-shared
 * %%
 * Copyright (C) 2021 tincomisc
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.Test;

import java.util.Map;

import static org.assertj.core.api.Assertions.assertThat;

@Slf4j
public class RecipeInstructionSplitterTest {

    private final RecipeInstructionSplitter recipeInstructionAnalyzer = new RecipeInstructionSplitter(
            Map.of(".", ""),
            Map.of(),
            true);

    @Test
    void testSplitInstructionGivenStringWithDotsInsdeParenthesesThenReturnsSubstringsWithoutSplittingParenthesesEnclosedContents() {
        assertThat(recipeInstructionAnalyzer.splitInstruction("1(2).3")).containsExactly("1(2)", "3");
        assertThat(recipeInstructionAnalyzer.splitInstruction("1(2.3)4")).containsExactly("1(2.3)4");
    }

    @Test
    void testSplitInstructionGivenStringWithDotsInsdeTagsThenReturnsSubstringsWithoutSplittingTagContents() {
        assertThat(recipeInstructionAnalyzer.splitInstruction("1<tag>2</tag>")).containsExactly("1<tag>2</tag>");
        assertThat(recipeInstructionAnalyzer.splitInstruction("<tag>1</tag>2")).containsExactly("<tag>1</tag>2");
        assertThat(recipeInstructionAnalyzer.splitInstruction("1<tag>2</tag>3")).containsExactly("1<tag>2</tag>3");

        assertThat(recipeInstructionAnalyzer.splitInstruction("1<tag>2.3</tag>4.5")).containsExactly("1<tag>2.3</tag>4", "5");
        assertThat(recipeInstructionAnalyzer.splitInstruction("1<tag>2.3</tag>4<tab>5.6</cc>7")).containsExactly("1<tag>2.3</tag>4<tab>5.6</cc>7");

        assertThat(recipeInstructionAnalyzer.splitInstruction("1.2.3.<tag>4.5</tag>.6.7")).containsExactly("1", "2", "3", "<tag>4.5</tag>", "6", "7");
    }

    @Test
    void testSplitInstructionGivenStringWithDotsThenReturnsSubstrings() {
        assertThat(recipeInstructionAnalyzer.splitInstruction("1.2.3")).containsExactly("1", "2", "3");
    }

    @Test
    void testSplitInstructionGivenStringWithSingleDotThenReturnsEmpty() {
        assertThat(recipeInstructionAnalyzer.splitInstruction(".")).isEmpty();
    }

    @Test
    void testSplitInstructionGivenStringWithSingleWordAndDotsThenReturnsSingleWord() {
        assertThat(recipeInstructionAnalyzer.splitInstruction("w.")).containsExactly("W");
    }
}
