package com.recipeton.shared.analysis.cooki.es;

/*-
 * #%L
 * recipeton-shared
 * %%
 * Copyright (C) 2021 tincomisc
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.recipeton.shared.analysis.configuration.RecipeAnalyzerCookiEsConfiguration;
import com.recipeton.shared.analysis.domain.RecipeAnalysisTag;
import com.recipeton.shared.analysis.domain.RecipeIngredientTerm;
import com.recipeton.shared.analysis.service.RecipeInstructionAnalyzer;
import com.recipeton.shared.analysis.test.support.RecipetonAnalyzerTestAssertTrait;
import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.Test;

import static com.recipeton.shared.analysis.domain.RecipeAnalysisTag.PLACE;
import static com.recipeton.shared.analysis.domain.RecipeInstructionMatch.SCORE_MAX;
import static java.util.List.of;
import static org.assertj.core.api.Assertions.assertThat;

@Slf4j
public class RecipeInstructionAnalyzerEsIngredientTest implements RecipetonAnalyzerTestAssertTrait {

    private final RecipeAnalyzerCookiEsConfiguration configuration = new RecipeAnalyzerCookiEsConfiguration();
    private final RecipeInstructionAnalyzer analyzer = createRecipeInstructionAnalyzer(configuration);

    @Test
    void testAnalyzeGivenInstructionWithIngredientAndIngredientHasLessWordsThenIngredientIsDetected() {

        assertThat(analyzer.analyze(of(
                "Ponga la lata de confit de pato en una olla con agua y caliente al baño maría durante 10-15 minutos " +
                        "para que la grasa que recubre los muslos de pato se funda",
                "Ponga la mermelada en el vaso"), createRecipeIngredientAnalyses(configuration, "mermelada de higos")))
                .element(2)
                .satisfies(ria -> assertThat(ria.getRecipeIngredientTerms())
                        .allSatisfy(t -> {
                            assertThat(t.getIngredient().getIngredientNotation()).isEqualTo("mermelada de higos");
                        }));
    }

    @Test
    void testAnalyzeGivenInstructionWithIngredientAndIngredientHasManyLessWordsThenIngredientIsDetected() {

        assertThat(analyzer.analyze(of(
                "platano, patata, coche, sal, un platillo volante y otras multiples cosas"), createRecipeIngredientAnalyses(configuration, "sal de roca ignea del monte fuji")))
                .singleElement()
                .satisfies(ria ->
                        assertThat(ria.getTerms()).singleElement().isInstanceOfSatisfying(RecipeIngredientTerm.class, t -> {
                            assertThat(t.getIngredient().getIngredientNotation()).isEqualTo("sal de roca ignea del monte fuji");
                        }));
    }

    @Test
    void testAnalyzeGivenInstructionWithIngredientAndIngredientInSingularIrregularThenIngredientIsDetected() {

        assertThat(analyzer.analyze(of("ponga una nuez en cada plato"), createRecipeIngredientAnalyses(configuration, "nueces")))
                .singleElement()
                .satisfies(ria -> {
                    assertThat(ria.getTags()).containsExactlyInAnyOrder(PLACE);
                    assertThat(ria.getRecipeIngredientTerms()).singleElement().satisfies(
                            t -> assertThat(t.getIngredient().getIngredientNotation()).isEqualTo("nueces")
                    );
                });
    }

    @Test
    void testAnalyzeGivenInstructionWithIngredientAndIngredientMostSignificativeWorkIsNotFirstThenIngredientIsDetected() {

        assertThat(analyzer.analyze(of("ponga los tomates, la cayena, el azucar"), createRecipeIngredientAnalyses(configuration, "pimienta de cayena")))
                .singleElement()
                .satisfies(ria -> {
                    assertThat(ria.getTags()).containsExactlyInAnyOrder(PLACE);
                    assertThat(ria.getRecipeIngredientTerms()).singleElement().satisfies(
                            t -> assertThat(t.getIngredient().getIngredientNotation()).isEqualTo("pimienta de cayena")
                    );
                });
    }

    @Test
    void testAnalyzeGivenInstructionWithTrademarkRegisteredIngredientThenTrademarkRegisteredIngredientIsFound() {

        assertThat(analyzer.analyze(of("Mire los Chupa Chups®"), createRecipeIngredientAnalyses(configuration, "Chupa Chups®"))).singleElement()
                .satisfies(ria -> assertThat(extractRecipeIngredientTermNotations(ria)).containsOnly("Chupa Chups®"));
    }

    @Test
    void testAnalyzeGivenInstructionWithoutIngredientAndIngredientHasManyWordsThenIngredientIsNotDetected() {

        assertThat(analyzer.analyze(of(
                "platano patata coche un platillo volante y otras multiples cosas"), createRecipeIngredientAnalyses(configuration, "sal de roca ignea del monte fuji")))
                .singleElement()
                .satisfies(
                        ria -> assertThat(ria.getTerms()).isEmpty());
    }

    @Test
    void testAnalyzeGivenMultiplePlaceInstructionWithNoIndentificableIngredientAndNoIngredientsMatchedThenInstructionIngredientTermsAreNotAdded() {

        assertThat(analyzer.analyze(of("Ponga los ingredientes de la masa en el vaso", "Ponga la cebolla en el vaso", "otra instruccion"), createRecipeIngredientAnalyses(configuration, "patata", "tomate")))
                .allSatisfy(
                        ria -> assertThat(extractRecipeIngredientTermNotations(ria)).isEmpty()
                );
    }

    @Test
    void testAnalyzeGivenPlaceAllInstructionThenAddsAllIngredientsToThatInstruction() {

        assertThat(analyzer.analyze(of("Ponga todos los ingredientes de la masa en el vaso"), createRecipeIngredientAnalyses(configuration, "patata", "tomate")))
                .singleElement()
                .satisfies(
                        ria -> assertThat(ria.getRecipeIngredientTerms())
                                .hasSize(2)
                                .anySatisfy(t -> {
                                    assertThat(t.isConclusive()).isTrue();
                                    assertThat(t.getTarget()).isEqualTo(RecipeIngredientTerm.Target.PROCESS);
                                    assertThat(t.getIngredient().getIngredientNotation()).isEqualTo("patata");
                                    assertThat(t.getMatch().getScore()).isEqualTo(SCORE_MAX);
                                })
                                .anySatisfy(t -> {
                                    assertThat(t.isConclusive()).isTrue();
                                    assertThat(t.getTarget()).isEqualTo(RecipeIngredientTerm.Target.PROCESS);
                                    assertThat(t.getIngredient().getIngredientNotation()).isEqualTo("tomate");
                                    assertThat(t.getMatch().getScore()).isEqualTo(SCORE_MAX);
                                })
                );
    }


    @Test
    void testAnalyzeGivenPlaceInMainCompartmentAddInMainCompartmentPlaceElsewhereThenOnlyInMainCompartmentInstructionsHaveInMainCompartmentTagAndOnlyInMainCompartmentAreConclusive() {

        assertThat(analyzer.analyze(of("Ponga en el vaso las hierbas especiales",
                "Pique <nobr>5 seg/vel 7</nobr>",
                "Con la espátula, baje los ingredientes hacia el fondo del vaso",
                "Añada el aceite y la pimienta",
                "Pique <nobr>5 seg/vel 7</nobr>",
                "Vierta el aceite en un bol o jarra pequeña y reserve"), createRecipeIngredientAnalyses(configuration, "hierbas especiales", "aceite de oliva", "pimienta")))
                .hasSize(6)
                .satisfies(l -> {
                    assertThat(l).element(0).satisfies(
                            ria -> {
                                assertThat(ria.getTags()).contains(PLACE, RecipeAnalysisTag.REF_IN_MAIN_COMPARTMENT);
                                assertThat(extractRecipeIngredientTermNotations(ria)).contains("hierbas especiales");
                                assertThat(extractRecipeIngredientTermsByIngredientNotation("hierbas especiales", ria)).anySatisfy(t -> assertThat(t.isConclusive()).isTrue());
                            }
                    );
                    assertThat(l).element(3).satisfies(
                            ria -> {
                                assertThat(ria.getTags()).contains(RecipeAnalysisTag.ADD, RecipeAnalysisTag.REF_IN_MAIN_COMPARTMENT);
                                assertThat(extractRecipeIngredientTermNotations(ria)).contains("aceite de oliva", "pimienta");
                                assertThat(extractRecipeIngredientTermsByIngredientNotation("aceite de oliva", ria)).anySatisfy(t -> assertThat(t.isConclusive()).isTrue());
                                assertThat(extractRecipeIngredientTermsByIngredientNotation("pimienta", ria)).anySatisfy(t -> assertThat(t.isConclusive()).isTrue());
                            }
                    );
                    assertThat(l).element(5).satisfies(
                            ria -> {
                                assertThat(ria.getTags()).doesNotContain(RecipeAnalysisTag.REF_IN_MAIN_COMPARTMENT);
                                assertThat(extractRecipeIngredientTermNotations(ria)).contains("aceite de oliva");
                                assertThat(ria.getRecipeIngredientTerms()).allSatisfy(t -> assertThat(t.isConclusive()).isFalse());
                            }
                    );
                });
    }

    @Test
    void testAnalyzeGivenSinglePlaceInstructionWithNoIndentificableIngredientAndNoIngredientsMatchedThenInstructionGetsAllIngredientTerms() {

        assertThat(analyzer.analyze(of("Ponga los ingredientes de la masa en el vaso", "otra instruccion"), createRecipeIngredientAnalyses(configuration, "patata", "tomate")))
                .hasSize(2)
                .satisfies(l -> {
                    assertThat(l).element(0).satisfies(ria -> assertThat(extractRecipeIngredientTermNotations(ria)).containsExactlyInAnyOrder("patata", "tomate"));
                    assertThat(l).element(1).satisfies(ria -> assertThat(ria.getTerms()).isEmpty());
                });
    }


}
