package com.recipeton.shared.analysis.cooki.de;

/*-
 * #%L
 * recipeton-shared
 * %%
 * Copyright (C) 2021 tincomisc
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.recipeton.shared.analysis.configuration.RecipeAnalyzerCookiDeConfiguration;
import com.recipeton.shared.analysis.service.preprocess.RecipeInstructionSplitter;
import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.Test;

import static org.assertj.core.api.Assertions.assertThat;

@Slf4j
public class RecipeInstructionSplitterCookiDeTest {

    private final RecipeInstructionSplitter recipeInstructionAnalyzer = new RecipeInstructionSplitter(new RecipeAnalyzerCookiDeConfiguration());

    @Test
    void testSplitInstructionGivenContainsSplittingSentenceConnectorThenSplits() {
        assertThat(recipeInstructionAnalyzer.splitInstruction("Pfeffer zugeben und <nobr>99 Sek./Stufe 99</nobr> zerkleinern und zu den Linsen geben"))
                .containsExactly("Pfeffer zugeben", "<nobr>99 Sek./Stufe 99</nobr> zerkleinern", "Zu den Linsen geben");
    }

    @Test
    void testSplitInstructionGivenContainsSplittingSentenceConnectorThenSplitsForward() {
        assertThat(recipeInstructionAnalyzer.splitInstruction("Schiebe alles mit dem Spatel nach unten und dünste 5 Min."))
                .containsExactly("Schiebe alles mit dem Spatel nach unten", "Dünste 5 Min");
    }
}
