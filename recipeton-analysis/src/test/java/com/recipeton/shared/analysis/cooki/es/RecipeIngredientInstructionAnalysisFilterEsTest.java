package com.recipeton.shared.analysis.cooki.es;

/*-
 * #%L
 * recipeton-shared
 * %%
 * Copyright (C) 2021 tincomisc
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.recipeton.shared.analysis.configuration.RecipeAnalyzerConfiguration;
import com.recipeton.shared.analysis.configuration.RecipeAnalyzerCookiEsConfiguration;
import com.recipeton.shared.analysis.domain.RecipeIngredientTerm;
import com.recipeton.shared.analysis.domain.RecipeInstructionAnalysis;
import com.recipeton.shared.analysis.service.filter.RecipeIngredientRecipeInstructionAnalysisFilter;
import com.recipeton.shared.analysis.test.support.RecipetonAnalyzerTestFixtureTrait;
import org.junit.jupiter.api.Test;

import java.util.stream.Stream;

import static org.assertj.core.api.Assertions.assertThat;

class RecipeIngredientInstructionAnalysisFilterEsTest implements RecipetonAnalyzerTestFixtureTrait {

    private final RecipeAnalyzerConfiguration configuration = new RecipeAnalyzerCookiEsConfiguration();
    private final RecipeIngredientRecipeInstructionAnalysisFilter recipeIngredientRecipeInstructionAnalysisFilter = new RecipeIngredientRecipeInstructionAnalysisFilter(createRecipeIngredientAnalysis("ingredient", configuration), configuration);

    private void apply(RecipeInstructionAnalysis... recipeInstructionAnalyses) {
        Stream.of(recipeInstructionAnalyses).forEach(recipeIngredientRecipeInstructionAnalysisFilter::apply);
    }

    @Test
    public void testAcceptGivenInstructionThatContainIngredientsThenTermsContainsIngredientTerm() {
        RecipeInstructionAnalysis ria = createRecipeInstructionAnalysis("some ingredient something", configuration);
        apply(ria);
        assertThat(ria.getTerms())
                .singleElement()
                .isInstanceOfSatisfying(RecipeIngredientTerm.class, t -> {
                    assertThat(t.getIngredient().getIngredientNotation()).isEqualTo("ingredient");
                });
    }

    @Test
    public void testAcceptGivenInstructionThatDoesNotContainIngredientThenFilterInconclusive() {
        RecipeInstructionAnalysis ria = createRecipeInstructionAnalysis("some text", configuration);
        apply(ria);
        assertThat(ria.getTerms()).isEmpty();
    }

    @Test
    public void testAcceptGivenInstructionThatDoesNotContainIngredientThenTermsEmpty() {
        RecipeInstructionAnalysis ria = createRecipeInstructionAnalysis("some text", configuration);
        apply(ria);
        assertThat(ria.getTerms()).isEmpty();
    }

    @Test
    public void testAcceptGivenInstructionsThatContainIngredientsThenInstructionWithMatchesContainsIngredientTermAndBestMatchHasHighestScore() {
        RecipeInstructionAnalysis ria1 = createRecipeInstructionAnalysis("some iedient something", configuration);
        RecipeInstructionAnalysis ria2 = createRecipeInstructionAnalysis("some ingredien something", configuration);
        RecipeInstructionAnalysis ria3 = createRecipeInstructionAnalysis("some ingredi something", configuration);

        apply(ria1, ria2, ria3);

        assertThat(ria1.getRecipeIngredientTerms()).singleElement()
                .satisfies(t -> assertThat(t.getIngredient().getIngredientNotation()).isEqualTo("ingredient"));
        assertThat(ria2.getRecipeIngredientTerms()).singleElement()
                .satisfies(t -> assertThat(t.getIngredient().getIngredientNotation()).isEqualTo("ingredient"));
        assertThat(ria3.getRecipeIngredientTerms()).singleElement()
                .satisfies(t -> assertThat(t.getIngredient().getIngredientNotation()).isEqualTo("ingredient"));

        assertThat(ria2.getRecipeIngredientTerms().get(0).getMatch().getScore())
                .isGreaterThan(ria1.getRecipeIngredientTerms().get(0).getMatch().getScore())
                .isGreaterThan(ria3.getRecipeIngredientTerms().get(0).getMatch().getScore());

    }


}
