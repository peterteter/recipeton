package com.recipeton.shared.analysis.cooki.es;

/*-
 * #%L
 * recipeton-shared
 * %%
 * Copyright (C) 2021 tincomisc
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.recipeton.shared.analysis.configuration.RecipeAnalyzerConfiguration;
import com.recipeton.shared.analysis.configuration.RecipeAnalyzerCookiEsConfiguration;
import com.recipeton.shared.analysis.domain.RecipeCommandProgram;
import com.recipeton.shared.analysis.domain.RecipeCommandRotation;
import com.recipeton.shared.analysis.domain.RecipeCommandTerm;
import com.recipeton.shared.analysis.domain.RecipeInstructionAnalysis;
import com.recipeton.shared.analysis.service.RecipeCommandAnalyzer;
import com.recipeton.shared.analysis.service.filter.RecipeCommandRecipeInstructionAnalysisFilter;
import com.recipeton.shared.analysis.test.support.RecipetonAnalyzerTestAssertTrait;
import org.junit.jupiter.api.Test;

import java.math.BigDecimal;
import java.time.Duration;

import static com.recipeton.shared.analysis.configuration.CookiConstants.COMMAND_SPEED_SLOW_VALUE;
import static com.recipeton.shared.analysis.configuration.CookiConstants.STEAMER_TEMPERATURE;
import static org.assertj.core.api.Assertions.assertThat;

class RecipeCommandRecipeInstructionAnalysisFilterEsTest implements RecipetonAnalyzerTestAssertTrait {

    private final RecipeAnalyzerConfiguration configuration = new RecipeAnalyzerCookiEsConfiguration();
    private final RecipeCommandAnalyzer recipeCommandAnalyzer = new RecipeCommandAnalyzer(configuration);

    private final RecipeCommandRecipeInstructionAnalysisFilter recipeCommandAnalyzerFilter = new RecipeCommandRecipeInstructionAnalysisFilter(recipeCommandAnalyzer, configuration.getCommandDelimiters().get(0));

    private RecipeInstructionAnalysis apply(String s) {
        return recipeCommandAnalyzerFilter.apply(createRecipeInstructionAnalysisWrapped(s, configuration));
    }

    private RecipeInstructionAnalysis applyFilterWrapping(String command) {
        return apply(command);
    }

    @Test
    public void testAcceptGivenInstructionThenTermsContainsSource() {
        String expected = "2 min/120°C/vel \uE002";
        assertThat(applyFilterWrapping(expected).getTerms()).singleElement()
                .isInstanceOfSatisfying(RecipeCommandTerm.class, c -> assertThat(c.getText()).isEqualTo(expected));
    }

    @Test
    public void testAcceptGivenInstructionWithCombinationOfIngredientAndCommandThenCommandPositionIsGreaterThanIngredientPosition() {
        RecipeInstructionAnalysis recipeInstructionAnalysis = createRecipeInstructionAnalysis("Añada los camarones, y pique <nobr>5 seg/vel 5.5</nobr>, la mezcla debe tener algunos trozos visibles de camarón", configuration);
        recipeCommandAnalyzerFilter.apply(recipeInstructionAnalysis);

        assertThat(recipeInstructionAnalysis.getTerms())
                .singleElement().isInstanceOfSatisfying(RecipeCommandTerm.class, c -> {
                    assertThat(c.getMatch().getOffset()).isEqualTo(35);
                    assertThat(c.getSpeed()).isEqualTo(new BigDecimal(5.5));
                    assertThat(c.getDuration()).isEqualTo(Duration.ofSeconds(5));
                }
        );
    }

    @Test
    public void testAcceptGivenInstructionWithDoughTermsContainsDough() {
        assertThat(applyFilterWrapping("Amasar \uE001/1 min").getTerms()).singleElement()
                .isInstanceOfSatisfying(RecipeCommandTerm.class, c -> {
                            assertThat(c.getProgram()).isEqualTo(RecipeCommandProgram.DOUGH);
                            assertThat(c.getDuration()).isEqualTo(Duration.ofMinutes(1));
                        }
                );
    }

    @Test
    public void testAcceptGivenInstructionWithDurationHalfSecondsThenTermsContainsDuration() {
        assertThatRecipeCommandTerm(apply("0.5 seg"), c -> assertThat(c.getDuration()).isEqualTo(Duration.ofMillis(500)));
    }

    @Test
    public void testAcceptGivenInstructionWithDurationHoursThenTermsContainsDuration() {
        assertThatRecipeCommandTerm(apply("5 h"), c -> assertThat(c.getDuration()).isEqualTo(Duration.ofHours(5)));
    }

    @Test
    public void testAcceptGivenInstructionWithDurationMinutesThenTermsContainsDuration() {
        assertThatRecipeCommandTerm(apply("5 min"), c -> assertThat(c.getDuration()).isEqualTo(Duration.ofMinutes(5)));
    }

    @Test
    public void testAcceptGivenInstructionWithDurationSecondsThenTermsContainsDuration() {
        assertThatRecipeCommandTerm(apply("5 seg"), c -> assertThat(c.getDuration()).isEqualTo(Duration.ofSeconds(5)));
    }

    @Test
    public void testAcceptGivenInstructionWithNoRecognizedContentThenTermsEmpty() {
        assertThat(applyFilterWrapping("otherjunk").getTerms()).isEmpty();
    }

    @Test
    public void testAcceptGivenInstructionWithReverseRotationCodeThenTermsContainsReverseRotation() {
        assertThatRecipeCommandTerm(apply("5 seg/\uE003/vel 4"), c -> assertThat(c.getRotation()).isEqualTo(RecipeCommandRotation.REVERSE));
    }

    @Test
    public void testAcceptGivenInstructionWithReverseRotationThenTermsContainsReverseRotation() {
        assertThatRecipeCommandTerm(apply("5 seg/reverse/vel 4"), c -> assertThat(c.getRotation()).isEqualTo(RecipeCommandRotation.REVERSE));
    }

    @Test
    public void testAcceptGivenInstructionWithSpeedCucharaThenTermsContainsSpeedSlow() {
        assertThatRecipeCommandTerm(apply("vel cuchara"), c -> assertThat(c.getSpeed()).isEqualTo(COMMAND_SPEED_SLOW_VALUE));
    }

    @Test
    public void testAcceptGivenInstructionWithSpeedNumericFractionThenTermsContainsSpeed() {
        assertThatRecipeCommandTerm(apply("vel 0.5"), c -> assertThat(c.getSpeed()).isEqualTo(new BigDecimal("0.5")));
    }

    @Test
    public void testAcceptGivenInstructionWithSpeedNumericThenTermsContainsSpeed() {
        assertThatRecipeCommandTerm(apply("vel 5"), c -> assertThat(c.getSpeed()).isEqualTo(new BigDecimal(5)));
    }

    @Test
    public void testAcceptGivenInstructionWithSpeedSlowCodeThenTermsContainsSpeedSlow() {
        assertThatRecipeCommandTerm(apply("vel \uE002"), c -> assertThat(c.getSpeed()).isEqualTo(COMMAND_SPEED_SLOW_VALUE));
    }

    @Test
    public void testAcceptGivenInstructionWithSpeedSlowThenTermsContainsSpeedSlow() {
        assertThatRecipeCommandTerm(apply("vel slow"), c -> assertThat(c.getSpeed()).isEqualTo(COMMAND_SPEED_SLOW_VALUE));
    }

    @Test
    public void testAcceptGivenInstructionWithTemperatureNumericThenTermsContainsTemperature() {
        assertThatRecipeCommandTerm(apply("50°C"), c -> assertThat(c.getTemperature()).isEqualTo((long) 50));
    }

    @Test
    public void testAcceptGivenInstructionWithTemperatureSpecialThenTermsContainsTemperature() {
        assertThatRecipeCommandTerm(apply("Varoma"), c -> assertThat(c.getTemperature()).isEqualTo(STEAMER_TEMPERATURE));
        assertThatRecipeCommandTerm(apply("vaporera"), c -> assertThat(c.getTemperature()).isEqualTo(STEAMER_TEMPERATURE));
        assertThatRecipeCommandTerm(apply("vapor"), c -> assertThat(c.getTemperature()).isEqualTo(STEAMER_TEMPERATURE));
    }

    @Test
    public void testAcceptGivenInstructionWithTimeInMinutesAndTemperatureAndSpeedThenTermAddedToInstructionAndContainsInformation() {
        assertThat(applyFilterWrapping("2 min/120°C/vel \uE002").getTerms()).singleElement()
                .isInstanceOfSatisfying(RecipeCommandTerm.class, c -> {
                    assertThat(c.getDuration()).isEqualTo(Duration.ofMinutes(2));
                    assertThat(c.getTemperature()).isEqualTo(120L);
                    assertThat(c.getSpeed()).isEqualTo(COMMAND_SPEED_SLOW_VALUE);
                });
    }

    @Test
    public void testAcceptGivenInstructionWithTurboAndRangeThenTermsContainsTurbo() {
        assertThat(applyFilterWrapping("Turbo/2 seg/1-2 veces").getTerms()).singleElement()
                .isInstanceOfSatisfying(RecipeCommandTerm.class, c -> {
                            assertThat(c.getProgram()).isEqualTo(RecipeCommandProgram.TURBO);
                            assertThat(c.getDuration()).isEqualTo(Duration.ofSeconds(2));
                            assertThat(c.getRepetitionFrom()).isEqualTo(1);
                            assertThat(c.getRepetitionTo()).isEqualTo(2);
                        }
                );
    }

    @Test
    public void testAcceptGivenInstructionWithTurboAndSingleThenTermsContainsTurbo() {
        assertThat(applyFilterWrapping("Turbo/3 seg/2 veces").getTerms()).singleElement()
                .isInstanceOfSatisfying(RecipeCommandTerm.class, c -> {
                            assertThat(c.getProgram()).isEqualTo(RecipeCommandProgram.TURBO);
                            assertThat(c.getDuration()).isEqualTo(Duration.ofSeconds(3));
                            assertThat(c.getRepetitionFrom()).isEqualTo(2);
                            assertThat(c.getRepetitionTo()).isNull();
                        }
                );

        assertThat(applyFilterWrapping("Turbo/2 seg/1 vez").getTerms()).singleElement()
                .isInstanceOfSatisfying(RecipeCommandTerm.class, c -> {
                            assertThat(c.getProgram()).isEqualTo(RecipeCommandProgram.TURBO);
                            assertThat(c.getDuration()).isEqualTo(Duration.ofSeconds(2));
                            assertThat(c.getRepetitionFrom()).isEqualTo(1);
                            assertThat(c.getRepetitionTo()).isNull();
                        }
                );
    }


}
