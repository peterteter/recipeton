package com.recipeton.shared.analysis.service;

/*-
 * #%L
 * recipeton-shared
 * %%
 * Copyright (C) 2021 tincomisc
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.recipeton.shared.analysis.domain.RecipeMeasurement;
import com.recipeton.shared.analysis.domain.RecipeUtensilAnalysis;
import com.recipeton.shared.analysis.test.support.RecipetonAnalyzerTestFixtureTrait;
import org.junit.jupiter.api.Test;

import java.math.BigDecimal;

import static com.recipeton.shared.analysis.domain.RecipeUnitDefinition.UNIT_GRAM;
import static org.assertj.core.api.Assertions.assertThat;

public class RecipeStandardAnalyzerTest implements RecipetonAnalyzerTestFixtureTrait {

    private final RecipeStandardAnalyzer analyzer = new RecipeStandardAnalyzer(createRecipeAnalyzerConfiguration());

    @Test
    void testAnalyzeRecipeMeasurementGivenMagnitudeRetunsRecipeMeasurementWithMagnitudeAndDefaultUnit() {
        RecipeMeasurement measurement = analyzer.analyzeRecipeMeasurement("1");
        assertThat(measurement.getOriginal()).isEqualTo("1");
        assertThat(measurement.getUnit()).isEqualTo("PIECE_DEFINE_ME_IN_CONFIG!");
        assertThat(measurement.getMagnitudeValue()).isEqualTo(new BigDecimal("1"));
        assertThat(measurement.getMagnitude()).isEqualTo("1");
    }

    @Test
    void testAnalyzeRecipeMeasurementGivenMagntudeAndUnitThenRetunsRecipeMeasurementWithMagnitudeAndUnit() {
        RecipeMeasurement measurement = analyzer.analyzeRecipeMeasurement("15.5 g");
        assertThat(measurement.getOriginal()).isEqualTo("15.5 g");
        assertThat(measurement.getUnit()).isEqualTo(UNIT_GRAM);
        assertThat(measurement.getMagnitudeValue()).isEqualTo(new BigDecimal("15.5"));
        assertThat(measurement.getMagnitude()).isEqualTo("15.5");
    }

    @Test
    void testAnalyzeUtensilGivenUtensilSetsUid() {
        assertThat(analyzer.analyzeUtensil("frying pan (20 cm)")).isEqualTo(new RecipeUtensilAnalysis()
                .setUid("panfryingcm20")
                .setText("frying pan (20 cm)"));
    }

    @Test
    void testFormatCategoryUuidGivenNameWithCapitalThenReturnsNameInLowercase() {
        assertThat(analyzer.toUid("BLA")).isEqualTo("bla");
    }

    @Test
    void testFormatCategoryUuidGivenNameWithParenthesesThenReturnsNameWithoutParentheses() {
        assertThat(analyzer.toUid("some(thing)")).isEqualTo("something");
    }

    @Test
    void testFormatCategoryUuidGivenNameWithSpacesThenReturnsNameWithoutSpaces() {
        assertThat(analyzer.toUid("some thing")).isEqualTo("thingsome");
    }

    @Test
    void testFormatCategoryUuidGivenNameWithSpecialCharactersThenReturnsNameWithoutSpecialCharacters() {
        assertThat(analyzer.toUid("navidadyañonuevo")).isEqualTo("navidadyanonuevo");
        assertThat(analyzer.toUid("fácil")).isEqualTo("facil");
    }

    @Test
    void testFormatCategoryUuidGivenNameWithSymbolsThenReturnsNameWithoutParentheses() {
        assertThat(analyzer.toUid("some+thing")).isEqualTo("something");
        assertThat(analyzer.toUid("some_thing")).isEqualTo("something");
        assertThat(analyzer.toUid("some-thing")).isEqualTo("something");
    }

}
