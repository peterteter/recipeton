package com.recipeton.shared.analysis.configuration;

/*-
 * #%L
 * recipeton-analysis
 * %%
 * Copyright (C) 2021 tincomisc
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.recipeton.shared.analysis.test.support.RecipetonAnalyzerTestFixtureTrait;
import org.junit.jupiter.api.Test;

import static com.recipeton.shared.analysis.configuration.RecipeAnalyzerConfiguration.TOKEN_LENGTH_MIN;
import static org.assertj.core.api.Assertions.assertThat;

class RecipeAnalyzerConfigurationTest implements RecipetonAnalyzerTestFixtureTrait {

    RecipeAnalyzerConfiguration configuration = createRecipeAnalyzerConfiguration();

    @Test
    void testGetTokenizationGivenMultipleWordAndParenthesesCharactersThanMinThenReturnsWordsLongerThanMin() {
        assertThat(configuration.getTokenization("AAA (C) BB", 2))
                .satisfies(t -> {
                    assertThat(t.getSource()).isEqualTo("AAA (C) BB");
                    assertThat(t.getPositions()).containsExactly(createTokenizationPosition(0, 3), createTokenizationPosition(8, 2));
                    assertThat(t.getTokens()).containsExactly("aaa", "bb");
                });
    }

    @Test
    void testGetTokenizationGivenMultipleWordAndShorterThatnMinThenReturnsWordsLongerThanMin() {
        assertThat(configuration.getTokenization("AAA C BB", 2))
                .satisfies(t -> {
                    assertThat(t.getSource()).isEqualTo("AAA C BB");
                    assertThat(t.getPositions()).containsExactly(createTokenizationPosition(0, 3), createTokenizationPosition(6, 2));
                    assertThat(t.getTokens()).containsExactly("aaa", "bb");

                });
    }

    @Test
    void testGetTokenizationGivenMultipleWordAndSymbolCharactersThanMinThenReturnsWordsLongerThanMin() {
        assertThat(configuration.getTokenization("AAA, BB", 2))
                .satisfies(t -> {
                    assertThat(t.getSource()).isEqualTo("AAA, BB");
                    assertThat(t.getPositions()).containsExactly(createTokenizationPosition(0, 4), createTokenizationPosition(5, 2));
                    assertThat(t.getTokens()).containsExactly("aaa", "bb");

                });

    }

    @Test
    void testGetTokenizationGivenMultipleWordThenReturnsSingleWordWithTokenAndPositionEqualsZero() {
        assertThat(configuration.getTokenization("AAA BBB", TOKEN_LENGTH_MIN))
                .satisfies(t -> {
                    assertThat(t.getSource()).isEqualTo("AAA BBB");
                    assertThat(t.getPositions()).containsExactly(createTokenizationPosition(0, 3), createTokenizationPosition(4, 3));
                    assertThat(t.getTokens()).containsExactly("aaa", "bbb");

                });
    }

    @Test
    void testGetTokenizationGivenSingleWordThenReturnsSingleWordWithTokenAndPositionEqualsZero() {
        assertThat(configuration.getTokenization("AbC", TOKEN_LENGTH_MIN))
                .satisfies(t -> {
                    assertThat(t.getSource()).isEqualTo("AbC");
                    assertThat(t.getPositions()).containsExactly(createTokenizationPosition(0, 3));
                    assertThat(t.getTokens()).containsExactly("abc");

                });
    }

    @Test
    void testGetTokenizationGivenWordWithSymbolCharactersThanMinThenReturnsWordAndPosition() {
        assertThat(configuration.getTokenization("a+-_()b", 1))
                .satisfies(t -> {
                    assertThat(t.getSource()).isEqualTo("a+-_()b");
                    assertThat(t.getPositions()).containsExactly(createTokenizationPosition(0, 7));
                    assertThat(t.getTokens()).containsExactly("ab");

                });

    }

    @Test
    void testGetTokensGivenNonLetterOrDigitPatternAndNumbersThenPreservesNumbers() {
        assertThat(configuration.getTokens("0123456789", TOKEN_LENGTH_MIN)).containsExactly("0123456789");
    }

    @Test
    void testGetTokensGivenNonLetterOrDigitPatternAndSpacesThenReturnsWithoutSpaces() {
        assertThat(configuration.getTokens("a b", 1)).containsExactly("a", "b");
    }

    @Test
    void testGetTokensGivenNonLetterOrDigitPatternAndSymbolThenRemovesSymbol() {
        assertThat(configuration.getTokens("a+-_()b", 1)).containsExactly("ab");
    }

    @Test
    void testGetTokensGivenNonLetterOrDigitPatternAndUppercaseThenReturnsLowercase() {
        assertThat(configuration.getTokens("AbC", TOKEN_LENGTH_MIN)).containsExactly("abc");
    }

}
