package com.recipeton.shared.analysis.cooki.en;

/*-
 * #%L
 * recipeton-shared
 * %%
 * Copyright (C) 2021 tincomisc
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.recipeton.shared.analysis.configuration.RecipeAnalyzerConfiguration;
import com.recipeton.shared.analysis.configuration.RecipeAnalyzerCookiEnConfiguration;
import com.recipeton.shared.analysis.domain.RecipeInstructionAnalysis;
import com.recipeton.shared.analysis.service.RecipeInstructionAnalyzer;
import com.recipeton.shared.analysis.test.support.RecipetonAnalyzerTestAssertTrait;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.junit.jupiter.api.Test;

import static java.util.List.of;
import static org.assertj.core.api.Assertions.assertThat;

@Slf4j
public class RecipeInstructionAnalyzerEnTest implements RecipetonAnalyzerTestAssertTrait {

    public static final String INSTRUCTION_1 = "Add water and salt";
    public static final String INSTRUCTION_2 = "blend <nobr>5 min/120°C/vel 1</nobr>";

    private final RecipeAnalyzerConfiguration configuration = new RecipeAnalyzerCookiEnConfiguration();
    private final RecipeInstructionAnalyzer analyzer = createRecipeInstructionAnalyzer(configuration);

    @Test
    void testAnalyzeGiveInstructionWithAddAndToMainCompartmentAndCommandActionThenInstructionsSplitted() {
        assertThat(analyzer.analyze(of(INSTRUCTION_1 + " and " + INSTRUCTION_2 + "."), createRecipeIngredientAnalyses(configuration, "")))
                .map(RecipeInstructionAnalysis::getText)
                .containsExactly(INSTRUCTION_1, StringUtils.capitalize(INSTRUCTION_2));

        assertThat(analyzer.analyze(of(INSTRUCTION_1 + ", " + INSTRUCTION_2 + "."), createRecipeIngredientAnalyses(configuration, "")))
                .map(RecipeInstructionAnalysis::getText)
                .containsExactly(INSTRUCTION_1, StringUtils.capitalize(INSTRUCTION_2));

        assertThat(analyzer.analyze(of(INSTRUCTION_1 + " and then " + INSTRUCTION_2 + "."), createRecipeIngredientAnalyses(configuration, "")))
                .map(RecipeInstructionAnalysis::getText)
                .containsExactly(INSTRUCTION_1, StringUtils.capitalize(INSTRUCTION_2));

        assertThat(analyzer.analyze(of(INSTRUCTION_1 + ", then " + INSTRUCTION_2 + "."), createRecipeIngredientAnalyses(configuration, "")))
                .map(RecipeInstructionAnalysis::getText)
                .containsExactly(INSTRUCTION_1, StringUtils.capitalize(INSTRUCTION_2));
    }

    @Test
    void testAnalyzeGiveInstructionWithAndMixCommandActionThenInstructionsSplitted() {
        assertThat(analyzer.analyze(of("Add beans and mix well"), createRecipeIngredientAnalyses(configuration, "")))
                .map(RecipeInstructionAnalysis::getText)
                .containsExactly("Add beans", "Mix well");
    }

    @Test
    void testAnalyzeGiveInstructionWithCommaAndCommandActionThenInstructionsSplitted() {
        assertThat(analyzer.analyze(of("Add beans, except half, and mix well"), createRecipeIngredientAnalyses(configuration, "")))
                .map(RecipeInstructionAnalysis::getText)
                .containsExactly("Add beans, except half", "Mix well");
    }

}
