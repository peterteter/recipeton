package com.recipeton.shared.analysis.cooki.en;

/*-
 * #%L
 * recipeton-shared
 * %%
 * Copyright (C) 2021 tincomisc
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.recipeton.shared.analysis.configuration.RecipeAnalyzerConfiguration;
import com.recipeton.shared.analysis.configuration.RecipeAnalyzerCookiEnConfiguration;
import com.recipeton.shared.analysis.service.RecipeIngredientAnalyzer;
import com.recipeton.shared.analysis.test.support.RecipetonAnalyzerTestFixtureTrait;
import org.junit.jupiter.api.Test;

import java.math.BigDecimal;

import static com.recipeton.shared.analysis.domain.RecipeUnitDefinition.UNIT_GRAM;
import static org.assertj.core.api.Assertions.assertThat;

public class RecipeIngredientStandardAnalyzerCookiEnTest implements RecipetonAnalyzerTestFixtureTrait {

    private final RecipeAnalyzerConfiguration configuration = new RecipeAnalyzerCookiEnConfiguration();
    private final RecipeIngredientAnalyzer analyzer = createRecipeIngredientAnalyzer(configuration);

    @Test
    void testToRecipeIngredientFormGivenIngredientWithFromAndMagnitudeAndNameThenReturnsRecipeIngredient() {
        String text = "3 ½ oz buckwheat grain, white or brown (see Tip)";
        assertThat(analyzer.analyze(text)).isEqualTo(
                createRecipeIngredientAnalysis("3½ oz buckwheat grain, white or brown (see Tip)", "buckwheat grain", configuration)
                        .setMagnitudeFromValue(new BigDecimal(3.5))
                        .setMagnitudeFrom("3½")
                        .setUnit("oz")
                        .setWeighted(true)
                        .setPreparation("white or brown (see Tip)")
                        .setOptional(false)
        );
    }

    @Test
    void testToRecipeIngredientFormGivenIngredientWithMagnitudeDescriptionAndNomenclatureThenReturnsRecipeIngredient() {
        String text = "6 large eggs";
        assertThat(analyzer.analyze(text)).isEqualTo(
                createRecipeIngredientAnalysis(text, "eggs", configuration)
                        .setMagnitudeFromValue(new BigDecimal(6))
                        .setMagnitudeFrom("6")
                        .setUnit("piece")
                        .setWeighted(false)
                        .setPreparation("large")
                        .setOptional(false)
        );
    }

    @Test
    void testToRecipeIngredientFormGivenIngredientWithMagnitudenAndNameThenReturnsRecipeIngredient() {
        String text = "240 g mayonnaise";
        assertThat(analyzer.analyze(text)).isEqualTo(
                createRecipeIngredientAnalysis(text, "mayonnaise", configuration)
                        .setMagnitudeFromValue(new BigDecimal(240))
                        .setMagnitudeFrom("240")
                        .setUnit(UNIT_GRAM)
                        .setWeighted(true)
                        .setOptional(false)
        );
    }

    @Test
    void testToRecipeIngredientFormGivenIngredientWithMagnitudenAndNameThenReturnsRecipeIngredient2() {
        String text2 = "120 g raw unsalted peanuts";
        assertThat(analyzer.analyze(text2)).isEqualTo(
                createRecipeIngredientAnalysis(text2, "peanuts", configuration)
                        .setMagnitudeFromValue(new BigDecimal(120))
                        .setMagnitudeFrom("120")
                        .setUnit(UNIT_GRAM)
                        .setWeighted(true)
                        .setPreparation("raw, unsalted")
                        .setOptional(false)
        );
    }

    @Test
    void testToRecipeIngredientFormGivenIngredientWithMagnitudenAndNameThenReturnsRecipeIngredient3() {
        String text3 = "10 g fresh parsley";
        assertThat(analyzer.analyze(text3)).isEqualTo(
                createRecipeIngredientAnalysis(text3, "parsley", configuration)
                        .setMagnitudeFromValue(new BigDecimal(10))
                        .setMagnitudeFrom("10")
                        .setUnit(UNIT_GRAM)
                        .setWeighted(true)
                        .setPreparation("fresh")
                        .setOptional(false)
        );
    }

    @Test
    void testToRecipeIngredientFormGivenIngredientWithMagnitudenAndNameThenReturnsRecipeIngredient4() {
        String text = "7 oz whole milk";
        assertThat(analyzer.analyze(text)).isEqualTo(
                createRecipeIngredientAnalysis(text, "milk", configuration)
                        .setMagnitudeFromValue(new BigDecimal(7))
                        .setMagnitudeFrom("7")
                        .setUnit("oz")
                        .setWeighted(true)
                        .setPreparation("whole")
                        .setOptional(false)
        );
    }

    @Test
    void testToRecipeIngredientFormGivenIngredientWithOfPreparationThenReturnsRecipeIngredient() {
        String text = "800 g milk of choice";
        assertThat(analyzer.analyze(text)).isEqualTo(
                createRecipeIngredientAnalysis(text, "milk", configuration)
                        .setMagnitudeFromValue(new BigDecimal(800))
                        .setMagnitudeFrom("800")
                        .setUnit(UNIT_GRAM)
                        .setWeighted(true)
                        .setPreparation("of choice")
                        .setOptional(false)
        );
    }

    @Test
    void testToRecipeIngredientFormGivenIngredientWithPreparationsBeforeThenReturnsRecipeIngredient() {
        String text = "400 g pouring (whipping) cream";
        assertThat(analyzer.analyze(text)).isEqualTo(
                createRecipeIngredientAnalysis(text, "cream", configuration)
                        .setMagnitudeFromValue(new BigDecimal(400))
                        .setMagnitudeFrom("400")
                        .setUnit(UNIT_GRAM)
                        .setWeighted(true)
                        .setPreparation("pouring, (whipping)")
                        .setOptional(false)
        );
    }
}
