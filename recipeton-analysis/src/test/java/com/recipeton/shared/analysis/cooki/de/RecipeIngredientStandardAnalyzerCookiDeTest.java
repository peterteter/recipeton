package com.recipeton.shared.analysis.cooki.de;

/*-
 * #%L
 * recipeton-shared
 * %%
 * Copyright (C) 2021 tincomisc
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.recipeton.shared.analysis.configuration.RecipeAnalyzerConfiguration;
import com.recipeton.shared.analysis.configuration.RecipeAnalyzerCookiDeConfiguration;
import com.recipeton.shared.analysis.service.RecipeIngredientAnalyzer;
import com.recipeton.shared.analysis.test.support.RecipetonAnalyzerTestFixtureTrait;
import org.junit.jupiter.api.Test;

import java.math.BigDecimal;

import static com.recipeton.shared.analysis.configuration.RecipeAnalyzerCookiDeConfiguration.UNIT_STUCK;
import static org.assertj.core.api.Assertions.assertThat;

// WIP!
public class RecipeIngredientStandardAnalyzerCookiDeTest implements RecipetonAnalyzerTestFixtureTrait {

    private final RecipeAnalyzerConfiguration configuration = new RecipeAnalyzerCookiDeConfiguration();
    private final RecipeIngredientAnalyzer analyzer = createRecipeIngredientAnalyzer(configuration);

    @Test
    void testToRecipeIngredientFormGivenIngredientWithMagnitudAndNameThenReturnsRecipeIngredient1() {
        String text = "2 Prisen Pfeffer, frisch gemahlen";
        assertThat(analyzer.analyze(text)).isEqualTo(
                createRecipeIngredientAnalysis(text, "Pfeffer", configuration)
                        .setMagnitudeFromValue(new BigDecimal(2))
                        .setMagnitudeFrom("2")
                        .setUnit("Prisen")
                        .setWeighted(false)
                        .setPreparation("frisch gemahlen")
                        .setOptional(false)
        );
    }

    @Test
    void testToRecipeIngredientFormGivenIngredientWithMagnitudAndNameThenReturnsRecipeIngredient2() {
        String text = "1 Granatapfel, Kerne herausgelöst (ca. 100 g)";
        assertThat(analyzer.analyze(text)).isEqualTo(
                createRecipeIngredientAnalysis(text, "Granatapfel", configuration)
                        .setMagnitudeFromValue(BigDecimal.ONE)
                        .setMagnitudeFrom("1")
                        .setUnit(UNIT_STUCK)
                        .setWeighted(false)
                        .setPreparation("Kerne herausgelöst (ca. 100 g)")
                        .setOptional(false)
        );
    }

    @Test
    void testToRecipeIngredientFormGivenIngredientWithMagnitudAndNameThenReturnsRecipeIngredient3() {
        String text = "5 Stängel frische Petersilie";
        assertThat(analyzer.analyze(text)).isEqualTo(
                createRecipeIngredientAnalysis(text, "Petersilie", configuration)
                        .setMagnitudeFromValue(new BigDecimal(5))
                        .setMagnitudeFrom("5")
                        .setUnit("Stängel")
                        .setWeighted(false)
                        .setPreparation("frische")
                        .setOptional(false)
        );
    }
}
