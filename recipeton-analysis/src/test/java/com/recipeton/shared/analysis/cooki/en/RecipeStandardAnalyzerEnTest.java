package com.recipeton.shared.analysis.cooki.en;

/*-
 * #%L
 * recipeton-shared
 * %%
 * Copyright (C) 2021 tincomisc
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.recipeton.shared.analysis.configuration.RecipeAnalyzerCookiEnConfiguration;
import com.recipeton.shared.analysis.domain.RecipeDifficultyDefinition;
import com.recipeton.shared.analysis.domain.RecipePriceDefinition;
import com.recipeton.shared.analysis.service.RecipeStandardAnalyzer;
import com.recipeton.shared.domain.RecipeDefinition;
import org.junit.jupiter.api.Test;

import static java.util.List.of;
import static org.assertj.core.api.Assertions.assertThat;

public class RecipeStandardAnalyzerEnTest {

    private final RecipeStandardAnalyzer analyzer = new RecipeStandardAnalyzer(new RecipeAnalyzerCookiEnConfiguration());

    @Test
    public void testAnalyzeRecipeDifficultyGivenAdvancedStringThenReturnsAdvanced() {
        assertThat(analyzer.analyzeRecipeDifficulty(new RecipeDefinition().setDifficulty("advanced"))).isEqualTo(RecipeDifficultyDefinition.ADVANCED);
    }

    @Test
    public void testAnalyzeRecipeDifficultyGivenEasyStringThenReturnsEasy() {
        assertThat(analyzer.analyzeRecipeDifficulty(new RecipeDefinition().setDifficulty("easy"))).isEqualTo(RecipeDifficultyDefinition.EASY);
    }

    @Test
    public void testAnalyzeRecipeDifficultyGivenMediumStringThenReturnsMeidum() {
        assertThat(analyzer.analyzeRecipeDifficulty(new RecipeDefinition().setDifficulty("medium"))).isEqualTo(RecipeDifficultyDefinition.MEDIUM);
    }

    @Test
    public void testAnalyzeRecipeDifficultyGivenUnknowStringThenReturnsUnknown() {
        assertThat(analyzer.analyzeRecipeDifficulty(new RecipeDefinition().setDifficulty("xxx"))).isEqualTo(RecipeDifficultyDefinition.UNKNOWN);
    }

    @Test
    public void testAnalyzeRecipePriceGivenCheapKeywordThenReturnsNoInformation() {
        assertThat(analyzer.analyzeRecipePrice(new RecipeDefinition().setKeywords(of("Low budget")))).isEqualTo(RecipePriceDefinition.LOW);
    }

    @Test
    public void testAnalyzeRecipePriceGivenNoKeywordsThenReturnsNoInformation() {
        assertThat(analyzer.analyzeRecipePrice(new RecipeDefinition())).isEqualTo(RecipePriceDefinition.NO_INFORMATION);
    }

    @Test
    public void testAnalyzeRecipePriceGivenUnmatchedStringThenReturnsNoInformation() {
        assertThat(analyzer.analyzeRecipePrice(new RecipeDefinition().setKeywords(of("xxxx")))).isEqualTo(RecipePriceDefinition.NO_INFORMATION);
    }

    // WIP. Just check instantiation successful atm
    @Test
    public void testAnalyzerGetConfigurationThenReturnsConfiguration() {
        assertThat(analyzer.getConfiguration()).isNotNull();
    }

    @Test
    public void testFormatCategoryUidGivenEndsWithPluralhenReturnsWithoutEnding() {
        assertThat(analyzer.toUid("Childrens Birthday")).isEqualTo("childrenbirthday");
    }

    @Test
    public void testFormatCategoryUidGivenEndsWithPosessiveThenReturnsWithoutEnding() {
        assertThat(analyzer.toUid("Children's Birthdays")).isEqualTo("childrenbirthday");
    }


}


