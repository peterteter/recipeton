package com.recipeton.shared.analysis.cooki.es;

/*-
 * #%L
 * recipeton-shared
 * %%
 * Copyright (C) 2021 tincomisc
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.recipeton.shared.analysis.configuration.RecipeAnalyzerConfiguration;
import com.recipeton.shared.analysis.configuration.RecipeAnalyzerCookiEsConfiguration;
import com.recipeton.shared.analysis.domain.RecipeIngredientAnalysis;
import com.recipeton.shared.analysis.service.RecipeIngredientAnalyzer;
import com.recipeton.shared.analysis.test.support.RecipetonAnalyzerTestFixtureTrait;
import org.junit.jupiter.api.Test;

import java.math.BigDecimal;

import static com.recipeton.shared.analysis.configuration.RecipeAnalyzerCookiEsConfiguration.UNIT_PIECE;
import static com.recipeton.shared.analysis.domain.RecipeUnitDefinition.UNIT_GRAM;
import static org.assertj.core.api.Assertions.assertThat;

public class RecipeIngredientStandardAnalyzerCookiEsTest implements RecipetonAnalyzerTestFixtureTrait {

    private final RecipeAnalyzerConfiguration configuration = new RecipeAnalyzerCookiEsConfiguration();
    private final RecipeIngredientAnalyzer analyzer = createRecipeIngredientAnalyzer(configuration);

    @Test
    void testAnalizeGivenIngredientWithAlternativeThenReturnsRecipeIngredientWithAlternative() {
        String text = "400 g de leche o bien 399 g de leche de almendras";
        RecipeIngredientAnalysis actual = analyzer.analyze(text);
        assertThat(actual).isEqualTo(
                createRecipeIngredientAnalysis("400 g de leche", "leche", configuration)
                        .setMagnitudeFrom("400")
                        .setMagnitudeFromValue(new BigDecimal(400))
                        .setWeighted(true)
                        .setUnit(UNIT_GRAM)
                        .setOptional(false)
                        .setAlternative(createRecipeIngredientAnalysis("399 g de leche de almendras", "leche de almendras", configuration)
                                .setMagnitudeFrom("399")
                                .setMagnitudeFromValue(new BigDecimal(399))
                                .setWeighted(true)
                                .setUnit(UNIT_GRAM)
                                .setOptional(false)
                        ));
    }

    @Test
    void testAnalizeGivenIngredientWithAlternativeWithLinebreakThenReturnsRecipeIngredientWithAlternative() {
        String text = "400 g de leche\no bien 399 g de leche de almendras";
        RecipeIngredientAnalysis actual = analyzer.analyze(text);
        assertThat(actual).isEqualTo(
                createRecipeIngredientAnalysis("400 g de leche", "leche", configuration)
                        .setMagnitudeFrom("400")
                        .setMagnitudeFromValue(new BigDecimal(400))
                        .setWeighted(true)
                        .setUnit(UNIT_GRAM)
                        .setOptional(false)
                        .setAlternative(createRecipeIngredientAnalysis("399 g de leche de almendras", "leche de almendras", configuration)
                                .setMagnitudeFrom("399")
                                .setMagnitudeFromValue(new BigDecimal(399))
                                .setWeighted(true)
                                .setUnit(UNIT_GRAM)
                                .setOptional(false)
                        ));
    }

    @Test
    void testAnalizeGivenIngredientWithCommaThenReturnsRecipeIngredient1() {
        String tex3 = "600 de patatas con piel, lavadas y cortadas en rodajas de 1 cm";
        assertThat(analyzer.analyze(tex3)).isEqualTo(
                createRecipeIngredientAnalysis(tex3, "patatas", configuration)
                        .setUnit(UNIT_GRAM)
                        .setWeighted(true)
                        .setMagnitudeFrom("600")
                        .setMagnitudeFromValue(new BigDecimal(600))
                        .setPreparation("con piel, lavadas y cortadas en rodajas de 1 cm")
                        .setOptional(false)
        );
    }

    @Test
    void testAnalizeGivenIngredientWithCommaThenReturnsRecipeIngredient2() {
        String text = "500 g de manzana Granny Smith pelada, en dados";
        assertThat(analyzer.analyze(text)).isEqualTo(
                createRecipeIngredientAnalysis(text, "manzana Granny Smith", configuration)
                        .setPreparation("pelada, en dados")
                        .setMagnitudeFrom("500")
                        .setMagnitudeFromValue(new BigDecimal(500))
                        .setUnit(UNIT_GRAM)
                        .setWeighted(true)
                        .setOptional(false)
        );
    }

    @Test
    void testAnalizeGivenIngredientWithFromAndToAndNameThenReturnsRecipeIngredient() {
        String text = "5 - 10 g de jengibre fresco en láminas (opcional)";
        assertThat(analyzer.analyze(text)).isEqualTo(
                createRecipeIngredientAnalysis(text, "jengibre", configuration)
                        .setMagnitudeFromValue(new BigDecimal(5))
                        .setMagnitudeFrom("5")
                        .setMagnitudeToValue(new BigDecimal(10))
                        .setMagnitudeTo("10")
                        .setUnit(UNIT_GRAM)
                        .setWeighted(true)
                        .setPreparation("fresco en láminas")
                        .setOptional(true)
        );
    }

    @Test
    void testAnalizeGivenIngredientWithHalfFromAndNameThenReturnsRecipeIngredient() {
        String text1 = "½ cucharadita de pimienta blanca molida";
        assertThat(analyzer.analyze(text1)).isEqualTo(
                createRecipeIngredientAnalysis(text1, "pimienta blanca", configuration)
                        .setMagnitudeFrom("½")
                        .setMagnitudeFromValue(new BigDecimal(0.5))
                        .setUnit("cucharadita")
                        .setPreparation("molida")
                        .setOptional(false)
        );

        String text2 = "¼ cucharadita de pimienta blanca molida";
        assertThat(analyzer.analyze(text2)).isEqualTo(
                createRecipeIngredientAnalysis(text2, "pimienta blanca", configuration)
                        .setMagnitudeFrom("¼")
                        .setMagnitudeFromValue(new BigDecimal(0.25))
                        .setUnit("cucharadita")
                        .setPreparation("molida")
                        .setOptional(false)
        );
    }

    @Test
    void testAnalizeGivenIngredientWithLanguageDependentFromAndNameThenReturnsRecipeIngredient() {
        String text = "Un platano entero";
        assertThat(analyzer.analyze(text)).isEqualTo(
                createRecipeIngredientAnalysis(text, "platano", configuration)
                        .setMagnitudeFrom("Un")
                        .setMagnitudeFromValue(BigDecimal.ONE)
                        .setUnit(UNIT_PIECE)
                        .setPreparation("entero")
                        .setOptional(false)
        );
    }

    @Test
    void testAnalizeGivenIngredientWithMagnitudeAndNameThenReturnsRecipeIngredient() {
        String text = "1 huevo";
        assertThat(analyzer.analyze(text)).isEqualTo(
                createRecipeIngredientAnalysis(text, "huevo", configuration)
                        .setUnit(UNIT_PIECE)
                        .setMagnitudeFrom("1")
                        .setMagnitudeFromValue(BigDecimal.ONE)
                        .setOptional(false)
        );
    }

    @Test
    void testAnalizeGivenIngredientWithMagnitudeUnitSeparatorAndNameAndDetailsAndDetailsContainsSameStringAsUnitThenReturnsRecipeIngredient() {
        String text1 = "300 g de panceta fresca de cerdo en trozos (3 cm)";
        assertThat(analyzer.analyze(text1)).isEqualTo(
                createRecipeIngredientAnalysis(text1, "panceta", configuration)
                        .setMagnitudeFrom("300")
                        .setMagnitudeFromValue(new BigDecimal(300))
                        .setUnit(UNIT_GRAM)
                        .setWeighted(true)
                        .setPreparation("fresca de cerdo en trozos (3 cm)")
                        .setOptional(false)

        );
    }

    @Test
    void testAnalizeGivenIngredientWithMagnitudeUnitSeparatorAndNameAndDetailsThenReturnsRecipeIngredient() {
        String text = "4 naranjas peladas (sin nada de piel blanca) y cortadas en rodajas";
        assertThat(analyzer.analyze(text)).isEqualTo(
                createRecipeIngredientAnalysis(text, "naranjas", configuration)
                        .setUnit(UNIT_PIECE)
                        .setMagnitudeFrom("4")
                        .setMagnitudeFromValue(new BigDecimal(4))
                        .setPreparation("peladas (sin nada de piel blanca) y cortadas en rodajas")
                        .setOptional(false)
        );
    }

    @Test
    void testAnalizeGivenIngredientWithMagnitudeUnitSeparatorAndNameAndDetailsThenReturnsRecipeIngredient2() {

        String text = "1 Kg de atun en conserva";
        assertThat(analyzer.analyze(text)).isEqualTo(
                createRecipeIngredientAnalysis(text, "atun", configuration)
                        .setMagnitudeFrom("1")
                        .setMagnitudeFromValue(BigDecimal.ONE)
                        .setUnit("Kg")
                        .setPreparation("en conserva")
                        .setOptional(false)

        );

    }

    @Test
    void testAnalizeGivenIngredientWithMagnitudeUnitSeparatorAndNameAndPreparationThenReturnsRecipeIngredient() {
        String text = "300 g de lentejas castellanas secas";
        assertThat(analyzer.analyze(text)).isEqualTo(
                createRecipeIngredientAnalysis(text, "lentejas castellanas", configuration)
                        .setMagnitudeFrom("300")
                        .setMagnitudeFromValue(new BigDecimal(300))
                        .setUnit(UNIT_GRAM)
                        .setWeighted(true)
                        .setPreparation("secas")
                        .setOptional(false)
        );
    }

    @Test
    void testAnalizeGivenIngredientWithMagnitudeUnitSeparatorAndNameThenReturnsRecipeIngredient() {
        String text = "10 g de zanahoria";
        assertThat(analyzer.analyze(text)).isEqualTo(
                createRecipeIngredientAnalysis(text, "zanahoria", configuration)
                        .setMagnitudeFrom("10")
                        .setMagnitudeFromValue(new BigDecimal(10))
                        .setUnit(UNIT_GRAM)
                        .setWeighted(true)
                        .setOptional(false)
        );
    }

    @Test
    void testAnalizeGivenIngredientWithNameThenReturnsRecipeIngredient() {
        String text = "harina";
        assertThat(analyzer.analyze(text)).isEqualTo(
                createRecipeIngredientAnalysis(text, "harina", configuration)
                        .setOptional(false)
        );
    }

    @Test
    void testAnalizeGivenIngredientWithoutUnitAndParenthesesThenReturnsRecipeIngredient() {
        String text = "vinagre de vino (½ vaso)";
        assertThat(analyzer.analyze(text)).isEqualTo(
                createRecipeIngredientAnalysis(text, "vinagre de vino", configuration)
                        .setUnit(null)
                        .setPreparation("(½ vaso)")
                        .setOptional(false)
        );
    }

}


