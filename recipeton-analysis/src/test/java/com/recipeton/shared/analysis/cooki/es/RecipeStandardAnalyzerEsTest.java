package com.recipeton.shared.analysis.cooki.es;

/*-
 * #%L
 * recipeton-shared
 * %%
 * Copyright (C) 2021 tincomisc
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.recipeton.shared.analysis.configuration.RecipeAnalyzerCookiEsConfiguration;
import com.recipeton.shared.analysis.domain.RecipeDifficultyDefinition;
import com.recipeton.shared.analysis.domain.RecipePriceDefinition;
import com.recipeton.shared.analysis.domain.RecipeUtensilAnalysis;
import com.recipeton.shared.analysis.service.RecipeStandardAnalyzer;
import com.recipeton.shared.domain.RecipeDefinition;
import org.apache.commons.lang3.tuple.Pair;
import org.junit.jupiter.api.Test;

import static java.util.List.of;
import static org.assertj.core.api.Assertions.assertThat;

public class RecipeStandardAnalyzerEsTest {

    private final RecipeStandardAnalyzer analyzer = new RecipeStandardAnalyzer(new RecipeAnalyzerCookiEsConfiguration());

    public RecipeDefinition createRecipeDefinition(String name) {
        RecipeDefinition recipeDefinition = new RecipeDefinition();
        recipeDefinition.setName(name);
        return recipeDefinition;
    }

    @Test
    public void testAnalyzeRecipeDifficultyGivenAdvancedStringThenReturnsAdvanced() {
        assertThat(analyzer.analyzeRecipeDifficulty(new RecipeDefinition().setDifficulty("avanzado"))).isEqualTo(RecipeDifficultyDefinition.ADVANCED);
    }

    @Test
    public void testAnalyzeRecipeDifficultyGivenEasyStringThenReturnsEasy() {
        assertThat(analyzer.analyzeRecipeDifficulty(new RecipeDefinition().setDifficulty("Fácil"))).isEqualTo(RecipeDifficultyDefinition.EASY);
    }

    @Test
    public void testAnalyzeRecipeDifficultyGivenMediumStringThenReturnsMeidum() {
        assertThat(analyzer.analyzeRecipeDifficulty(new RecipeDefinition().setDifficulty("medio"))).isEqualTo(RecipeDifficultyDefinition.MEDIUM);
    }

    @Test
    public void testAnalyzeRecipeDifficultyGivenUnknowStringThenReturnsUnknown() {
        assertThat(analyzer.analyzeRecipeDifficulty(new RecipeDefinition().setDifficulty("xxx"))).isEqualTo(RecipeDifficultyDefinition.UNKNOWN);
    }

    @Test
    public void testAnalyzeRecipePriceGivenCheapKeywordThenReturnsNoInformation() {
        assertThat(analyzer.analyzeRecipePrice(new RecipeDefinition().setKeywords(of("barato")))).isEqualTo(RecipePriceDefinition.LOW);
    }

    @Test
    public void testAnalyzeRecipePriceGivenNoKeywordsThenReturnsNoInformation() {
        assertThat(analyzer.analyzeRecipePrice(new RecipeDefinition())).isEqualTo(RecipePriceDefinition.NO_INFORMATION);
    }

    @Test
    public void testAnalyzeRecipePriceGivenUnmatchedStringThenReturnsNoInformation() {
        assertThat(analyzer.analyzeRecipePrice(new RecipeDefinition().setKeywords(of("xxxx")))).isEqualTo(RecipePriceDefinition.NO_INFORMATION);
    }

    @Test
    void testAnalyzeUtensilGivenUtensilSetsUid() {
        assertThat(analyzer.analyzeUtensil("bandeja de 20 cm")).isEqualTo(new RecipeUtensilAnalysis()
                .setUid("cmbandeja20")
                .setText("bandeja de 20 cm"));

        assertThat(analyzer.analyzeUtensil("Tazón de metal de 16 cm de diámetro")).isEqualTo(new RecipeUtensilAnalysis()
                .setUid("tasonmetaldiametrocm16")
                .setText("Tazón de metal de 16 cm de diámetro"));
    }

    @Test
    public void testFormatCategoryUidGivenEndsWithPluralThenReturnsWithoutEnding() {
        assertThat(analyzer.toUid("Sin Grasas")).isEqualTo("singrasa");
        assertThat(analyzer.toUid("Sin Grasa")).isEqualTo("singrasa");
    }

    @Test
    public void testFormatCategoryUidGivenEndsWithVerbalThenReturnsWithoutEnding() {
        assertThat(analyzer.toUid("cocinar")).isEqualTo("cosina");
        assertThat(analyzer.toUid("cocinado")).isEqualTo("cosina");
    }

    @Test
    public void testFormatCategoryUidGivenHasWithDemonymInMiddleOfTheWordThenDoesNotRemove() {
        assertThat(analyzer.toUid("AnoAnoX")).isEqualTo("anoanoc");
    }

    @Test
    public void testFormatTitleGivenAsciiCharactersOnlyThenReturnsSameCapitalized() {
        assertThat(analyzer.analyzeTitle(createRecipeDefinition("Abc"))).isEqualTo("Abc");
    }

    @Test
    public void testFormatTitleGivenNotCapitalizedThenReturnsCapitalized() {
        assertThat(analyzer.analyzeTitle(createRecipeDefinition("abc"))).isEqualTo("Abc");
        assertThat(analyzer.analyzeTitle(createRecipeDefinition("ABC"))).isEqualTo("Abc");
    }

    @Test
    public void testFormatTitleGivenNotTrimmedThenReturnsTrimmed() {
        assertThat(analyzer.analyzeTitle(createRecipeDefinition(" Abc"))).isEqualTo("Abc");
        assertThat(analyzer.analyzeTitle(createRecipeDefinition("Abc "))).isEqualTo("Abc");
    }

    @Test
    public void testFormatTitleGivenQuoteCharactersThenReturnsWithoutQuouteCharaters() {
        assertThat(analyzer.analyzeTitle(createRecipeDefinition("'Abc'"))).isEqualTo("Abc");
        assertThat(analyzer.analyzeTitle(createRecipeDefinition("\"Abc\""))).isEqualTo("Abc");
    }

    @Test
    public void testFormatTitleGivenSpecialCharactersInFirstPositionThenReturnsSpecialCharactersReplacedByAsciiEquivalent() {
        assertThat(analyzer.analyzeTitle(createRecipeDefinition("óntologo"))).isEqualTo("Ontologo");
    }

    @Test
    public void testFormatTitleGivenSpecialCharactersInNotFirstPositionThenReturnsSpecialCharactersNotReplaced() {
        assertThat(analyzer.analyzeTitle(createRecipeDefinition("Situación crítica"))).isEqualTo("Situación crítica");
    }

    @Test
    public void testGetRecipeCategoryUidNamePairGivenCountriesDemonymThenReturnsSameCategory() {
        assertThat(analyzer.getRecipeCategoryUidNamePair("Africa", false).getValue()).isEqualTo("#Africa");
        assertThat(analyzer.getRecipeCategoryUidNamePair("Africano", false).getValue()).isEqualTo("#Africa");

        assertThat(analyzer.getRecipeCategoryUidNamePair("Japon", false).getValue()).isEqualTo("#Japon");
        assertThat(analyzer.getRecipeCategoryUidNamePair("Japones", false).getValue()).isEqualTo("#Japon");
    }

    @Test
    public void testGetRecipeCategoryUidNamePairGivenExistingCategoryThenReturnsCategory() {
        assertThat(analyzer.getRecipeCategoryUidNamePair("Plato principal - carnes y pollo", false))
                .isEqualTo(Pair.of("0_040mainmeat", "Carnes y aves"));
        assertThat(analyzer.getRecipeCategoryUidNamePair("Plato principal", false))
                .isEqualTo(Pair.of("0_070mainother", "Otros principales"));
    }

    @Test
    public void testGetRecipeCategoryUidNamePairGivenOtherCasesThenRemovesTheseCases() {
        assertThat(analyzer.getRecipeCategoryUidNamePair("Cocinando para grupos", false).getValue()).isEqualTo("#Grupos");
        assertThat(analyzer.getRecipeCategoryUidNamePair("Cocinado para grupos", false).getValue()).isEqualTo("#Grupos");
        assertThat(analyzer.getRecipeCategoryUidNamePair("Cocinado al Vapor", false).getValue()).isEqualTo("#Vapor");
        assertThat(analyzer.getRecipeCategoryUidNamePair("Cocinar para grupos", false).getValue()).isEqualTo("#Grupos");
        assertThat(analyzer.getRecipeCategoryUidNamePair("Cocina para grupos", false).getValue()).isEqualTo("#Grupos");

        assertThat(analyzer.getRecipeCategoryUidNamePair("Para grupos", false).getValue()).isEqualTo("#Grupos");
        assertThat(analyzer.getRecipeCategoryUidNamePair("Para la pareja", false).getValue()).isEqualTo("#Pareja");
        assertThat(analyzer.getRecipeCategoryUidNamePair("Para el amigo", false).getValue()).isEqualTo("#Amigo");

        assertThat(analyzer.getRecipeCategoryUidNamePair("En pareja", false).getValue()).isEqualTo("#Pareja");
        assertThat(analyzer.getRecipeCategoryUidNamePair("En 3 pasos", false).getValue()).isEqualTo("#3 pasos");

        assertThat(analyzer.getRecipeCategoryUidNamePair("Al Vapor", false).getValue()).isEqualTo("#Vapor");

        assertThat(analyzer.getRecipeCategoryUidNamePair("De Diario", false).getValue()).isEqualTo("#Diario");

        assertThat(analyzer.getRecipeCategoryUidNamePair("Del Chef", false).getValue()).isEqualTo("#Chef");
    }

    @Test
    public void testGetRecipeCategoryUidNamePairGivenRawFoodSimilarVariantThenReturnsRawFood() {
        assertThat(analyzer.getRecipeCategoryUidNamePair("Raw food", false).getValue()).isEqualTo("#Raw food");
        assertThat(analyzer.getRecipeCategoryUidNamePair("Raw Foed", false).getValue()).isEqualTo("#Raw food");
    }

    @Test
    public void testGetRecipeCategoryUidNamePairGivenRawFoodVariantThenReturnsRawFood() {
        assertThat(analyzer.getRecipeCategoryUidNamePair("Raw food (en crudo)", false).getValue()).isEqualTo("#Raw food");
        assertThat(analyzer.getRecipeCategoryUidNamePair("Raw food", false).getValue()).isEqualTo("#Raw food");
        assertThat(analyzer.getRecipeCategoryUidNamePair("Raw Food", false).getValue()).isEqualTo("#Raw food");
        assertThat(analyzer.getRecipeCategoryUidNamePair("rAW FOOD", false).getValue()).isEqualTo("#Raw food");
    }

    @Test
    public void testGetRecipeCategoryUidNamePairGivenSimilarToExistingCategoryThenReturnsCategory() {
        assertThat(analyzer.getRecipeCategoryUidNamePair("Bebes", false))
                .isEqualTo(Pair.of("0_170baby", "Alimentación infantil"));
        assertThat(analyzer.getRecipeCategoryUidNamePair("infantil", false))
                .isEqualTo(Pair.of("0_170baby", "Alimentación infantil"));
    }

    @Test
    public void testGetRecipeCategoryUidNamePairGivenStartsWithCleanedPrefixThenReturnsWithoutCleanedPrefix() {
        assertThat(analyzer.getRecipeCategoryUidNamePair("para grupos", false).getValue()).isEqualTo("#Grupos");
    }

    @Test
    public void testGetRecipeCategoryUidNamePairGivenStartsWithCocinarThenRemovesCocinar() {
        assertThat(analyzer.getRecipeCategoryUidNamePair("Cocina patatas", false).getValue()).isEqualTo("#Patatas");
        assertThat(analyzer.getRecipeCategoryUidNamePair("Cocinar patatas", false).getValue()).isEqualTo("#Patatas");
    }

    @Test
    public void testGetRecipeCategoryUidNamePairGivenStartsWithContieneThenRemovesContiene() {
        assertThat(analyzer.getRecipeCategoryUidNamePair("Contiene patatas", false).getValue()).isEqualTo("#Patatas");
    }

}


