package com.recipeton.shared.analysis.cooki.es;

/*-
 * #%L
 * recipeton-shared
 * %%
 * Copyright (C) 2021 tincomisc
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.recipeton.shared.analysis.configuration.RecipeAnalyzerCookiEsConfiguration;
import com.recipeton.shared.analysis.domain.RecipeAnalysisTag;
import com.recipeton.shared.analysis.test.support.RecipetonAnalyzerTestAssertTrait;
import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.Test;

@Slf4j
public class RecipeInstructionAnalyzerEsUtensilActionTest implements RecipetonAnalyzerTestAssertTrait {

    private final RecipeAnalyzerCookiEsConfiguration configuration = new RecipeAnalyzerCookiEsConfiguration();

    @Test
    void testAnalyzeGivenInstructionWithButterflyActionThenUtensilActionIsDetected() {
        analyzeAndAssertThatRecipeUtensilTermAndTagEquals("Coloque la mariposa", RecipeAnalysisTag.WHISK_PUT, configuration);
        analyzeAndAssertThatRecipeUtensilTermAndTagEquals("Coloque la mariposa en las cuchillas", RecipeAnalysisTag.WHISK_PUT, configuration);

        analyzeAndAssertThatRecipeUtensilTermAndTagEquals("Retire la mariposa", RecipeAnalysisTag.WHISK_REMOVE, configuration);
        analyzeAndAssertThatRecipeUtensilTermAndTagEquals("Quite la mariposa", RecipeAnalysisTag.WHISK_REMOVE, configuration);
    }

    @Test
    void testAnalyzeGivenInstructionWithEmptyMixingBowlActionThenUtensilActionIsDetected() {
        analyzeAndAssertThatRecipeUtensilTermAndTagEquals("Vierta el helado a un tazón", RecipeAnalysisTag.TRANSFER_TO_BOWL, configuration);
        analyzeAndAssertThatRecipeUtensilTermAndTagEquals("Vierta el helado en un tazón", RecipeAnalysisTag.TRANSFER_TO_BOWL, configuration);
        analyzeAndAssertThatRecipeUtensilTermAndTagEquals("Retire a un tazón y reserve", RecipeAnalysisTag.TRANSFER_TO_BOWL, configuration);
        analyzeAndAssertThatRecipeUtensilTermAndTagEquals("Retire a un bol y reserve", RecipeAnalysisTag.TRANSFER_TO_BOWL, configuration);
        analyzeAndAssertThatRecipeUtensilTermAndTagEquals("Retire del vaso y reserve", RecipeAnalysisTag.TRANSFER_TO_BOWL, configuration);
        analyzeAndAssertThatRecipeUtensilTermAndTagEquals("Saque del vaso y reserve", RecipeAnalysisTag.TRANSFER_TO_BOWL, configuration);
        analyzeAndAssertThatRecipeUtensilTermAndTagEquals("Saque del vaso", RecipeAnalysisTag.TRANSFER_TO_BOWL, configuration);

        analyzeAndAssertThatRecipeUtensilTermAndTagEquals("Transfiera a un tazón y reserve", RecipeAnalysisTag.TRANSFER_TO_BOWL, configuration);
    }

    @Test
    void testAnalyzeGivenInstructionWithOvenThenUtensilActionIsDetected() {
        analyzeAndAssertThatRecipeUtensilTermAndTagEquals("precaliente el horno", RecipeAnalysisTag.OVEN_OPERATION, configuration);
        analyzeAndAssertThatRecipeUtensilTermAndTagEquals("precaliente el horno a 180 grados", RecipeAnalysisTag.OVEN_OPERATION, configuration);
    }

    @Test
    void testAnalyzeGivenInstructionWithSpatulaMixActionThenUtensilActionIsDetected() {
        analyzeAndAssertThatRecipeUtensilTermAndTagEquals("Mezcle con la espátula", RecipeAnalysisTag.SPATULA_MIX, configuration);
        analyzeAndAssertThatRecipeUtensilTermAndTagEquals("Mezcle la cosa con la espátula", RecipeAnalysisTag.SPATULA_MIX, configuration);
        analyzeAndAssertThatRecipeUtensilTermAndTagEquals("Termine de mezclar con la espátula", RecipeAnalysisTag.SPATULA_MIX, configuration);
        analyzeAndAssertThatRecipeUtensilTermAndTagEquals("Termine de mezclar la cosa esa con la espátula", RecipeAnalysisTag.SPATULA_MIX, configuration);
    }

    @Test
    void testAnalyzeGivenInstructionWithSpatulaMixWellActionThenUtensilActionIsDetected() {
        analyzeAndAssertThatRecipeUtensilTermAndTagEquals("Mezcle bien con la espátula", RecipeAnalysisTag.SPATULA_MIX_WELL, configuration);
        analyzeAndAssertThatRecipeUtensilTermAndTagEquals("Mezcle muy bien con la espátula", RecipeAnalysisTag.SPATULA_MIX_WELL, configuration);
        analyzeAndAssertThatRecipeUtensilTermAndTagEquals("Mezcle muy bien la cosa con la espátula", RecipeAnalysisTag.SPATULA_MIX_WELL, configuration);
        analyzeAndAssertThatRecipeUtensilTermAndTagEquals("Mezcle la cosa muy bien con la espátula", RecipeAnalysisTag.SPATULA_MIX_WELL, configuration);
        analyzeAndAssertThatRecipeUtensilTermAndTagEquals("Termine de mezclar muy bien con espátula", RecipeAnalysisTag.SPATULA_MIX_WELL, configuration);

        analyzeAndAssertThatRecipeUtensilTermNotPresent("Mezcle bien con varillas", configuration);
    }

    @Test
    void testAnalyzeGivenInstructionWithSpatulaScrapActionThenUtensilActionIsDetected() {
        analyzeAndAssertThatRecipeUtensilTermAndTagEquals("Baje los restos del interior del vaso con la ayuda de la espátula", RecipeAnalysisTag.SPATULA_SCRAP_SIDE, configuration);
        analyzeAndAssertThatRecipeUtensilTermAndTagEquals("Baje los restos de las paredes del vaso con la ayuda de la espátula", RecipeAnalysisTag.SPATULA_SCRAP_SIDE, configuration);
        analyzeAndAssertThatRecipeUtensilTermAndTagEquals("Baje los restos de las paredes con la ayuda de la espátula", RecipeAnalysisTag.SPATULA_SCRAP_SIDE, configuration);
        analyzeAndAssertThatRecipeUtensilTermAndTagEquals("Con la espátula baje hacia el fondo del vaso", RecipeAnalysisTag.SPATULA_SCRAP_SIDE, configuration);
    }

}
