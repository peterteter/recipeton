package com.recipeton.shared.analysis.cooki.fr;

/*-
 * #%L
 * recipeton-shared
 * %%
 * Copyright (C) 2021 tincomisc
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.recipeton.shared.analysis.configuration.RecipeAnalyzerConfiguration;
import com.recipeton.shared.analysis.configuration.RecipeAnalyzerCookiFrConfiguration;
import com.recipeton.shared.analysis.service.RecipeIngredientAnalyzer;
import com.recipeton.shared.analysis.test.support.RecipetonAnalyzerTestFixtureTrait;
import org.junit.jupiter.api.Test;

import java.math.BigDecimal;

import static com.recipeton.shared.analysis.domain.RecipeUnitDefinition.UNIT_GRAM;
import static org.assertj.core.api.Assertions.assertThat;

// WIP!
public class RecipeIngredientStandardAnalyzerCookiFrTest implements RecipetonAnalyzerTestFixtureTrait {

    private final RecipeAnalyzerConfiguration configuration = new RecipeAnalyzerCookiFrConfiguration();
    private final RecipeIngredientAnalyzer analyzer = createRecipeIngredientAnalyzer(configuration);

    @Test
    void testToRecipeIngredientFormGivenIngredientWithMagnitudAndNameThenReturnsRecipeIngredient3() {
        String text = "10 g d'eau";
        assertThat(analyzer.analyze(text)).isEqualTo(
                createRecipeIngredientAnalysis(text, "eau", configuration)
                        .setMagnitudeFromValue(new BigDecimal(10))
                        .setMagnitudeFrom("10")
                        .setUnit(UNIT_GRAM)
                        .setWeighted(true)
                        .setOptional(false)
        );
    }
}
