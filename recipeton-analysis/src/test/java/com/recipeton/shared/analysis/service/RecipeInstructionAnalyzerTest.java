package com.recipeton.shared.analysis.service;

/*-
 * #%L
 * recipeton-shared
 * %%
 * Copyright (C) 2021 tincomisc
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.recipeton.shared.analysis.configuration.RecipeAnalyzerConfiguration;
import com.recipeton.shared.analysis.domain.RecipeInstructionAnalysis;
import com.recipeton.shared.analysis.test.support.RecipetonAnalyzerTestAssertTrait;
import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.Test;

import java.util.List;

import static java.util.List.of;
import static org.assertj.core.api.Assertions.assertThat;

@Slf4j
public class RecipeInstructionAnalyzerTest implements RecipetonAnalyzerTestAssertTrait {

    private final RecipeAnalyzerConfiguration configuration = createRecipeAnalyzerConfiguration();
    private final RecipeInstructionAnalyzer analyzer = createRecipeInstructionAnalyzer(configuration);

    @Test
    void testAnalyzeGivenStringWithAllJunkSequencesThenRemovesInstruction() {
        List<String> recipeInstructions = of("<strong>\n\n<br /></strong>");

        List<RecipeInstructionAnalysis> recipeInstructionAnalyses = createRecipeInstructionAnalyzer(configuration).analyze(recipeInstructions, createRecipeIngredientAnalyses(configuration));
        assertThat(recipeInstructionAnalyses).isEmpty();
    }

    @Test
    void testAnalyzeGivenStringWithJunkJunkSequencesThenRemovesJunkJunkSequences() {
        List<String> recipeInstructions = of("aa<br>bb");

        List<RecipeInstructionAnalysis> recipeInstructionAnalyses = createRecipeInstructionAnalyzer(configuration).analyze(recipeInstructions, createRecipeIngredientAnalyses(configuration));

        assertThat(recipeInstructionAnalyses).singleElement().satisfies(ria -> assertThat(ria.getText()).isEqualTo("Aabb"));
    }

    @Test
    void testAnalyzeGivenStringWithValidSequencesThenReturnsInstructionsWithoutModification() {
        List<String> recipeInstructions = of("abc abc");

        List<RecipeInstructionAnalysis> recipeInstructionAnalyses = createRecipeInstructionAnalyzer(configuration).analyze(recipeInstructions, createRecipeIngredientAnalyses(configuration));

        assertThat(recipeInstructionAnalyses).singleElement().satisfies(ria -> assertThat(ria.getText()).isEqualTo("Abc abc"));
    }

    @Test
    void testGetIngredientNotationTokenVariantsGivenIngredientNotationWithAlternativeThenVariantsIncludeAlternative() {
        assertThat(analyzer.getIngredientNotationTokenVariants(createRecipeIngredientAnalysis("egg", configuration).setAlternative(createRecipeIngredientAnalysis("eye", configuration))))
                .containsExactly(new String[]{"egg"}, new String[]{"eye"});
    }

    @Test
    void testGetIngredientNotationTokenVariantsGivenIngredientNotationWithOneWordReturnsWord() {
        assertThat(analyzer.getIngredientNotationTokenVariants(createRecipeIngredientAnalysis("te", configuration)))
                .containsExactly(new String[]{"te"});
    }

    @Test
    void testGetIngredientNotationTokenVariantsGivenIngredientNotationWithSingleCharTokensThenDoesNotReturnSingleCharacterTokens() {
        assertThat(analyzer.getIngredientNotationTokenVariants(createRecipeIngredientAnalysis("11 3 22", configuration)))
                .containsExactly(new String[]{"11", "22"}, new String[]{"11"}, new String[]{"22"});
    }

    @Test
    void testGetIngredientNotationTokenVariantsGivenIngredientNotationWithThreeTokensReturnsTokenVariations() {
        assertThat(analyzer.getIngredientNotationTokenVariants(createRecipeIngredientAnalysis("11 22 33", configuration)))
                .containsExactly(new String[]{"11", "22", "33"}, new String[]{"11", "22"}, new String[]{"11"}, new String[]{"22", "33"}, new String[]{"22"}, new String[]{"33"});
    }

    @Test
    void testGetIngredientNotationTokenVariantsGivenIngredientNotationWithTwoTokensReturnsTokenVariations() {
        assertThat(analyzer.getIngredientNotationTokenVariants(createRecipeIngredientAnalysis("11 22", configuration)))
                .containsExactly(new String[]{"11", "22"}, new String[]{"11"}, new String[]{"22"});
    }

    @Test
    public void testGetTextWithoutTagsGivenTextWithNoTagsThenReturnsText() {
        assertThat(analyzer.getTextAsHumanReadable(createRecipeInstructionAnalysis("bla", configuration))).isEqualTo("bla");
    }

    @Test
    public void testGetTextWithoutTagsGivenTextWithSquareBracketsThenReturnsTextWithoutBrackets() {
        assertThat(analyzer.getTextAsHumanReadable(createRecipeInstructionAnalysis("[bla]", configuration))).isEqualTo("bla");
    }

    @Test
    public void testGetTextWithoutTagsGivenTextWithStrongTagThenReturnsTextWithoutTag() {
        assertThat(analyzer.getTextAsHumanReadable(createRecipeInstructionAnalysis("<nobr>bla</nobr>", configuration))).isEqualTo("bla");
    }


}
