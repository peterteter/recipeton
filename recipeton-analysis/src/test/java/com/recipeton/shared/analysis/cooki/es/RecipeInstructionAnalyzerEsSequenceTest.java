package com.recipeton.shared.analysis.cooki.es;

/*-
 * #%L
 * recipeton-shared
 * %%
 * Copyright (C) 2021 tincomisc
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.recipeton.shared.analysis.configuration.RecipeAnalyzerCookiEsConfiguration;
import com.recipeton.shared.analysis.domain.RecipeIngredientAnalysis;
import com.recipeton.shared.analysis.service.RecipeInstructionAnalyzer;
import com.recipeton.shared.analysis.test.support.RecipetonAnalyzerTestAssertTrait;
import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.Test;

import java.util.List;

import static com.recipeton.shared.analysis.domain.RecipeAnalysisTag.*;
import static java.util.List.of;
import static org.assertj.core.api.Assertions.assertThat;

/**
 * Prueba y mejora!
 *
 * <p>
 * Introduzca el cestillo en el vaso y pese el arroz
 * PLACE + IN_MAIN_COMPARTMENT
 * WEIGH
 * <p>
 * <p>
 * Support Multiple times same ingredient! Try to find declared amount. (use ingredient pattern XXX unit de ingredientmatch)
 * - Ponga en el vaso 50 g de aceite de oliva y los ajos y cueza <nobr>3 min/120°C/vel 1</nobr>."
 * - Introduzca el cestillo en el vaso y pese el arroz
 * - extraiga el cestillo, ponga el arroz en una fuente
 * - Vacíe el vaso."
 * - Ponga en el vaso el tomate triturado, 70 g de aceite de oliva
 * <p>
 * <p>
 * Total 2 cucharaditas de sal
 * - Ponga 1 cucharadita de sal
 * ....
 * - Ponga 1 cucharadita de sal
 */


@Slf4j
public class RecipeInstructionAnalyzerEsSequenceTest implements RecipetonAnalyzerTestAssertTrait {

    private final RecipeAnalyzerCookiEsConfiguration configuration = new RecipeAnalyzerCookiEsConfiguration();
    private final List<RecipeIngredientAnalysis> empty = createRecipeIngredientAnalyses(configuration);
    private final RecipeInstructionAnalyzer analyzer = createRecipeInstructionAnalyzer(configuration);

    @Test
    void testAnalyzeGiveInstructionWithAddAndToMainCompartmentAndNoIngredientThenTagCreatedIngredient() {

        assertThat(analyzer.analyze(of("Vierta la masa en el vaso y caliente <nobr>5 min/120°C/vel 1</nobr>."), createRecipeIngredientAnalyses(configuration, "")))
                .hasSize(2)
                .satisfies(l -> {
                    assertThat(l).element(0).satisfies(ria -> {
                        assertThat(ria.getText()).isEqualTo("Vierta la masa en el vaso");
                        assertThat(ria.getTags()).containsExactlyInAnyOrder(PLACE, REF_IN_MAIN_COMPARTMENT);
                    });
                    assertThat(l).element(1).satisfies(ria -> {
                        assertThat(ria.getText()).isEqualTo("Caliente <nobr>5 min/120°C/vel 1</nobr>");
                        assertThat(ria.getTags()).containsExactly(COMMAND);
                    });
                });
    }

    @Test
    void testAnalyzeGiveInstructionsCommandWithParallelNonActionExplanationThenReturnsInstruction() {

        assertThat(analyzer.analyze(of("Caliente <nobr>5 min/120°C/vel 1</nobr> y, mientras tanto, mire por la ventana"), createRecipeIngredientAnalyses(configuration, "")))
                .hasSize(2)
                .satisfies(l -> {
                    assertThat(l).element(0).satisfies(ria -> {
                        assertThat(ria.getText()).isEqualTo("Caliente <nobr>5 min/120°C/vel 1</nobr>");
                        assertThat(ria.getTags()).containsExactlyInAnyOrder(COMMAND);
                    });
                    assertThat(l).element(1).satisfies(ria -> {
                        assertThat(ria.getText()).isEqualTo("Mientras tanto, mire por la ventana");
                        assertThat(ria.getTags()).containsExactlyInAnyOrder(PARALLEL);
                    });
                });
    }

    @Test
    void testAnalyzeGiveInstructionsCommandWithParallelPlaceStepThenReturnsInstructions() {

        assertThat(analyzer.analyze(of("Rehogue [10 min/120°C/reverse/vel 1]", "Mientras tanto, ponga los ramilletes de brócoli en la vaporera"), createRecipeIngredientAnalyses(configuration, "")))
                .hasSize(2)
                .satisfies(l -> {
                    assertThat(l).element(0).satisfies(ria -> {
                        assertThat(ria.getText()).isEqualTo("Rehogue [10 min/120°C/reverse/vel 1]");
                        assertThat(ria.getTags()).containsExactlyInAnyOrder(COMMAND);
                    });
                    assertThat(l).element(1).satisfies(ria -> {
                        assertThat(ria.getText()).isEqualTo("Mientras tanto, ponga los ramilletes de brócoli en la vaporera");
                        assertThat(ria.getTags()).containsExactlyInAnyOrder(PARALLEL, PLACE, REF_IN_UTENSIL);
                    });
                });
    }

    @Test
    void testAnalyzeGiveInstructionsWithParallelExecutionSepparatedByCommasAndParallelIsCommandThenReturnsInstructionsAndParallelInstructionTagged() {

        assertThat(analyzer.analyze(of("Caliente <nobr>5 min/120°C/vel 1</nobr>. Mientras tanto, pique <nobr>5 min/vel 8</nobr>"), createRecipeIngredientAnalyses(configuration, "")))
                .hasSize(2)
                .satisfies(l -> {
                    assertThat(l).element(0).satisfies(ria -> {
                        assertThat(ria.getText()).isEqualTo("Caliente <nobr>5 min/120°C/vel 1</nobr>");
                        assertThat(ria.getTags()).containsExactlyInAnyOrder(COMMAND);
                    });
                    assertThat(l).element(1).satisfies(ria -> {
                        assertThat(ria.getText()).isEqualTo("Mientras tanto, pique <nobr>5 min/vel 8</nobr>");
                        assertThat(ria.getTags()).containsExactlyInAnyOrder(PARALLEL, COMMAND);
                    });
                });
    }

    @Test
    void testAnalyzeGivenAddAccesoryToMainCompartmentAndAddIngredientThenReturnsIngredientAddedInMainCompartment() {
        assertThat(analyzer.analyze(of("Introduzca el cestillo en el vaso. Agregue el arroz"), empty))
                .hasSize(2)
                .satisfies(l -> {
                    assertThat(l).element(0).satisfies(
                            ria -> assertThat(ria.getTags())
                                    .containsExactlyInAnyOrder(REF_IN_MAIN_COMPARTMENT, SIMMERING_BASKET_PUT_INSIDE)
                    );

                    assertThat(l).element(1).satisfies(
                            ria -> assertThat(ria.getTags())
                                    .containsExactlyInAnyOrder(ADD, REF_IN_MAIN_COMPARTMENT)
                    );

                });
    }

    @Test
    void testAnalyzeGivenAddAccesoryToMainCompartmentAndWeightIngredientThenReturnsWeighInMainCompartment() {
        assertThat(analyzer.analyze(of("Introduzca el cestillo en el vaso y pese el arroz"), empty))
                .hasSize(2)
                .satisfies(l -> {
                    assertThat(l).element(0).satisfies(
                            ria -> assertThat(ria.getTags())
                                    .containsExactlyInAnyOrder(REF_IN_MAIN_COMPARTMENT, SIMMERING_BASKET_PUT_INSIDE)
                    );

                    assertThat(l).element(1).satisfies(
                            ria -> assertThat(ria.getTags())
                                    .containsExactlyInAnyOrder(WEIGH, REF_IN_MAIN_COMPARTMENT)
                    );

                });
    }

    @Test
    void testAnalyzeGivenAddInstructionAfterPlaceInMainCompartmentInstructionThenAddInstructionHasInMainCompartmentTag() {

        assertThat(analyzer.analyze(of("Ponga el tomate en el vaso", "Agregue la patata"), empty)).allSatisfy(
                ria -> assertThat(ria.getTags()).contains(REF_IN_MAIN_COMPARTMENT)
        );
    }

    @Test
    void testAnalyzeGivenAddInstructionAfterPlaceInMainCompartmentInstructionWhenAddInstructionIsInToDifferentCompartmentThenAddInstructionHasNotInMainCompartmentTag() {
        assertThat(analyzer.analyze(of("Ponga en el vaso el tomate", "añada la patata a un bol"), empty))
                .hasSize(2)
                .satisfies(l -> {
                    assertThat(l).element(0).satisfies(
                            ria -> assertThat(ria.getTags())
                                    .doesNotContain(REF_IN_EXTERNAL_ACCESORY)
                                    .contains(REF_IN_MAIN_COMPARTMENT)
                    );

                    assertThat(l).element(1).satisfies(
                            ria -> assertThat(ria.getTags())
                                    .contains(REF_IN_EXTERNAL_ACCESORY)
                                    .doesNotContain(REF_IN_MAIN_COMPARTMENT)
                    );

                });
    }

    @Test
    void testAnalyzeGivenAddInstructionAfterPlaceNotInMainCompartmentInstructionThenAddInstructionNotHasInMainCompartmentTag() {

        assertThat(analyzer.analyze(of("Ponga el tomate en la bandeja", "Agregue la patata"), empty)).allSatisfy(
                ria -> assertThat(ria.getTags()).doesNotContain(REF_IN_MAIN_COMPARTMENT)
        );
    }

    @Test
    void testAnalyzeGivenAddInstructionAfterWeighOnTopThenAddInstructionHasOnTop() {

        assertThat(analyzer.analyze(of("Coloque un tazón sobre la tapa del vaso, pese la manzana "), empty))
                .hasSize(2)
                .satisfies(l -> {
                    assertThat(l).element(0).satisfies(
                            ria -> assertThat(ria.getTags()).contains(PLACE, REF_TOP_OF_MAIN_COMPARTMENT)
                    );
                    assertThat(l).element(1).satisfies(
                            ria -> assertThat(ria.getTags()).contains(REF_TOP_OF_MAIN_COMPARTMENT, WEIGH)
                    );
                });
    }

    @Test
    void testAnalyzeGivenAddInstructionToMainCompartmentThenInstructionHasInMainCompartmentTagAndNotInExternalCompartment() {
        List<String> recipeInstructions = of("Ponga en el vaso la cebolla, los dientes de ajo y el aceite");

        assertThat(analyzer.analyze(recipeInstructions, empty))
                .singleElement()
                .satisfies(
                        ria -> assertThat(ria.getTags())
                                .contains(REF_IN_MAIN_COMPARTMENT)
                                .doesNotContain(REF_IN_EXTERNAL_ACCESORY)
                );
    }

    @Test
    void testAnalyzeGivenPlaceMainCompartmentAndThenAddIngredientInstructionsThenAddIngredientIsInMainCompartment() {
        assertThat(
                analyzer.analyze(of("Coloque el vaso en su posicion. Agregue tomate"), createRecipeIngredientAnalyses(configuration, "tomate")))
                .hasSize(2)
                .satisfies(l -> {
                    assertThat(l).element(0).satisfies(ria -> {
                        assertThat(ria.getTags()).contains(PLACE, MIXING_BOWL_PUT);
                    });
                    assertThat(l).element(1).satisfies(ria -> {
                        assertThat(ria.getTags()).containsExactlyInAnyOrder(ADD, REF_IN_MAIN_COMPARTMENT);
                    });
                });
    }

    @Test
    void testAnalyzeGivenRefOnTopInstructionThenInstructionHasOnTopTag() {

        assertThat(analyzer.analyze(of("Ponga un bol sobre la tapa del vaso"), empty))
                .singleElement()
                .satisfies(
                        ria -> assertThat(ria.getTags()).contains(REF_TOP_OF_MAIN_COMPARTMENT)
                );
    }

    @Test
    void testAnalyzeGivenServeInstructionWithIngredientsThenIngredientsAreDecoration() {

        assertThat(analyzer.analyze(of("Sirva muy frío, espolvoreado con los dados de jamón serrano y el huevo duro picado."), empty))
                .allSatisfy(
                        ria -> assertThat(ria.getTags()).containsExactlyInAnyOrder(SERVING)
                );
    }
}
