package com.recipeton.shared.analysis.cooki.en;

/*-
 * #%L
 * recipeton-shared
 * %%
 * Copyright (C) 2021 tincomisc
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.recipeton.shared.analysis.configuration.RecipeAnalyzerConfiguration;
import com.recipeton.shared.analysis.configuration.RecipeAnalyzerCookiEnConfiguration;
import com.recipeton.shared.analysis.domain.*;
import com.recipeton.shared.analysis.service.RecipeInstructionAnalyzer;
import com.recipeton.shared.analysis.test.support.RecipetonAnalyzerTestAssertTrait;
import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.Test;

import static com.recipeton.shared.analysis.domain.RecipeAnalysisTag.*;
import static java.util.List.of;
import static org.assertj.core.api.Assertions.assertThat;

@Slf4j
public class RecipeInstructionAnalyzerEnSequenceTest implements RecipetonAnalyzerTestAssertTrait {

    private final RecipeAnalyzerConfiguration configuration = new RecipeAnalyzerCookiEnConfiguration();
    private final RecipeInstructionAnalyzer analyzer = createRecipeInstructionAnalyzer(configuration);

    @Test
    void testAnalyzeGivenInstructionsWithPlaceCommandAdditionCommandSequenceThenInstructionsSplittedIn4() {
        assertThat(
                analyzer.analyze(of(
                        "Place almond flour and salt into mixing bowl and mix <nobr>11 sec/speed 4</nobr>.",
                        "Add frozen stuff and mix <nobr>4 sec/speed 9</nobr>."), createRecipeIngredientAnalyses(configuration, ""))).hasSize(4).satisfies(l -> {
            assertThat(l).element(0).satisfies(ria -> {
                assertThat(ria.getText()).isEqualTo("Place almond flour and salt into mixing bowl");
                assertThat(ria.getTags()).containsExactlyInAnyOrder(PLACE, RecipeAnalysisTag.REF_IN_MAIN_COMPARTMENT);
            });
            assertThat(l).element(1).satisfies(ria -> {
                assertThat(ria.getText()).isEqualTo("Mix <nobr>11 sec/speed 4</nobr>");
                assertThat(ria.getTags()).containsExactly(RecipeAnalysisTag.COMMAND);
            });
            assertThat(l).element(2).satisfies(ria -> {
                assertThat(ria.getText()).isEqualTo("Add frozen stuff");
                assertThat(ria.getTags()).containsExactlyInAnyOrder(RecipeAnalysisTag.ADD, RecipeAnalysisTag.REF_IN_MAIN_COMPARTMENT);
            });
            assertThat(l).element(3).satisfies(ria -> {
                assertThat(ria.getText()).isEqualTo("Mix <nobr>4 sec/speed 9</nobr>");
                assertThat(ria.getTags()).containsExactly(RecipeAnalysisTag.COMMAND);
            });
        });
    }

    @Test
    void testAnalyzeGivenPlaceMainCompartmentAndThenAddIngredientInstructionsThenAddIngredientIsInMainCompartment() {
        assertThat(
                analyzer.analyze(of("Place mixing bowl back into position. Add tomato"), createRecipeIngredientAnalyses(configuration, "tomate")))
                .hasSize(2)
                .satisfies(l -> {
                    assertThat(l).element(0).satisfies(ria -> {
                        assertThat(ria.getTags()).containsExactlyInAnyOrder(PLACE, RecipeAnalysisTag.MIXING_BOWL_PUT);
                    });
                    assertThat(l).element(1).satisfies(ria -> {
                        assertThat(ria.getTags()).containsExactlyInAnyOrder(RecipeAnalysisTag.ADD, RecipeAnalysisTag.REF_IN_MAIN_COMPARTMENT);
                    });
                });
    }

    @Test
    void testGetTermsSortedGiveInstructionWithCommandActionAundUtensilPreparationThenInstructionsAnalysisContainsCommandAndUtensilTermAndUtensilTermIsInOrderBeforeCommandTerm() {
        assertThat(analyzer.analyze(of("Sauté [10 min/steamer/spoon/speed 1], without measuring cup"), createRecipeIngredientAnalyses(configuration, "")))
                .singleElement().satisfies(ria -> {
            assertThat(analyzer.getTermsSorted(ria))
                    .hasSize(2)
                    .satisfies(t -> {
                        assertThat(t).element(0).isInstanceOf(RecipeUtensilActionTerm.class);
                        assertThat(t).element(1).isInstanceOf(RecipeCommandTerm.class);
                    });
        });
    }

    @Test
    void testGetTermsSortedGivenInstructionWithIngredientsThenReturnsIngredientsInOrder() {
        assertThat(analyzer.analyze(of("Place egg, tomato and potato into mixing bowl"),
                createRecipeIngredientAnalyses(configuration, "potato", "egg", "tomato")))
                .singleElement().satisfies(ria -> {
            assertThat(analyzer.getTermsSorted(ria))
                    .hasSize(5)
                    .satisfies(t -> {
                        assertThat(t).element(0).isInstanceOfSatisfying(RecipeInstructionTerm.class, rit -> {
                            assertThat(rit.getTag()).isEqualTo(PLACE);
                        });
                        assertThat(t).element(1).isInstanceOfSatisfying(RecipeInstructionTerm.class, rit -> {
                            assertThat(rit.getTag()).isEqualTo(REF_IN_MAIN_COMPARTMENT);
                        });
                        assertThat(t).element(2).isInstanceOfSatisfying(RecipeIngredientTerm.class, rit -> {
                            assertThat(rit.getIngredient().getIngredientNotation()).isEqualTo("egg");
                        });
                        assertThat(t).element(3).isInstanceOfSatisfying(RecipeIngredientTerm.class, rit -> {
                            assertThat(rit.getIngredient().getIngredientNotation()).isEqualTo("tomato");
                        });
                        assertThat(t).element(4).isInstanceOfSatisfying(RecipeIngredientTerm.class, rit -> {
                            assertThat(rit.getIngredient().getIngredientNotation()).isEqualTo("potato");
                        });
                    });
        });
    }


    @Test
    void testGetTermsSortedGivenInstructionWithRecipeInstructionsThenReturnsInstructionsInOrder() {
        assertThat(analyzer.analyze(of("Place XXX into mixing bowl placing simmering basket instead of measuring cup"),
                createRecipeIngredientAnalyses(configuration)))
                .singleElement().satisfies(ria -> {
            assertThat(analyzer.getTermsSorted(ria))
                    .hasSize(3)
                    .satisfies(t -> {
                        assertThat(t).element(0).isInstanceOfSatisfying(RecipeInstructionTerm.class, rit -> {
                            assertThat(rit.getTag()).isEqualTo(PLACE);
                        });
                        assertThat(t).element(1).isInstanceOfSatisfying(RecipeInstructionTerm.class, rit -> {
                            assertThat(rit.getTag()).isEqualTo(REF_IN_MAIN_COMPARTMENT);
                        });
                        assertThat(t).element(2).isInstanceOfSatisfying(RecipeUtensilActionTerm.class, rit -> {
                            assertThat(rit.getTag()).isEqualTo(SIMMERING_BASKET_AS_LID_PUT);
                        });
                    });
        });
    }


}
