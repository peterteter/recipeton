package com.recipeton.shared.analysis.cooki.es;

/*-
 * #%L
 * recipeton-shared
 * %%
 * Copyright (C) 2021 tincomisc
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.recipeton.shared.analysis.configuration.RecipeAnalyzerCookiEsConfiguration;
import com.recipeton.shared.analysis.service.preprocess.RecipeInstructionSplitter;
import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.Test;

import static org.assertj.core.api.Assertions.assertThat;

@Slf4j
public class RecipeInstructionSplitterCookiEsTest {

    private final RecipeInstructionSplitter recipeInstructionAnalyzer = new RecipeInstructionSplitter(new RecipeAnalyzerCookiEsConfiguration());

    @Test
    void testSplitInstructionGivenContainsAbbreviatureWithDotThenReturnsStringNotSplittingInAbbreviatureDot() {
        assertThat(recipeInstructionAnalyzer.splitInstruction("a aprox. b")).containsExactly("A aprox. b");
        assertThat(recipeInstructionAnalyzer.splitInstruction("a aprox. b. c")).containsExactly("A aprox. b", "C");
    }

    @Test
    void testSplitInstructionGivenContainsSplittingSentenceConnectorThenSplits() {
        assertThat(recipeInstructionAnalyzer.splitInstruction("a y mezcle b")).containsExactly("A", "Mezcle b");
        assertThat(recipeInstructionAnalyzer.splitInstruction("a, mezcle b")).containsExactly("A", "Mezcle b");
        assertThat(recipeInstructionAnalyzer.splitInstruction(", mezcle b")).containsExactly("Mezcle b");

        assertThat(recipeInstructionAnalyzer.splitInstruction("a, y mezcle b")).containsExactly("A", "Mezcle b");

        assertThat(recipeInstructionAnalyzer.splitInstruction("a, después mezcle b")).containsExactly("A", "Mezcle b");
        assertThat(recipeInstructionAnalyzer.splitInstruction("a, luego mezcle b")).containsExactly("A", "Mezcle b");
    }

    @Test
    void testSplitInstructionGivenContainsFixedSplittingSentenceConnectorThenSplits() {
        assertThat(recipeInstructionAnalyzer.splitInstruction("a y, mientras tanto, b")).containsExactly("A", "Mientras tanto, b");
    }

}
