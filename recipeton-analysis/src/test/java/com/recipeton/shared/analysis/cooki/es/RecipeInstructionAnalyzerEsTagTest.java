package com.recipeton.shared.analysis.cooki.es;

/*-
 * #%L
 * recipeton-shared
 * %%
 * Copyright (C) 2021 tincomisc
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.recipeton.shared.analysis.configuration.RecipeAnalyzerCookiEsConfiguration;
import com.recipeton.shared.analysis.domain.RecipeAnalysisTag;
import com.recipeton.shared.analysis.test.support.RecipetonAnalyzerTestAssertTrait;
import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.Test;

@Slf4j
public class RecipeInstructionAnalyzerEsTagTest implements RecipetonAnalyzerTestAssertTrait {

    private final RecipeAnalyzerCookiEsConfiguration configuration = new RecipeAnalyzerCookiEsConfiguration();

    @Test
    void testAnalyzeGivenInstructionWithAddExpressionThenAddTagIsAdded() {
        analyzeAndAssertThatTags("Añada el agua", configuration, RecipeAnalysisTag.ADD);
        analyzeAndAssertThatTags("Incorpore el agua", configuration, RecipeAnalysisTag.ADD);
        analyzeAndAssertThatTags("Agregue el agua", configuration, RecipeAnalysisTag.ADD);
    }

    @Test
    void testAnalyzeGivenInstructionWithPlaceExpressionThenPlaceTagIsAdded() {
        analyzeAndAssertThatTags("Vierta el agua", configuration, RecipeAnalysisTag.PLACE);
        analyzeAndAssertThatTags("Coloque el pollo", configuration, RecipeAnalysisTag.PLACE);
    }

    @Test
    void testAnalyzeGivenInstructionWithPlaceInExternalExpressionThenPlaceInExternalTagIsAdded() {
        analyzeAndAssertThatTags("Ponga la lata de confit de pato en una olla con agua", configuration, RecipeAnalysisTag.PLACE, RecipeAnalysisTag.REF_IN_EXTERNAL_ACCESORY);
        analyzeAndAssertThatTags("Coloque la lata de confit de pato en una olla con agua", configuration, RecipeAnalysisTag.PLACE, RecipeAnalysisTag.REF_IN_EXTERNAL_ACCESORY);
        analyzeAndAssertThatTags("Vierta en una sopera", configuration, RecipeAnalysisTag.PLACE, RecipeAnalysisTag.REF_IN_EXTERNAL_ACCESORY);
        analyzeAndAssertThatTags("Ponga en un bol", configuration, RecipeAnalysisTag.PLACE, RecipeAnalysisTag.REF_IN_EXTERNAL_ACCESORY);
        analyzeAndAssertThatTags("Ponga en una sartén", configuration, RecipeAnalysisTag.PLACE, RecipeAnalysisTag.REF_IN_EXTERNAL_ACCESORY);
        analyzeAndAssertThatTags("Coloque en una charola", configuration, RecipeAnalysisTag.PLACE, RecipeAnalysisTag.REF_IN_EXTERNAL_ACCESORY);
        analyzeAndAssertThatTags("Vierta en una tazón", configuration, RecipeAnalysisTag.PLACE, RecipeAnalysisTag.REF_IN_EXTERNAL_ACCESORY);
    }

    @Test
    void testAnalyzeGivenInstructionWithServingExpressionThenServingTagIsAdded() {
        analyzeAndAssertThatTags("Y disfrute", configuration, RecipeAnalysisTag.SERVING);
        analyzeAndAssertThatTags("Disfrute del plato", configuration, RecipeAnalysisTag.SERVING);
        analyzeAndAssertThatTags("Sírvalo caliente", configuration, RecipeAnalysisTag.SERVING);
        analyzeAndAssertThatTags("Sirva immediatamente", configuration, RecipeAnalysisTag.SERVING);
        analyzeAndAssertThatTags("Antes de servir ponga velas", configuration, RecipeAnalysisTag.SERVING, RecipeAnalysisTag.PLACE);
    }

    @Test
    void testAnalyzeGivenInstructionWithWeightExpressionThenWeightTagIsAdded() {
        analyzeAndAssertThatTags("Pese el arroz en el cestillo y luego...", configuration, RecipeAnalysisTag.WEIGH, RecipeAnalysisTag.REF_IN_UTENSIL);
    }


}
