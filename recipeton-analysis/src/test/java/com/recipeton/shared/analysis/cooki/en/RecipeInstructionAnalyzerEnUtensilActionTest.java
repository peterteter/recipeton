package com.recipeton.shared.analysis.cooki.en;

/*-
 * #%L
 * recipeton-shared
 * %%
 * Copyright (C) 2021 tincomisc
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.recipeton.shared.analysis.configuration.RecipeAnalyzerConfiguration;
import com.recipeton.shared.analysis.configuration.RecipeAnalyzerCookiEnConfiguration;
import com.recipeton.shared.analysis.domain.RecipeAnalysisTag;
import com.recipeton.shared.analysis.test.support.RecipetonAnalyzerTestAssertTrait;
import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.Test;

@Slf4j
public class RecipeInstructionAnalyzerEnUtensilActionTest implements RecipetonAnalyzerTestAssertTrait {

    private final RecipeAnalyzerConfiguration configuration = new RecipeAnalyzerCookiEnConfiguration();

    @Test
    void testAnalyzeGivenInstructionWithMisingBowlRemoveLidThenAddsTermMixingUtensilAction() {
        analyzeAndAssertThatRecipeUtensilTermAndTagEquals("Remove mixing bowl lid", RecipeAnalysisTag.MIXING_BOWL, configuration);
    }

    @Test
    void testAnalyzeGivenInstructionWithMisingBowlRemoveThenAddsTermMixingUtensilAction() {
        analyzeAndAssertThatRecipeUtensilTermAndTagEquals("Remove mixing bowl", RecipeAnalysisTag.MIXING_BOWL_REMOVE, configuration);
    }


}
