package com.recipeton.shared.analysis.service.filter;

/*-
 * #%L
 * recipeton-shared
 * %%
 * Copyright (C) 2021 tincomisc
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.recipeton.shared.analysis.domain.RecipeAnalysisTag;
import com.recipeton.shared.analysis.domain.RecipeInstructionAnalysis;
import com.recipeton.shared.analysis.domain.RecipeInstructionMatch;
import com.recipeton.shared.analysis.domain.RecipeInstructionTerm;

import java.util.stream.Stream;

import static com.recipeton.shared.analysis.domain.RecipeAnalysisTag.PLACEMENT_TAGS;
import static com.recipeton.shared.analysis.domain.RecipeAnalysisTag.PLACE_INSTRUCTION_TAGS;

public class IngredientPlacementContextPropagationRecipeInstructionAnalysisFilter implements RecipeInstructionAnalysisFilter {

    private static final String QUERY = "is_prev_placement?";

    public void addTerm(RecipeInstructionAnalysis recipeInstructionAnalysis, RecipeAnalysisTag placement) {
        recipeInstructionAnalysis.addTerm(new RecipeInstructionTerm(placement, RecipeInstructionMatch.ofContext(QUERY)));
    }

    @Override
    public RecipeInstructionAnalysis apply(RecipeInstructionAnalysis recipeInstructionAnalysis) {
        if (!recipeInstructionAnalysis.isTagAnyPresent(PLACE_INSTRUCTION_TAGS) || recipeInstructionAnalysis.isTagAnyPresent(PLACEMENT_TAGS)) {
            return recipeInstructionAnalysis;
        }

        // Think about looking further back... will need to check also for REMOVE operations then
        RecipeInstructionAnalysis prev = recipeInstructionAnalysis.getPrev();
        if (prev != null && prev.isTagPresent(RecipeAnalysisTag.MIXING_BOWL_PUT)) {
            addTerm(recipeInstructionAnalysis, RecipeAnalysisTag.REF_IN_MAIN_COMPARTMENT);
            return recipeInstructionAnalysis;
        }
        if (prev != null && prev.isTagPresent(RecipeAnalysisTag.SIMMERING_BASKET_PUT_INSIDE)) {
            addTerm(recipeInstructionAnalysis, RecipeAnalysisTag.REF_IN_MAIN_COMPARTMENT);
            return recipeInstructionAnalysis;
        }

        while (prev != null) {
            if (prev.isTagAnyPresent(PLACE_INSTRUCTION_TAGS)) {
                // Will be set, or initially or by this filter in previous execution
                RecipeAnalysisTag placement = Stream.of(PLACEMENT_TAGS).filter(prev::isTagPresent).findFirst().orElse(null);
                if (placement != null) {
                    addTerm(recipeInstructionAnalysis, placement);
                    return recipeInstructionAnalysis;
                }
            }
            prev = prev.getPrev();
        }
        return recipeInstructionAnalysis;
    }
}
