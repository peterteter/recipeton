package com.recipeton.shared.analysis.service.filter;

/*-
 * #%L
 * recipeton-shared
 * %%
 * Copyright (C) 2021 tincomisc
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.recipeton.shared.analysis.configuration.RecipeAnalyzerConfiguration;
import com.recipeton.shared.analysis.domain.*;
import me.xdrop.fuzzywuzzy.FuzzySearch;

import java.util.List;

public class RecipeIngredientRecipeInstructionAnalysisFilter implements RecipeInstructionAnalysisFilter {

    private final RecipeIngredientAnalysis recipeIngredientAnalysis;

    private final String ingredientTextSearch;
    private final int ingredientTokenCount;

    private final int matchRatioMin;
    private final int matchRatioMinShort;
    private final int shortSearchSubstringLengthMax;
    private final int searchComparisonMaxLengthDiff;

    public RecipeIngredientRecipeInstructionAnalysisFilter(RecipeIngredientAnalysis recipeIngredientAnalysis, RecipeAnalyzerConfiguration configuration) {
        this(recipeIngredientAnalysis, configuration, recipeIngredientAnalysis.getIngredientNotationTokens());
    }

    public RecipeIngredientRecipeInstructionAnalysisFilter(RecipeIngredientAnalysis recipeIngredientAnalysis, RecipeAnalyzerConfiguration configuration, String... ingredientTokens) {
        this.recipeIngredientAnalysis = recipeIngredientAnalysis;
        this.matchRatioMin = configuration.getInstructionIngredientMatchRatioMin();
        this.matchRatioMinShort = configuration.getMatchRatioMinShort();
        this.shortSearchSubstringLengthMax = configuration.getShortSearchSubstringLengthMax();
        this.searchComparisonMaxLengthDiff = configuration.getSearchComparisonMaxLengthDiff();

        ingredientTextSearch = String.join(" ", ingredientTokens);
        ingredientTokenCount = ingredientTokens.length;
    }

    private void addTerm(RecipeInstructionAnalysis recipeInstructionAnalysis, int fromTokenIndex, int toTokenIndex, int ratio) {
        RecipeInstructionTokenization tokenization = recipeInstructionAnalysis.getTokenization();
        String source = tokenization.getSource();
        RecipeInstructionTokenization.Position positionFrom = tokenization.getPositions().get(fromTokenIndex);
        RecipeInstructionTokenization.Position positionTo = tokenization.getPositions().get(toTokenIndex - 1);
        String matchText = source.substring(positionFrom.getOffset(), positionTo.getOffset() + positionTo.getLength()).trim();
        RecipeInstructionMatch match = new RecipeInstructionMatch(positionFrom.getOffset(), matchText, ratio, "fuzz?" + ingredientTextSearch);
        recipeInstructionAnalysis.addTerm(new RecipeIngredientTerm(recipeIngredientAnalysis, match, false));
    }

    @Override
    public RecipeInstructionAnalysis apply(RecipeInstructionAnalysis recipeInstructionAnalysis) {
        if (recipeInstructionAnalysis.isTagPresent(RecipeAnalysisTag.ALL_INGREDIENTS)) {
            recipeInstructionAnalysis.addTerm(RecipeIngredientTerm.ofMatchOnAll(recipeIngredientAnalysis, "__ALL__"));
            return recipeInstructionAnalysis;
        }

        RecipeInstructionTokenization tokenization = recipeInstructionAnalysis.getTokenization();
        int tokenizationLength = tokenization.getLength();
        for (int i = 0; i < tokenizationLength; i++) {
            int fromTokenIndex = i;
            int toTokenIndex = Math.min(i + ingredientTokenCount, tokenizationLength);

            List<String> tokens = tokenization.getTokens().subList(fromTokenIndex, toTokenIndex);
            String recipeInstructionTokenSubstring = String.join(" ", tokens);

            if (isSimilarLength(recipeInstructionTokenSubstring)) {
                int ratio = FuzzySearch.weightedRatio(ingredientTextSearch, recipeInstructionTokenSubstring);
                if (ratio >= getRatioMin(recipeInstructionTokenSubstring)) {
                    addTerm(recipeInstructionAnalysis, fromTokenIndex, toTokenIndex, ratio);
                }
            }

        }
        return recipeInstructionAnalysis;
    }

    public int getRatioMin(String recipeInstructionTokenSubstring) {
        return recipeInstructionTokenSubstring.length() <= shortSearchSubstringLengthMax ? this.matchRatioMinShort : this.matchRatioMin;
    }

    public boolean isSimilarLength(String recipeInstructionTokenSubstring) {
        return Math.abs(recipeInstructionTokenSubstring.length() - ingredientTextSearch.length()) <= searchComparisonMaxLengthDiff;
    }
}
