package com.recipeton.shared.analysis.domain;

/*-
 * #%L
 * recipeton-analysis
 * %%
 * Copyright (C) 2021 tincomisc
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import java.util.List;
import java.util.Optional;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

import static com.recipeton.shared.analysis.domain.RecipeInstructionMatch.SCORE_MAX;
import static com.recipeton.shared.util.StreamUtil.toStream;

public class RecipeInstructionRegexMatcher implements RecipeInstructionMatcher {

    private final Pattern pattern;

    public RecipeInstructionRegexMatcher(String regex) {
        this(Pattern.compile(regex));
    }

    public RecipeInstructionRegexMatcher(Pattern pattern) {
        this.pattern = pattern;
    }

    public static List<RecipeInstructionMatcher> toRecipeInstructionMatchers(String... regexExpressions) {
        return toStream(regexExpressions)
                .map(RecipeInstructionRegexMatcher::new)
                .collect(Collectors.toList());
    }

    @Override
    public Optional<RecipeInstructionMatch> findMatch(RecipeInstructionAnalysis recipeInstructionAnalysis) {
        Matcher matcher = pattern.matcher(recipeInstructionAnalysis.getText());
        if (matcher.find()) {
            return Optional.of(new RecipeInstructionMatch(matcher.start(), matcher.group(), SCORE_MAX, "match?" + pattern));
        }

        return Optional.empty();
    }
}
