package com.recipeton.shared.analysis.service.filter;

/*-
 * #%L
 * recipeton-shared
 * %%
 * Copyright (C) 2021 tincomisc
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.recipeton.shared.analysis.domain.RecipeAnalysisTag;
import com.recipeton.shared.analysis.domain.RecipeInstructionAnalysis;
import com.recipeton.shared.analysis.domain.RecipeInstructionMatch;
import com.recipeton.shared.analysis.domain.RecipeInstructionTerm;

import static com.recipeton.shared.analysis.domain.RecipeAnalysisTag.*;

public class IngredientWeighOnTopContextPropagationRecipeInstructionAnalysisFilter implements RecipeInstructionAnalysisFilter {

    private static final String QUERY = "is_prev_on_top_of_compartment?";

    private void addTerm(RecipeInstructionAnalysis recipeInstructionAnalysis, RecipeAnalysisTag tag) {
        recipeInstructionAnalysis.addTerm(new RecipeInstructionTerm(tag, RecipeInstructionMatch.ofContext(QUERY)));
    }

    @Override
    public RecipeInstructionAnalysis apply(RecipeInstructionAnalysis recipeInstructionAnalysis) {
        if (!recipeInstructionAnalysis.isTagPresent(WEIGH) || recipeInstructionAnalysis.isTagAnyPresent(REF_IN_MAIN_COMPARTMENT, REF_TOP_OF_MAIN_COMPARTMENT)) {
            return recipeInstructionAnalysis;
        }

        // Think about looking further back... will need to check also for REMOVE operations then
        RecipeInstructionAnalysis prev = recipeInstructionAnalysis.getPrev();
        if (prev != null && prev.isTagPresent(MIXING_BOWL_PUT)) {
            addTerm(recipeInstructionAnalysis,  REF_TOP_OF_MAIN_COMPARTMENT);
            return recipeInstructionAnalysis;
        }
        if (prev != null && prev.isTagPresent(SIMMERING_BASKET_PUT_INSIDE)) {
            addTerm(recipeInstructionAnalysis, REF_IN_MAIN_COMPARTMENT);
            return recipeInstructionAnalysis;
        }

        while (prev != null) {
            if (prev.isTagAnyPresent(PLACE_INSTRUCTION_TAGS)) {
                if (prev.isTagPresent(REF_TOP_OF_MAIN_COMPARTMENT)) {
                    addTerm(recipeInstructionAnalysis, REF_TOP_OF_MAIN_COMPARTMENT);
                }
                // else strange scenario. No context. Weigh where???
                return recipeInstructionAnalysis;
            }
            prev = prev.getPrev();
        }
        return recipeInstructionAnalysis;
    }
}
