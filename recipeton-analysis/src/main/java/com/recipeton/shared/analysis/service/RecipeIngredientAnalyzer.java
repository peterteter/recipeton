package com.recipeton.shared.analysis.service;

/*-
 * #%L
 * recipeton-shared
 * %%
 * Copyright (C) 2021 tincomisc
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.recipeton.shared.analysis.configuration.RecipeAnalyzerConfiguration;
import com.recipeton.shared.analysis.domain.RecipeIngredientAnalysis;
import com.recipeton.shared.util.TextUtil;
import com.recipeton.shared.util.VulgarFraction;

import java.math.BigDecimal;
import java.util.List;
import java.util.Optional;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

import static com.recipeton.shared.util.StreamUtil.toStream;

public interface RecipeIngredientAnalyzer {

    Pattern NUMERIC_AND_VULGAR_FRACTION_WITH_SPACE = Pattern.compile("(\\d+) ([¼-¾⅐-⅞])");
    Pattern NUMERIC_AND_VULGAR_FRACTION = Pattern.compile("(\\d+)([¼-¾⅐-⅞])");

    String RANGE_SEPARATOR = "-";
    Set<String> INGREDIENT_PREPARATION_SEPARATOR_SYMBOLS = Set.of(
            ".",
            ","
    );
    Set<String> INGREDIENT_PREPARATION_START_SYMBOLS = Set.of(
            "("
    );

    RecipeIngredientAnalysis analyze(String text);

    RecipeAnalyzerConfiguration getConfiguration();

    default Optional<BigDecimal> getMagnitude(String token) {
        if (TextUtil.isNumeric(token)) {
            return Optional.of(new BigDecimal(token));
        }

        Matcher numericAndVulgarFraction = NUMERIC_AND_VULGAR_FRACTION.matcher(token);
        if (numericAndVulgarFraction.find()) {
            return Optional.of(new BigDecimal(numericAndVulgarFraction.group(1)).add(VulgarFraction.by(numericAndVulgarFraction.group(2)).getValue()));
        }

        Matcher vulgarFraction = VulgarFraction.PATTERN.matcher(token);
        if (vulgarFraction.find()) {
            return Optional.of(VulgarFraction.by(token).getValue());
        }

        return Optional.ofNullable(getConfiguration().getNumericStrings().get(token.toLowerCase())).map(BigDecimal::new);
    }

    default List<RecipeIngredientAnalysis> toRecipeIngredientAnalyses(List<String> texts) {
        return toStream(texts).map(this::analyze).collect(Collectors.toList());
    }
}
