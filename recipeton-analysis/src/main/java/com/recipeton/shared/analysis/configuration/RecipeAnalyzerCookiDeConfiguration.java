package com.recipeton.shared.analysis.configuration;

/*-
 * #%L
 * recipeton-shared
 * %%
 * Copyright (C) 2021 tincomisc
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.recipeton.shared.analysis.domain.*;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.tuple.Pair;

import java.util.Set;
import java.util.function.Function;
import java.util.regex.Pattern;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static com.recipeton.shared.analysis.configuration.CookiConstants.*;
import static com.recipeton.shared.analysis.domain.RecipeInstructionRegexMatcher.toRecipeInstructionMatchers;
import static com.recipeton.shared.analysis.service.preprocess.RecipeInstructionSplitter.DO_NOT_SPLIT;
import static com.recipeton.shared.util.MapUtil.toMap;
import static com.recipeton.shared.util.MapUtil.toMapP;
import static com.recipeton.shared.util.TextUtil.*;
import static java.util.List.of;
import static org.apache.commons.lang3.StringUtils.containsIgnoreCase;

// WIP! COPIED FROM Es
public class RecipeAnalyzerCookiDeConfiguration extends RecipeAnalyzerConfiguration {

    public static final String UNIT_STUCK = "stück";

    public RecipeAnalyzerCookiDeConfiguration() {
        super("COOKI_DE", rd -> containsIgnoreCase(rd.getLocale(), "de") && containsIgnoreCase(rd.getSource(), "cooki"));

        this.setStopWords(Set.of(
                "a", "ab", "aber", "ach", "acht", "achte", "achten", "achter", "achtes", "ag", "alle", "allein", "allem", "allen", "aller", "allerdings", "alles", "allgemeinen", "als", "also", "am", "an", "andere", "anderen", "andern", "anders", "au", "auch", "auf", "aus", "ausser", "außer", "ausserdem", "außerdem", "b", "bald", "bei", "beide", "beiden", "beim", "beispiel", "bekannt", "bereits", "besonders", "besser", "besten", "bin", "bis", "bisher", "bist", "c", "d", "da", "dabei", "dadurch", "dafür", "dagegen", "daher", "dahin", "dahinter", "damals", "damit", "danach", "daneben", "dank", "dann", "daran", "darauf", "daraus", "darf", "darfst", "darin", "darüber", "darum", "darunter", "das", "dasein", "daselbst", "dass", "daß", "dasselbe", "davon", "davor", "dazu", "dazwischen", "dein", "deine", "deinem", "deiner", "dem", "dementsprechend", "demgegenüber", "demgemäss", "demgemäß", "demselben", "demzufolge", "den", "denen", "denn", "denselben", "der", "deren", "derjenige", "derjenigen", "dermassen", "dermaßen", "derselbe", "derselben", "des", "deshalb", "desselben", "dessen", "deswegen", "d.h", "dich", "die", "diejenige", "diejenigen", "dies", "diese", "dieselbe", "dieselben", "diesem", "diesen", "dieser", "dieses", "dir", "doch", "dort", "drei", "drin", "dritte", "dritten", "dritter", "drittes", "du", "durch", "durchaus", "dürfen", "dürft", "durfte", "durften", "e", "eben", "ebenso", "ehrlich", "ei", "ei,", "eigen", "eigene", "eigenen", "eigener", "eigenes", "ein", "einander", "eine", "einem", "einen", "einer", "eines", "einige", "einigen", "einiger", "einiges", "einmal", "eins", "elf", "en", "ende", "endlich", "entweder", "er", "Ernst", "erst", "erste", "ersten", "erster", "erstes", "es", "etwa", "etwas", "euch", "f", "früher", "fünf", "fünfte", "fünften", "fünfter", "fünftes", "für", RecipeUnitDefinition.UNIT_GRAM, "gab", "ganz", "ganze", "ganzen", "ganzer", "ganzes", "gar", "gedurft", "gegen", "gegenüber", "gehabt", "gehen", "geht", "gekannt", "gekonnt", "gemacht", "gemocht", "gemusst", "genug", "gerade", "gern", "gesagt", "geschweige", "gewesen", "gewollt", "geworden", "gibt", "ging", "gleich", "gott", "gross", "groß", "grosse", "große", "grossen", "großen", "grosser", "großer", "grosses", "großes", "gut", "gute", "guter", "gutes", "h", "habe", "haben", "habt", "hast", "hat", "hatte", "hätte", "hatten", "hätten", "heisst", "her", "heute", "hier", "hin", "hinter", "hoch", "i", "ich", "ihm", "ihn", "ihnen", "ihr", "ihre", "ihrem", "ihren", "ihrer", "ihres", "im", "immer", "in", "indem", "infolgedessen", "ins", "irgend", "ist", "j", "ja", "jahr", "jahre", "jahren", "je", "jede", "jedem", "jeden", "jeder", "jedermann", "jedermanns", "jedoch", "jemand", "jemandem", "jemanden", "jene", "jenem", "jenen", "jener", "jenes", "jetzt", "k", "kam", "kann", "kannst", "kaum", "kein", "keine", "keinem", "keinen", "keiner", "kleine", "kleinen", "kleiner", "kleines", "kommen", "kommt", "können", "könnt", "konnte", "könnte", "konnten", "kurz", "l", "lang", "lange", "leicht", "leide", "lieber", "los", "m", "machen", "macht", "machte", "mag", "magst", "mahn", "man", "manche", "manchem", "manchen", "mancher", "manches", "mann", "mehr", "mein", "meine", "meinem", "meinen", "meiner", "meines", "mensch", "menschen", "mich", "mir", "mit", "mittel", "mochte", "möchte", "mochten", "mögen", "möglich", "mögt", "morgen", "muss", "muß", "müssen", "musst", "müsst", "musste", "mussten", "n", "na", "nach", "nachdem", "nahm", "natürlich", "neben", "nein", "neue", "neuen", "neun", "neunte", "neunten", "neunter", "neuntes", "nicht", "nichts", "nie", "niemand", "niemandem", "niemanden", "noch", "nun", "nur", "o", "ob", "oben", "oder", "offen", "oft", "ohne", "Ordnung", "p", "q", "r", "recht", "rechte", "rechten", "rechter", "rechtes", "richtig", "rund", "s", "sa", "sache", "sagt", "sagte", "sah", "satt", "schlecht", "Schluss", "schon", "sechs", "sechste", "sechsten", "sechster", "sechstes", "sehr", "sei", "seid", "seien", "sein", "seine", "seinem", "seinen", "seiner", "seines", "seit", "seitdem", "selbst", "sich", "sie", "sieben", "siebente", "siebenten", "siebenter", "siebentes", "sind", "so", "solang", "solche", "solchem", "solchen", "solcher", "solches", "soll", "sollen", "sollte", "sollten", "sondern", "sonst", "sowie", "später", "statt", "t", "tag", "tage", "tagen", "tat", "teil", "tel", "tritt", "trotzdem", "tun", "u", "über", "überhaupt", "übrigens", "uhr", "um", "und", "und?", "uns", "unser", "unsere", "unserer", "unter", "v", "vergangenen", "viel", "viele", "vielem", "vielen", "vielleicht", "vier", "vierte", "vierten", "vierter", "viertes", "vom", "von", "vor", "w", "wahr?", "während", "währenddem", "währenddessen", "wann", "war", "wäre", "waren", "wart", "warum", "was", "wegen", "weil", "weit", "weiter", "weitere", "weiteren", "weiteres", "welche", "welchem", "welchen", "welcher", "welches", "wem", "wen", "wenig", "wenige", "weniger", "weniges", "wenigstens", "wenn", "wer", "werde", "werden", "werdet", "wessen", "wie", "wieder", "will", "willst", "wir", "wird", "wirklich", "wirst", "wo", "wohl", "wollen", "wollt", "wollte", "wollten", "worden", "wurde", "würde", "wurden", "würden", "x", "y", "z", "z.b", "zehn", "zehnte", "zehnten", "zehnter", "zehntes", "zeit", "zu", "zuerst", "zugleich", "zum", "zunächst", "zur", "zurück", "zusammen", "zwanzig", "zwar", "zwei", "zweite", "zweiten", "zweiter", "zweites", "zwischen", "zwölf", "euer", "eure", "hattest", "hattet", "jedes", "mußt", "müßt", "sollst", "sollt", "soweit", "weshalb", "wieso", "woher", "wohin"))
                .setRecipeUnitDefinitions(
                        new RecipeUnitDefinition("kg"),
                        new RecipeUnitDefinition(RecipeUnitDefinition.UNIT_GRAM),
                        new RecipeUnitDefinition(UNIT_STUCK),
                        new RecipeUnitDefinition("l"),
                        new RecipeUnitDefinition("ml"),
                        new RecipeUnitDefinition("dose", "dosen"),
                        new RecipeUnitDefinition("el"),
                        new RecipeUnitDefinition("tl"),
                        new RecipeUnitDefinition("prise", "prisen"),
                        new RecipeUnitDefinition("stängel")
                )
                .setUnitPieceDefault(UNIT_STUCK)
                .setUnitWeightedDefault(RecipeUnitDefinition.UNIT_GRAM)
                .setNumericStrings(toMap(
                        "eins", ONE,
                        "zwei", TWO,
                        "drei", THREE,
                        "vier", FOUR,
                        "fünf", FIVE,
                        "sechs", SIX,
                        "sieben", SEVEN,
                        "acht", EIGHT,
                        "neun", NINE,
                        "zehn", TEN
                ))
                .setIngredientPreparationTokens(Set.of(
                        "abgeschnitten",
                        "abgetropft",
                        "abgezupft",
                        "altbacken",
                        "am",
                        "ausgelöst",
                        "eingeweicht",
                        "entkernt",
                        "etwas",
                        "fest",
                        "frisch",
                        "frische",
                        "gekauft",
                        "geräuchert",
                        "geschnetzelt",
                        "getrocknet",
                        "geviertelt",
                        "gewürfelt",
                        "gut",
                        "halbiert",
                        "in",
                        "mehlig",
                        "ohne",
                        "parboiled",
                        "selbstgemacht",
                        "weich"
// "gut ausgedrückt",
// "in mundgerechten Stücken",
//                "am Stück",
//                "etwas mehr nach Geschmack",
//                "etwas mehr zum Einfetten",
//                "fest kochend",
//                "frisch gemahlen",
//                "in Scheiben",
//                "in Streifen",
//                "in Stücken",
//                "in Wedges",
//                "in dicken Streifen",
//                "in kleinen Stücken",
//                "mehlig kochend",
//                "ohne Stiele",
//                "ohne Vorkochen",
                ))
                .setIngredientAlternativeSeparatorTokenSet("oder")
                .setIngredientOptionalIndicatorTokens(Set.of(
                )) //                "(optional)"
                .setInstructionSplitToNextTokenSequences(
                        Stream.concat(
                                Stream.of(
                                        Pair.of(" approx.", DO_NOT_SPLIT), // Careful. Due to limitationa all need to start from space or punctuation
                                        Pair.of(".", "")
                                ),
                                Stream.of(
                                        "dünste"
                                ).map(String::toLowerCase).flatMap(v ->
                                        Stream.of(
                                                " und ", // Careful. Due to limitationa all need to start from space or punctuation
                                                ", "
                                        ).map(s -> Pair.of(s + v, StringUtils.capitalize(v)))
                                )
                        ).collect(Collectors.toMap(Pair::getLeft, Pair::getRight))
                )
                .setInstructionSplitToCurrentTokenSequences(
                        Stream.concat(
                                Stream.empty(),
                                Stream.of(
                                        "abschmecken",
                                        "abspülen",
                                        "auffangen",
                                        "auffüllen",
                                        "aufrollen",
                                        "aufsetzen",
                                        "bestreichen",
                                        "dünsten",
                                        "einhängen",
                                        "einsetzen",
                                        "einwiegen",
                                        "füllen",
                                        "garen",
                                        "geben",
                                        "hacken",
                                        "herausnehmen",
                                        "kochen",
                                        "lassen",
                                        "leeren",
                                        "legen",
                                        "machen",
                                        "pürieren",
                                        "reichen",
                                        "servieren",
                                        "stellen",
                                        "umfüllen",
                                        "vermischen",
                                        "verschließen",
                                        "wiegen",
                                        "würzen",
                                        "zerkleinern",
                                        "zugeben"
                                ).map(String::toLowerCase).flatMap(v ->
                                        Stream.of(
                                                " und ", // Careful. Due to limitationa all need to start from space or punctuation
                                                ", "
                                        ).map(s -> Pair.of(v + s, " " + v))
                                )
                        ).collect(Collectors.toMap(Pair::getLeft, Pair::getRight))
                )
                .setUtensilTokens(Set.of(
                ))
                .setCommandProgramPredicates(toMap(
                        RecipeCommandProgram.DOUGH, stringPredicatePatterns("(?i)" + TM_DOUGH + "|dough"),
                        RecipeCommandProgram.FERMENT, of(),
                        RecipeCommandProgram.HEAT_ONLY, of(),
                        RecipeCommandProgram.SOUS_VIDE, of(),
                        RecipeCommandProgram.TURBO, stringPredicatePatterns("(?i)turbo")
                ))
                .setCommandDurationPredicates(toMap(
                        HALF_SECOND, stringPredicatePatterns("^0\\.5 Sek\\.")
                ))
                .setCommandDurationHoursPattern(stringPattern("^([0-9]+) Std\\."))
                .setCommandDurationMinutesPattern(stringPattern("^([0-9]+) Min\\."))
                .setCommandDurationSecondsPattern(stringPattern("^([0-9]+) Sek\\."))
                .setCommandTemperaturePredicates(toMap(
                        STEAMER_TEMPERATURE, stringPredicatePatterns("(?i)varoma|steamer")
                ))
                .setCommandTemperaturePattern(stringPattern("^([0-9]+)°C"))
                .setCommandSpeedPredicates(toMap(
                        COMMAND_SPEED_SLOW_VALUE, stringPredicatePatterns("(?i)stuffe " + TM_SLOW + "|stuffe slow")
                ))
                .setCommandRotationReversePredicates(stringPredicatePatterns("(?i)" + TM_REVERSE + "|reverse")).setCommandSpeedPattern(stringPattern("stuffe ([//.0-9]+)"))
                .setCommandRepetitionFromPattern(stringPattern("^([0-9]+) (?:mal)"))
                .setCommandRepetitionFromToPattern(stringPattern("^([0-9]+)-([0-9]+) mal"))
                .setInstructionMatchers(toMapP(
                        Pair.of(RecipeAnalysisTag.ADD, toRecipeInstructionMatchers("(?i) zugeben")),
                        Pair.of(RecipeAnalysisTag.PLACE, toRecipeInstructionMatchers("(?i) geben")),

// Complete. All very important. Be careful with regex overlaps!
//                        Pair.of(RecipeAnalysisTag.ADD_BACK, toRecipeInstructionMatchers("(?i)regrese|retorne")),
//                        Pair.of(RecipeAnalysisTag.WEIGHT, toRecipeInstructionMatchers()),
//                        Pair.of(RecipeAnalysisTag.ON_TOP_OF_MAIN_COMPARTMENT, toRecipeInstructionMatchers("(?i)onto mixing bowl lid")),
                        Pair.of(RecipeAnalysisTag.REF_IN_MAIN_COMPARTMENT, toRecipeInstructionMatchers("(?i)in den mixtopf")),
//                        Pair.of(RecipeAnalysisTag.IN_ACCESORY, toRecipeInstructionMatchers()),
//                        Pair.of(RecipeAnalysisTag.ALL_INGREDIENTS, toRecipeInstructionMatchers()),
//                        Pair.of(RecipeAnalysisTag.ALL_INGREDIENTS_REMAINING, toRecipeInstructionMatchers()),
//                        Pair.of(RecipeAnalysisTag.PARALLEL, toRecipeInstructionMatchers()),
                        Pair.of(RecipeAnalysisTag.SERVING, toRecipeInstructionMatchers("(?i)servieren")),

                        // Utensils. Complete!!
//                        Pair.of(RecipeAnalysisTag.OVEN_PREHEAT, toRecipeInstructionMatchers("(?i)preheat.*oven")),
//                        Pair.of(RecipeAnalysisTag.REFRIGERATE, toRecipeInstructionMatchers("(?i)(keep|place|store).*(refrigerator|fridge|refrigerated)")),
//
//                        Pair.of(RecipeAnalysisTag.FREEZE, toRecipeInstructionMatchers("(?i)chill.*fridge")),
//
//                        Pair.of(RecipeAnalysisTag.SUMMERING_BASKET, toRecipeInstructionMatchers("(?i)using.*simmering basket.*(drain|strain)")),
//                        Pair.of(RecipeAnalysisTag.SIMMERING_BASKET_PUT, toRecipeInstructionMatchers("(?i)(?:insert).*simmering basket")),
//                        Pair.of(RecipeAnalysisTag.SIMMERING_BASKET_REMOVE, toRecipeInstructionMatchers("(?i)(?:remove).*simmering basket")),
//                        Pair.of(RecipeAnalysisTag.SIMMERING_BASKET_AS_LID_PUT, toRecipeInstructionMatchers("(?i)placing simmering basket instead of measuring cup")), // Needs context analysis to place before tm step
//                        Pair.of(RecipeAnalysisTag.SIMMERING_BASKET_AS_LID_REMOVE, defineMe()),
//                        Pair.of(RecipeAnalysisTag.SIMMERING_BASKET_REMOVE_MEASURING_CUP_PUT, defineMe()),
//
//                        Pair.of(RecipeAnalysisTag.MIXING_BOWL, toRecipeInstructionMatchers("(?i)(?:remove).*mixing.*bowl lid")),
//                        Pair.of(RecipeAnalysisTag.MIXING_BOWL_PUT, defineMe()),
//                        Pair.of(RecipeAnalysisTag.MIXING_BOWL_REMOVE, toRecipeInstructionMatchers("(?i)(?:remove).*(?:mixing bowl(?!.*? lid))")),
//                        Pair.of(RecipeAnalysisTag.MIXING_BOWL_EMPTY, toRecipeInstructionMatchers("(?i)(?:empty).*mixing.*bowl")),
//                        Pair.of(RecipeAnalysisTag.MIXING_BOWL_CLEAN, toRecipeInstructionMatchers("(?i)(?:clean|rinse).*mixing.*bowl")),
//
//                        Pair.of(RecipeAnalysisTag.TRANSFER_TO_BOWL, toRecipeInstructionMatchers("(?i)transfer.*(?:into|onto|to).*(?:bowl|container|jar|jug|sheet|tray)")),
//
//                        Pair.of(RecipeAnalysisTag.MEASURING_CUP, defineMe()),
//                        Pair.of(RecipeAnalysisTag.MEASURING_CUP_NOTUSE, toRecipeInstructionMatchers("(?i)(?:remove|without).*measuring cup")),
//                        Pair.of(RecipeAnalysisTag.MEASURING_CUP_PUT, toRecipeInstructionMatchers("(?i)insert.*measuring cup")),
//                        Pair.of(RecipeAnalysisTag.MEASURING_CUP_HOLD_VIBRATION, toRecipeInstructionMatchers("(?i)hold.*measuring cup")),
//
//                        Pair.of(RecipeAnalysisTag.BUTTERFLY, defineMe()),
//                        Pair.of(RecipeAnalysisTag.BUTTERFLY_PUT, toRecipeInstructionMatchers("(?i)insert.*butterfly")),
//                        Pair.of(RecipeAnalysisTag.BUTTERFLY_REMOVE, toRecipeInstructionMatchers("(?i)remove.*butterfly")),
//
//                        Pair.of(RecipeAnalysisTag.SPATULA, defineMe()),
//                        Pair.of(RecipeAnalysisTag.SPATULA_MIX, toRecipeInstructionMatchers("(?i)mix with spatula")),
//                        Pair.of(RecipeAnalysisTag.SPATULA_SCRAP_SIDE, toRecipeInstructionMatchers("(?i)scrape.*bowl.*spatula")),
//                        Pair.of(RecipeAnalysisTag.SPATULA_MIX_WELL, toRecipeInstructionMatchers("(?i)mix.*well with spatula")),
//
//                        Pair.of(RecipeAnalysisTag.STEAMER, toRecipeInstructionMatchers("(?i)close.*(?:varoma|steamer)")),
//                        Pair.of(RecipeAnalysisTag.STEAMER_PUT, toRecipeInstructionMatchers("(?i)(?:place|return).*(?:varoma|steamer).*(?:to|in).*position")),
//                        Pair.of(RecipeAnalysisTag.STEAMER_REMOVE, toRecipeInstructionMatchers("(?i)remove.*(?:varoma|steamer)")),
//                        Pair.of(RecipeAnalysisTag.STEAMER_TRAY_PUT, toRecipeInstructionMatchers("(?i)insert.*(?:varoma|steamer).*tray")),
//                        Pair.of(RecipeAnalysisTag.STEAMER_TRAY_REMOVE, toRecipeInstructionMatchers("(?i)remove.*(?:varoma|steamer).*tray")),
//                        Pair.of(RecipeAnalysisTag.STEAMER_DISH_PUT, toRecipeInstructionMatchers("(?i)place.*into (varoma|steamer) dish")),
//                        Pair.of(RecipeAnalysisTag.STEAMER_DISH_REMOVE, toRecipeInstructionMatchers("(?i)remove.*(varoma|steamer) dish")),
//
//                        Pair.of(RecipeAnalysisTag.THERMOMIX_PARTS, toRecipeInstructionMatchers("(?i)insert.*(?:blade cover)")),
//                        Pair.of(RecipeAnalysisTag.USEFUL_ITEMS, toRecipeInstructionMatchers()),
                        Pair.of(RecipeAnalysisTag.KITCHEN_EQUIPMENT, toRecipeInstructionMatchers())
                ))
                .setRecipeDifficultDefinitionPredicates(toMap(
                        RecipeDifficultyDefinition.EASY, stringPredicatePattern("(?i)einfach"),
                        RecipeDifficultyDefinition.MEDIUM, stringPredicatePattern("(?i)medium"),
                        RecipeDifficultyDefinition.ADVANCED, stringPredicatePattern("(?i)aufwändig")
                ))
                .setRecipePriceDefinitionPredicates(toMap(
                        RecipePriceDefinition.LOW, stringPredicatePattern("(?i)preiswert|geldsparend")
                ))
                .addRecipeCategoryByPrincipal(BASIC, "Grundrezepte")
                .addRecipeCategoryByPrincipal(START, "Vorspeisen und Salate")
                .addRecipeCategoryByPrincipal(SOUP, "Suppen und Suppeneinlagen",
                        "Suppen")
                .addRecipeCategoryByPrincipal(PASTARICE, "Pasta und Reisgerichte",
                        "Pasta- und Reisgerichte",
                        "Nudel- und Reisgerichte")
                .addRecipeCategoryByPrincipal(MAINMEAT, "Hauptspeisen mit Fleisch und Geflügel",
                        "Hauptgerichte mit Fleisch")
                .addRecipeCategoryByPrincipal(MAINFISH, "Hauptspeisen mit Fisch und Meeresfrüchten",
                        "Hauptgerichte mit Fisch")
                .addRecipeCategoryByPrincipal(MAINVEG, "Hauptspeisen, vegetarisch")
                .addRecipeCategoryByPrincipal(MAINOTHER, "Hauptgerichte",
                        "Hauptgerichte, sonstige")
                .addRecipeCategoryByPrincipal(SIDE, "Beilagen")
                .addRecipeCategoryByPrincipal(BAKE, "Brot und Brötchen",
                        "Backen",
                        "Brot und Gebäck")
                .addRecipeCategoryByPrincipal(BAKE_SAVORY, "Backen, herzhaft",
                        "Backen, pikant")
                .addRecipeCategoryByPrincipal(DESSERT, "Desserts, Pâtisserie und Süssigkeiten",
                        "Desserts",
                        "Desserts und Süßigkeiten")
                .addRecipeCategoryByPrincipal(BAKE_SWEET, "Backen, süß",
                        "Backen, süss")
                .addRecipeCategoryByPrincipal(SAUCE_SWEET, "Saucen, Dips und Brotaufstriche, süß",
                        "Saucen, Dips und Aufstriche, süß",
                        "Saucen, Dips und Brotaufstriche, süss")
                .addRecipeCategoryByPrincipal(DRINK, "Getränke")
                .addRecipeCategoryByPrincipal(SNACK, "Snacks",
                        "Snacks und Finger Food",
                        "Snacks und Fingerfood")
                .addRecipeCategoryByPrincipal(SAUCE_SAVO, "Saucen, Dips und Brotaufstriche",
                        "Saucen, Dips und Brotaufstriche - pikant",
                        "Saucen, Dips und Aufstriche, herzhaft",
                        "Saucen, Dips und Brotaufstriche, herzhaft")
                .addRecipeCategoryByPrincipal(BABY, "Baby`s Beikost",
                        "Säuglingsnahrung")
                .addRecipeCategoryByPrincipal(BREAK, "Frühstück")
                .addRecipeCategoryByPrincipal(MENU, "Menüs und mehr")
                .setRecipeCategoryNamePreprocessFunction(
                        new Function<>() {
                            private final Pattern cleanupPattern = Pattern.compile("(?i)" + String.join("|",
                                    "kochen",
                                    "küche"
                            ));

                            @Override
                            public String apply(String s) {
                                return RECIPE_CATEGORY_NAME_FORMAT_DEFAULT_FUNCTION.apply(removeAll(s, cleanupPattern));
                            }
                        }
                );
    }
}
