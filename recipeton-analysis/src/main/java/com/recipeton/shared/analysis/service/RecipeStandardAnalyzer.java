package com.recipeton.shared.analysis.service;

/*-
 * #%L
 * recipeton-shared
 * %%
 * Copyright (C) 2021 tincomisc
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.recipeton.shared.analysis.configuration.RecipeAnalyzerConfiguration;
import com.recipeton.shared.analysis.domain.RecipeDifficultyDefinition;
import com.recipeton.shared.analysis.domain.RecipeMeasurement;
import com.recipeton.shared.analysis.domain.RecipePriceDefinition;
import com.recipeton.shared.analysis.domain.RecipeUtensilAnalysis;
import com.recipeton.shared.domain.RecipeDefinition;
import com.recipeton.shared.util.TextUtil;
import lombok.extern.slf4j.Slf4j;
import me.xdrop.fuzzywuzzy.FuzzySearch;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.tuple.Pair;

import java.math.BigDecimal;
import java.util.HashMap;
import java.util.Map;
import java.util.stream.Collectors;

import static com.recipeton.shared.service.RecipeCategoryDefinitionUtil.RECIPE_CATEGORY_EXTRA_PREFIX;
import static com.recipeton.shared.util.StreamUtil.toStream;

@Slf4j
public class RecipeStandardAnalyzer implements RecipeAnalyzer {

    private final RecipeAnalyzerConfiguration configuration;
    private final RecipeIngredientAnalyzer recipeIngredientAnalyzer;
    private final RecipeInstructionAnalyzer recipeInstructionAnalyzer;

    private final Map<String, Pair<String, String>> recipeCategoryUidPreferredNamePairsByFormattedName;

    public RecipeStandardAnalyzer(RecipeAnalyzerConfiguration configuration, RecipeIngredientAnalyzer recipeIngredientAnalyzer, RecipeInstructionAnalyzer recipeInstructionAnalyzer) {
        this.configuration = configuration;
        this.recipeIngredientAnalyzer = recipeIngredientAnalyzer;
        this.recipeInstructionAnalyzer = recipeInstructionAnalyzer;

        recipeCategoryUidPreferredNamePairsByFormattedName = new HashMap<>();
        this.configuration.getRecipeCategoryNamesByUid().forEach((preferredUid, recipeCategoryNameVariants) -> {
            if (recipeCategoryNameVariants.size() == 0) {
                log.warn("Analyzer configured category main variant missing. Recipe category will be ignored until added to config. id={}, uid={}", configuration.getId(), preferredUid);
                return;
            }
            String preferredName = recipeCategoryNameVariants.get(0); // Do  not modify 1st in the list. Its the literally preferred name
            recipeCategoryNameVariants.stream()
                    .map(this::formatRecipeCategoryNameSimplified)
                    .forEach(fn -> recipeCategoryUidPreferredNamePairsByFormattedName.put(fn, Pair.of(preferredUid, preferredName)));
        });
    }

    public RecipeStandardAnalyzer(RecipeAnalyzerConfiguration configuration) {
        this(configuration, new RecipeIngredientStandardAnalyzer(configuration), new RecipeInstructionAnalyzer(configuration));
    }

    @Override
    public String analyzeLocale(RecipeDefinition recipeDefinition) {
        return configuration.getRecipeLocaleLabelFunction().apply(recipeDefinition);
    }


    @Override
    public RecipeDifficultyDefinition analyzeRecipeDifficulty(RecipeDefinition recipeDefinition) {
        return configuration.getRecipeDifficultDefinitionPredicates().entrySet().stream()
                .filter(e -> e.getValue().test(recipeDefinition.getDifficulty()))
                .map(Map.Entry::getKey)
                .findFirst()
                .orElse(RecipeDifficultyDefinition.UNKNOWN);
    }

    @Override
    public RecipeMeasurement analyzeRecipeMeasurement(String text) {
        if (StringUtils.isBlank(text)) {
            return null;
        }
        String[] parts = text.split(" ");
        RecipeMeasurement recipeMeasurement = new RecipeMeasurement().setOriginal(text);
        boolean hasUnit = parts.length > 1;
        if (hasUnit) {
            String magnitudePart = parts[0];
            String unitPart = parts[1];
            recipeMeasurement.setMagnitude(magnitudePart)
                    .setMagnitudeValue(TextUtil.isNumeric(magnitudePart) ? new BigDecimal(magnitudePart) : null)
                    .setUnit(unitPart);
        } else {
            String magnitudePart = parts[0];
            recipeMeasurement.setMagnitude(magnitudePart)
                    .setMagnitudeValue(TextUtil.isNumeric(magnitudePart) ? new BigDecimal(magnitudePart) : null)
                    .setUnit(configuration.getUnitPieceDefault());
        }
        return recipeMeasurement;
    }

    @Override
    public RecipePriceDefinition analyzeRecipePrice(RecipeDefinition recipeDefinition) {
        String keywords = toStream(recipeDefinition.getKeywords()).collect(Collectors.joining(","));
        return configuration.getRecipePriceDefinitionPredicates().entrySet().stream()
                .filter(e -> e.getValue().test(keywords))
                .map(Map.Entry::getKey)
                .findFirst()
                .orElse(RecipePriceDefinition.NO_INFORMATION);
    }

    @Override
    public String analyzeTitle(RecipeDefinition recipeDefinition) {
        return configuration.getRecipeTitleFormatFunction().apply(recipeDefinition.getName());
    }

    @Override
    public RecipeUtensilAnalysis analyzeUtensil(String text) {
        String textFormatted = StringUtils.trimToNull(text);
        if (textFormatted == null) {
            return null;
        }

        return new RecipeUtensilAnalysis().setUid(configuration.toUid(textFormatted)).setText(textFormatted);
    }

    private String formatRecipeCategoryName(String proposedName) {
        return configuration.getRecipeCategoryNamePreprocessFunction().apply(proposedName);
    }

    private String formatRecipeCategoryNameSimplified(String proposedName) {
// Maybe keeping spaces can help making not so similar
//        String[] tokens = configuration.getTokens(proposedName);
//        return Stream.of(tokens)
//                .sorted(Comparator.reverseOrder())
//                .collect(Collectors.joining(" "));

        return configuration.toUid(proposedName);
    }

    @Override
    public RecipeAnalyzerConfiguration getConfiguration() {
        return configuration;
    }

    @Override
    public Pair<String, String> getRecipeCategoryUidNamePair(String proposedName, boolean mainCategory) {
        String preferredName = formatRecipeCategoryName(proposedName);
        if (StringUtils.isBlank(preferredName)) {
            return null;
        }

        String simplifiedTitle = formatRecipeCategoryNameSimplified(preferredName);
        return recipeCategoryUidPreferredNamePairsByFormattedName.computeIfAbsent(simplifiedTitle,
                n -> {
                    if (configuration.getRecipeCategoryMatchRatioMin() > 0) {
                        int bestRatio = configuration.getRecipeCategoryMatchRatioMin();
                        Pair<String, String> best = null;
                        for (Map.Entry<String, Pair<String, String>> entry : recipeCategoryUidPreferredNamePairsByFormattedName.entrySet()) {
                            String variantSimplifiedName = entry.getKey();
                            int ratio = FuzzySearch.weightedRatio(simplifiedTitle, variantSimplifiedName);
                            if (ratio > bestRatio) {
                                best = entry.getValue();
                                bestRatio = ratio;
                            }
                        }
                        if (best != null) {
                            return best;
                        }

                    }
                    String proposedVariantName = mainCategory ? preferredName : RECIPE_CATEGORY_EXTRA_PREFIX + preferredName;
                    return Pair.of(configuration.toUid(preferredName), proposedVariantName);
                });
    }

    @Override
    public RecipeIngredientAnalyzer getRecipeIngredientAnalyzer() {
        return recipeIngredientAnalyzer;
    }

    @Override
    public RecipeInstructionAnalyzer getRecipeInstructionAnalyzer() {
        return recipeInstructionAnalyzer;
    }

    @Override
    public boolean isHandlerOf(RecipeDefinition recipeDefinition) {
        return configuration.getHandlerOfPredicate().test(recipeDefinition);
    }

    public String toUid(String text) {
        return getConfiguration().toUid(text);
    }
}
