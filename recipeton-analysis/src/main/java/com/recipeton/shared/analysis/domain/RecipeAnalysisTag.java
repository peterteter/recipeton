package com.recipeton.shared.analysis.domain;

/*-
 * #%L
 * recipeton-shared
 * %%
 * Copyright (C) 2021 tincomisc
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

public enum RecipeAnalysisTag {

    PLACE,
    ADD,

    /**
     * Return something that was before. Not a new ingredient
     */
    ADD_BACK,

    /**
     * Instruction contains specific weight instructions
     * i.e: Weigh in steamer
     */
    WEIGH,

    /**
     * IO
     */
    REF_TOP_OF_MAIN_COMPARTMENT,
    REF_IN_MAIN_COMPARTMENT,
    REF_IN_UTENSIL,
    REF_IN_EXTERNAL_ACCESORY,

    /**
     * Instruction refers to all ingredients instead of naming them one by one
     * <p>
     * DO
     */
    ALL_INGREDIENTS,
    ALL_INGREDIENTS_WITH_EXCEPTIONS, // NOTE: Special case for post process. Should add all non previously matched ingredients
    ALL_INGREDIENTS_REMAINING, // NOTE: Special case for post process. Should add all non previously matched ingredients
    /**
     * Instruction should be carried on in parallel with previous.
     */
    PARALLEL, // NOTE: Special case for post process. Relate to existing tmcontrol so importer can render step before or change tmcontrol message
    /**
     * Recipe instructions are about serving. Probably subsequent instructions (if present) are too.
     */
    SERVING,

    /**
     * Specific technical command
     */
    COMMAND,

    /**
     * Utensil actions
     */
    OVEN_OPERATION(true),

    REFRIGERATE(true),

    FREEZE(true),

    SIMMERING_BASKET_PUT_INSIDE(true),
    SIMMERING_BASKET_REMOVE(true),
    SUMMERING_BASKET(true),
    SIMMERING_BASKET_AS_LID_PUT(true),
    SIMMERING_BASKET_REMOVE_MEASURING_CUP_PUT(true),
    SIMMERING_BASKET_AS_LID_REMOVE(true),

    MIXING_BOWL(true),
    MIXING_BOWL_REMOVE(true),
    MIXING_BOWL_PUT(true),
    MIXING_BOWL_EMPTY(true),
    MIXING_BOWL_CLEAN(true),

    TRANSFER_TO_BOWL(true),

    MEASURING_CUP(true),
    MEASURING_CUP_NOTUSE(true),
    MEASURING_CUP_REMOVE(true),
    MEASURING_CUP_PUT(true),
    MEASURING_CUP_HOLD_VIBRATION(true),

    WHISK(true),
    WHISK_PUT(true),
    WHISK_REMOVE(true),

    SPATULA(true),
    SPATULA_SCRAP_SIDE(true),
    SPATULA_MIX(true),
    SPATULA_MIX_WELL(true),

    STEAMER(true),
    STEAMER_PUT(true),
    STEAMER_REMOVE(true),
    STEAMER_TRAY_PUT(true),
    STEAMER_TRAY_REMOVE(true),
    STEAMER_DISH_PUT(true),
    STEAMER_DISH_REMOVE(true),
    OTHER_OPERATION(true),
    USEFUL_ITEMS(true),
    KITCHEN_EQUIPMENT(true);

    public static final RecipeAnalysisTag[] PLACE_INSTRUCTION_TAGS = {PLACE, ADD, ADD_BACK};
    public static final RecipeAnalysisTag[] PLACEMENT_TAGS = {REF_IN_MAIN_COMPARTMENT, REF_IN_EXTERNAL_ACCESORY, REF_IN_UTENSIL, REF_TOP_OF_MAIN_COMPARTMENT};

    private final boolean utensilAction;

    RecipeAnalysisTag() {
        this(false);
    }

    RecipeAnalysisTag(boolean utensilAction) {
        this.utensilAction = utensilAction;
    }

    public boolean isUtensil() {
        return utensilAction;
    }
}
