package com.recipeton.shared.analysis.domain;

/*-
 * #%L
 * recipeton-shared
 * %%
 * Copyright (C) 2021 tincomisc
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import lombok.Data;
import lombok.Setter;
import lombok.ToString;
import lombok.experimental.Accessors;

import java.math.BigDecimal;

import static lombok.AccessLevel.NONE;

@Data
@ToString(onlyExplicitlyIncluded = true)
@Accessors(chain = true)
public class RecipeIngredientAnalysis {

    @ToString.Include
    @Setter(NONE)
    private String uid;
    @ToString.Include
    @Setter(NONE)
    private String ingredientNotation;
    @Setter(NONE)
    private String[] ingredientNotationTokens;

    // Sometimes an alternative used name can be hidden in preparation. Usually between parentheses..
    // Preparation may indicate that is an accessory ingredient. i.e. : "para engrasar". should add tag or flag for this
    @ToString.Include
    private String preparation;

    @ToString.Include
    private String source;

    private BigDecimal magnitudeFromValue;
    @ToString.Include
    private String magnitudeFrom;

    private BigDecimal magnitudeToValue;
    @ToString.Include
    private String magnitudeTo;

    @ToString.Include
    private String unit;


    /**
     * May be used or not in the recipe
     */
    private boolean optional;

    /**
     * May not be declared in main part of preparation.
     * <p>
     * STILL NEED TO ADD DETECTION FOR THIS. Should be based on keywords in preparation text.
     * <p>
     * i.e.: Sprinkles in cake, oil to grease the pan
     */
    private boolean accessory;

    private boolean weighted;

    private RecipeIngredientAnalysis alternative;

    public boolean isMagnitudeFromPresent() {
        return magnitudeFrom != null;
    }

    public boolean isUnitPresent() {
        return unit != null;
    }

    public RecipeIngredientAnalysis setIngredientNotation(String uid, String ingredientNotation, String... ingredientNotationTokens) {
        this.uid = uid;
        this.ingredientNotation = ingredientNotation;
        this.ingredientNotationTokens = ingredientNotationTokens;
        return this;
    }
}
