package com.recipeton.shared.analysis.domain;

/*-
 * #%L
 * recipeton-analysis
 * %%
 * Copyright (C) 2021 tincomisc
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import lombok.Data;

@Data
public class RecipeInstructionMatch {
    public static final int SCORE_MAX = 100;

    private final int offset;
    private final String text;
    private final int score;
    private final boolean contextual;
    /**
     * Description of match criteria
     */
    private final String query;


    public RecipeInstructionMatch(int offset, String text, int score, String query) {
        this(offset, text, score, false, query);
    }

    public RecipeInstructionMatch(int offset, String text, int score, boolean contextual, String query) {
        this.offset = offset;
        this.text = text;
        this.score = score;
        this.contextual = contextual;
        this.query = query;
    }

    public static RecipeInstructionMatch ofContext(String query) {
        return new RecipeInstructionMatch(0, "_CONTEXT_", SCORE_MAX, true, query);
    }
}
