package com.recipeton.shared.analysis.lang;

/*-
 * #%L
 * recipeton-analysis
 * %%
 * Copyright (C) 2021 tincomisc
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.recipeton.shared.util.MapUtil;
import org.apache.commons.lang3.StringUtils;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.function.Function;

public class Spanish21TokenConverter implements Function<String, String> {

    public static final Map<String, String> SPANISH_21_MAPPING = MapUtil.toMap(
            "v", "b", // La letra “V” es reemplazada por la “B”.

            "ll", "y",    // El dígrafo “LL” es relevado por la letra “Y”, la cual dejará de representar  a la avocal i.

            "x", "cs", // Los fonemas representados por "CC", o "XC" o de la letra “X” serán escritos con las letras "CS".
            "cc", "cs",

            "h", "", // La letra “H” será abandonada.

            "qu", "c", // La combinación “QU” y la letra "K" serán reemplazadas por la letra“C”.
            "k", "c",

            "ch", "ss", // La combinación “CH” desaparecerá y su sonido será escrito con el dígrafo “SS”.

            "sce", "se", // La combinación “SC” delante de las vocales “e” e “i” será relevada por la letra “S”.
            "sci", "si",

            "ge", "je", // La letra “G” delante de las vocales “e” e “i” será sustituida por la “J”.
            "gi", "gi",

            "gu", "g", // La combinación "GU" será sustituida por la letra "G" y la diéresis “¨” dejará de usarse.

            "ce", "se", // La grafía “C” delante de las vocales “e” e “i” será canjeada por la “S”.
            "ci", "si",


            "z", "s", // La letra “Z” es sustituida por la letra “S”.

            // Ignored: El sonido "rr" será escrito con el dígrafo “RR”, salvo que esté al inicio de la palabra.

            "ph", "f" // Not spanish but sound extra
    );

    private final String[] from;
    private final String[] to;

    public Spanish21TokenConverter() {
        List<String> from = new ArrayList<>();
        List<String> to = new ArrayList<>();

        SPANISH_21_MAPPING.forEach((key, value) -> {
            from.add(key);
            to.add(value);
        });


        this.from = from.toArray(String[]::new);
        this.to = to.toArray(String[]::new);
    }

    @Override
    public String apply(String s) {
        return StringUtils.replaceEach(s, from, to);
    }
}
