package com.recipeton.shared.analysis.service;

/*-
 * #%L
 * recipeton-shared
 * %%
 * Copyright (C) 2021 tincomisc
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.recipeton.shared.analysis.configuration.RecipeAnalyzerConfiguration;
import com.recipeton.shared.analysis.domain.*;
import com.recipeton.shared.analysis.service.filter.*;
import com.recipeton.shared.analysis.service.preprocess.RecipeInstructionSplitter;
import com.recipeton.shared.util.TextUtil;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.tuple.Pair;

import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static com.recipeton.shared.analysis.configuration.RecipeAnalyzerConfiguration.*;
import static com.recipeton.shared.analysis.domain.RecipeAnalysisTag.*;
import static com.recipeton.shared.util.StreamUtil.concat;

@Slf4j
@AllArgsConstructor
public class RecipeInstructionAnalyzer {

    private final RecipeAnalyzerConfiguration configuration;
    private final RecipeInstructionSplitter recipeInstructionSplitter;
    private final List<RecipeInstructionAnalysisFilter> recipeInstructionAnalysisPreFilters;
    private final String[] cleanSearchList;
    private final String[] cleanReplacementList;

    public RecipeInstructionAnalyzer(RecipeAnalyzerConfiguration configuration) {
        this.configuration = configuration;
        this.recipeInstructionSplitter = new RecipeInstructionSplitter(configuration);

        List<String> aux1 = new ArrayList<>();
        List<String> aux2 = new ArrayList<>();
        configuration.getInstructionCleanReplacements().forEach((k, v) -> {
            aux1.add(k);
            aux2.add(v);
        });
        cleanSearchList = aux1.toArray(String[]::new);
        cleanReplacementList = aux2.toArray(String[]::new);

        RecipeCommandAnalyzer recipeCommandAnalyzer = new RecipeCommandAnalyzer(configuration);

        this.recipeInstructionAnalysisPreFilters = concat(
                configuration.getInstructionMatchers().entrySet()
                        .stream()
                        .map(e -> {
                            if (e.getKey().isUtensil()) {
                                return new RecipeUtensilRecipeInstructionAnalysisFilter(e.getKey(), e.getValue());
                            } else {
                                return new TaggingRecipeInstructionAnalysisFilter(e.getKey(), e.getValue());
                            }
                        }),

                configuration.getCommandDelimiters().stream().map(p -> new RecipeCommandRecipeInstructionAnalysisFilter(recipeCommandAnalyzer, p)),

                // IMPORTANT!
                // ADD MORE NEEDED HERE! for instance check cup/lid adjustments before control step. may need to check before of after step for adjustment commands
                // Same for meanwhile (parallel) step!
                // Neet do detect when single instruction has action and add ingredients. Needs to be splitted!!! Probably
                // at last sentence before command
                Stream.of(new IngredientPlacementContextPropagationRecipeInstructionAnalysisFilter(), new IngredientWeighOnTopContextPropagationRecipeInstructionAnalysisFilter())
        ).collect(Collectors.toList());
    }

    public List<RecipeInstructionAnalysis> analyze(List<String> recipeInstructions, List<RecipeIngredientAnalysis> recipeIngredientAnalyses) {
        return analyze(recipeInstructions, recipeIngredientAnalyses, false);
    }

    public List<RecipeInstructionAnalysis> analyze(List<String> recipeInstructions, List<RecipeIngredientAnalysis> recipeIngredientAnalyses, boolean strictAnalysis) { // NOPMD Complex
        List<RecipeInstructionAnalysis> recipeInstructionAnalyses = toRecipeInstructionAnalyses(recipeInstructions);

        recipeInstructionAnalysisPreFilters
                .forEach(f -> recipeInstructionAnalyses.forEach(f::apply));

        recipeIngredientAnalyses.stream()
                .filter(i -> StringUtils.isNotBlank(i.getIngredientNotation()))
                .flatMap(i -> getIngredientNotationTokenVariants(i).stream()
                        .map(v -> new RecipeIngredientRecipeInstructionAnalysisFilter(i, configuration, v))
                )
                .forEach(f -> recipeInstructionAnalyses.forEach(f::apply));

        Set<RecipeIngredientAnalysis> missingRecipeIngredientAnalysis = new HashSet<>(recipeIngredientAnalyses);
        for (RecipeIngredientAnalysis recipeIngredientAnalysis : recipeIngredientAnalyses) {
            List<Pair<RecipeInstructionAnalysis, RecipeIngredientTerm>> termPairs = findRecipeIngredientTermsByTop(recipeIngredientAnalysis, recipeInstructionAnalyses);

            if (termPairs.isEmpty()) { // Not found at all expand criteria and try to match again
                List<String[]> preparationVariants = getIngredientPreparationTokenVariants(recipeIngredientAnalysis);
                if (preparationVariants != null) {
                    preparationVariants.stream()
                            .map(v -> new RecipeIngredientRecipeInstructionAnalysisFilter(recipeIngredientAnalysis, configuration, v)) // NOPMD
                            .forEach(f -> recipeInstructionAnalyses.forEach(f::apply));
                    termPairs = findRecipeIngredientTermsByTop(recipeIngredientAnalysis, recipeInstructionAnalyses);
                }
            }

            if (!termPairs.isEmpty()) { // found
                boolean conclusiveMatch = termPairs.stream()
                        .filter(p -> p.getKey().isTagsAllPresent(REF_TOP_OF_MAIN_COMPARTMENT, WEIGH))
                        .findFirst().map(Pair::getValue)
                        .map(t -> {
                            t.setConclusive(true);
                            t.setTarget(RecipeIngredientTerm.Target.PREPROCESS);
                            return true;
                        }).orElse(false);
                // Is there any case where after weigh ingredient would need to be added????
                // or maybe that weigh would be AFTER add....
                if (!conclusiveMatch) {
                    conclusiveMatch = termPairs.stream()
                            .filter(p -> {
                                RecipeInstructionAnalysis recipeInstructionAnalysis = p.getKey();
                                return recipeInstructionAnalysis.isTagAnyPresent(REF_IN_MAIN_COMPARTMENT, REF_IN_UTENSIL) && recipeInstructionAnalysis.isTagAnyPresent(PLACE_INSTRUCTION_TAGS);
                            })
                            .findFirst().map(Pair::getValue)
                            .map(t -> {
                                t.setConclusive(true);
                                t.setTarget(RecipeIngredientTerm.Target.PROCESS);
                                return true;
                            }).orElse(false);
                }

                if (!conclusiveMatch) {
                    conclusiveMatch = termPairs.stream()
                            .filter(p -> p.getKey().isTagPresent(RecipeAnalysisTag.SERVING))
                            .findFirst().map(Pair::getValue)
                            .map(t -> {
                                t.setConclusive(true);
                                t.setTarget(RecipeIngredientTerm.Target.SERVING);
                                return true;
                            }).orElse(false);
                }

                if (!conclusiveMatch) {
                    if (strictAnalysis) {
                        throw new IllegalStateException("Analyze found ingredient but NOT in a ingredient declared step. What is done with this ingredient??? ingredient=" + recipeIngredientAnalysis);
                    }
                    log.info("Analyze found ingredient but NOT in a ingredient declared step. What is done with this ingredient??? ingredient={}", recipeIngredientAnalysis);
                }
                missingRecipeIngredientAnalysis.remove(recipeIngredientAnalysis);
            }
        }

        if (!missingRecipeIngredientAnalysis.isEmpty()) {
            List<RecipeInstructionAnalysis> recipeInstructionsOfPlaceMainContainer = recipeInstructionAnalyses.stream()
                    .filter(ria -> ria.isTagPresent(REF_IN_MAIN_COMPARTMENT) && ria.isTagAnyPresent(PLACE_INSTRUCTION_TAGS))
                    .collect(Collectors.toList());
            boolean single = recipeInstructionsOfPlaceMainContainer.size() == 1;
            if (single) {
                RecipeInstructionAnalysis singleRecipeInstructionOfAdditionType = recipeInstructionsOfPlaceMainContainer.get(0);
                missingRecipeIngredientAnalysis.forEach(ui -> singleRecipeInstructionOfAdditionType.addTerm(RecipeIngredientTerm.ofMatchOnAll(ui, "__ALL_GUESSED1__")));
            } else {
                RecipeInstructionAnalysis addAllRemainingIngredientsInstruction = recipeInstructionsOfPlaceMainContainer.stream().filter(i -> i.isTagPresent(RecipeAnalysisTag.ALL_INGREDIENTS_REMAINING)).findFirst().orElse(null);
                if (addAllRemainingIngredientsInstruction == null) {
                    log.warn("Could not find any ingredient and addition step. Will not add any ingredient. count={}", recipeInstructionsOfPlaceMainContainer);
                } else {
                    missingRecipeIngredientAnalysis.forEach(ui -> addAllRemainingIngredientsInstruction.addTerm(RecipeIngredientTerm.ofMatchOnAll(ui, "__ALL_GUESSED2__")));
                }
            }
        }

        // ADD SANITY WARNING IF  PLACE OR ADD STEPS DO NOT CONTAIN ANY CONCLUSIVE TERMS!!!!

        return recipeInstructionAnalyses;
    }

    public String cleanInstruction(String text) {
        if (text == null) {
            return null;
        }
        return StringUtils.replaceEach(text, cleanSearchList, cleanReplacementList).trim();
    }

    private List<Pair<RecipeInstructionAnalysis, RecipeIngredientTerm>> findRecipeIngredientTermsByTop(RecipeIngredientAnalysis recipeIngredientAnalysis, List<RecipeInstructionAnalysis> recipeInstructionAnalyses) {
        return recipeInstructionAnalyses.stream()
                .map(ria -> {
                    Optional<RecipeIngredientTerm> recipeIngredientTermsByTopRecipeIngredient = getRecipeIngredientTermsByTopRecipeIngredient(ria, recipeIngredientAnalysis);
                    return recipeIngredientTermsByTopRecipeIngredient.map(t -> Pair.of(ria, t));
                })
                .filter(Optional::isPresent).map(Optional::get)
                .collect(Collectors.toList());
    }

    public List<String[]> getIngredientNotationTokenVariants(RecipeIngredientAnalysis recipeIngredientAnalysis) {
        String[] ingredientNotationTokens = recipeIngredientAnalysis.getIngredientNotationTokens();
        List<String[]> variants = new ArrayList<>();
        getIngredientTokenVariants(variants, ingredientNotationTokens);
        RecipeIngredientAnalysis alternative = recipeIngredientAnalysis.getAlternative();
        if (alternative != null) {
            getIngredientTokenVariants(variants, alternative.getIngredientNotationTokens());
        }
        return variants;
    }

    public List<String[]> getIngredientPreparationTokenVariants(RecipeIngredientAnalysis recipeIngredientAnalysis) {
        String preparationText = recipeIngredientAnalysis.getPreparation();
        if (preparationText == null) {
            return null;
        }

        String cleaned = TextUtil.removeAll(preparationText, PATTERN_SYMBOLS).toLowerCase();
        String[] tokens = Stream.of(cleaned.split(" "))
                .map(String::trim)
                .filter(t -> t.length() > TOKEN_LENGTH_MIN)
                .filter(t -> !configuration.getStopWords().contains(t))
                .filter(t -> !configuration.getIngredientPreparationTokens().contains(t))
                .map(t -> configuration.getTokenFormatFunction().apply(t))
                .filter(StringUtils::isNotBlank)
                .map(String::trim)
                .map(TextUtil::normalizeAscii)
                .toArray(String[]::new);
        List<String[]> variants = new ArrayList<>();
        getIngredientTokenVariants(variants, tokens);
        return variants;
    }

    private void getIngredientTokenVariants(List<String[]> variants, String... ingredientNotationTokens) {
        int first = 0;
        int last = ingredientNotationTokens.length;
        for (int i = first; i < last; i++) {
            for (int j = last; j > i; j--) {
                variants.add(Arrays.copyOfRange(ingredientNotationTokens, i, j));
            }
        }
    }

    public Optional<RecipeIngredientTerm> getRecipeIngredientTermsByTopRecipeIngredient(RecipeInstructionAnalysis recipeInstructionAnalysis, RecipeIngredientAnalysis recipeIngredientAnalysis) {
        return recipeInstructionAnalysis.streamRecipeIngredientTerms()
                .filter(t -> t.getIngredient().equals(recipeIngredientAnalysis))
                .map(t -> Pair.of(t, getScore(t)))
                .sorted(Comparator.comparing(Pair::getRight))
                .map(Pair::getLeft)
                .findFirst();
    }

    // better use function if need to change
    private double getScore(RecipeIngredientTerm recipeIngredientTerm) {
        String ingredientNotation = recipeIngredientTerm.getIngredient().getIngredientNotation();
        String matchText = recipeIngredientTerm.getMatch().getText();
        double balance = (double) matchText.length() / ingredientNotation.length();
        double score = recipeIngredientTerm.getMatch().getScore() * balance;
        log.trace("score {} ({},{})({})", score, ingredientNotation, matchText, recipeIngredientTerm.getMatch().getQuery());
        return score;
    }

    public List<RecipeTerm> getTermsSorted(RecipeInstructionAnalysis recipeInstructionAnalysis) {
        // REMOVE ORDER and calculate here based on types, conclusive and match position
        return recipeInstructionAnalysis.getTerms().stream()
                .filter(RecipeTerm::isConclusive)
                .sorted((o1, o2) -> {
                    if (o1 instanceof RecipeInstructionTerm && !(o2 instanceof RecipeInstructionTerm)) {
                        return -1;
                    }
                    if (o2 instanceof RecipeInstructionTerm && !(o1 instanceof RecipeInstructionTerm)) {
                        return 1;
                    }
                    if (o1 instanceof RecipeUtensilActionTerm && !(o2 instanceof RecipeUtensilActionTerm)) {
                        return -1;
                    }
                    if (o2 instanceof RecipeUtensilActionTerm && !(o1 instanceof RecipeUtensilActionTerm)) {
                        return 1;
                    }
                    return o1.getOffset() - o2.getOffset();
                })
                .collect(Collectors.toList());
    }

    public String getTextAsHumanReadable(RecipeInstructionAnalysis recipeInstructionAnalysis) {
        return TextUtil.removeAll(recipeInstructionAnalysis.getText(), DELIMITERS_PATTERN);
    }

    private List<RecipeInstructionAnalysis> toRecipeInstructionAnalyses(List<String> recipeInstructions) {
        List<RecipeInstructionAnalysis> collect = recipeInstructions.stream()
                .map(this::cleanInstruction)
                .flatMap(s -> recipeInstructionSplitter.splitInstruction(s).stream())
                .filter(StringUtils::isNotBlank)
                .map(String::trim)
                .map(s -> configuration.getTokenization(s, TOKEN_LENGTH_MIN))
                .filter(t -> !t.isEmpty()) // Instruction without tokens is just made of stop words
                .map(RecipeInstructionAnalysis::new)
                .collect(Collectors.toList());

        for (int i = 0; i < collect.size(); i++) {
            RecipeInstructionAnalysis recipeInstructionAnalysis = collect.get(i);
            if (i > 0) {
                recipeInstructionAnalysis.setPrev(collect.get(i - 1));
            }
            if (i < collect.size() - 1) {
                recipeInstructionAnalysis.setNext(collect.get(i + 1));
            }

        }

        return collect;
    }

}
