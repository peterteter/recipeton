package com.recipeton.shared.analysis.configuration;

/*-
 * #%L
 * recipeton-shared
 * %%
 * Copyright (C) 2021 tincomisc
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.recipeton.shared.analysis.domain.*;
import com.recipeton.shared.analysis.lang.Spanish21TokenConverter;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.tuple.Pair;

import java.util.Set;
import java.util.function.Function;
import java.util.regex.Pattern;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static com.recipeton.shared.analysis.configuration.CookiConstants.*;
import static com.recipeton.shared.analysis.domain.RecipeInstructionRegexMatcher.toRecipeInstructionMatchers;
import static com.recipeton.shared.analysis.service.preprocess.RecipeInstructionSplitter.DO_NOT_SPLIT;
import static com.recipeton.shared.util.MapUtil.toMap;
import static com.recipeton.shared.util.MapUtil.toMapP;
import static com.recipeton.shared.util.TextUtil.*;
import static org.apache.commons.lang3.StringUtils.containsIgnoreCase;

public class RecipeAnalyzerCookiEsConfiguration extends RecipeAnalyzerConfiguration {

    public static final String UNIT_PIECE = "ud";

    private static final Spanish21TokenConverter SPANISH_21_TOKEN_CONVERTER = new Spanish21TokenConverter();

    public RecipeAnalyzerCookiEsConfiguration() {
        super("COOKI_ES", rd -> containsIgnoreCase(rd.getLocale(), "es") && containsIgnoreCase(rd.getSource(), "cooki"));

        this.setStopWords(Set.of(
                "algún", "alguna", "algunas", "alguno", "algunos", "ambos", "ampleamos", "ante", "antes", "aquel", "aquellas", "aquellos", "aqui", "arriba", "atras",
                "bajo", "bastante", "bien",
                "cada", "cierta", "ciertas", "cierto", "ciertos", "como", "con", "conseguimos", "conseguir", "consigo", "consigue", "consiguen", "consigues", "cual", "cuando",
                "dentro", "desde", "donde", "dos",
                "el", "ellas", "ellos", "empleais", "emplean", "emplear", "empleas", "empleo", "en", "encima", "entonces", "entre", "era", "eramos", "eran", "eras", "eres", "es", "esta", "estaba", "estado", "estais", "estamos", "estan", "estoy",
                "fin", "fue", "fueron", "fui", "fuimos",
                "gueno",
                "ha", "hace", "haceis", "hacemos", "hacen", "hacer", "haces", "hago",
                "incluso", "intenta", "intentais", "intentamos", "intentan", "intentar", "intentas", "intento", "ir",
                "la", "largo", "las", "lo", "los",
                "mientras", "mio", "modo", "muchos", "muy",
                "nos", "nosotros",
                "otro",
                "para", "pero", "podeis", "podemos", "poder", "podria", "podriais", "podriamos", "podrian", "podrias", "por", "por qué", "porque", "primero", "puede", "pueden", "puedo",
                "quien",
                "sabe", "sabeis", "sabemos", "saben", "saber", "sabes", "ser", "si", "siendo",
//                "sin",  // Its meaningful in the context of categories. Ingredients already deal with it
                "sobre", "sois", "solamente", "solo", "somos", "soy", "su", "sus",
                "también", "teneis", "tenemos", "tener", "tengo", "tiempo", "tiene", "tienen", "todo", "trabaja", "trabajais", "trabajamos", "trabajan", "trabajar", "trabajas", "trabajo", "tras", "tuyo",
                "ultimo", "un", "una", "unas", "uno", "unos", "usa", "usais", "usamos", "usan", "usar", "usas", "uso",
                "va", "vais", "valor", "vamos", "van", "vaya", "verdad", "verdadera", "verdadero", "vosotras", "vosotros", "voy",
                "yo",
                "él", "ésta", "éstas", "éste", "éstos", "última", "últimas", "último", "últimos",
                "a", "añadió", "aún", "actualmente", "adelante", "además", "afirmó", "agregó", "ahí", "ahora", "al", "algo", "alrededor", "anterior", "apenas", "aproximadamente", "aquí", "así", "aseguró", "aunque", "ayer", "buen", "buena", "buenas", "bueno", "buenos", "cómo", "casi", "cerca", "cinco", "comentó", "conocer", "consideró", "considera", "contra", "cosas", "creo", "cuales", "cualquier", "cuanto", "cuatro", "cuenta", "da", "dado", "dan", "dar", "de", "debe", "deben", "debido", "decir", "dejó", "del", "demás", "después", "dice", "dicen", "dicho", "dieron", "diferente", "diferentes", "dijeron", "dijo", "dio", "durante",
                "e", "ejemplo", "ella", "ello", "embargo", "encuentra", "esa", "esas", "ese", "eso", "esos", "está", "están", "estaban", "estar", "estará", "estas", "este", "esto", "estos", "estuvo", "ex", "existe", "existen", "explicó", "expresó", "fuera", "gran", "grandes", "había", "habían", "haber", "habrá", "hacerlo", "hacia", "haciendo", "han", "hasta", "hay", "haya", "he", "hecho", "hemos", "hicieron", "hizo", "hoy", "hubo", "igual", "indicó", "informó", "junto", "lado", "le", "les", "llegó", "lleva", "llevar", "luego", "lugar", "más", "manera", "manifestó", "mayor", "me", "mediante", "mejor", "mencionó", "menos", "mi", "misma", "mismas", "mismo", "mismos", "momento", "mucha", "muchas", "mucho", "nada", "nadie", "ni", "ningún", "ninguna", "ningunas", "ninguno", "ningunos", "no", "nosotras", "nuestra", "nuestras", "nuestro", "nuestros", "nueva", "nuevas", "nuevo", "nuevos", "nunca", "o", "ocho", "otra", "otras", "otros", "parece", "parte", "partir", "pasada", "pasado", "pesar", "poca", "pocas", "poco", "pocos", "podrá", "podrán", "podría", "podrían", "poner", "posible", "próximo", "próximos", "primer", "primera", "primeros", "principalmente", "propia", "propias", "propio", "propios", "pudo", "pueda", "pues", "qué", "que", "quedó", "queremos", "quién", "quienes", "quiere", "realizó", "realizado", "realizar", "respecto", "sí", "sólo", "se", "señaló", "sea", "sean", "según", "segunda", "segundo", "seis", "será", "serán", "sería", "sido", "siempre", "siete", "sigue", "siguiente", "sino", "sola", "solas", "solos", "son",
                "tal", "tampoco", "tan", "tanto", "tenía", "tendrá", "tendrán", "tenga", "tenido", "tercera", "toda", "todas", "todavía", "todos", "total", "trata", "través", "tres", "tuvo", "usted", "varias", "varios", "veces", "ver", "vez", "y", "ya"))
                .setTokenFormatFunction(
                        new Function<>() {
                            private final Pattern cleanupPattern = Pattern.compile("(?i)" + String.join("|",
                                    "r$",
                                    "s$",
                                    "(?<=a)ndo$",
                                    "(?<=a)do$"
                            ));

                            @Override
                            public String apply(String s) {
                                String apply = SPANISH_21_TOKEN_CONVERTER.apply(s);
                                if (apply.length() <= TOKEN_LENGTH_MIN) {
                                    return apply;
                                } else {
                                    return removeAll(apply, cleanupPattern);
                                }
                            }
                        })
                .setRecipeUnitDefinitions(
                        new RecipeUnitDefinition("cda", "cdas", "cucharada", "cucharadas"),
                        new RecipeUnitDefinition("cda (rasa)", "cda rasa", "cdas rasas", "cucharada rasa", "cucharadas rasas"),
                        new RecipeUnitDefinition("cdta", "cdtas", "cucharadita", "cucharaditas"),
                        new RecipeUnitDefinition("cdta (rasa)", "cdta rasa", "cdtas", "cucharadita rasa", "cucharaditas rasas"),
                        new RecipeUnitDefinition("diente", "dientes"),
                        new RecipeUnitDefinition(RecipeUnitDefinition.UNIT_GRAM, "gramo", "gramos"),
                        new RecipeUnitDefinition("kg"),
                        new RecipeUnitDefinition("l", "litro", "litros"),
                        new RecipeUnitDefinition("lata", "latas"),
                        new RecipeUnitDefinition("ml"),
                        new RecipeUnitDefinition("pellizco", "pellizcos"),
                        new RecipeUnitDefinition("ramita", "ramitas"),
                        new RecipeUnitDefinition(UNIT_PIECE, "pieza", "piezas", "trozo", "trozos")
                )
                .setUnitPieceDefault(UNIT_PIECE)
                .setUnitWeightedDefault(RecipeUnitDefinition.UNIT_GRAM)
                .setNumericStrings(toMap(
                        "un", ONE,
                        "uno", ONE,
                        "una", ONE,
                        "dos", TWO,
                        "tres", THREE,
                        "cuatro", FOUR,
                        "cinco", FIVE,
                        "seis", SIX,
                        "siete", SEVEN,
                        "ocho", EIGHT,
                        "nueve", NINE,
                        "diez", TEN
                ))
                .setIngredientUnitSeparatorTokens(Set.of("de")) // 300 g [de] manzanas
                .setIngredientPreparationTokens(Set.of(
                        "en",
                        "para", // aceite para la sarten
                        "adobado",
                        "batido",
                        "ligeramente",
                        "con",
                        "congelada",
                        "congeladas",
                        "congelado",
                        "congelados",
                        "cortada",
                        "cortadas",
                        "cortado",
                        "cortados",
//                "de", // peligroso. casos como pimienta [de cayena], [de oliva] ...
                        "desmenuzada",
                        "desmenuzadas",
                        "desmenuzado",
                        "desmenuzados",
                        "entera",
                        "enteras",
                        "entero",
                        "enteros",
                        "fresco",
                        "fresca",
                        "frescos",
                        "frescas",
                        "molida",
                        "molidas",
                        "molido",
                        "molidos",
                        "pelada",
                        "peladas",
                        "pelado",
                        "pelados",
                        "recién",
                        "seco",
                        "secos",
                        "seca",
                        "secas",
                        "sin",
                        "troceada",
                        "troceadas",
                        "troceado",
                        "troceados",
                        "virgen"
                ))
                .setIngredientAlternativeSeparatorTokenSet("o bien")
                .setIngredientOptionalIndicatorTokens(Set.of("(opcional)"))
                .setInstructionSplitToNextTokenSequences(
                        Stream.concat(
                                Stream.of(
                                        Pair.of(" aprox.", DO_NOT_SPLIT), // Careful. Due to limitationa all need to start from space or punctuation
                                        Pair.of(" y, mientras tanto,", "Mientras tanto,"),
                                        Pair.of("Mientras tanto,", DO_NOT_SPLIT), // special beginning of sentence
                                        Pair.of(".", "")
                                ),
                                Stream.of(
                                        // Each verb turned into : y verb -> Verb and , verb -> Verb
                                        "acreme",
                                        "agregue",
                                        "amase",
                                        "bata",
                                        "caliente",
                                        "cocine",
                                        "continue cocinando",
                                        "continúe cocinando",
                                        "corte", // causes some problems
                                        "córtelo",
                                        "córtela",
                                        "deje reducir",
                                        "deje reposar",
                                        "derrita",
                                        "deshebre",
                                        "disuelva",
                                        "dore",
                                        "emulsione",
                                        "funda",
//                "deje reposar", // Maybe loses too much context
                                        "mezcle",
                                        "mezcla",
                                        "hierva",
                                        "inicie",
                                        "infusione",
                                        "incorpore",
//              "lave", // ... y lave en el cestillo
                                        "licue",
                                        "licúe",
                                        "monte",
                                        "muela",
                                        "pique",
                                        "programe",
                                        "pulverice",
                                        "ralle",
                                        "rállelo",
                                        "reserve en", // Attention. never reserve only!
                                        "reduzca",
                                        "rehogue",
                                        //"reparta", // Useful in a few cases but bad for many others. Not directly for machine and affects context
                                        //"remueva", // Needs context to know if needs to split
                                        "sin colocar",
                                        "sin programar tiempo",
                                        "sirva",
                                        "sofría",
                                        // "transfiera", // Will create false positives. Also affects context as it may split tightly related instructions. Would need better understanding of the sentences to split
                                        "tempere",
                                        "tamice",
                                        "triturando",
                                        "triture",
                                        "trocee",
                                        "haga puré",
                                        "saque", // Evaluate
                                        "termine de mezclar",
                                        "vacíe",
                                        "vierta",
                                        "vuelva a batir",
                                        "vuelva a mezclar",
                                        "vuelva a programar",
                                        //
                                        "añada",
                                        "coloque",
                                        "introduzca",
//                                        "extraiga",
//                                        "retire",
                                        "ponga",
                                        "pese",
                                        "sitúe"
//                                ).flatMap(v -> Stream.of( // Pre variants
//                                        v + " ",
//                                        )
                                ).map(String::toLowerCase).flatMap(v ->
                                        Stream.of(
                                                " y ", // Careful. Due to limitationa all need to start from space or punctuation
                                                " y, ",
                                                ", y ",
                                                ", después ",
                                                ", luego ",
                                                ", ",
                                                " e "
                                        ).map(s -> Pair.of(s + v, StringUtils.capitalize(v))))
                        ).collect(Collectors.toMap(Pair::getLeft, Pair::getRight))
                )
                .setUtensilTokens(Set.of(
                        "aluminio",
                        "bol",
                        "congelador",
                        "cuchillo",
                        "espátula",
                        "frigorífico",
                        "horno",
                        "nevera",
                        "tabla"))
                .setCommandProgramPredicates(toMap(
                        RecipeCommandProgram.DOUGH, stringPredicatePatterns("(?i)" + TM_DOUGH + "|dough"),
                        RecipeCommandProgram.FERMENT, stringPredicatePatterns("(?i)fermentar"),
                        RecipeCommandProgram.HEAT_ONLY, stringPredicatePatterns("(?i)calentar"),
                        RecipeCommandProgram.SOUS_VIDE, stringPredicatePatterns("(?i)Al vacio"),
                        RecipeCommandProgram.TURBO, stringPredicatePatterns("(?i)turbo")
                ))
                .setCommandDurationPredicates(toMap(
                        HALF_SECOND, stringPredicatePatterns("0\\.5 seg")
                ))
                .setCommandDurationHoursPattern(stringPattern("^([0-9]+) h"))
                .setCommandDurationMinutesPattern(stringPattern("^([0-9]+) min"))
                .setCommandDurationSecondsPattern(stringPattern("^([0-9]+) seg"))
                .setCommandTemperaturePredicates(toMap(
                        STEAMER_TEMPERATURE, stringPredicatePatterns("(?i)vaporera|vapor|steamer|varoma")
                ))
                .setCommandTemperaturePattern(stringPattern("^([0-9]+)°C"))
                .setCommandSpeedPredicates(toMap(
                        COMMAND_SPEED_SLOW_VALUE, stringPredicatePatterns("(?i)vel " + TM_SLOW + "|vel slow|vel cuchara|vel lento")
                ))
                .setCommandRotationReversePredicates(stringPredicatePatterns("(?i)" + TM_REVERSE + "|reverse|reversa")).setCommandSpeedPattern(stringPattern("vel ([//.0-9]+)"))
                .setCommandRepetitionFromPattern(stringPattern("^([0-9]+) (?:veces|vez)"))
                .setCommandRepetitionFromToPattern(stringPattern("^([0-9]+)-([0-9]+) veces"))
                .setInstructionMatchers(toMapP(
                        Pair.of(RecipeAnalysisTag.ADD, toRecipeInstructionMatchers("(?i)añada|agregue|incorpore")),
                        Pair.of(RecipeAnalysisTag.PLACE, toRecipeInstructionMatchers("(?i)coloque|ponga|vierta")),

                        Pair.of(RecipeAnalysisTag.ADD_BACK, toRecipeInstructionMatchers("(?i)regrese|retorne")),
                        Pair.of(RecipeAnalysisTag.WEIGH, toRecipeInstructionMatchers("(?i)^pese ")),

                        Pair.of(RecipeAnalysisTag.REF_TOP_OF_MAIN_COMPARTMENT, toRecipeInstructionMatchers("(?i)sobre la tapa de[l]? vaso")),
                        Pair.of(RecipeAnalysisTag.REF_IN_MAIN_COMPARTMENT, toRecipeInstructionMatchers("(?i) el vaso| al vaso")), // Sometimes Ponga el vaso (without en el vaso)...
                        Pair.of(RecipeAnalysisTag.REF_IN_UTENSIL, toRecipeInstructionMatchers("(?i) en el cestillo| en el varoma| en la vaporera")),
                        Pair.of(RecipeAnalysisTag.REF_IN_EXTERNAL_ACCESORY, toRecipeInstructionMatchers("(?i)(?:^en | en |^a | a )(?:el |la |un |una )(?:bandeja|bol|charola|fuente|olla|sartén|refractario|sopera|tazón)")), // / be careful. add spaces to avoid bad matches

                        Pair.of(RecipeAnalysisTag.ALL_INGREDIENTS, toRecipeInstructionMatchers("(?i)todos los ingredientes")),
                        Pair.of(RecipeAnalysisTag.ALL_INGREDIENTS_REMAINING, toRecipeInstructionMatchers("(?i)los ingredientes restantes")),

                        Pair.of(RecipeAnalysisTag.PARALLEL, toRecipeInstructionMatchers("(?i)mientras tanto")),
                        Pair.of(RecipeAnalysisTag.SERVING, toRecipeInstructionMatchers("(?i)disfrute|sírvalo|sirva|antes de servir")),

                        // Utensils. Complete!!
                        Pair.of(RecipeAnalysisTag.OVEN_OPERATION, toRecipeInstructionMatchers("(?i)Precaliente el horno")),

                        Pair.of(RecipeAnalysisTag.REFRIGERATE, toRecipeInstructionMatchers("(?i)(?:Reserve|Deje reposar).*en.*(?:frigo|nevera|refrigerador)", "(?i)Refrigere")),
                        Pair.of(RecipeAnalysisTag.FREEZE, toRecipeInstructionMatchers("(?i)(?:(?:Reserve|Conserve|Coloque).*en|Lleve.*al).*congelador")),

                        Pair.of(RecipeAnalysisTag.SIMMERING_BASKET_PUT_INSIDE, toRecipeInstructionMatchers("(?i)(?:inserte|introduzca).*cestillo")),
                        Pair.of(RecipeAnalysisTag.SIMMERING_BASKET_REMOVE, toRecipeInstructionMatchers("(?i)(?:extraiga|retire|extraer).*cestillo")),
                        Pair.of(RecipeAnalysisTag.SUMMERING_BASKET, toRecipeInstructionMatchers("(?i)(?:escurra|vierta).*cestillo")),
                        Pair.of(RecipeAnalysisTag.SIMMERING_BASKET_AS_LID_PUT, toRecipeInstructionMatchers("(?i)cestillo.*tapa.*cubilete", "(?i)(en lugar).*cubilete.*cestillo")),
//                        Pair.of(RecipeAnalysisTag.SIMMERING_BASKET_AS_LID_REMOVE, defineMe()),
//                        Pair.of(RecipeAnalysisTag.SIMMERING_BASKET_REMOVE_MEASURING_CUP_PUT, defineMe()),

//                        Pair.of(RecipeAnalysisTag.MIXING_BOWL, defineMe()),
//                        Pair.of(RecipeAnalysisTag.MIXING_BOWL_REMOVE, defineMe()),
                        Pair.of(RecipeAnalysisTag.MIXING_BOWL_PUT, toRecipeInstructionMatchers("(?i)coloque el vaso")),
                        Pair.of(RecipeAnalysisTag.MIXING_BOWL_EMPTY, toRecipeInstructionMatchers("(?i)(?:Vacíe).*el vaso")),
                        Pair.of(RecipeAnalysisTag.MIXING_BOWL_CLEAN, toRecipeInstructionMatchers("(?i)(?:Lave|Limpie|Aclare|Enjuague).*el vaso")),

                        Pair.of(RecipeAnalysisTag.TRANSFER_TO_BOWL, toRecipeInstructionMatchers("(?i)(?:Transfiera|Retire|Saque|Vierta).*(?:del vaso|(?:a|en) (?:un tazón|un bol|un recipiente|una bandeja))")),

//                        Pair.of(RecipeAnalysisTag.MEASURING_CUP, defineMe()),
                        Pair.of(RecipeAnalysisTag.MEASURING_CUP_NOTUSE, toRecipeInstructionMatchers("(?i)(?:retire|sin poner|sin colocar).*cubilete")),
                        Pair.of(RecipeAnalysisTag.MEASURING_CUP_PUT, toRecipeInstructionMatchers("(?i)ponga.*cubilete")),
                        Pair.of(RecipeAnalysisTag.MEASURING_CUP_HOLD_VIBRATION, toRecipeInstructionMatchers("(?i)sujete.*cubilete")),

//                        Pair.of(RecipeAnalysisTag.BUTTERFLY, defineMe()),
                        Pair.of(RecipeAnalysisTag.WHISK_PUT, toRecipeInstructionMatchers("(?i)Coloque la mariposa")),
                        Pair.of(RecipeAnalysisTag.WHISK_REMOVE, toRecipeInstructionMatchers("(?i)(?:Retire|Quite).*mariposa")),

//                        Pair.of(RecipeAnalysisTag.SPATULA, defineMe()),
                        Pair.of(RecipeAnalysisTag.SPATULA_MIX, toRecipeInstructionMatchers("(?i)(?:Mezcle|Remueva|Termine de mezclar)(?!(.*bien)).*(?:espátula)")),
                        Pair.of(RecipeAnalysisTag.SPATULA_SCRAP_SIDE, toRecipeInstructionMatchers("(?i)espátula.*baje.*fondo.*vaso.*|baje.*restos.*(vaso|pared).*espátula")),
                        Pair.of(RecipeAnalysisTag.SPATULA_MIX_WELL, toRecipeInstructionMatchers("(?i)(?:Mezcle|Remueva|Termine de mezclar).*(?:bien).*espátula")),

                        Pair.of(RecipeAnalysisTag.STEAMER, toRecipeInstructionMatchers("(?i)Tape.*(?:vaporera|varoma)")),
                        Pair.of(RecipeAnalysisTag.STEAMER_PUT, toRecipeInstructionMatchers("(?i)(?:Coloque|Sitúe).*(?:vaporera|varoma).*posición")),
                        Pair.of(RecipeAnalysisTag.STEAMER_REMOVE, toRecipeInstructionMatchers("(?i)Retire.*(?:vaporera|varoma)")),
//                        Pair.of(RecipeAnalysisTag.STEAMER_TRAY_PUT, defineMe())),
//                        Pair.of(RecipeAnalysisTag.STEAMER_TRAY_REMOVE, defineMe())),
//                        Pair.of(RecipeAnalysisTag.STEAMER_DISH_PUT, defineMe())),
//                        Pair.of(RecipeAnalysisTag.STEAMER_DISH_REMOVE, defineMe())),

//                        Pair.of(RecipeAnalysisTag.THERMOMIX_PARTS, defineMe())),

//                        Pair.of(RecipeAnalysisTag.USEFUL_ITEMS, defineMe())),
                        Pair.of(RecipeAnalysisTag.KITCHEN_EQUIPMENT, toRecipeInstructionMatchers())

                ))
                .setRecipeDifficultDefinitionPredicates(toMap(
                        RecipeDifficultyDefinition.EASY, stringPredicatePattern("(?i)fácil"),
                        RecipeDifficultyDefinition.MEDIUM, stringPredicatePattern("(?i)medio"),
                        RecipeDifficultyDefinition.ADVANCED, stringPredicatePattern("(?i)avanzado")
                ))
                .setRecipePriceDefinitionPredicates(toMap(
                        RecipePriceDefinition.LOW, stringPredicatePattern("(?i)económico|barato")
                ))
                .addRecipeCategoryByPrincipal(BASIC, "Básicos",
                        "Básico")
                .addRecipeCategoryByPrincipal(START, "Entrantes y ensaladas",
                        "Entradas y ensaladas",
                        "Entrante",
                        "Entrantes",  // Check results without plurals and remove them if is the same... I think it will be
                        "Entrada",
                        "Entradas",
                        "Ensaladas",
                        "Ensalada")
                .addRecipeCategoryByPrincipal(SOUP, "Sopas",
                        "Sopa")
                .addRecipeCategoryByPrincipal(PASTARICE, "Pastas y arroces",
                        "Pasta",
                        "Pastas",
                        "Arroz",
                        "Arroces")
                .addRecipeCategoryByPrincipal(MAINMEAT, "Carnes y aves",
                        "Plato principal - Carnes y aves",
                        "Plato principal - carnes y pollo",
                        "Carne",
                        "Carnes",
                        "Pollo",
                        "Ave",
                        "Aves")
                .addRecipeCategoryByPrincipal(MAINFISH, "Pescados y mariscos",
                        "Plato principal - Pescados y mariscos",
                        "Pescados",
                        "Pescado",
                        "Mariscos",
                        "Marisco"
                )
                .addRecipeCategoryByPrincipal(MAINVEG,
                        "Vegetarianos",
                        "Plato principal - Vegetarianos",
                        "Platos principales vegetarianos",
                        "Vegetarianos",
                        "Vegetariano")
                .addRecipeCategoryByPrincipal(MAINOTHER,
                        "Otros principales",
                        "Otros platos principales",
                        "Plato principal",
                        "Plato prinicipal",
                        "Plato principal - otros")
                .addRecipeCategoryByPrincipal(SIDE,
                        "Acompañamientos",
                        "Guarniciones")
                .addRecipeCategoryByPrincipal(BAKE,
                        "Panadería y bollería",
                        "Pan y bollería",
                        "Masas",
                        "Horneados",
                        "Pan",
                        "Bizcocho",
                        "Bizcochos",
                        "Galleta",
                        "Galletas",
                        "Panaderia",
                        "Bolleria")
                .addRecipeCategoryByPrincipal(BAKE_SAVORY,
                        "Masas saladas",
                        "Masas saladas (quiches, pizzas, empanadas)",
                        "Empanadas, quiches y pizzas",
                        "Horneados salados",
                        "Quiche",
                        "Pizza",
                        "Pizzas",
                        "Pizza y Focaccia",
                        "Pizza y Focaccias",
                        "Focaccia",
                        "Empanada",
                        "Empanadas")
                .addRecipeCategoryByPrincipal(DESSERT,
                        "Postres y dulces",
                        "Postres",
                        "Postre")
                .addRecipeCategoryByPrincipal(BAKE_SWEET,
                        "Repostería",
                        "Horneados dulces")
                .addRecipeCategoryByPrincipal(SAUCE_SWEET,
                        "Salsas y cremas dulces, mermeladas",
                        "Salsas y cremas dulces, mermeladas.",
                        "Salsas, dips y untables - dulces",
                        "Mermeladas",
                        "Mermelada",
                        "Confituras",
                        "Confitura")
                .addRecipeCategoryByPrincipal(DRINK,
                        "Bebidas")
                .addRecipeCategoryByPrincipal(SNACK,
                        "Entrantes y aperitivos",
                        "Botanas y canapés",
                        "Canapés",
                        "Snacks",
                        "Snack",
                        "Fiesta",
                        "Snacks y picoteo",
                        "Refrigerios")
                .addRecipeCategoryByPrincipal(SAUCE_SAVO,
                        "Salsas saladas, dips y patés",
                        "Salsas, dips y untables - salados",
                        "Salsas, dips y untables",
                        "Dips",
                        "Pates",
                        "Pate")
                .addRecipeCategoryByPrincipal(BABY,
                        "Alimentación infantil",
                        "Alimentación para bebés",
                        "Infantil",
                        "Bebés",
                        "Bebé")
                .addRecipeCategoryByPrincipal(BREAK, "Desayuno")
                .addRecipeCategoryByPrincipal(MENU, "Menús",
                        "Menús y más")
                .setRecipeCategoryNamePreprocessFunction(
                        new Function<>() {
                            private final Pattern cleanupPattern = stringPattern("(?i)" + String.join("|",
                                    "^Al ",
                                    "^Alimentación ",
                                    "^Con ",
                                    "^Cena ",
                                    "^Calentado ",
                                    "Caliente",
                                    "^Cocin(?:a|ar|ado|ando) (?:al|para)?",
                                    "^Comida(s)? ",
                                    "^Contiene ",
                                    "^De ",
                                    "^Del ",
                                    "^El ",
                                    "^En ",
                                    "^La ",
                                    "^Para (?:la|el)?",
                                    "^Platos de ",
                                    "^Platillo(s)? ",
                                    "^Receta(s)? ",
                                    "Sabroso",
                                    ".*Thermom.*"
                            ));

                            @Override
                            public String apply(String s) {
                                return RECIPE_CATEGORY_NAME_FORMAT_DEFAULT_FUNCTION.apply(removeAll(s, cleanupPattern));
                            }
                        });
    }
}