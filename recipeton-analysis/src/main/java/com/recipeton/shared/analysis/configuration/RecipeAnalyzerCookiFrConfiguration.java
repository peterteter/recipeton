package com.recipeton.shared.analysis.configuration;

/*-
 * #%L
 * recipeton-shared
 * %%
 * Copyright (C) 2021 tincomisc
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.recipeton.shared.analysis.domain.*;
import com.recipeton.shared.util.MapUtil;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.tuple.Pair;

import java.util.Set;
import java.util.function.Function;
import java.util.regex.Pattern;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static com.recipeton.shared.analysis.configuration.CookiConstants.*;
import static com.recipeton.shared.analysis.domain.RecipeInstructionRegexMatcher.toRecipeInstructionMatchers;
import static com.recipeton.shared.analysis.service.preprocess.RecipeInstructionSplitter.DO_NOT_SPLIT;
import static com.recipeton.shared.util.MapUtil.toMapP;
import static com.recipeton.shared.util.TextUtil.*;
import static java.util.List.of;
import static org.apache.commons.lang3.StringUtils.containsIgnoreCase;

// WIP! COPIED FROM Es
public class RecipeAnalyzerCookiFrConfiguration extends RecipeAnalyzerConfiguration {

    public static final String UNIT_PIECE = "piece";

    public RecipeAnalyzerCookiFrConfiguration() {
        super("COOKI_FR", rd -> containsIgnoreCase(rd.getLocale(), "fr") && containsIgnoreCase(rd.getSource(), "cooki"));

        this.setStopWords(Set.of(
                "a", "à", "â", "abord", "afin", "ah", "ai", "aie", "ainsi", "allaient", "allo", "allô", "allons", "après", "assez", "attendu", "au", "aucun", "aucune", "aujourd", "aujourd'hui", "auquel", "aura", "auront", "aussi", "autre", "autres", "aux", "auxquelles", "auxquels", "avaient", "avais", "avait", "avant", "avec", "avoir", "ayant", "b", "bah", "beaucoup", "bien", "bigre", "boum", "bravo", "brrr", "c", "ça", "car", "ce", "ceci", "cela", "celle", "celle-ci", "celle-là", "celles", "celles-ci", "celles-là", "celui", "celui-ci", "celui-là", "cent", "cependant", "certain", "certaine", "certaines", "certains", "certes", "ces", "cet", "cette", "ceux", "ceux-ci", "ceux-là", "chacun", "chaque", "cher", "chère", "chères", "chers", "chez", "chiche", "chut", "ci", "cinq", "cinquantaine", "cinquante", "cinquantième", "cinquième", "clac", "clic", "combien", "comme", "comment", "compris", "concernant", "contre", "couic", "crac", "d", "da", "dans", "de", "debout", "dedans", "dehors", "delà", "depuis", "derrière", "des", "dès", "désormais", "desquelles", "desquels", "dessous", "dessus", "deux", "deuxième", "deuxièmement", "devant", "devers", "devra", "différent", "différente", "différentes", "différents", "dire", "divers", "diverse", "diverses", "dix", "dix-huit", "dixième", "dix-neuf", "dix-sept", "doit", "doivent", "donc", "dont", "douze", "douzième", "dring", "du", "duquel", "durant", "e", "effet", "eh", "elle", "elle-même", "elles", "elles-mêmes", "en", "encore", "entre", "envers", "environ", "es", "ès", "est", "et", "etant", "étaient", "étais", "était", "étant", "etc", "été", "etre", "être", "eu", "euh", "eux", "eux-mêmes", "excepté", "f", "façon", "fais", "faisaient", "faisant", "fait", "feront", "fi", "flac", "floc", "font", RecipeUnitDefinition.UNIT_GRAM, "gens", "h", "ha", "hé", "hein", "hélas", "hem", "hep", "hi", "ho", "holà", "hop", "hormis", "hors", "hou", "houp", "hue", "hui", "huit", "huitième", "hum", "hurrah", "i", "il", "ils", "importe", "j", "je", "jusqu", "jusque", "k", "l", "la", "là", "laquelle", "las", "le", "lequel", "les", "lès", "lesquelles", "lesquels", "leur", "leurs", "longtemps", "lorsque", "lui", "lui-même", "m", "ma", "maint", "mais", "malgré", "me", "même", "mêmes", "merci", "mes", "mien", "mienne", "miennes", "miens", "mille", "mince", "moi", "moi-même", "moins", "mon", "moyennant", "n", "na", "ne", "néanmoins", "neuf", "neuvième", "ni", "nombreuses", "nombreux", "non", "nos", "notre", "nôtre", "nôtres", "nous", "nous-mêmes", "nul", "o", "o|", "ô", "oh", "ohé", "olé", "ollé", "on", "ont", "onze", "onzième", "ore", "ou", "où", "ouf", "ouias", "oust", "ouste", "outre", "p", "paf", "pan", "par", "parmi", "partant", "particulier", "particulière", "particulièrement", "pas", "passé", "pendant", "personne", "peu", "peut", "peuvent", "peux", "pff", "pfft", "pfut", "pif", "plein", "plouf", "plus", "plusieurs", "plutôt", "pouah", "pour", "pourquoi", "premier", "première", "premièrement", "près", "proche", "psitt", "puisque", "q", "qu", "quand", "quant", "quanta", "quant-à-soi", "quarante", "quatorze", "quatre", "quatre-vingt", "quatrième", "quatrièmement", "que", "quel", "quelconque", "quelle", "quelles", "quelque", "quelques", "quelqu'un", "quels", "qui", "quiconque", "quinze", "quoi", "quoique", "r", "revoici", "revoilà", "rien", "s", "sa", "sacrebleu", "sans", "sapristi", "sauf", "se", "seize", "selon", "sept", "septième", "sera", "seront", "ses", "si", "sien", "sienne", "siennes", "siens", "sinon", "six", "sixième", "soi", "soi-même", "soit", "soixante", "son", "sont", "sous", "stop", "suis", "suivant", "sur", "surtout", "t", "ta", "tac", "tant", "te", "té", "tel", "telle", "tellement", "telles", "tels", "tenant", "tes", "tic", "tien", "tienne", "tiennes", "tiens", "toc", "toi", "toi-même", "ton", "touchant", "toujours", "tous", "tout", "toute", "toutes", "treize", "trente", "très", "trois", "troisième", "troisièmement", "trop", "tsoin", "tsouin", "tu", "u", "un", "une", "unes", "uns", "v", "va", "vais", "vas", "vé", "vers", "via", "vif", "vifs", "vingt", "vivat", "vive", "vives", "vlan", "voici", "voilà", "vont", "vos", "votre", "vôtre", "vôtres", "vous", "vous-mêmes", "vu", "w", "x", "y", "z", "zut", "alors", "aucuns", "bon", "devrait", "dos", "droite", "début", "essai", "faites", "fois", "force", "haut", "ici", "juste", "maintenant", "mine", "mot", "nommés", "nouveaux", "parce", "parole", "personnes", "pièce", "plupart", "seulement", "soyez", "sujet", "tandis", "valeur", "voie", "voient", "état", "étions"))
                .setTokenFormatFunction(
                        new Function<>() {
                            private final Pattern cleanupPattern = Pattern.compile("(?i)" + String.join("|",
                                    "s$",
                                    "^l'"
                            ));

                            @Override
                            public String apply(String s) {
                                if (s.length() <= TOKEN_LENGTH_MIN) {
                                    return s;
                                } else {
                                    return removeAll(s, cleanupPattern);
                                }
                            }
                        })
                .setRecipeUnitDefinitions(
                        // Add missing!
                        new RecipeUnitDefinition("kg"),
                        new RecipeUnitDefinition(RecipeUnitDefinition.UNIT_GRAM),
                        new RecipeUnitDefinition(UNIT_PIECE, "morceau"),
                        new RecipeUnitDefinition("l"),
                        new RecipeUnitDefinition("ml"),
                        new RecipeUnitDefinition("c. à café"),
                        new RecipeUnitDefinition("c. à soupe"),
                        new RecipeUnitDefinition("brins"),
                        new RecipeUnitDefinition("pincée", "pincées")
                )
                .setUnitPieceDefault(UNIT_PIECE)
                .setUnitWeightedDefault(RecipeUnitDefinition.UNIT_GRAM)
                .setNumericStrings(MapUtil.toMap(
                        "un", ONE,
                        "deux", TWO,
                        "trois", THREE,
                        "quatre", FOUR,
                        "cinq", FIVE,
                        "six", SIX,
                        "sept", SEVEN,
                        "huit", EIGHT,
                        "neuf", NINE,
                        "dix", TEN
                ))
                .setIngredientTokenSplitPattern(stringPattern(" |d'"))
                .setIngredientUnitSeparatorTokens(Set.of("de", "d'")) // 300 g [de] pommes, 10 g [d']eau
                .setIngredientPreparationTokens(Set.of(
                        // Complete!!!!!
                        "cuits",
                        "ciselée",
                        "en", // en conserve
                        "décortiquées",
                        "entières",
                        "égouttés",
                        "épaisse",
                        "équeutés",
                        "pluches",
                        "effeuillé",
                        "frais",
                        "fraîche",
                        "fraîchement",
                        "hachée",
                        "moulu",
                        "râpé",
                        "rincés"
                ))
                .setIngredientAlternativeSeparatorTokenSet("ou ")
                .setIngredientOptionalIndicatorTokens(Set.of("(optionnel)"))
                .setInstructionSplitToNextTokenSequences(
                        Stream.concat(
                                Stream.of(
                                        Pair.of(" approx.", DO_NOT_SPLIT),  // Careful. Due to limitationa all need to start from space or punctuation
                                        Pair.of(".", "")
                                ),
                                Stream.of(
                                        // NOTE: Complete!!
                                        "cuire",
                                        "chauffer",
                                        "chauffez",
                                        "dorer",
                                        "émulsionner",
                                        "émulsionnez",
                                        "fouetter",
                                        "fouettez",
                                        "frire",
                                        "frirez",
                                        "laisser",
                                        "laissez",
                                        "mélanger",
                                        "mélangez",
                                        "mixer",
                                        "mixez",
                                        "réduire",
                                        // --
                                        "peser"
                                ).map(String::toLowerCase).flatMap(v -> Stream.of(
                                        " et ", // Careful. Due to limitationa all need to start from space or punctuation
                                        ", et puis ",
                                        ", puis ",
                                        " puis ",
                                        ", faire ",
                                        ", faites ",
                                        ", "
                                        ).map(s -> Pair.of(s + v, StringUtils.capitalize(v)))
                                )
                        ).collect(Collectors.toMap(Pair::getLeft, Pair::getRight))
                )
                .setUtensilTokens(Set.of())
                .setCommandProgramPredicates(MapUtil.toMap(
                        RecipeCommandProgram.DOUGH, stringPredicatePatterns("(?i)" + TM_DOUGH + "|dough"),
                        RecipeCommandProgram.FERMENT, of(s -> containsIgnoreCase(s, "ferment")),
                        RecipeCommandProgram.HEAT_ONLY, of(),
                        RecipeCommandProgram.SOUS_VIDE, of(),
                        RecipeCommandProgram.TURBO, of(s -> containsIgnoreCase(s, "turbo"))
                ))
                .setCommandDurationPredicates(MapUtil.toMap(
                        HALF_SECOND, stringPredicatePatterns("0\\.5 sec")
                ))
                .setCommandDurationHoursPattern(stringPattern("^([0-9]+) h"))
                .setCommandDurationMinutesPattern(stringPattern("^([0-9]+) min"))
                .setCommandDurationSecondsPattern(stringPattern("^([0-9]+) sec"))
                .setCommandTemperaturePredicates(MapUtil.toMap(
                        STEAMER_TEMPERATURE, stringPredicatePatterns("(?i)varoma|steamer")
                ))
                .setCommandTemperaturePattern(stringPattern("^([0-9]+)°C"))
                .setCommandSpeedPredicates(MapUtil.toMap(
                        COMMAND_SPEED_SLOW_VALUE, stringPredicatePatterns("(?i)vitesse" + TM_SLOW + "|vitesse slow")
                ))
                .setCommandRotationReversePredicates(stringPredicatePatterns("(?i)" + TM_REVERSE + "|reverse")).setCommandSpeedPattern(stringPattern("vitesse ([//.0-9]+)"))
                .setCommandRepetitionFromPattern(stringPattern("^([0-9]+) (?:fois)"))
                .setCommandRepetitionFromToPattern(stringPattern("^([0-9]+)-([0-9]+) fois"))
                .setInstructionMatchers(toMapP(
// COMPLETE!!!
                        Pair.of(RecipeAnalysisTag.ADD, toRecipeInstructionMatchers("(?i)ajouter")),
                        Pair.of(RecipeAnalysisTag.PLACE, toRecipeInstructionMatchers("(?i)mettre")),

//                        Pair.of(RecipeAnalysisTag.ADD_BACK, toRecipeInstructionMatchers("(?i)regrese|retorne")),

                        Pair.of(RecipeAnalysisTag.WEIGH, toRecipeInstructionMatchers("(?i)^peser (sur|dans)")),
//                        Pair.of(RecipeAnalysisTag.ON_TOP_OF_MAIN_COMPARTMENT, toRecipeInstructionMatchers("(?i)onto mixing bowl lid")),
                        Pair.of(RecipeAnalysisTag.REF_IN_MAIN_COMPARTMENT, toRecipeInstructionMatchers("(?i)le bol")),
//                        Pair.of(RecipeAnalysisTag.IN_ACCESORY, toRecipeInstructionMatchers()),
//                        Pair.of(RecipeAnalysisTag.ALL_INGREDIENTS, toRecipeInstructionMatchers()),
//                        Pair.of(RecipeAnalysisTag.ALL_INGREDIENTS_REMAINING, toRecipeInstructionMatchers()),
//                        Pair.of(RecipeAnalysisTag.PARALLEL, toRecipeInstructionMatchers()),
                        Pair.of(RecipeAnalysisTag.SERVING, toRecipeInstructionMatchers("(?i)servez")),


                        // COMPLETE!!!
//                        Pair.of(RecipeAnalysisTag.OVEN_PREHEAT, defineMe("(?i)preheat.*oven")),
//                        Pair.of(RecipeAnalysisTag.REFRIGERATE, defineMe("(?i)(keep|place|store).*(refrigerator|fridge|refrigerated)")),

//                        Pair.of(RecipeAnalysisTag.FREEZE, defineMe("(?i)chill.*fridge")),

//                        Pair.of(RecipeAnalysisTag.SUMMERING_BASKET, defineMe("(?i)using.*simmering basket.*(drain|strain)")),
//                        Pair.of(RecipeAnalysisTag.SIMMERING_BASKET_PUT, defineMe("(?i)(?:insert).*simmering basket")),
//                        Pair.of(RecipeAnalysisTag.SIMMERING_BASKET_REMOVE, defineMe("(?i)(?:remove).*simmering basket")),
                        Pair.of(RecipeAnalysisTag.SIMMERING_BASKET_AS_LID_PUT, toRecipeInstructionMatchers("(?i)remplaçant.*gobelet doseur.*panier cuisson")), // Needs context analysis to place before tm step
//                        Pair.of(RecipeAnalysisTag.SIMMERING_BASKET_AS_LID_REMOVE, defineMe()),
//                        Pair.of(RecipeAnalysisTag.SIMMERING_BASKET_REMOVE_MEASURING_CUP_PUT, defineMe()),

//                        Pair.of(RecipeAnalysisTag.MIXING_BOWL, toRecipeInstructionMatchers("(?i)(?:remove).*mixing.*bowl lid")),
//                        Pair.of(RecipeAnalysisTag.MIXING_BOWL_PUT, defineMe()),
//                        Pair.of(RecipeAnalysisTag.MIXING_BOWL_REMOVE, defineMe("(?i)(?:remove).*(?:mixing bowl(?!.*? lid))")),
//                        Pair.of(RecipeAnalysisTag.MIXING_BOWL_EMPTY, defineMe("(?i)(?:empty).*mixing.*bowl")),
                        Pair.of(RecipeAnalysisTag.MIXING_BOWL_CLEAN, toRecipeInstructionMatchers("(?i)(?:Rincez).*le bol")),

                        Pair.of(RecipeAnalysisTag.TRANSFER_TO_BOWL, toRecipeInstructionMatchers("(?i)transvaser.*(?:dans).*(?:récipient|saladier)")),
//                        Pair.of(RecipeAnalysisTag.MEASURING_CUP, defineMe()),
                        Pair.of(RecipeAnalysisTag.MEASURING_CUP_NOTUSE, toRecipeInstructionMatchers("(?i)(?:sans).*gobelet doseur")),
//                        Pair.of(RecipeAnalysisTag.MEASURING_CUP_PUT, defineMe("(?i)insert.*measuring cup")),
//                        Pair.of(RecipeAnalysisTag.MEASURING_CUP_HOLD_VIBRATION, defineMe("(?i)hold.*measuring cup")),

//                        Pair.of(RecipeAnalysisTag.BUTTERFLY, defineMe()),
//                        Pair.of(RecipeAnalysisTag.BUTTERFLY_PUT, toRecipeInstructionMatchers("(?i)insert.*butterfly")),
//                        Pair.of(RecipeAnalysisTag.BUTTERFLY_REMOVE, toRecipeInstructionMatchers("(?i)remove.*butterfly")),

//                        Pair.of(RecipeAnalysisTag.SPATULA, defineMe()),
//                        Pair.of(RecipeAnalysisTag.SPATULA_MIX, toRecipeInstructionMatchers("(?i)mix with spatula")),
//                        Pair.of(RecipeAnalysisTag.SPATULA_SCRAP_SIDE, toRecipeInstructionMatchers("(?i)scrape.*bowl.*spatula")),
//                        Pair.of(RecipeAnalysisTag.SPATULA_MIX_WELL, toRecipeInstructionMatchers("(?i)mix.*well with spatula")),

//                        Pair.of(RecipeAnalysisTag.STEAMER, toRecipeInstructionMatchers("(?i)close.*(?:varoma|steamer)")),
//                        Pair.of(RecipeAnalysisTag.STEAMER_PUT, toRecipeInstructionMatchers("(?i)(?:place|return).*(?:varoma|steamer).*(?:to|in).*position")),
//                        Pair.of(RecipeAnalysisTag.STEAMER_REMOVE, toRecipeInstructionMatchers("(?i)remove.*(?:varoma|steamer)")),
//                        Pair.of(RecipeAnalysisTag.STEAMER_TRAY_PUT, toRecipeInstructionMatchers("(?i)insert.*(?:varoma|steamer).*tray")),
//                        Pair.of(RecipeAnalysisTag.STEAMER_TRAY_REMOVE, toRecipeInstructionMatchers("(?i)remove.*(?:varoma|steamer).*tray")),
//                        Pair.of(RecipeAnalysisTag.STEAMER_DISH_PUT, toRecipeInstructionMatchers("(?i)place.*into (varoma|steamer) dish")),
//                        Pair.of(RecipeAnalysisTag.STEAMER_DISH_REMOVE, toRecipeInstructionMatchers("(?i)remove.*(varoma|steamer) dish")),

//                        Pair.of(RecipeAnalysisTag.THERMOMIX_PARTS, toRecipeInstructionMatchers("(?i)insert.*(?:blade cover)")),
//                        Pair.of(RecipeAnalysisTag.USEFUL_ITEMS, defineMe()),
                        Pair.of(RecipeAnalysisTag.KITCHEN_EQUIPMENT, toRecipeInstructionMatchers())

                ))
                .setRecipeDifficultDefinitionPredicates(MapUtil.toMap(
                        RecipeDifficultyDefinition.EASY, stringPredicatePattern("(?i)facile"),
                        RecipeDifficultyDefinition.MEDIUM, stringPredicatePattern("(?i)moyen"),
                        RecipeDifficultyDefinition.ADVANCED, stringPredicatePattern("(?i)difficile")
                ))
                .setRecipePriceDefinitionPredicates(MapUtil.toMap(
                        RecipePriceDefinition.LOW, stringPredicatePattern("(?i)economique")
                ))
                .addRecipeCategoryByPrincipal(BASIC, "Basiques",
                        "Les basiques")
                .addRecipeCategoryByPrincipal(START, "Entrées",
                        "Entrées et salades")
                .addRecipeCategoryByPrincipal(SOUP, "Soupes")
                .addRecipeCategoryByPrincipal(PASTARICE, "Pâtes et riz")
                .addRecipeCategoryByPrincipal(MAINMEAT, "Viande et poulet",
                        "Plat principal - Viandes",
                        "Plat principal - Viande et poulet")
                .addRecipeCategoryByPrincipal(MAINFISH, "Poissons et fruits de mer",
                        "Plat principal - Poissons et fruits de mer")
                .addRecipeCategoryByPrincipal(MAINVEG, "Végétarien",
                        "Plat principal - Végétariens")
                .addRecipeCategoryByPrincipal(MAINOTHER, "Autre principal",
                        "Plat principal",
                        "Plat principal - Autres")
                .addRecipeCategoryByPrincipal(SIDE, "Accompagnements")
                .addRecipeCategoryByPrincipal(BAKE, "Pains et rouleaux",
                        "Pains et viennoiseries")
                .addRecipeCategoryByPrincipal(BAKE_SAVORY, "Tartes, quiches et pizzas",
                        "Quiches, pizzas et cakes",
                        "Tartes, quiches et snacks")
                .addRecipeCategoryByPrincipal(DESSERT, "Desserts et confiseries",
                        "Desserts et sucreries",
                        "Entremets et glaces")
                .addRecipeCategoryByPrincipal(BAKE_SWEET, "Pâtisserie, gâteaux et tartes",
                        "Pâtisseries et tartes sucrées",
                        "Gâteaux et biscuits")
                .addRecipeCategoryByPrincipal(SAUCE_SWEET, "Confitures, coulis et pâtes à tartiner",
                        "Pâtes à tartiner",
                        "Coulis",
                        "Confitures")
                .addRecipeCategoryByPrincipal(DRINK, "Boissons")
                .addRecipeCategoryByPrincipal(SNACK, "Snacks",
                        "En-cas",
                        "Apéritifs, mises en bouche et snacks")
                .addRecipeCategoryByPrincipal(SAUCE_SAVO, "Sauces, dips et confitures, sucré",
                        "Sauces, dips et tartinades",
                        "Sauces, dips et tartinades, salé",
                        "Sauces, crèmes et tartinades",
                        "Sauces et tartinades")
                .addRecipeCategoryByPrincipal(BABY, "Recettes pour bébés",
                        "Recettes pour les tout-petits")
                .addRecipeCategoryByPrincipal(BREAK, "Petit Déjeuner",
                        "Petits-déjeuners")
                .addRecipeCategoryByPrincipal(MENU, "Menus")
                .setRecipeCategoryNamePreprocessFunction(
                        new Function<>() {
                            private final Pattern cleanupPattern = Pattern.compile("(?i)" + String.join("|",
                                    "^Le ",
                                    "^Cuisiner ",
                                    "Rapide"
                            ));

                            @Override
                            public String apply(String s) {
                                return RECIPE_CATEGORY_NAME_FORMAT_DEFAULT_FUNCTION.apply(removeAll(s, cleanupPattern));
                            }
                        });
    }
}