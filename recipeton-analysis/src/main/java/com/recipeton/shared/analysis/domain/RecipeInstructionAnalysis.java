package com.recipeton.shared.analysis.domain;

/*-
 * #%L
 * recipeton-shared
 * %%
 * Copyright (C) 2021 tincomisc
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import lombok.Data;
import lombok.ToString;
import lombok.experimental.Accessors;

import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@Data
@ToString(onlyExplicitlyIncluded = true)
@Accessors(chain = true)
public class RecipeInstructionAnalysis implements RecipeAnalysisTagged {

    @ToString.Include
    private final RecipeInstructionTokenization tokenization;
    @ToString.Include
    private final Set<RecipeTerm> terms = new HashSet<>();

    private RecipeInstructionAnalysis prev;
    private RecipeInstructionAnalysis next;

    public void addTerm(RecipeTerm recipeTerm) {
        terms.add(recipeTerm);
    }

    public List<RecipeIngredientTerm> getRecipeIngredientTerms() {
        return streamRecipeIngredientTerms()
                .collect(Collectors.toList());
    }

    @Override
    public Collection<RecipeAnalysisTag> getTags() {
        return streamTermsByTagged().flatMap(t -> t.getTags().stream()).collect(Collectors.toSet());
    }

    public String getText() {
        return getTokenization().getSource();
    }

    public boolean isTagAnyPresent(RecipeAnalysisTag... tags) {
        return Stream.of(tags).anyMatch(this::isTagPresent);
    }

    @Override
    public boolean isTagPresent(RecipeAnalysisTag tag) {
        return streamTermsByTagged().anyMatch(t -> t.isTagPresent(tag));
    }

    public boolean isTagPresentInNext(RecipeAnalysisTag tag) {
        return Optional.ofNullable(getNext()).map(r -> r.isTagPresent(tag)).orElse(false);
    }

    public boolean isTagPresentInPrev(RecipeAnalysisTag tag) {
        return Optional.ofNullable(getPrev()).map(r -> r.isTagPresent(tag)).orElse(false);
    }

    public boolean isTagsAllPresent(RecipeAnalysisTag... tags) {
        return Stream.of(tags).allMatch(this::isTagPresent);
    }

    public Stream<RecipeIngredientTerm> streamRecipeIngredientTerms() {
        return getTerms().stream()
                .filter(t -> t instanceof RecipeIngredientTerm)
                .map(t -> (RecipeIngredientTerm) t);
    }

    private Stream<RecipeAnalysisTagged> streamTermsByTagged() {
        return terms.stream()
                .filter(t -> t instanceof RecipeAnalysisTagged)
                .map(t -> (RecipeAnalysisTagged) t);
    }
}
