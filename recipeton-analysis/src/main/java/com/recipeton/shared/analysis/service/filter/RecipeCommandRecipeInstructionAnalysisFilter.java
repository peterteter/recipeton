package com.recipeton.shared.analysis.service.filter;

/*-
 * #%L
 * recipeton-shared
 * %%
 * Copyright (C) 2021 tincomisc
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.recipeton.shared.analysis.domain.RecipeCommandTerm;
import com.recipeton.shared.analysis.domain.RecipeInstructionAnalysis;
import com.recipeton.shared.analysis.domain.RecipeInstructionMatch;
import com.recipeton.shared.analysis.service.RecipeCommandAnalyzer;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.tuple.Pair;

import java.util.Optional;
import java.util.stream.Stream;

public class RecipeCommandRecipeInstructionAnalysisFilter implements RecipeInstructionAnalysisFilter {

    private static final String QUERY = "is_command?";

    private final RecipeCommandAnalyzer recipeCommandAnalyzer;
    private final String delimiterStart;
    private final String delimiterEnd;

    public RecipeCommandRecipeInstructionAnalysisFilter(RecipeCommandAnalyzer recipeCommandAnalyzer, Pair<String, String> commandDelimiters) {
        this.recipeCommandAnalyzer = recipeCommandAnalyzer;
        this.delimiterStart = commandDelimiters.getLeft();
        this.delimiterEnd = commandDelimiters.getRight();
    }

    @Override
    public RecipeInstructionAnalysis apply(RecipeInstructionAnalysis recipeInstructionAnalysis) {
        String text = recipeInstructionAnalysis.getText();
        try {
            int startIndex = text.indexOf(delimiterStart);
            while (startIndex != -1) {
                int fromIndex = startIndex + delimiterStart.length();
                int endIndex = text.indexOf(delimiterEnd, fromIndex);
                String action = text.substring(fromIndex, endIndex);

                createCommandMatch(action, fromIndex).ifPresent(recipeInstructionAnalysis::addTerm);

                startIndex = text.indexOf(delimiterStart, endIndex + delimiterEnd.length());
            }
        } catch (StringIndexOutOfBoundsException e) {
            throw new IllegalStateException("Malformed tags found text=" + text, e);
        }
        return recipeInstructionAnalysis;

    }

    public Optional<RecipeCommandTerm> createCommandMatch(String action, int matchOffset) {
        if (StringUtils.isBlank(action)) {
            return Optional.empty();
        }

        RecipeCommandTerm recipeCommandTerm = new RecipeCommandTerm(action, new RecipeInstructionMatch(matchOffset, action, RecipeInstructionMatch.SCORE_MAX, QUERY));
        Stream.of(StringUtils.split(action, "/")).forEach(p -> recipeCommandAnalyzer.enrich(recipeCommandTerm, p));

        if (!recipeCommandTerm.isValid()) {
            return Optional.empty();
        }
        return Optional.of(recipeCommandTerm);
    }
}
