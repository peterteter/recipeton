package com.recipeton.shared.analysis.service;

/*-
 * #%L
 * recipeton-shared
 * %%
 * Copyright (C) 2021 tincomisc
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.recipeton.shared.analysis.configuration.RecipeAnalyzerConfiguration;
import com.recipeton.shared.analysis.domain.RecipeCommandProgram;
import com.recipeton.shared.analysis.domain.RecipeCommandRotation;
import com.recipeton.shared.analysis.domain.RecipeCommandTerm;

import java.math.BigDecimal;
import java.time.Duration;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.function.Predicate;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class RecipeCommandAnalyzer {

    private final RecipeAnalyzerConfiguration configuration;

    public RecipeCommandAnalyzer(RecipeAnalyzerConfiguration configuration) {
        this.configuration = configuration;
    }

    public void enrich(RecipeCommandTerm recipeCommandTerm, String part) { // NOPMD Complex

        for (Map.Entry<Duration, List<Predicate<String>>> entry : configuration.getCommandDurationPredicates().entrySet()) {
            if (entry.getValue().stream().anyMatch(p -> p.test(part))) {
                recipeCommandTerm.setDuration(entry.getKey());
                return;
            }
        }
        Optional<Duration> durationSeconds = extractLong(part, configuration.getCommandDurationSecondsPattern(), 1).map(Duration::ofSeconds);
        if (durationSeconds.isPresent()) {
            recipeCommandTerm.setDuration(durationSeconds.get());
            return;
        }

        Optional<Duration> durationMinutes = extractLong(part, configuration.getCommandDurationMinutesPattern(), 1).map(Duration::ofMinutes);
        if (durationMinutes.isPresent()) {
            recipeCommandTerm.setDuration(durationMinutes.get());
            return;
        }

        Optional<Duration> durationHours = extractLong(part, configuration.getCommandDurationHoursPattern(), 1).map(Duration::ofHours);
        if (durationHours.isPresent()) {
            recipeCommandTerm.setDuration(durationHours.get());
            return;
        }

        for (Map.Entry<Long, List<Predicate<String>>> entry : configuration.getCommandTemperaturePredicates().entrySet()) {
            if (entry.getValue().stream().anyMatch(p -> p.test(part))) {
                recipeCommandTerm.setTemperature(entry.getKey());
                return;
            }
        }
        Optional<Long> temperature = extractLong(part, configuration.getCommandTemperaturePattern(), 1);
        if (temperature.isPresent()) {
            recipeCommandTerm.setTemperature(temperature.get());
            return;
        }

        for (Map.Entry<BigDecimal, List<Predicate<String>>> entry : configuration.getCommandSpeedPredicates().entrySet()) {
            if (entry.getValue().stream().anyMatch(p -> p.test(part))) {
                recipeCommandTerm.setSpeed(entry.getKey());
                return;
            }
        }
        Optional<BigDecimal> speed = extractNumeric(part, configuration.getCommandSpeedPattern(), 1);
        if (speed.isPresent()) {
            recipeCommandTerm.setSpeed(speed.get());
            return;
        }

        if (configuration.getCommandRotationReversePredicates().stream().anyMatch(p -> p.test(part))) {
            recipeCommandTerm.setRotation(RecipeCommandRotation.REVERSE);
            return;
        }

        for (Map.Entry<RecipeCommandProgram, List<Predicate<String>>> entry : configuration.getCommandProgramPredicates().entrySet()) {
            if (entry.getValue().stream().anyMatch(p -> p.test(part))) {
                recipeCommandTerm.setProgram(entry.getKey());
                return;
            }
        }

        Matcher m = configuration.getCommandRepetitionFromToPattern().matcher(part);
        if (m.find()) {
            recipeCommandTerm.setRepetitionFrom(Integer.valueOf(m.group(1)));
            recipeCommandTerm.setRepetitionTo(Integer.valueOf(m.group(2)));
            return;
        }

        m = configuration.getCommandRepetitionFromPattern().matcher(part);
        if (m.find()) {
            recipeCommandTerm.setRepetitionFrom(Integer.valueOf(m.group(1)));
            return;
        }
    }

    public Optional<Long> extractLong(String text, Pattern pattern, int group) {
        return extractString(text, pattern, group).map(Long::valueOf);
    }

    public Optional<BigDecimal> extractNumeric(String text, Pattern pattern, int group) {
        return extractString(text, pattern, group).map(BigDecimal::new);
    }

    public Optional<String> extractString(String text, Pattern pattern, int group) {
        Matcher m = pattern.matcher(text);
        if (m.find()) {
            return Optional.of(m.group(group));
        }
        return Optional.empty();
    }


}
