package com.recipeton.shared.analysis.configuration;

/*-
 * #%L
 * recipeton-shared
 * %%
 * Copyright (C) 2021 tincomisc
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.recipeton.shared.analysis.domain.*;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.tuple.Pair;

import java.util.Set;
import java.util.function.Function;
import java.util.regex.Pattern;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static com.recipeton.shared.analysis.configuration.CookiConstants.*;
import static com.recipeton.shared.analysis.domain.RecipeInstructionRegexMatcher.toRecipeInstructionMatchers;
import static com.recipeton.shared.analysis.service.preprocess.RecipeInstructionSplitter.DO_NOT_SPLIT;
import static com.recipeton.shared.util.MapUtil.toMap;
import static com.recipeton.shared.util.MapUtil.toMapP;
import static com.recipeton.shared.util.TextUtil.*;
import static java.util.List.of;
import static org.apache.commons.lang3.StringUtils.containsIgnoreCase;

// WIP! COPIED FROM Es
public class RecipeAnalyzerCookiEnConfiguration extends RecipeAnalyzerConfiguration {

    public static final String UNIT_PIECE = "piece";
    public static final String UNIT_OZ = "oz";

    public RecipeAnalyzerCookiEnConfiguration() {
        super("COOKI_EN", rd -> containsIgnoreCase(rd.getLocale(), "en") && containsIgnoreCase(rd.getSource(), "cooki"));

        this.setStopWords(Set.of(
                "able", "about", "above", "abroad", "according", "accordingly", "across", "actually", "adj", "after", "afterwards", "again", "against", "ago", "ahead", "ain't", "all", "allow", "allows", "almost", "alone", "along", "alongside", "already", "also", "although", "always", "am", "amid", "amidst", "among", "amongst", "an", "and", "another", "any", "anybody", "anyhow", "anyone", "anything", "anyway", "anyways", "anywhere", "apart", "appear", "appreciate", "appropriate", "are", "aren't", "around", "as", "a's", "aside", "ask", "asking", "associated", "at", "available", "away", "awfully", "back", "backward", "backwards", "be", "became", "because", "become", "becomes", "becoming", "been", "before", "beforehand", "begin", "behind", "being", "believe", "below", "beside", "besides", "best", "better", "between", "beyond", "both", "brief", "but", "by", "came", "can", "cannot", "cant", "can't", "caption", "cause", "causes", "certain", "certainly", "changes", "clearly", "c'mon", "co", "co.", "com", "come", "comes", "concerning", "consequently", "consider", "considering", "contain", "containing", "contains", "corresponding", "could", "couldn't", "course", "c's", "currently", "dare", "daren't", "definitely", "described", "despite", "did", "didn't", "different", "directly", "do", "does", "doesn't", "doing", "done", "don't", "down", "downwards", "during", "each", "edu", "eg", "eight", "eighty", "either", "else", "elsewhere", "end", "ending", "enough", "entirely", "especially", "et", "etc", "even", "ever", "evermore", "every", "everybody", "everyone", "everything", "everywhere", "ex", "exactly", "example", "except", "fairly", "far", "farther", "few", "fewer", "fifth", "first", "five", "followed", "following", "follows", "for", "forever", "former", "formerly", "forth", "forward", "found", "four", "from", "further", "furthermore", "get", "gets", "getting", "given", "gives", "go", "goes", "going", "gone", "got", "gotten", "greetings", "had", "hadn't", "half", "happens", "hardly", "has", "hasn't", "have", "haven't", "having", "he", "he'd", "he'll", "hello", "help", "hence", "her", "here", "hereafter", "hereby", "herein", "here's", "hereupon", "hers", "herself", "he's", "hi", "him", "himself", "his", "hither", "hopefully", "how", "howbeit", "however", "hundred", "i'd", "ie", "if", "ignored", "i'll", "i'm", "immediate", "in", "inasmuch", "inc", "inc.", "indeed", "indicate", "indicated", "indicates", "inner", "inside", "insofar", "instead", "into", "inward", "is", "isn't", "it", "it'd", "it'll", "its", "it's", "itself", "i've", "just", "k", "keep", "keeps", "kept", "know", "known", "knows", "last", "lately", "later", "latter", "latterly", "least", "less", "lest", "let", "let's", "like", "liked", "likely", "likewise", "little", "look", "looking", "looks", "low", "lower", "ltd", "made", "mainly", "make", "makes", "many", "may", "maybe", "mayn't", "me", "mean", "meantime", "meanwhile", "merely", "might", "mightn't", "mine", "minus", "miss", "more", "moreover", "most", "mostly", "mr", "mrs", "much", "must", "mustn't", "my", "myself", "name", "namely", "nd", "near", "nearly", "necessary", "need", "needn't", "needs", "neither", "never", "neverf", "neverless", "nevertheless", "new", "next", "nine", "ninety", "no", "nobody", "non", "none", "nonetheless", "noone", "no-one", "nor", "normally", "not", "nothing", "notwithstanding", "novel", "now", "nowhere", "obviously", "of", "off", "often", "oh", "ok", "okay", "old", "on", "once", "one", "ones", "one's", "only", "onto", "opposite", "or", "other", "others", "otherwise", "ought", "oughtn't", "our", "ours", "ourselves", "out", "outside", "over", "overall", "own", "particular", "particularly", "past", "per", "perhaps", "placed", "please", "plus", "possible", "presumably", "probably", "provided", "provides", "que", "quite", "qv", "rather", "rd", "re", "really", "reasonably", "recent", "recently", "regarding", "regardless", "regards", "relatively", "respectively", "right", "round", "said", "same", "saw", "say", "saying", "says", "second", "secondly", "see", "seeing", "seem", "seemed", "seeming", "seems", "seen", "self", "selves", "sensible", "sent", "serious", "seriously", "seven", "several", "shall", "shan't", "she", "she'd", "she'll", "she's", "should", "shouldn't", "since", "six", "so", "some", "somebody", "someday", "somehow", "someone", "something", "sometime", "sometimes", "somewhat", "somewhere", "soon", "sorry", "specified", "specify", "specifying", "still", "sub", "such", "sup", "sure", "take", "taken", "taking", "tell", "tends", "th", "than", "thank", "thanks", "thanx", "that", "that'll", "thats", "that's", "that've", "the", "their", "theirs", "them", "themselves", "then", "thence", "there", "thereafter", "thereby", "there'd", "therefore", "therein", "there'll", "there're", "theres", "there's", "thereupon", "there've", "these", "they", "they'd", "they'll", "they're", "they've", "thing", "things", "think", "third", "thirty", "this", "thorough", "thoroughly", "those", "though", "three", "through", "throughout", "thru", "thus", "till", "to", "together", "too", "took", "toward", "towards", "tried", "tries", "truly", "try", "trying", "t's", "twice", "two", "un", "under", "underneath", "undoing", "unfortunately", "unless", "unlike", "unlikely", "until", "unto", "up", "upon", "upwards", "us", "use", "used", "useful", "uses", "using", "usually", "v", "value", "various", "versus", "very", "via", "viz", "vs", "want", "wants", "was", "wasn't", "way", "we", "we'd", "welcome", "well", "we'll", "went", "were", "we're", "weren't", "we've", "what", "whatever", "what'll", "what's", "what've", "when", "whence", "whenever", "where", "whereafter", "whereas", "whereby", "wherein", "where's", "whereupon", "wherever", "whether", "which", "whichever", "while", "whilst", "whither", "who", "who'd", "whoever", "whole", "who'll", "whom", "whomever", "who's", "whose", "why", "will", "willing", "wish", "with", "within", "without", "wonder", "won't", "would", "wouldn't", "yes", "yet", "you", "you'd", "you'll", "your", "you're", "yours", "yourself", "yourselves", "you've", "zero", "a", "how's", "i", "when's", "why's", "b", "c", "d", "e", "f", RecipeUnitDefinition.UNIT_GRAM, "h", "j", "l", "m", "n", "o", "p", "q", "r", "s", "t", "u", "uucp", "w", "x", "y", "z", "I", "www", "amount", "bill", "bottom", "call", "computer", "con", "couldnt", "cry", "de", "describe", "detail", "due", "eleven", "empty", "fifteen", "fifty", "fill", "find", "fire", "forty", "front", "full", "give", "hasnt", "herse", "himse", "interest", "itse”", "mill", "move", "myse”", "part", "put", "show", "side", "sincere", "sixty", "system", "ten", "thick", "thin", "top", "twelve", "twenty", "abst", "accordance", "act", "added", "adopted", "affected", "affecting", "affects", "ah", "announce", "anymore", "apparently", "approximately", "aren", "arent", "arise", "auth", "beginning", "beginnings", "begins", "biol", "briefly", "ca", "date", "ed", "effect", "et-al", "ff", "fix", "gave", "giving", "heres", "hes", "hid", "home", "id", "im", "immediately", "importance", "important", "index", "information", "invention", "itd", "keys", "kg", "km", "largely", "lets", "line", "'ll", "means", "mg", "million", "ml", "mug", "na", "nay", "necessarily", "nos", "noted", "obtain", "obtained", "omitted", "ord", "owing", "page", "pages", "poorly", "possibly", "potentially", "pp", "predominantly", "present", "previously", "primarily", "promptly", "proud", "quickly", "ran", "readily", "ref", "refs", "related", "research", "resulted", "resulting", "results", "run", "sec", "section", "shed", "shes", "showed", "shown", "showns", "shows", "significant", "significantly", "similar", "similarly", "slightly", "somethan", "specifically", "state", "states", "stop", "strongly", "substantially", "successfully", "sufficiently", "suggest", "thered", "thereof", "therere", "thereto", "theyd", "theyre", "thou", "thoughh", "thousand", "throug", "til", "tip", "ts", "ups", "usefully", "usefulness", "'ve", "vol", "vols", "wed", "whats", "wheres", "whim", "whod", "whos", "widely", "words", "world", "youd", "youre"))
                .setRecipeUnitDefinitions(
                        new RecipeUnitDefinition("bunch"),
                        new RecipeUnitDefinition("can"),
                        new RecipeUnitDefinition(RecipeUnitDefinition.UNIT_GRAM, "grams"),
                        new RecipeUnitDefinition("kg"),
                        new RecipeUnitDefinition("l", "liter", "liters"),
                        new RecipeUnitDefinition("lb", "pound", "pounds"),
                        new RecipeUnitDefinition("ml"),
                        new RecipeUnitDefinition(UNIT_OZ),
                        new RecipeUnitDefinition("pinch", "pinches"),
                        new RecipeUnitDefinition("sprig", "sprigs"),
                        new RecipeUnitDefinition("tbsp"),
                        new RecipeUnitDefinition("tsp"),
                        new RecipeUnitDefinition(UNIT_PIECE, "pieces", "unit", "units")
                )
                .setUnitPieceDefault(UNIT_PIECE)
                .setUnitWeightedDefault(UNIT_OZ)
                .setNumericStrings(toMap(
                        "a", ONE,
                        "an", ONE,
                        "one", ONE,
                        "two", TWO,
                        "three", THREE,
                        "four", FOUR,
                        "five", FIVE,
                        "six", SIX,
                        "seven", SEVEN,
                        "eight", EIGHT,
                        "nine", NINE,
                        "ten", TEN
                ))
                //.setIngredientUnitSeparatorTokens(Set.of())  // 300 g [xxx] apples. Not found in english
                .setIngredientPreparationTokens(Set.of(
                        "(whipping)",
                        "beaten",
                        "dry-roasted",
                        "canned",
                        "deseeded",
                        "cut",
                        "cutted",
                        "chilled",
                        "diced",
                        "dried",
                        "halved",
                        "quartered",
                        "dry",
                        "large",
                        "small",
                        "drained",
                        "filtered",
                        "squeezed",
                        "fresh",
                        "freshly",
                        "frozen",
                        "ground",
                        "of",
                        "peeled",
                        "pitted",
                        "pouring",
                        "raw",
                        "salted",
                        "skimmed",
                        "sliced",
                        "slivered",
                        "smoked",
                        "smooth",
                        "stripped",
                        "trimmed",
                        "toasted",
                        "thawed",
                        "unsalted",
                        "unsweetened",
                        "whipping",
                        "whole"))
                .setIngredientAlternativeSeparatorTokenSet(null) // Would need a way to distinguish of as separator from of as part of ingredient
//                .setIngredientAlternativeSeparatorTokenSet(" or ") // Careful. Needs spaces!
                .setIngredientOptionalIndicatorTokens(Set.of("(optional)"))
                .setInstructionSplitToNextTokenSequences(
                        Stream.concat(
                                Stream.of(
                                        Pair.of(", meanwhile", "Meanwhile"), // Careful. Due to limitationa all need to start from space or punctuation
                                        Pair.of("Meanwhile,", DO_NOT_SPLIT), // special beginning of sentence
                                        Pair.of(" approx.", DO_NOT_SPLIT),
                                        Pair.of(" in.", DO_NOT_SPLIT),
                                        Pair.of(" min.", DO_NOT_SPLIT),
                                        Pair.of(" max.", DO_NOT_SPLIT),
                                        Pair.of(".", "")
                                ),
                                Stream.of(
                                        "place",

                                        "<nobr>",
                                        "beat",
                                        "blend",
                                        "boil",
                                        "bring to a boil",
                                        "bring to the boil",
                                        "chop",
                                        "churn",
                                        "combine",
                                        "cook",
                                        "cream",
                                        "crush",
                                        "dissolve",
                                        "dry roast",
                                        "emulsify",
                                        "grate",
                                        "grind",
                                        "heat",
                                        "infuse",
                                        "knead",
                                        "mash",
                                        "melt",
                                        "mill",
                                        "mince",
                                        "mix",
                                        "poach",
                                        "proof",
                                        "pulse",
                                        "pulverise",
                                        "purée",
                                        "reheat",
                                        "repeat chopping",
                                        "roast",
                                        "roughly chop",
                                        "sauté",
                                        "sift",
                                        "simmer",
                                        "steam",
                                        "stir",
                                        "toast",
                                        "transfer",
                                        "warm",
                                        "weigh",
                                        "whip",
                                        "whisk",
                                        "zest",
// Experiment
                                        "with simmering basket instead of measuring cup on top of mixing bowl lid",
                                        "with simmering basket in place of measuring cup"
//                        "insert measuring cup",
//                        "remove simmering basket"
                                ).map(String::toLowerCase).flatMap(v ->
                                        Stream.of(
                                                v + " ", // English needs variants and avoid use words as prefixes
                                                "without varoma lid " + v + " ",
                                                "without measuring cup " + v + " ",
                                                "without setting a time " + v + " ",
                                                "increasing speed gradually " + v + " ",
                                                "with aid of spatula " + v + " "
                                        )
                                ).flatMap(v ->
                                        Stream.of(
                                                " and ", // Careful. Due to limitationa all need to start from space or punctuation
                                                ", and ",
                                                " and then ",
                                                ", and then ",
                                                " then ",
                                                ", then ",
                                                ", "
                                        ).map(s -> Pair.of(s + v, StringUtils.capitalize(v)))
                                )
                        ).collect(Collectors.toMap(Pair::getLeft, Pair::getRight))
                )
                .setUtensilTokens(Set.of(
                ))
                .setCommandProgramPredicates(toMap(
                        RecipeCommandProgram.DOUGH, stringPredicatePatterns("(?i)" + TM_DOUGH + "|dough"),
                        RecipeCommandProgram.FERMENT, of(),
                        RecipeCommandProgram.HEAT_ONLY, of(),
                        RecipeCommandProgram.SOUS_VIDE, of(),
                        RecipeCommandProgram.TURBO, stringPredicatePatterns("(?i)turbo")
                ))
                .setCommandDurationPredicates(toMap(
                        HALF_SECOND, stringPredicatePatterns("0\\.5 sec")
                ))
                .setCommandDurationHoursPattern(stringPattern("^([0-9]+) h"))
                .setCommandDurationMinutesPattern(stringPattern("^([0-9]+) min"))
                .setCommandDurationSecondsPattern(stringPattern("^([0-9]+) sec"))
                .setCommandTemperaturePredicates(toMap(
                        STEAMER_TEMPERATURE, stringPredicatePatterns("(?i)varoma|steamer")
                ))
                .setCommandTemperaturePattern(stringPattern("^([0-9]+)°C"))
                .setCommandSpeedPredicates(toMap(
                        COMMAND_SPEED_SLOW_VALUE, stringPredicatePatterns("(?i)speed" + TM_SLOW + "|speed slow")
                ))
                .setCommandRotationReversePredicates(stringPredicatePatterns("(?i)" + TM_REVERSE + "|reverse")).setCommandSpeedPattern(stringPattern("speed ([//.0-9]+)"))
                .setCommandRepetitionFromPattern(stringPattern("^([0-9]+) (?:time|times)"))
                .setCommandRepetitionFromToPattern(stringPattern("^([0-9]+)-([0-9]+) times"))
                .setInstructionMatchers(toMapP(
                        Pair.of(RecipeAnalysisTag.ADD, toRecipeInstructionMatchers("(?i)add")),
                        Pair.of(RecipeAnalysisTag.PLACE, toRecipeInstructionMatchers("(?i)place")),

//                        Pair.of(RecipeAnalysisTag.ADD_BACK, toRecipeInstructionMatchers("(?i)regrese|retorne")),

                        Pair.of(RecipeAnalysisTag.WEIGH, toRecipeInstructionMatchers("(?i)(?:weigh .*into it|weigh in )")),

                        Pair.of(RecipeAnalysisTag.REF_TOP_OF_MAIN_COMPARTMENT, toRecipeInstructionMatchers("(?i)onto mixing bowl lid")),
                        Pair.of(RecipeAnalysisTag.REF_IN_MAIN_COMPARTMENT, toRecipeInstructionMatchers("(?i)into mixing bowl")),
                        Pair.of(RecipeAnalysisTag.REF_IN_UTENSIL, toRecipeInstructionMatchers("(?i)onto (varoma|steamer|simmering basket)")),
                        Pair.of(RecipeAnalysisTag.REF_IN_EXTERNAL_ACCESORY, toRecipeInstructionMatchers("(?i)onto (?:a|an )(?:serving plate|plate|serving plater|plater)")),

                        Pair.of(RecipeAnalysisTag.ALL_INGREDIENTS, toRecipeInstructionMatchers("(?i)all ingredients")),
                        Pair.of(RecipeAnalysisTag.ALL_INGREDIENTS_WITH_EXCEPTIONS, toRecipeInstructionMatchers("(?i)all ingredients.*except")),
                        Pair.of(RecipeAnalysisTag.ALL_INGREDIENTS_REMAINING, toRecipeInstructionMatchers("(?i)(?:all )?remaining.*ingredients")),

                        Pair.of(RecipeAnalysisTag.PARALLEL, toRecipeInstructionMatchers("(?i)meanwhile")),
                        Pair.of(RecipeAnalysisTag.SERVING, toRecipeInstructionMatchers("(?i)(?: serve|^serve )")),

                        // Utensils. Complete!!
                        Pair.of(RecipeAnalysisTag.OVEN_OPERATION, toRecipeInstructionMatchers("(?i)preheat.*oven")),
                        Pair.of(RecipeAnalysisTag.REFRIGERATE, toRecipeInstructionMatchers("(?i)(keep|place|store).*(refrigerator|fridge|refrigerated)")),

                        Pair.of(RecipeAnalysisTag.FREEZE, toRecipeInstructionMatchers("(?i)chill.*fridge", "(?i)freeze.*(?:minutes|hour)")),

                        Pair.of(RecipeAnalysisTag.SUMMERING_BASKET, toRecipeInstructionMatchers("(?i)using.*simmering basket.*(drain|strain)")),
                        Pair.of(RecipeAnalysisTag.SIMMERING_BASKET_PUT_INSIDE, toRecipeInstructionMatchers("(?i)(?:insert).*simmering basket")),
                        Pair.of(RecipeAnalysisTag.SIMMERING_BASKET_REMOVE, toRecipeInstructionMatchers("(?i)(?:remove).*simmering basket")),
                        Pair.of(RecipeAnalysisTag.SIMMERING_BASKET_AS_LID_PUT, toRecipeInstructionMatchers("(?i)(?:placing|place|with) simmering basket (instead|in place) of measuring cup")), // Needs context analysis to place before tm step
//                        Pair.of(RecipeAnalysisTag.SIMMERING_BASKET_AS_LID_REMOVE, defineMe()),
//                        Pair.of(RecipeAnalysisTag.SIMMERING_BASKET_REMOVE_MEASURING_CUP_PUT, defineMe()),

                        Pair.of(RecipeAnalysisTag.MIXING_BOWL, toRecipeInstructionMatchers("(?i)(?:remove).*mixing.*bowl lid")),
                        Pair.of(RecipeAnalysisTag.MIXING_BOWL_PUT, toRecipeInstructionMatchers("(?i)place mixing bowl.*back into position")),
                        Pair.of(RecipeAnalysisTag.MIXING_BOWL_REMOVE, toRecipeInstructionMatchers("(?i)(?:remove).*(?:mixing bowl(?!.*? lid))")),
                        Pair.of(RecipeAnalysisTag.MIXING_BOWL_EMPTY, toRecipeInstructionMatchers("(?i)(?:empty).*mixing.*bowl")),
                        Pair.of(RecipeAnalysisTag.MIXING_BOWL_CLEAN, toRecipeInstructionMatchers("(?i)(?:clean|rinse).*mixing.*bowl")),

                        Pair.of(RecipeAnalysisTag.TRANSFER_TO_BOWL, toRecipeInstructionMatchers("(?i)transfer.*(?:into|onto|to).*(?:bowl|container|jar|jug|sheet|tray)")),

//                        Pair.of(RecipeAnalysisTag.MEASURING_CUP, defineMe()),
                        Pair.of(RecipeAnalysisTag.MEASURING_CUP_NOTUSE, toRecipeInstructionMatchers("(?i)(?:remove|without).*measuring cup")),
                        Pair.of(RecipeAnalysisTag.MEASURING_CUP_PUT, toRecipeInstructionMatchers("(?i)insert.*measuring cup")),
                        Pair.of(RecipeAnalysisTag.MEASURING_CUP_HOLD_VIBRATION, toRecipeInstructionMatchers("(?i)hold.*measuring cup")),

//                        Pair.of(RecipeAnalysisTag.BUTTERFLY, defineMe()),
                        Pair.of(RecipeAnalysisTag.WHISK_PUT, toRecipeInstructionMatchers("(?i)insert.*butterfly")),
                        Pair.of(RecipeAnalysisTag.WHISK_REMOVE, toRecipeInstructionMatchers("(?i)remove.*butterfly")),

                        Pair.of(RecipeAnalysisTag.SPATULA, toRecipeInstructionMatchers("(?i)with aid.*spatula")),
                        Pair.of(RecipeAnalysisTag.SPATULA_MIX, toRecipeInstructionMatchers("(?i)mix with.*spatula")),
                        Pair.of(RecipeAnalysisTag.SPATULA_SCRAP_SIDE, toRecipeInstructionMatchers("(?i)scrape.*bowl.*spatula")),
                        Pair.of(RecipeAnalysisTag.SPATULA_MIX_WELL, toRecipeInstructionMatchers("(?i)mix.*well with.*spatula")),

                        Pair.of(RecipeAnalysisTag.STEAMER, toRecipeInstructionMatchers("(?i)(?:secure|close).*(?:varoma|steamer)")),
                        Pair.of(RecipeAnalysisTag.STEAMER_PUT, toRecipeInstructionMatchers("(?i)(?:place|return).*(?:varoma|steamer).*(?:to|in).*position")),
                        Pair.of(RecipeAnalysisTag.STEAMER_REMOVE, toRecipeInstructionMatchers("(?i)remove.*(?:varoma|steamer)")),
                        Pair.of(RecipeAnalysisTag.STEAMER_TRAY_PUT, toRecipeInstructionMatchers("(?i)insert.*(?:varoma|steamer).*tray")),
                        Pair.of(RecipeAnalysisTag.STEAMER_TRAY_REMOVE, toRecipeInstructionMatchers("(?i)remove.*(?:varoma|steamer).*tray")),
                        Pair.of(RecipeAnalysisTag.STEAMER_DISH_PUT, toRecipeInstructionMatchers("(?i)place.*into (varoma|steamer) dish")),
                        Pair.of(RecipeAnalysisTag.STEAMER_DISH_REMOVE, toRecipeInstructionMatchers("(?i)remove.*(varoma|steamer) dish")),

                        Pair.of(RecipeAnalysisTag.OTHER_OPERATION, toRecipeInstructionMatchers("(?i)insert.*(?:blade cover)")),
//                        Pair.of(RecipeAnalysisTag.USEFUL_ITEMS, defineMe()),
                        Pair.of(RecipeAnalysisTag.KITCHEN_EQUIPMENT, toRecipeInstructionMatchers())

                ))
                .setRecipeDifficultDefinitionPredicates(toMap(
                        RecipeDifficultyDefinition.EASY, stringPredicatePattern("(?i)easy"),
                        RecipeDifficultyDefinition.MEDIUM, stringPredicatePattern("(?i)medium"),
                        RecipeDifficultyDefinition.ADVANCED, stringPredicatePattern("(?i)advanced")
                ))
                .setRecipePriceDefinitionPredicates(toMap(
                        RecipePriceDefinition.LOW, stringPredicatePattern("(?i)low budget")
                ))
                .addRecipeCategoryByPrincipal(BASIC, "Basics")
                .addRecipeCategoryByPrincipal(START, "Starters & salads",
                        "Starters",
                        "Starters and salads")
                .addRecipeCategoryByPrincipal(SOUP, "Soups")
                .addRecipeCategoryByPrincipal(PASTARICE, "Pasta & rice",
                        "Pasta & rice dishes",
                        "Pasta and rice dishes")
                .addRecipeCategoryByPrincipal(MAINMEAT, "Meat & poultry",
                        "Main dishes - meat and poultry")
                .addRecipeCategoryByPrincipal(MAINFISH, "Fish & seafood",
                        "Main dishes - fish and seafood")
                .addRecipeCategoryByPrincipal(MAINVEG, "Vegetarian",
                        "Main dishes - vegetarian")
                .addRecipeCategoryByPrincipal(MAINOTHER, "Other mains",
                        "Main dishes",
                        "Main dishes - other")
                .addRecipeCategoryByPrincipal(SIDE, "Side dishes")
                .addRecipeCategoryByPrincipal(BAKE, "Breads & rolls",
                        "Breads and rolls",
                        "Baking")
                .addRecipeCategoryByPrincipal(BAKE_SAVORY, "Baking - savory",
                        "Baking - savoury")
                .addRecipeCategoryByPrincipal(DESSERT, "Desserts & sweets",
                        "Desserts and sweets",
                        "Desserts")
                .addRecipeCategoryByPrincipal(BAKE_SWEET, "Baking - sweet")
                .addRecipeCategoryByPrincipal(SAUCE_SWEET, "Sauces - sweet",
                        "Sauces, dips & spreads - sweet",
                        "Sauces, dips and spreads - sweet",
                        "Jam")
                .addRecipeCategoryByPrincipal(DRINK, "Drinks")
                .addRecipeCategoryByPrincipal(SNACK, "Snacks",
                        "Snacks and finger food")
                .addRecipeCategoryByPrincipal(SAUCE_SAVO, "Sauces, dips & spreads",
                        "Sauces, dips and spreads - savory",
                        "Sauces, dips and spreads - savoury")
                .addRecipeCategoryByPrincipal(BABY, "Baby food")
                .addRecipeCategoryByPrincipal(BREAK, "Breakfast")
                .addRecipeCategoryByPrincipal(MENU, "Menus and more",
                        "All-In-One cooking")
                .setRecipeCategoryNamePreprocessFunction(
                        new Function<>() {
                            private final Pattern cleanupPattern = Pattern.compile("(?i)" + String.join("|",
                                    "^Cooking for ",
                                    "^Cooking ",
                                    "notused"
                            ));

                            @Override
                            public String apply(String s) {
                                return RECIPE_CATEGORY_NAME_FORMAT_DEFAULT_FUNCTION.apply(removeAll(s, cleanupPattern));
                            }
                        })
                .setTokenFormatFunction(
                        new Function<>() {
                            private final Pattern cleanupPattern = Pattern.compile("(?i)" + String.join("|",
                                    "'s",
                                    "er$",
                                    "s$"
                            ));

                            @Override
                            public String apply(String s) {
                                if (s.length() <= TOKEN_LENGTH_MIN) {
                                    return s;
                                } else {
                                    return removeAll(s, cleanupPattern);
                                }
                            }
                        }
                )
                .setRecipeLocaleLabelFunction(rd -> "en_VI");
    }
}