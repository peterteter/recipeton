package com.recipeton.shared.analysis.configuration;

/*-
 * #%L
 * recipeton-shared
 * %%
 * Copyright (C) 2021 tincomisc
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.recipeton.shared.analysis.domain.*;
import com.recipeton.shared.domain.RecipeDefinition;
import com.recipeton.shared.util.TextUtil;
import lombok.Data;
import lombok.ToString;
import lombok.experimental.Accessors;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.tuple.Pair;

import java.math.BigDecimal;
import java.time.Duration;
import java.util.*;
import java.util.function.Function;
import java.util.function.Predicate;
import java.util.regex.Pattern;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static com.recipeton.shared.service.RecipeCategoryDefinitionUtil.createRecipeCategoryUidByPrincipal;
import static com.recipeton.shared.util.StreamUtil.toStream;
import static com.recipeton.shared.util.TextUtil.BETWEEN_PARENTHESES_PATTERN;
import static java.util.List.of;


@Data
@Accessors(chain = true)
@ToString(onlyExplicitlyIncluded = true)
public class RecipeAnalyzerConfiguration {
    public static final int TOKEN_LENGTH_MIN = 2;

    public static final Duration HALF_SECOND = Duration.ofMillis(500);

    public static final Pattern DELIMITERS_PATTERN = Pattern.compile("<nobr>|<\\/nobr>|\\[|\\]");

    public static final String ONE = "1";
    public static final String TWO = "2";
    public static final String THREE = "3";
    public static final String FOUR = "4";
    public static final String FIVE = "5";
    public static final String SIX = "6";
    public static final String SEVEN = "7";
    public static final String EIGHT = "8";
    public static final String NINE = "9";
    public static final String TEN = "10";

    public static final String BASIC = "000basic";
    public static final String START = "010start";
    public static final String SOUP = "020soup";
    public static final String PASTARICE = "030pastarice";
    public static final String MAINMEAT = "040mainmeat";
    public static final String MAINFISH = "050mainfish";
    public static final String MAINVEG = "060mainveg";
    public static final String MAINOTHER = "070mainother";
    public static final String SIDE = "080side";
    public static final String BAKE = "090bake";
    public static final String BAKE_SAVORY = "100bakesavory";
    public static final String DESSERT = "110dessert";
    public static final String BAKE_SWEET = "120bakesweet";
    public static final String SAUCE_SWEET = "130saucesweet";
    public static final String DRINK = "140drink";
    public static final String SNACK = "150snack";
    public static final String SAUCE_SAVO = "160saucesavory";
    public static final String BABY = "170baby";
    public static final String BREAK = "180break";
    public static final String MENU = "190menu";

    public static final int DEFAULT_ON_UNIT_UNDEFINED_WEIGHTED = 10;
    public static final int CATEGORY_MATCH_RATIO_MIN = 80;
    public static final int INSTRUCTION_UTENSIL_MATCH_RATIO_MIN = 90;
    //    public static final int INSTRUCTION_INGREDIENT_MATCH_RATIO_MIN = 70;
    public static final int INSTRUCTION_INGREDIENT_MATCH_RATIO_MIN = 80;
    public static final Function<String, String> RECIPE_CATEGORY_NAME_FORMAT_DEFAULT_FUNCTION = s -> TextUtil.capitalizeFirst(TextUtil.removeAll(s, BETWEEN_PARENTHESES_PATTERN)).trim();
    public static final Pattern PATTERN_SYMBOLS = Pattern.compile("[\\(\\)\\,\\.\\/\\+\\-\\_]");
    public static final int INSTRUCTION_INGREDIENT_MATCH_RATION_MIN_SHORT = 99;
    public static final int SHORT_SEARCH_SUBSTRING_LENGTH_MAX = 2;
    public static final int SEARCH_COMPARISON_MAX_LENGTH_DIFF = 3;

    private final String id;
    private final Predicate<RecipeDefinition> handlerOfPredicate;
    private Set<String> stopWords;
    /**
     * Should do inflection, removal/normalization of useless prefixes or suffixes...
     */
    private Function<String, String> tokenFormatFunction = s -> s;

    private Set<RecipeUnitDefinition> recipeUnitDefinitions;

    private double onUnitUndefinedSetAsWeightedDefaultWhenMagnitudeGreaterThan = DEFAULT_ON_UNIT_UNDEFINED_WEIGHTED;

    private Set<String> unitsWeighted = Set.of(RecipeUnitDefinition.UNIT_GRAM, "kg", "oz", "lb", "pound");
    private String unitPieceDefault = "PIECE_DEFINE_ME_IN_CONFIG!";
    private String unitWeightedDefault = RecipeUnitDefinition.UNIT_GRAM;

    private Map<String, String> numericStrings = Collections.emptyMap();

    private String ingredientMagnitudeRangeSeparator = "-";
    private Pattern ingredientTokenSplitPattern = TextUtil.SPACE_PATTERN;
    private Set<String> ingredientUnitSeparatorTokens = Collections.emptySet();
    private String ingredientAlternativeSeparatorTokenSet; // Should set in implementation
    private Set<String> ingredientPreparationTokens = Collections.emptySet();
    private Set<String> ingredientOptionalIndicatorTokens = Collections.emptySet();

    private Map<RecipeAnalysisTag, List<RecipeInstructionMatcher>> instructionMatchers;
    private int instructionIngredientMatchRatioMin = INSTRUCTION_INGREDIENT_MATCH_RATIO_MIN;
    private int matchRatioMinShort = INSTRUCTION_INGREDIENT_MATCH_RATION_MIN_SHORT;
    private int shortSearchSubstringLengthMax = SHORT_SEARCH_SUBSTRING_LENGTH_MAX;
    private int searchComparisonMaxLengthDiff = SEARCH_COMPARISON_MAX_LENGTH_DIFF;

    private Map<String, String> instructionSplitToNextTokenSequences = Collections.emptyMap();
    private Map<String, String> instructionSplitToCurrentTokenSequences = Collections.emptyMap();
    private Map<String, String> instructionCleanReplacements = Map.of(
            "\n", "",
            "\u00A0", " ",

            "<br>", "",
            "<br />", "",
            "<strong>", "",
            "</strong>", "",

            "  ", " "
    );

    private List<Pair<String, String>> commandDelimiters = of(
            Pair.of("<nobr>", "</nobr>"),
            Pair.of("[", "]")
    );
    private Map<RecipeCommandProgram, List<Predicate<String>>> commandProgramPredicates;
    private List<Predicate<String>> commandRotationReversePredicates;
    private Map<Duration, List<Predicate<String>>> commandDurationPredicates;
    private Pattern commandDurationHoursPattern;
    private Pattern commandDurationMinutesPattern;
    private Pattern commandDurationSecondsPattern;
    private Map<Long, List<Predicate<String>>> commandTemperaturePredicates;
    private Pattern commandTemperaturePattern;
    private Map<BigDecimal, List<Predicate<String>>> commandSpeedPredicates;
    private Pattern commandSpeedPattern;
    private Pattern commandRepetitionFromPattern;
    private Pattern commandRepetitionFromToPattern;

    private int instructionUtensilMatchRatioMin = INSTRUCTION_UTENSIL_MATCH_RATIO_MIN;
    private Set<String> utensilTokens = Collections.emptySet();

    private Map<RecipeDifficultyDefinition, Predicate<String>> recipeDifficultDefinitionPredicates;
    private Map<RecipePriceDefinition, Predicate<String>> recipePriceDefinitionPredicates;
    private Function<RecipeDefinition, String> recipeLocaleLabelFunction = RecipeDefinition::getLocale;
    private int recipeCategoryMatchRatioMin = CATEGORY_MATCH_RATIO_MIN;
    /**
     * Prefefined recipe categories and list of possible aliases
     * mark UID
     */
    private Map<String, List<String>> recipeCategoryNamesByUid = new HashMap<>();
    /**
     * Preprocess recipe categories. Should return null for names to be ignored
     */
    private Function<String, String> recipeCategoryNamePreprocessFunction = RECIPE_CATEGORY_NAME_FORMAT_DEFAULT_FUNCTION;

    private Function<String, String> recipeTitleFormatFunction = s -> StringUtils.capitalize(TextUtil.removeQuotes(TextUtil.normalizeAsciiFirstChar(s)).trim().toLowerCase());


    protected RecipeAnalyzerConfiguration addRecipeCategoryByPrincipal(String uid, String title, String... titleVariants) {
        String principalUid = createRecipeCategoryUidByPrincipal(uid);
        if (recipeCategoryNamesByUid.containsKey(principalUid)) {
            throw new IllegalArgumentException("Recipe category with uid=" + principalUid + " already registered");
        }
        recipeCategoryNamesByUid.put(principalUid, Stream.concat(Stream.of(title), toStream(titleVariants)).collect(Collectors.toList()));
        return this;
    }

    public RecipeInstructionTokenization getTokenization(String text, int lengthMin) {
        String[] splits = text.split(" ");

        if (splits.length <= 0) {
            return new RecipeInstructionTokenization(text, Collections.emptyList(), Collections.emptyList());
        }

        List<String> tokens = new ArrayList<>();
        List<RecipeInstructionTokenization.Position> positions = new ArrayList<>();
        int offset = 0;
        int splitIndex = 0;
        do {
            String split = splits[splitIndex];
            String cleaned = TextUtil.removeAll(split, PATTERN_SYMBOLS).toLowerCase();
            String trimmed = cleaned.trim();
            if (!getStopWords().contains(trimmed)) {
                String formatted = getTokenFormatFunction().apply(trimmed);
                String normalized = TextUtil.normalizeAscii(formatted).trim();
                if (normalized.length() >= lengthMin) {
                    tokens.add(normalized);
                    positions.add(new RecipeInstructionTokenization.Position(offset, split.length())); // NOPMD BS
                }
            }
            offset += split.length() + 1;
            splitIndex++;
        } while (splitIndex < splits.length);

        return new RecipeInstructionTokenization(text, tokens, positions);

    }


    public String[] getTokens(String text, int lengthMin) {
        String cleaned = TextUtil.removeAll(text, PATTERN_SYMBOLS).toLowerCase();
        return Stream.of(cleaned.split(" "))
                .map(String::trim)
                .filter(t -> !getStopWords().contains(t))
                .map(t -> getTokenFormatFunction().apply(t))
                .filter(StringUtils::isNotBlank)
                .map(TextUtil::normalizeAscii)
                .filter(t -> t.length() >= lengthMin)
                .toArray(String[]::new);
    }

    public RecipeAnalyzerConfiguration setRecipeUnitDefinitions(RecipeUnitDefinition... recipeUnitDefinitions) {
        this.recipeUnitDefinitions = Set.of(recipeUnitDefinitions);
        return this;
    }

    public String toUid(String text) {
        return toUid(getTokens(text, 1));
    }

    public String toUid(String... tokens) {
        return Stream.of(tokens)
                .sorted(Comparator.reverseOrder())
                .collect(Collectors.joining(""));
    }


}
