package com.recipeton.shared.analysis.service;

/*-
 * #%L
 * recipeton-shared
 * %%
 * Copyright (C) 2021 tincomisc
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.recipeton.shared.analysis.configuration.RecipeAnalyzerConfiguration;
import com.recipeton.shared.analysis.domain.RecipeIngredientAnalysis;
import lombok.Getter;
import org.apache.commons.lang3.StringUtils;

import java.math.BigDecimal;
import java.util.*;
import java.util.regex.Matcher;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static com.recipeton.shared.util.StreamUtil.toStream;

public class RecipeIngredientStandardAnalyzer implements RecipeIngredientAnalyzer {

    private final RecipeAnalyzerConfiguration configuration;
    private final Set<String> unitNames;

    public RecipeIngredientStandardAnalyzer(RecipeAnalyzerConfiguration configuration) {
        this.configuration = configuration;

        this.unitNames = this.configuration.getRecipeUnitDefinitions().stream()
                .flatMap(u -> Stream.concat(toStream(u.getName()), toStream(u.getVariants())))
                .map(String::toLowerCase)
                .collect(Collectors.toSet());
    }

    @Override
    public RecipeIngredientAnalysis analyze(String text) {
        if (StringUtils.isBlank(text)) {
            return null;
        }

        String ingredientAlternativeSeparatorToken = configuration.getIngredientAlternativeSeparatorTokenSet();
        int separatorIndex = ingredientAlternativeSeparatorToken == null ? -1 : StringUtils.indexOf(text, ingredientAlternativeSeparatorToken);
        if (separatorIndex == -1) {
            return analyzeSingleIngredient(text);
        }
        return analyzeSingleIngredient(text.substring(0, separatorIndex))
                .setAlternative(analyze(StringUtils.substringAfter(text, ingredientAlternativeSeparatorToken)));
    }

    // Try to write again with wired state machine. Not sure if it will be more maintainable.
    private RecipeIngredientAnalysis analyzeSingleIngredient(String text) { // NOPMD Complex
        AnalyzeState state = new AnalyzeState(getIngredientTextPreformatted(text));

        while (!state.isDone()) {
            int tokenIndex = state.getTokenIndex();
            String token = state.next();

            if (tokenIndex == 0 && !state.isMagnitudeFromFound()) {
                Optional<BigDecimal> magnitude = getMagnitude(token);
                if (magnitude.isPresent()) {
                    state.getRecipeIngredientAnalysis().setMagnitudeFrom(token).setMagnitudeFromValue(magnitude.get());
                    continue;
                }
            }

            if (tokenIndex == 1 && !state.isMagnitudeRangeSeparatorFound() && state.isMagnitudeFromFound() && RANGE_SEPARATOR.equals(token)) {
                state.setMagnitudeRangeSeparatorFound();
                continue;
            }

            if (tokenIndex == 2 && state.isMagnitudeRangeSeparatorFound()) {
                Optional<BigDecimal> magnitude = getMagnitude(token);
                if (magnitude.isPresent()) {
                    state.getRecipeIngredientAnalysis().setMagnitudeTo(token).setMagnitudeToValue(magnitude.get());
                    continue;
                }
            }

            if (state.isMagnitudeFromFound() && !state.isNotationParseStarted() && unitNames.contains(token.toLowerCase())) {
                state.getRecipeIngredientAnalysis().setUnit(token).setWeighted(configuration.getUnitsWeighted().contains(token));
                continue;
            }

            if (!state.isUnitParsed() && !state.isNotationParseStarted()
                    && (state.isMagnitudeFromFound() || state.isUnitPresent()) && configuration.getIngredientUnitSeparatorTokens().contains(token)) {
                state.setUnitParsed();
                continue;
            }

            // If notation has not started and preparation tokens are present. Qualifiers before notation)
            if (!state.isNotationParseStarted() && configuration.getIngredientPreparationTokens().contains(token)) {
                state.addPreparationBeforeNotation(token);
                continue;
            }

            // Parsing notation and find token (... apple [diced] ...) or word starting with symbol (... apple (two pieces) ...)
            if (!state.isNotationParseEnded()
                    && state.isNotationParseStarted()
                    && (configuration.getIngredientPreparationTokens().contains(token) || INGREDIENT_PREPARATION_START_SYMBOLS.stream().anyMatch(p -> StringUtils.startsWith(token, p)))) {
                state.setNotationParseEnded();
            }

            if (!state.isNotationParseEnded()) {
                String notationToken = token;
                for (String separator : INGREDIENT_PREPARATION_SEPARATOR_SYMBOLS) {
                    if (StringUtils.endsWith(token, separator)) {
                        String stripped = StringUtils.stripEnd(token, separator);
                        if (configuration.getIngredientPreparationTokens().contains(stripped)) { // ... pear [sliced,] ...
                            state.addPreparationAfterNotation(token);
                            notationToken = null;
                        } else { // ... [pear,] ....
                            notationToken = stripped;
                        }
                        state.setNotationParseEnded();
                        break;
                    }
                }
                state.doNotationAppend(notationToken);
                continue;
            }

            if (!state.isOptional() && configuration.getIngredientOptionalIndicatorTokens().contains(token)) {
                state.setOptional();
                continue;
            }

            state.addPreparationAfterNotation(token);
        }

        return state.doFinalize();
    }

    @Override
    public RecipeAnalyzerConfiguration getConfiguration() {
        return configuration;
    }

    private String getIngredientTextPreformatted(String text) {
        return replaceFirstNumericWithVulgarFractionsWithSpaceToSingleToken(text)
                .replaceAll("/n", " ")
                .trim();
    }

    public String replaceFirstNumericWithVulgarFractionsWithSpaceToSingleToken(String text) {
        Matcher match = NUMERIC_AND_VULGAR_FRACTION_WITH_SPACE.matcher(text);
        if (match.find()) {
            return match.replaceAll("$1$2");
        }
        return text;
    }

    public final class AnalyzeState {

        private final List<String> preparationBeforeNotation = new ArrayList<>();
        private final List<String> preparationAfterNotation = new ArrayList<>();
        @Getter
        private final RecipeIngredientAnalysis recipeIngredientAnalysis;
        private final Deque<String> stack;
        private final int stackInitialSize;

        private final StringBuilder ingredientNotation = new StringBuilder(); // NOPMD it is ok

        @Getter
        private boolean magnitudeRangeSeparatorFound;
        @Getter
        private boolean unitParsed;
        @Getter
        private boolean notationParseEnded;

        AnalyzeState(String text) {
            stack = Stream.of(configuration.getIngredientTokenSplitPattern().split(text)).collect(Collectors.toCollection(ArrayDeque::new));
            stackInitialSize = stack.size();

            recipeIngredientAnalysis = new RecipeIngredientAnalysis().setSource(text);
        }

        public void addPreparationAfterNotation(String token) {
            this.preparationAfterNotation.add(token);
        }

        public void addPreparationBeforeNotation(String token) {
            preparationBeforeNotation.add(token);
        }

        public RecipeIngredientAnalysis doFinalize() {
            // CHECK if preparation has terms like "para engrasar" or "al gusto", create new boolean 'virtuallyOptional' and treat as optional when not found

            String notation = this.ingredientNotation.toString();
            String uid = configuration.toUid(notation);
            String[] tokens = configuration.getTokens(notation, RecipeAnalyzerConfiguration.TOKEN_LENGTH_MIN);
            recipeIngredientAnalysis.setIngredientNotation(uid, notation, tokens);

            String preparation = null;
            if (!preparationBeforeNotation.isEmpty()) {
                preparation = String.join(", ", preparationBeforeNotation);
            }
            if (!preparationAfterNotation.isEmpty()) {
                String preparationAfter = String.join(" ", preparationAfterNotation);
                if (preparation == null) {
                    preparation = preparationAfter;
                } else {
                    preparation = preparation + ", " + preparationAfter;
                }
            }
            recipeIngredientAnalysis.setPreparation(preparation);

            if (recipeIngredientAnalysis.getUnit() == null && recipeIngredientAnalysis.getMagnitudeFromValue() != null) {
                if (recipeIngredientAnalysis.getMagnitudeFromValue().doubleValue() > configuration.getOnUnitUndefinedSetAsWeightedDefaultWhenMagnitudeGreaterThan()) {
                    recipeIngredientAnalysis
                            .setUnit(configuration.getUnitWeightedDefault())
                            .setWeighted(true);
                } else {
                    recipeIngredientAnalysis.setUnit(configuration.getUnitPieceDefault());
                }
            }

            return recipeIngredientAnalysis;
        }

        private void doNotationAppend(String notationToken) {
            if (StringUtils.isBlank(notationToken)) {
                return;
            }
            if (StringUtils.isNotBlank(ingredientNotation)) {
                ingredientNotation.append(" ");
            }
            ingredientNotation.append(notationToken);
        }

        public int getTokenIndex() {
            return stackInitialSize - stack.size();
        }

        boolean isDone() {
            return stack.isEmpty();
        }

        public boolean isMagnitudeFromFound() {
            return recipeIngredientAnalysis.isMagnitudeFromPresent();
        }

        public boolean isNotationParseStarted() {
            return ingredientNotation.length() > 0;
        }

        public boolean isOptional() {
            return recipeIngredientAnalysis.isOptional();
        }

        public boolean isUnitPresent() {
            return recipeIngredientAnalysis.isUnitPresent();
        }

        public String next() {
            return stack.pop();
        }

        public void setMagnitudeRangeSeparatorFound() {
            this.magnitudeRangeSeparatorFound = true;
        }

        public void setNotationParseEnded() {
            notationParseEnded = true;
        }

        private void setOptional() {
            recipeIngredientAnalysis.setOptional(true);
        }

        public void setUnitParsed() {
            this.unitParsed = true;
        }
    }
}
