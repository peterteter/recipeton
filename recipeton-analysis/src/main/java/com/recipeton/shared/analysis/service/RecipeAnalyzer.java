package com.recipeton.shared.analysis.service;

/*-
 * #%L
 * recipeton-shared
 * %%
 * Copyright (C) 2021 tincomisc
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.recipeton.shared.analysis.configuration.RecipeAnalyzerConfiguration;
import com.recipeton.shared.analysis.domain.*;
import com.recipeton.shared.domain.RecipeDefinition;
import com.recipeton.shared.util.TextUtil;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.tuple.Pair;

import java.math.BigDecimal;
import java.util.List;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;

public interface RecipeAnalyzer {

    String analyzeLocale(RecipeDefinition recipeDefinition);

    RecipeDifficultyDefinition analyzeRecipeDifficulty(RecipeDefinition recipeDefinition);

    RecipeMeasurement analyzeRecipeMeasurement(String text);

    default BigDecimal analyzeRecipeMeasurementValue(String text) {
        return Optional.ofNullable(analyzeRecipeMeasurement(text)).map(RecipeMeasurement::getMagnitudeValue).orElse(null);
    }

    RecipePriceDefinition analyzeRecipePrice(RecipeDefinition recipeDefinition);

    String analyzeTitle(RecipeDefinition recipeDefinition);

    RecipeUtensilAnalysis analyzeUtensil(String s);

    default List<RecipeIngredientAnalysis> findIngredientsByUnmatched(List<RecipeIngredientAnalysis> recipeIngredientAnalyses, List<RecipeInstructionAnalysis> recipeInstructionAnalyses) {
        Set<RecipeIngredientAnalysis> foundIngredientAnalyses = recipeInstructionAnalyses.stream()
                .flatMap(ria -> ria.getTerms().stream())
                .filter(t -> t instanceof RecipeIngredientTerm)
                .map(t -> (RecipeIngredientTerm) t)
                .map(RecipeIngredientTerm::getIngredient)
                .collect(Collectors.toSet());

        return recipeIngredientAnalyses.stream()
                .filter(a -> !a.isOptional())
                .filter(a -> !foundIngredientAnalyses.contains(a))
                .collect(Collectors.toList());
    }

    RecipeAnalyzerConfiguration getConfiguration();

    // Use config to localize
    default String getMeasuringCupPutMessage() {
        return "Place lid and measuring cup on top";
    }

    /**
     * Returns null if no resolution for proposedName
     * This means that it should be ignored.
     *
     * @param proposedName
     * @param mainCategory
     * @return
     */
    Pair<String, String> getRecipeCategoryUidNamePair(String proposedName, boolean mainCategory);

    RecipeIngredientAnalyzer getRecipeIngredientAnalyzer();

    RecipeInstructionAnalyzer getRecipeInstructionAnalyzer();

    boolean isHandlerOf(RecipeDefinition recipeDefinition);

    // Use config to localize
    default CharSequence toBackgroundReport(String uid, RecipeDefinition recipeDefinition) {
        StringBuilder backGround = new StringBuilder("<i>Original title</i><br>")
                .append(recipeDefinition.getName())
                .append("<br>");
        if (recipeDefinition.getCollections() != null) {
            backGround.append("<br>").append("<i>Collections</i><br>");
            for (String c : recipeDefinition.getCollections()) {
                backGround.append("• ").append(c).append("<br>");
            }
        }

        backGround
                .append("<br>").append("<br>")
                .append("<i>Ref</i> ").append(uid)
                .append("<br>").append("<br>")
                .append("<i>Source</i><br>").append(recipeDefinition.getSource())
                .append("<br>");
        if (StringUtils.isNotBlank(recipeDefinition.getAuthor())) {
            backGround
                    .append("<br>")
                    .append("<i>Author</i><br>")
                    .append(TextUtil.replaceAll(recipeDefinition.getAuthor(), TextUtil.EOL_PATTERN, "<br>"));
        }
        return backGround;
    }

    // Use config to localize
    default Pair<String, String> toRecipeIngredientUnmatchedReport(List<RecipeIngredientAnalysis> ingredientsByUnmatched) {
        return Pair.of(
                "<b>WARNING!</b> Ingredient mismatch!",
                "Some ingredients were not found in instructions.<br>"
                        + "<b>Check manually!</b> recipe overview.<br>"
                        + "<i>Correct recipe and reimport!</i><br><br>"
                        + "<b>Problematic ingredients:</b><br>"
                        + ingredientsByUnmatched.stream()
                        .map(RecipeIngredientAnalysis::getIngredientNotation)
                        .map(s -> " • " + s)
                        .collect(Collectors.joining("<br>"))
        );
    }

}