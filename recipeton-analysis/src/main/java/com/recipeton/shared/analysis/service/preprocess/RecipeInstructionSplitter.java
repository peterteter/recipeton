package com.recipeton.shared.analysis.service.preprocess;

/*-
 * #%L
 * recipeton-shared
 * %%
 * Copyright (C) 2021 tincomisc
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.recipeton.shared.analysis.configuration.RecipeAnalyzerConfiguration;
import com.recipeton.shared.util.StringMatchTraverser;
import lombok.Data;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.tuple.Pair;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static org.apache.commons.lang3.tuple.Pair.of;

@Data
public class RecipeInstructionSplitter {

    public static final String DO_NOT_SPLIT = "+++";

    public static final char DOT_CHAR = '.';
    public static final char PARENTHESES_OPEN = '(';
    public static final char PARENTHESES_CLOSE = ')';
    public static final char TAG_NAME_START = '<';
    public static final char TAG_NAME_END = '>';

    private final StringMatchTraverser<SplitReplacement> stringMatchTraverser;
    private final boolean autocapitalize;

    public RecipeInstructionSplitter(RecipeAnalyzerConfiguration configuration) {
        this(configuration.getInstructionSplitToNextTokenSequences(), configuration.getInstructionSplitToCurrentTokenSequences(), true);
    }

    public RecipeInstructionSplitter(Map<String, String> splitToNextTokenSequences, Map<String, String> splitToCurrentTokenSequences, boolean autocapitalize) {

        Map<String, SplitReplacement> splitReplacements = Stream.concat(
                splitToNextTokenSequences.entrySet().stream()
                        .map(e -> of(e.getKey(), SplitReplacement.toNext(e.getValue()))),
                splitToCurrentTokenSequences.entrySet().stream()
                        .map(e -> of(e.getKey(), SplitReplacement.toCurrent(e.getValue())))
        ).collect(Collectors.toMap(
                Pair::getKey,
                Pair::getValue
        ));

        this.stringMatchTraverser = new StringMatchTraverser<>(splitReplacements);
        this.autocapitalize = autocapitalize;
    }

    private void appendSplit(List<String> splits, String split) {
        if (StringUtils.isNotBlank(split)) {
            splits.add(autocapitalize ? StringUtils.capitalize(split) : split);
        }
    }

    private void computeSplits(String text, List<String> splits) { // NOPMD Complex
        stringMatchTraverser.reset();
        boolean inTagBody = false;
        boolean inTag = false;
        int inParentheses = 0;
        StringBuilder sb = new StringBuilder();
        for (int i = 0; i < text.length(); i++) {
            char c = text.charAt(i);
            // Found a termination in match tree?
            if (stringMatchTraverser.nextChar(c) && !inTagBody && inParentheses == 0) {
                String matchSequence = stringMatchTraverser.getMatchedSequence();
                SplitReplacement splitReplacement = stringMatchTraverser.getMatchReplacement();
                if (!DO_NOT_SPLIT.equals(splitReplacement.appendToNext)) {
                    String split = sb.subSequence(0, sb.length() - matchSequence.length() + 1).toString().trim();

                    appendSplit(splits, splitReplacement.appendToCurrent == null ? split : split + splitReplacement.appendToCurrent);

                    sb = new StringBuilder(); // NOPMD
                    if (splitReplacement.appendToNext != null) { // NOPMD BS
                        sb.append(splitReplacement.appendToNext);
                    }
                    continue;
                }
            }

            if (c == PARENTHESES_OPEN) {
                inParentheses += 1;
            } else if (c == PARENTHESES_CLOSE) {
                inParentheses -= 1;
            } else if (c == TAG_NAME_START) { // No nesting test needed (afaik)
                inTag = true;
            } else if (c == TAG_NAME_END) {
                if (inTag) {
                    inTag = false;
                }
                inTagBody = !inTagBody;
            }
            sb.append(c);
        }
        if (sb.length() > 0) {
            appendSplit(splits, sb.toString().trim());
        }
    }

    public List<String> splitInstruction(String text) {
        if (StringUtils.isBlank(text)) {
            return Collections.emptyList();
        }

        List<String> splits = new ArrayList<>();
        computeSplits(text, splits);
        return splits;
    }

    @Data
    private static class SplitReplacement {
        private final String appendToCurrent;
        private final String appendToNext;

        public static SplitReplacement toCurrent(String replacement) {
            return new SplitReplacement(replacement, null);
        }

        public static SplitReplacement toNext(String replacement) {
            return new SplitReplacement(null, replacement);
        }
    }


}
