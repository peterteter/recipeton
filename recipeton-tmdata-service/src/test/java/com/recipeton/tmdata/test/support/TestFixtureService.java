package com.recipeton.tmdata.test.support;

/*-
 * #%L
 * recipeton-tmdata-service
 * %%
 * Copyright (C) 2021 tincomisc
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.recipeton.tmdata.config.ApplicationConfiguration;
import com.recipeton.tmdata.domain.control.TmControlRotationDirectionType;
import com.recipeton.tmdata.domain.control.TmControlSpeedType;
import com.recipeton.tmdata.domain.control.TmControlTemperatureType;
import com.recipeton.tmdata.domain.material.MaterialFlag;
import com.recipeton.tmdata.domain.material.MaterialObject;
import com.recipeton.tmdata.domain.material.MaterialObjectFlag;
import com.recipeton.tmdata.domain.misc.Locale;
import com.recipeton.tmdata.domain.misc.RangeType;
import com.recipeton.tmdata.domain.misc.TmVersion;
import com.recipeton.tmdata.domain.recipe.*;
import com.recipeton.tmdata.domain.recipe.lang.RecipeGroupLang;
import com.recipeton.tmdata.domain.recipe.lang.RecipeLang;
import com.recipeton.tmdata.domain.recipe.lang.RecipeLangAttribute;
import com.recipeton.tmdata.domain.recipe.lang.RecipeTimeLang;
import com.recipeton.tmdata.domain.unit.UnitNotation;
import com.recipeton.tmdata.domain.unit.UnitNotationPriority;
import com.recipeton.tmdata.domain.unit.UnitType;
import com.recipeton.tmdata.domain.utensil.UtensilType;
import com.recipeton.tmdata.service.material.MaterialObjectFlagRepository;
import com.recipeton.tmdata.service.material.MaterialService;
import com.recipeton.tmdata.service.misc.LocaleRepository;
import com.recipeton.tmdata.service.misc.LocaleService;
import com.recipeton.tmdata.service.misc.RangeTypeRepository;
import com.recipeton.tmdata.service.misc.TmVersionRepository;
import com.recipeton.tmdata.service.nutrition.RecipeNutritionService;
import com.recipeton.tmdata.service.recipe.*;
import com.recipeton.tmdata.service.recipe.lang.RecipeLangAttributeRepository;
import com.recipeton.tmdata.service.unit.UnitService;
import com.recipeton.tmdata.service.utensil.UtensilService;
import lombok.AllArgsConstructor;
import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;
import org.springframework.core.io.Resource;
import org.springframework.core.io.ResourceLoader;
import org.springframework.stereotype.Service;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.transaction.support.TransactionTemplate;

import java.math.BigDecimal;
import java.nio.file.Files;
import java.nio.file.Path;
import java.time.Duration;
import java.util.ArrayList;

import static com.recipeton.shared.analysis.domain.RecipeUnitDefinition.UNIT_GRAM;
import static com.recipeton.shared.util.ListUtil.toListMutable;
import static java.util.List.of;

@Slf4j
@Service
@AllArgsConstructor
public class TestFixtureService {

    private final ResourceLoader resourceLoader;

    private final PlatformTransactionManager platformTransactionManager;
    private final ApplicationConfiguration applicationConfiguration;
    private final GuidedIngredientStepService guidedIngredientStepService;
    private final GuidedUtensilStepService guidedUtensilStepService;
    private final GuidedTmStepService guidedTmStepService;
    private final LocaleRepository localeRepository;
    private final LocaleService localeService;
    private final MaterialObjectFlagRepository materialObjectFlagRepository;
    private final MaterialService materialService;
    private final RangeTypeRepository rangeTypeRepository;
    private final RecipeCategoryService recipeCategoryService;
    private final RecipeDifficultyRepository recipeDifficultyRepository;
    private final RecipeLangAttributeRepository recipeLangAttributeRepository;
    private final RecipeNutritionService recipeNutritionService;
    private final RecipePriceRepository recipePriceRepository;
    private final RecipeService recipeService;
    private final RecipeTimeTypeRepository recipeTimeTypeRepository;
    private final TmControlService tmControlService;
    private final TmVersionRepository tmVersionRepository;
    private final UnitService unitService;
    private final UtensilService utensilService;
    private final RecipeIngredientService recipeIngredientService;

    public Path copyResourceToImportPath(String resource) {
        return copyResourceToImportPath(resourceLoader.getResource(resource));
    }

    @SneakyThrows
    public Path copyResourceToImportPath(Resource resource) {
        Path targetPath = applicationConfiguration.getBookRecipesImportPath().resolve(resource.getFilename());
        Files.copy(resource.getInputStream(), targetPath);
        return targetPath;
    }

    public GuidedIngredientStepText createGuidedIngredientStepText(GuidedIngredientStepTextAttribute.Enum attribute, String text, Locale locale) {
        return guidedIngredientStepService.getPersistedGuidedIngredientStepText(attribute, false, text, locale);
    }

    @SneakyThrows
    public Recipe createRecipeBaseByTest(int recipeId, Locale locale) {
        String recipeOriginalTitle = "origtit_" + (long) recipeId;
        String recipeTitle = "Custom" + recipeId + " Experiment";

        UnitNotation servingUnitNotation = unitService.getPersisted("ration", locale, UnitNotationPriority.Enum.RECIPE_SPECIFIC, UnitType.Enum.PORTION);
        return new Recipe()
                .setUid((long) recipeId + "_UID")
                .setOriginalTitle(recipeOriginalTitle)
                .setRecipeCategories(of(
                        recipeCategoryService.getPersisted("experimentUid", "CustomExperiment1", locale, "testcat", false) // Add custom
                ))
                .setRecipeGroups(of(
                        createRecipeGroup(null, locale)
                )).setLangs(of(
                        new RecipeLang()
                                .setAttribute(recipeLangAttributeRepository.getOne(RecipeLangAttribute.Enum.TITLE))
                                .setText(recipeTitle)
                                .setLocale(locale)
                ))
                .setRecipeMaterialObject(of(
                        new RecipeMaterialObject()
                                .setPosition(1L)
                                .setPrimary(true)
                                .setMaterialObject(
                                        new MaterialObject()
                                                .setMaterialObjectFlags(toListMutable(materialObjectFlagRepository.getOne(MaterialObjectFlag.Enum.RESULT)))
                                                .setMaterial(materialService.createMaterial(Path.of("r99926.jpg"), 95779L, MaterialFlag.Enum.PROFESSIONAL))
                                )
                ))
                .setRecipeIngredients(new ArrayList<>())
                .setRecipeSteps(new ArrayList<>())
                .setRecipePrice(recipePriceRepository.getOne(RecipePrice.Enum.MEDIUM))
                .setRecipeDifficulty(recipeDifficultyRepository.getOne(RecipeDifficulty.Enum.EASY))
                .setRecipeUtensils(of(
                        new RecipeUtensil()
                                .setPosition(1L)
                                .setUtensil(utensilService.getPersisted(
                                        "testOven", "Oven", "Place to cook", UtensilType.Enum.KITCHEN_EQUIPMENT, locale))
                )) // fridge, kitchen towel, oven...
                .setRecipeTime(of(
                        new RecipeTime()
                                .setRecipeTimeType(recipeTimeTypeRepository.getOne(RecipeTimeType.Enum.TOTAL))
                                .setLangs(of(
                                        new RecipeTimeLang()
                                                .setLocale(locale)
                                                .setText("Recipe time text")
                                ))
                                .setRecipeTimeRanges(of(
                                        new RecipeTimeRange()
                                                .setRangeType(rangeTypeRepository.getOne(RangeType.Enum.FROM))
                                                .setSecondsValue(100L)
                                ))
                ))
                .setRecipeTmVersions(of(
                        new RecipeTmVersion().setTmVersion(
                                tmVersionRepository.getOne(TmVersion.Enum.APP)
                        )
                ))
                .setServingUnitNotation(servingUnitNotation)
                .setRecipeNutritionalValue(recipeNutritionService.createRecipeNutritionalValue(new BigDecimal(100), new BigDecimal(200), new BigDecimal(300), new BigDecimal(400), new BigDecimal(500), new BigDecimal(600), new BigDecimal(700.5), servingUnitNotation, locale))
                .setServingQuantity(BigDecimal.ONE)
                .setVersion(BigDecimal.ZERO)
                .setYear(2015L)
//                .setCompanies() // Needed??
                .setDeleted(false);


    }

    public RecipeGroup createRecipeGroup(String text, Locale locale) {
        return new RecipeGroup()
                .setLangs(text == null ? null : of(
                        new RecipeGroupLang()
                                .setLocale(locale)
                                .setText(text)
                ));
    }

    public RecipeIngredient createRecipeIngredient(String name, RecipeGroup recipeGroup, Locale locale) {
        return recipeIngredientService.createRecipeIngredient(name + "@testUid", name, "Diced", BigDecimal.ONE, new BigDecimal(2), UNIT_GRAM, locale, recipeGroup, false);
    }

    public TransactionTemplate getTransactionRequiresNewTemplate() {
        TransactionTemplate t = new TransactionTemplate(platformTransactionManager);
        t.setPropagationBehavior(Propagation.REQUIRES_NEW.value());
        return t;
    }

    public Locale setUpLocale() {
        return localeService.getLocaleByDefault();
    }

    public Locale setUpPersistedLocale() {
        return localeRepository.getOne(1L);
    }

    @Transactional(propagation = Propagation.REQUIRES_NEW)
    public Recipe setUpPersistedRecipeByGuidedStepTmSetting(int recipeId, Duration from, Duration to, TmControlTemperatureType.Enum temperature, TmControlSpeedType.Enum speed, TmControlRotationDirectionType.Enum direction) {
        Locale locale = setUpLocale();
        Recipe recipe = createRecipeBaseByTest(recipeId, locale);
        RecipeGroup recipeGroup = recipe.getRecipeGroups().get(0);
        recipe.setRecipeSteps(of(
                recipeService.createRecipeStep("RecipeStepContent for tmsetting", recipeGroup, locale,
                        guidedTmStepService.createGuidedStepTmSetting(
                                guidedTmStepService.createGuidedTmSettingStep(
                                        tmControlService.createTmControl(from, to, temperature, speed, direction),
                                        "You should set this and that", "Executing what you specified", locale))
                )
        ));

        RecipeIngredient recipeIngredient = createRecipeIngredient("Banana", recipeGroup, locale);
        recipe.getRecipeIngredients().add(recipeIngredient);

        return recipeService.prepareAndSave(recipe, locale);
    }

    @Transactional(propagation = Propagation.REQUIRES_NEW)
    public Recipe setUpPersistedRecipeByGuidedStepTmSettingDough(int recipeId, Duration duration) {
        Locale locale = setUpLocale();
        Recipe recipe = createRecipeBaseByTest(recipeId, locale);
        RecipeGroup recipeGroup = recipe.getRecipeGroups().get(0);
        recipe.setRecipeSteps(of(
                recipeService.createRecipeStep("RecipeStepContent for tmsettingDough", recipeGroup, locale,
                        guidedTmStepService.createGuidedStepTmSetting(
                                guidedTmStepService.createGuidedTmSettingStep(tmControlService.createTmControlByDough(duration, null, TmControlTemperatureType.Enum.T0), "Message before motor running", "Message WITH motor running", locale))
                )
        ));

        RecipeIngredient recipeIngredient = createRecipeIngredient("Potato", recipeGroup, locale);
        recipe.getRecipeIngredients().add(recipeIngredient);

        return recipeService.prepareAndSave(recipe, locale);
    }

    @Transactional(propagation = Propagation.REQUIRES_NEW)
    public Recipe setUpPersistedRecipeByGuidedStepTmSettingSoftBlending(int recipeId, Duration duration, TmControlSpeedType.Enum normal1, TmControlSpeedType.Enum normal2, TmControlSpeedType.Enum normal3, TmControlTemperatureType.Enum temperatureType) {
        Locale locale = setUpLocale();
        Recipe recipe = createRecipeBaseByTest(recipeId, locale);
        RecipeGroup recipeGroup = recipe.getRecipeGroups().get(0);
        recipe.setRecipeSteps(of(
                recipeService.createRecipeStep("RecipeStepContent for tmsettingSoftBlend", recipeGroup, locale,
                        guidedTmStepService.createGuidedStepTmSetting(
                                guidedTmStepService.createGuidedTmSettingStep(tmControlService.createTmControlBySoftBlending(duration, normal1, normal2, normal3, temperatureType),
                                        "Message before motor running", "Message WITH motor running", locale))
                )
        ));

        RecipeIngredient recipeIngredient = createRecipeIngredient("Potato", recipeGroup, locale);
        recipe.getRecipeIngredients().add(recipeIngredient);

        return recipeService.prepareAndSave(recipe, locale);
    }

    @Transactional(propagation = Propagation.REQUIRES_NEW)
    public Recipe setUpPersistedRecipeByGuidedStepUtensil(int recipeId, GuidedUtensilActionType.Enum actionType, String text) {
        Locale locale = setUpLocale();
        Recipe recipe = createRecipeBaseByTest(recipeId, locale);
        RecipeGroup recipeGroup = recipe.getRecipeGroups().get(0);
        recipe.setRecipeSteps(of(
                recipeService.createRecipeStep("RecipeStepContent for utensil", recipeGroup, locale,
                        guidedUtensilStepService.createGuidedStepUtensil(actionType, text, locale)
                )
        ));
        RecipeIngredient recipeIngredient = createRecipeIngredient("Potato", recipeGroup, locale);
        recipe.getRecipeIngredients().add(recipeIngredient);
        return recipeService.prepareAndSave(recipe, locale);
    }

    @SneakyThrows
    @Transactional
    public RecipeCategory setUpPersistedRecipeCategory(String uid, boolean primary) {
        return recipeCategoryService.getPersisted(uid, uid, setUpLocale(), primary ? "someFileName.jpg" : null, primary);
    }
}
