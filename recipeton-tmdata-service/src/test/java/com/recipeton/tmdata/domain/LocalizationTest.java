package com.recipeton.tmdata.domain;

/*-
 * #%L
 * recipeton-tmdata-service
 * %%
 * Copyright (C) 2021 tincomisc
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.recipeton.tmdata.test.support.TestFixtureTrait;
import lombok.extern.slf4j.Slf4j;
import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.Test;

@Slf4j
public class LocalizationTest implements TestFixtureTrait {

    @Test
    void testToSortKeyGivenEmptyTextThenReturnsEmpty() {
        Assertions.assertThat(createLangEntity("").toSortKey()).isEmpty();
    }

    @Test
    void testToSortKeyGivenNullTextThenReturnsNull() {
        Assertions.assertThat(createLangEntity(null).toSortKey()).isNull();
    }

    @Test
    void testToSortKeyGivenSomeTextThenReturnsEncodedStringRepresentation() {
        Assertions.assertThat(createLangEntity("sSSs").toSortKey()).isEqualTo("KKKK");
        Assertions.assertThat(createLangEntity("someText").toSortKey()).isEqualTo("KC?/M/UM");
    }


}
