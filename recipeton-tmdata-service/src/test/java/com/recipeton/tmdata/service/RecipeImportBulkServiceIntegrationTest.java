package com.recipeton.tmdata.service;

/*-
 * #%L
 * recipeton-tmdata-service
 * %%
 * Copyright (C) 2021 tincomisc
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.recipeton.tmdata.domain.recipe.RecipeDifficulty;
import com.recipeton.tmdata.domain.recipe.lang.RecipeLangAttribute;
import com.recipeton.tmdata.service.recipe.RecipeImportBulkService;
import com.recipeton.tmdata.service.recipe.RecipeRepository;
import com.recipeton.tmdata.test.support.AbstractIntegrationTest;
import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

import java.nio.file.Files;
import java.nio.file.Path;

import static com.recipeton.shared.util.ThrowingHelper.uConsumer;
import static java.util.concurrent.TimeUnit.SECONDS;
import static org.assertj.core.api.Assertions.assertThat;
import static org.awaitility.Awaitility.await;

@Slf4j
public class RecipeImportBulkServiceIntegrationTest extends AbstractIntegrationTest {

    @Autowired
    private RecipeImportBulkService recipeImportBulkService;
    @Autowired
    private RecipeRepository recipeRepository;

    @Test
    @Transactional
    void testDoImportGivenValidYamlThenSavesAnalyzedRecipeAndMovesFilesToDone() {
        Path recipePath = tFS.copyResourceToImportPath("classpath:/testdata/es/r0001.yaml");
        Path picturePath = tFS.copyResourceToImportPath("classpath:/testdata/es/r0001.jpg");

        tFS.getTransactionRequiresNewTemplate().executeWithoutResult(uConsumer(
                t -> recipeImportBulkService.doImport()
        ));

        Path expectedDataPath = applicationConfiguration.getBookRecipesImportCompletePath().resolve("r0001.jpg");
        await().atMost(2, SECONDS).until(() -> Files.exists(expectedDataPath));

        assertThat(recipeRepository.findFirstByUid("r0001")).hasValueSatisfying(
                r -> {
                    assertThat(r.getRecipeGroups()).hasSize(2);
                    assertThat(r.getOriginalTitle()).isEqualTo("Arroz con verduras");
                    assertThat(r.getLangs()).hasSize(3)
                            .anySatisfy(l -> {
                                assertThat(l.getAttribute().getId()).isEqualTo(RecipeLangAttribute.Enum.TITLE.getId());
                                assertThat(l.getText()).isEqualTo("Arroz con verduras");

                            })
                            .anySatisfy(l -> {
                                assertThat(l.getAttribute().getId()).isEqualTo(RecipeLangAttribute.Enum.TIP.getId());
                                assertThat(l.getText()).isEqualTo("Este plato es muy saludable :)");
                            });


                    assertThat(r.getRecipeDifficulty().getId()).isEqualTo(RecipeDifficulty.Enum.MEDIUM.getId());

                    assertThat(r.getRecipeCategories()).hasSize(2)
                            .anySatisfy(rc -> assertThat(rc.getUid()).isEqualTo("personal")) // keyword
                            .anySatisfy(rc -> assertThat(rc.getUid()).isEqualTo("0_030pastarice")); // predefined

                    assertThat(r.getRecipeSteps())
                            .hasSize(12)
                            .satisfies(l -> {
                                assertThat(l).element(0).satisfies(rs -> {
                                    assertThatRecipeStepLanguageEquals(rs, "Ponga en el vaso el aceite y el ajo");
                                });
                                assertThat(l).element(1).satisfies(rs -> {
                                    assertThatRecipeStepLanguageEquals(rs, "Pique [2 seg/vel 7]");
                                });
                                assertThat(l).element(2).satisfies(rs -> {
                                    assertThatRecipeStepLanguageEquals(rs, "Sofría [3 min/120°C/vel 1]");
                                });
                                assertThat(l).element(3).satisfies(rs -> {
                                    assertThatRecipeStepLanguageEquals(rs, "Incorpore los pimientos, los champiñones y la sal");
                                });
                                assertThat(l).element(4).satisfies(rs -> {
                                    assertThatRecipeStepLanguageEquals(rs, "Rehogue [10 min/120°C/reverse/vel 1]. Mientras tanto, ponga los ramilletes de brócoli en la vaporera");
                                });
//                                assertThat(l).element(5).satisfies(rs -> {
//                                    assertThatRecipeStepLanguageEquals(rs, "Mientras tanto, ponga los ramilletes de brócoli en la vaporera");
//                                });
                                assertThat(l).element(5).satisfies(rs -> {
                                    assertThatRecipeStepLanguageEquals(rs, "Introduzca el cestillo en el vaso");
                                });
                                assertThat(l).element(6).satisfies(rs -> {
                                    assertThatRecipeStepLanguageEquals(rs, "Vierta el arroz");
                                });
                                assertThat(l).element(7).satisfies(rs -> {
                                    assertThatRecipeStepLanguageEquals(rs, "Añada la sal y el agua");
                                });
                                assertThat(l).element(8).satisfies(rs -> {
                                    assertThatRecipeStepLanguageEquals(rs, "Programe [10 min/100°C/vel 4]");
                                });
                                assertThat(l).element(9).satisfies(rs -> {
                                    assertThatRecipeStepLanguageEquals(rs, "Sitúe el recipiente vaporera en su posición");
                                });
                                assertThat(l).element(10).satisfies(rs -> {
                                    assertThatRecipeStepLanguageEquals(rs, "Programe [10 min/Vapor/vel 2]");
                                });
                                assertThat(l).element(11).satisfies(rs -> {
                                    assertThatRecipeStepLanguageEquals(rs, "Sirva el arroz con la verduras y disfrute");
                                });
                            });

                }
        );

        assertThat(recipePath).doesNotExist();
        assertThat(picturePath).doesNotExist();

        // TODO assert file has correct size
        assertThat(applicationConfiguration.getBookAssetsPath().resolve("r0001.jpg")).exists();
        assertThat(applicationConfiguration.getBookRecipesImportCompletePath().resolve("r0001.yaml")).exists();
        assertThat(expectedDataPath).exists();
    }
}
