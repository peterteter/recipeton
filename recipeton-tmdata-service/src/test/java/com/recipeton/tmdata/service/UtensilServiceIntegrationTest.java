package com.recipeton.tmdata.service;

/*-
 * #%L
 * recipeton-tmdata-service
 * %%
 * Copyright (C) 2021 tincomisc
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.recipeton.tmdata.domain.misc.Locale;
import com.recipeton.tmdata.domain.utensil.Utensil;
import com.recipeton.tmdata.domain.utensil.UtensilType;
import com.recipeton.tmdata.domain.utensil.lang.UtensilLangAttribute;
import com.recipeton.tmdata.service.utensil.UtensilService;
import com.recipeton.tmdata.test.support.AbstractIntegrationTest;
import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

import static org.assertj.core.api.Assertions.assertThat;

@Slf4j
public class UtensilServiceIntegrationTest extends AbstractIntegrationTest {

    @Autowired
    private UtensilService utensilService;

    @Test
    @Transactional
    void testGetOrCreateUtensilGivenNewUtensilThenUtensilIsCreatedAndLangNameEqualsName() {
        String name = "test_" + System.nanoTime();
        Locale locale = tFS.setUpLocale();

        Utensil utensil = utensilService.getPersisted("nameuid", name, null, UtensilType.Enum.USEFUL_ITEM, locale);
        assertThat(utensil).isNotNull();
        assertThat(utensil.getLangs()).singleElement()
                .satisfies(ul -> {
                    assertThat(ul.getText()).isEqualTo(name);
                    assertThat(ul.getAttribute().getId()).isEqualTo(UtensilLangAttribute.Enum.NAME.getId());
                });
    }
}
