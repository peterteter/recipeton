package com.recipeton.tmdata.service;

/*-
 * #%L
 * recipeton-tmdata-service
 * %%
 * Copyright (C) 2021 tincomisc
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.recipeton.tmdata.domain.control.TmControlRotationDirectionType;
import com.recipeton.tmdata.domain.control.TmControlSpeedType;
import com.recipeton.tmdata.domain.control.TmControlTemperatureType;
import com.recipeton.tmdata.domain.control.TmControlTurboType;
import com.recipeton.tmdata.domain.misc.Locale;
import com.recipeton.tmdata.domain.recipe.*;
import com.recipeton.tmdata.service.recipe.*;
import com.recipeton.tmdata.test.support.AbstractIntegrationTest;
import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigDecimal;
import java.time.Duration;

import static java.util.List.of;
import static org.assertj.core.api.Assertions.assertThat;

// NOTE: SPLIT ALL TESTS!!!
// Test that recipesteps are in sequence
// Test that guidedsteps are in sequence
// ...
@Slf4j
public class RecipeServiceIntegrationTest extends AbstractIntegrationTest {

    @Autowired
    private RecipeRepository recipeRepository;
    @Autowired
    private RecipeService recipeService;
    @Autowired
    private GuidedCreatedIngredientStepService guidedCreatedIngredientStepService;
    @Autowired
    private GuidedIngredientStepService guidedIngredientStepService;
    @Autowired
    private GuidedFreetextStepService guidedFreetextStepService;
    @Autowired
    private GuidedTmStepService guidedTmStepService;
    @Autowired
    private TmControlService tmControlService;

    @Test
    @Transactional
    void testCreateRecipeGivenHavingTmSettingStepThenWip() {
        Recipe recipe3001 = recipeRepository.getOne(tFS.setUpPersistedRecipeByGuidedStepTmSetting(3001, Duration.ofSeconds(5), null, TmControlTemperatureType.Enum.T30, TmControlSpeedType.Enum.NORMAL5, TmControlRotationDirectionType.Enum.CLOCKWISE).getId());
        assertThat(recipe3001.getRecipeIngredients()).hasSize(1);
    }

    @Test
    @Transactional
    void testCreateRecipeGivenMultipleStepsWithSameIngredientThenRecipeIngredientsHasOnlyOneIngredient() {
        Long id = tFS.getTransactionRequiresNewTemplate().execute(ts -> {
            Locale locale = tFS.setUpLocale();
            Recipe recipe = tFS.createRecipeBaseByTest(15001, locale);
            RecipeGroup recipeGroup = recipe.getRecipeGroups().get(0);
            recipe.setRecipeSteps(of(
                    recipeService.createRecipeStep("RecipeStepContent for ingredient", recipeGroup, locale,
                            guidedIngredientStepService.createGuidedStepIngredient(tFS.createRecipeIngredient("Potato", recipeGroup, locale), new BigDecimal(100), null, tFS.createGuidedIngredientStepText(GuidedIngredientStepTextAttribute.Enum.PLACE, "Sample guided ingredient step text (SHOW SCALE-PLACE)", locale), GuidedIngredientStepWeighingAttribute.Enum.SHOW_SCALE),
                            guidedIngredientStepService.createGuidedStepIngredient(tFS.createRecipeIngredient("Potato", recipeGroup, locale), new BigDecimal(101), null, tFS.createGuidedIngredientStepText(GuidedIngredientStepTextAttribute.Enum.ADD, "Sample guided ingredient step text (SHOW SCALE-ADD)", locale), GuidedIngredientStepWeighingAttribute.Enum.SHOW_SCALE),
                            guidedIngredientStepService.createGuidedStepIngredient(tFS.createRecipeIngredient("Potato", recipeGroup, locale), new BigDecimal(102), null, tFS.createGuidedIngredientStepText(GuidedIngredientStepTextAttribute.Enum.PLACE, "Sample guided ingredient step text (SHOW_SCALE_AND_MIXING_BOWL-PLACE)", locale), GuidedIngredientStepWeighingAttribute.Enum.SHOW_SCALE_AND_MIXING_BOWL),
                            guidedIngredientStepService.createGuidedStepIngredient(tFS.createRecipeIngredient("Potato", recipeGroup, locale), new BigDecimal(103), null, tFS.createGuidedIngredientStepText(GuidedIngredientStepTextAttribute.Enum.WEIGHT_OUTSIDE, "Sample guided ingredient step text (SHOW SCALE-WEIGHTOUTSIDE)", locale), GuidedIngredientStepWeighingAttribute.Enum.DO_NOT_SHOW_SCALE)
                    )
            ));
            return recipeService.prepareAndSave(recipe, locale).getId();
        });

        assertThat(recipeRepository.getOne(id))
                .satisfies(r -> assertThat(r.getRecipeIngredients()).isNotEmpty());
    }


    // NOTE: SPLIT all this tests and complete assertions
    // Keep this one disabled as it is handy to a test database
    @Test
    @Transactional
    void testCreateRecipeGivenRecipeThenWip1() throws Exception {

//        // Works
//        // NOTE: Experiment with material picture
        Recipe recipe2001 = recipeRepository.getOne(tFS.setUpPersistedRecipeByGuidedStepUtensil(2001, GuidedUtensilActionType.Enum.MEASURING_CUP, "Sample Utensil text").getId());
        Recipe recipe2002 = recipeRepository.getOne(tFS.setUpPersistedRecipeByGuidedStepUtensil(2002, GuidedUtensilActionType.Enum.MEASURING_CUP, "Sample Utensil text").getId());

        Recipe recipe3001 = recipeRepository.getOne(tFS.setUpPersistedRecipeByGuidedStepTmSetting(3001, Duration.ofSeconds(5), null, TmControlTemperatureType.Enum.T30, TmControlSpeedType.Enum.NORMAL5, TmControlRotationDirectionType.Enum.CLOCKWISE).getId());
        Recipe recipe3002 = recipeRepository.getOne(tFS.setUpPersistedRecipeByGuidedStepTmSetting(3002, Duration.ofSeconds(10), Duration.ofSeconds(20), TmControlTemperatureType.Enum.T30, TmControlSpeedType.Enum.NORMAL5, TmControlRotationDirectionType.Enum.CLOCKWISE).getId());

        Recipe recipe3011 = recipeRepository.getOne(tFS.setUpPersistedRecipeByGuidedStepTmSetting(3011, Duration.ofSeconds(10), null, TmControlTemperatureType.Enum.T0, TmControlSpeedType.Enum.NORMAL5, TmControlRotationDirectionType.Enum.CLOCKWISE).getId());
        Recipe recipe3012 = recipeRepository.getOne(tFS.setUpPersistedRecipeByGuidedStepTmSetting(3012, Duration.ofSeconds(10), Duration.ofSeconds(20), TmControlTemperatureType.Enum.T0, TmControlSpeedType.Enum.NORMAL5, TmControlRotationDirectionType.Enum.CLOCKWISE).getId());

        Recipe recipe3021 = recipeRepository.getOne(tFS.setUpPersistedRecipeByGuidedStepTmSetting(3021, Duration.ofSeconds(5), null, TmControlTemperatureType.Enum.T30, TmControlSpeedType.Enum.NORMAL5, TmControlRotationDirectionType.Enum.COUNTER_CLOCKWISE).getId());
        Recipe recipe3022 = recipeRepository.getOne(tFS.setUpPersistedRecipeByGuidedStepTmSetting(3022, Duration.ofSeconds(10), Duration.ofSeconds(20), TmControlTemperatureType.Enum.T30, TmControlSpeedType.Enum.NORMAL5, TmControlRotationDirectionType.Enum.COUNTER_CLOCKWISE).getId());

        Recipe recipe3031 = recipeRepository.getOne(tFS.setUpPersistedRecipeByGuidedStepTmSetting(3021, Duration.ofSeconds(5), null, TmControlTemperatureType.Enum.T30, TmControlSpeedType.Enum.NORMAL5, TmControlRotationDirectionType.Enum.DISABLED_COUNTERCLOCKWISE).getId());
        Recipe recipe3032 = recipeRepository.getOne(tFS.setUpPersistedRecipeByGuidedStepTmSetting(3022, Duration.ofSeconds(10), Duration.ofSeconds(20), TmControlTemperatureType.Enum.T30, TmControlSpeedType.Enum.NORMAL5, TmControlRotationDirectionType.Enum.DISABLED_COUNTERCLOCKWISE).getId());


        // Works
        Recipe recipe3601 = recipeRepository.getOne(tFS.setUpPersistedRecipeByGuidedStepTmSettingSoftBlending(3601, Duration.ofSeconds(60), TmControlSpeedType.Enum.NORMAL1, TmControlSpeedType.Enum.NORMAL2, TmControlSpeedType.Enum.NORMAL3, TmControlTemperatureType.Enum.T30).getId());
        Recipe recipe3602 = recipeRepository.getOne(tFS.setUpPersistedRecipeByGuidedStepTmSettingSoftBlending(3602, Duration.ofSeconds(60), TmControlSpeedType.Enum.SOFT1, TmControlSpeedType.Enum.SOFT2, TmControlSpeedType.Enum.SOFT3, TmControlTemperatureType.Enum.T30).getId());

        Recipe recipe3701 = recipeRepository.getOne(tFS.setUpPersistedRecipeByGuidedStepTmSettingDough(3701, Duration.ofSeconds(60)).getId());
    }

    @Test
    @Transactional
    public void testCreateRecipeGivenRecipeWithGuidedStepCreatedIngredientThenSucceeds() {
        Long id = tFS.getTransactionRequiresNewTemplate().execute(ts -> {
            Locale locale = tFS.setUpLocale();
            Recipe recipe = tFS.createRecipeBaseByTest(5001, locale);
            RecipeGroup recipeGroup = recipe.getRecipeGroups().get(0);
            // Create Step...
            recipe.setRecipeSteps(of(
                    recipeService.createRecipeStep("RecipeStepContent for createdingredient", recipeGroup, locale,
                            guidedCreatedIngredientStepService.createGuidedStepCreatedIngredient("Sample guided created ingredient step NOTATION (place)",
                                    new BigDecimal(50), GuidedIngredientStepWeighingAttribute.Enum.SHOW_SCALE,
                                    locale, GuidedCreatedIngredientStepTextAttribute.Enum.PLACE)
                    )
            ));

            recipe.getRecipeIngredients().add(tFS.createRecipeIngredient("Banana", recipeGroup, locale));

            return recipeService.prepareAndSave(recipe, locale).getId();
        });

        assertThat(recipeRepository.getOne(id))
                .satisfies(r -> assertThat(r.getRecipeIngredients()).isNotEmpty());
    }

    @Test
    @Transactional
    public void testCreateRecipeGivenRecipeWithGuidedStepFreetextTypeDefaultThenSucceeds() {
        Long id = tFS.getTransactionRequiresNewTemplate().execute(ts -> {
            Locale locale = tFS.setUpLocale();
            Recipe recipe = tFS.createRecipeBaseByTest(1001, locale);
            RecipeGroup recipeGroup = recipe.getRecipeGroups().get(0);
            recipe.setRecipeSteps(of(
                    recipeService.createRecipeStep("RecipeStep freetext", recipeGroup, locale,
                            guidedFreetextStepService.createGuidedStepFreetext(GuidedFreetextType.Enum.DEFAULT, "Freetext Sample", locale)
                    )
            ));
            recipe.getRecipeIngredients().add(tFS.createRecipeIngredient("Banana", recipeGroup, locale));

            return recipeService.prepareAndSave(recipe, locale).getId();
        });

        assertThat(recipeRepository.getOne(id))
                .satisfies(r -> assertThat(r.getRecipeIngredients()).isNotEmpty());
    }

    @Test
    @Transactional
    public void testCreateRecipeGivenRecipeWithGuidedStepFreetextTypeParallelThenSucceeds() {
        Long id = tFS.getTransactionRequiresNewTemplate().execute(ts -> {
            Locale locale = tFS.setUpLocale();
            Recipe recipe = tFS.createRecipeBaseByTest(1001, locale);
            RecipeGroup recipeGroup = recipe.getRecipeGroups().get(0);
            recipe.setRecipeSteps(of(
                    recipeService.createRecipeStep("RecipeStep freetext", recipeGroup, locale,
                            guidedFreetextStepService.createGuidedStepFreetext(GuidedFreetextType.Enum.PARALLEL, "Freetext Sample", locale)
                    )
            ));
            recipe.getRecipeIngredients().add(tFS.createRecipeIngredient("Banana", recipeGroup, locale));

            return recipeService.prepareAndSave(recipe, locale).getId();
        });

        assertThat(recipeRepository.getOne(id))
                .satisfies(r -> assertThat(r.getRecipeIngredients()).isNotEmpty());
    }

    @Test
    @Transactional
    public void testCreateRecipeGivenRecipeWithGuidedStepFreetextTypeServingThenSucceeds() {
        Long id = tFS.getTransactionRequiresNewTemplate().execute(ts -> {
            Locale locale = tFS.setUpLocale();
            Recipe recipe = tFS.createRecipeBaseByTest(1001, locale);
            RecipeGroup recipeGroup = recipe.getRecipeGroups().get(0);
            recipe.setRecipeSteps(of(
                    recipeService.createRecipeStep("RecipeStep freetext", recipeGroup, locale,
                            guidedFreetextStepService.createGuidedStepFreetext(GuidedFreetextType.Enum.SERVING, "Freetext Sample", locale)
                    )
            ));
            recipe.getRecipeIngredients().add(tFS.createRecipeIngredient("Banana", recipeGroup, locale));

            return recipeService.prepareAndSave(recipe, locale).getId();
        });

        assertThat(recipeRepository.getOne(id))
                .satisfies(r -> assertThat(r.getRecipeIngredients()).isNotEmpty());
    }

    @Test
    @Transactional
    void testCreateRecipeGivenRecipeWithGuidedStepIngredientThenSucceeds() {
        Long id = tFS.getTransactionRequiresNewTemplate().execute(ts -> {
            Locale locale = tFS.setUpLocale();
            Recipe recipe = tFS.createRecipeBaseByTest(4001, locale);
            RecipeGroup recipeGroup = recipe.getRecipeGroups().get(0);
            recipe.setRecipeSteps(of(
                    recipeService.createRecipeStep("RecipeStepContent for ingredient", recipeGroup, locale,
                            guidedIngredientStepService.createGuidedStepIngredient(tFS.createRecipeIngredient("Potato", recipeGroup, locale), new BigDecimal(100), null, tFS.createGuidedIngredientStepText(GuidedIngredientStepTextAttribute.Enum.PLACE, "Sample guided ingredient step text (SHOW SCALE-PLACE)", locale), GuidedIngredientStepWeighingAttribute.Enum.SHOW_SCALE),
                            guidedIngredientStepService.createGuidedStepIngredient(tFS.createRecipeIngredient("Banana", recipeGroup, locale), new BigDecimal(100), null, tFS.createGuidedIngredientStepText(GuidedIngredientStepTextAttribute.Enum.ADD, "Sample guided ingredient step text (SHOW SCALE-ADD)", locale), GuidedIngredientStepWeighingAttribute.Enum.SHOW_SCALE),
                            guidedIngredientStepService.createGuidedStepIngredient(tFS.createRecipeIngredient("Pineaple", recipeGroup, locale), new BigDecimal(100), null, tFS.createGuidedIngredientStepText(GuidedIngredientStepTextAttribute.Enum.PLACE, "Sample guided ingredient step text (SHOW_SCALE_AND_MIXING_BOWL-PLACE)", locale), GuidedIngredientStepWeighingAttribute.Enum.SHOW_SCALE_AND_MIXING_BOWL),
                            guidedIngredientStepService.createGuidedStepIngredient(tFS.createRecipeIngredient("Carrot", recipeGroup, locale), new BigDecimal(100), null, tFS.createGuidedIngredientStepText(GuidedIngredientStepTextAttribute.Enum.WEIGHT_OUTSIDE, "Sample guided ingredient step text (SHOW SCALE-WEIGHTOUTSIDE)", locale), GuidedIngredientStepWeighingAttribute.Enum.DO_NOT_SHOW_SCALE)
                    )
            ));

            return recipeService.prepareAndSave(recipe, locale).getId();
        });

        assertThat(recipeRepository.getOne(id))
                .satisfies(r -> assertThat(r.getRecipeIngredients()).hasSize(4));
    }

    @Test
    @Transactional
    public void testCreateRecipeGivenRecipeWithGuidedStepTurboThenSucceeds() {
        Long id = tFS.getTransactionRequiresNewTemplate().execute(ts -> {
            Locale locale = tFS.setUpLocale();
            Recipe recipe = tFS.createRecipeBaseByTest(3501, locale);
            RecipeGroup recipeGroup = recipe.getRecipeGroups().get(0);
//        RecipeGroup recipeGroup = setUpPersistedRecipeGroup("rgl_" + recipeId, locale);
            recipe.setRecipeSteps(of(
                    recipeService.createRecipeStep("RecipeStepContent for tmsettingTurbo", recipeGroup, locale,
                            guidedTmStepService.createGuidedStepTmSetting(
                                    guidedTmStepService.createGuidedTmSettingStep(
                                            tmControlService.createTmControlByTurbo(TmControlTurboType.Enum.HALF_SECOND, 3, TmControlTemperatureType.Enum.T0),
                                            "Message before motor running", "Message WITH motor running", locale))
                    )
            ));

            RecipeIngredient recipeIngredient = tFS.createRecipeIngredient("Potato", recipeGroup, locale);
            recipe.getRecipeIngredients().add(recipeIngredient);

            return recipeService.prepareAndSave(recipe, locale).getId();
        });

        assertThat(recipeRepository.getOne(id))
                .satisfies(r -> assertThat(r.getRecipeIngredients()).isNotEmpty());
    }


}
