package com.recipeton.tmdata.config;

/*-
 * #%L
 * recipeton-tmdata-service
 * %%
 * Copyright (C) 2021 tincomisc
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.recipeton.shared.analysis.configuration.RecipeAnalyzerCookiDeConfiguration;
import com.recipeton.shared.analysis.configuration.RecipeAnalyzerCookiEnConfiguration;
import com.recipeton.shared.analysis.configuration.RecipeAnalyzerCookiEsConfiguration;
import com.recipeton.shared.analysis.configuration.RecipeAnalyzerCookiFrConfiguration;
import com.recipeton.shared.analysis.service.RecipeAnalyzer;
import com.recipeton.shared.analysis.service.RecipeStandardAnalyzer;
import com.recipeton.shared.service.RecipeDefinitionStorageService;
import com.recipeton.shared.util.ObjectMapperFactory;
import com.recipeton.tmdata.Application;
import com.recipeton.tmdata.domain.misc.Country;
import com.recipeton.tmdata.domain.misc.Language;
import lombok.Data;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.io.Resource;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

import java.nio.file.Path;

@Configuration
@ConfigurationProperties(prefix = "recipeton")
@EntityScan(basePackageClasses = {Application.class})
@EnableJpaRepositories(basePackageClasses = {Application.class})
@Data
public class ApplicationConfiguration {

    public static final int IMPORT_RECIPE_CATEGORIES_COUNT_MIN = 10;
    public static final int IMPORT_RECIPE_CATEGORIES_COUNT_MAX = 1500;

    private boolean serviceMode;

    private Path bookAssetsSharedPath;

    private String bookName;
    private Country.Enum bookCountry;
    private Language.Enum bookLanguage;

    private Path bookRootPath;

    private Path bookExportPath;

    private Path bookRecipesImportCompletePath;
    private Path bookRecipesImportPath;
    private Path bookRecipesImportErrorPath;
    private Path bookRecipesImportSkipPath;

    private Path bookAssetsPath;
    private Path bookAssetsThemeBrightPath;
    private Path bookAssetsThemeDarkPath;

    private ImageDimensions bookImageLargeDimensions;
    private ImageDimensions bookImageSmallDimensions;

    private String bookSplashDefaultLogoFileName;
    private String bookSplashFileName;
    private String bookSplashDefaultFileName;
    private String bookIconFileName;
    private String bookRecipeCategoryDefaultIconFileName;

    private boolean failOnBookAssetUnresolved;

    private boolean dataDefaultAutocreateDisabled;
    private Resource dataDefaultShoppingCategories;
    private Resource dataDefaultShoppingCategoryLangs;
    private Resource dataDefaultCountryLangs;
    private Resource dataDefaultLanguageLangs;
    private Resource dataDefaultRecipeDifficultyLangs;
    private Resource dataDefaultRecipePriceLangs;
    private Resource dataDefaultRecipeTimeTypeLangs;
    private Resource dataDefaultRecipeNutritionalValueTypeLangs;
    private Resource dataDefaultGuidedCreatedIngredientStepTextLangs;
    private Resource dataDefaultGuidedIngredientStepTextLangs;

    private String databaseVersion;
    private String databaseExportDateTime; // NOTE: Change for LocalDateTime and format
    private int databaseRelease;

    /**
     * Only import recipes with device with matching name
     */
    private String importDeviceNameFilter;

    private boolean importRecipeKeywordsAsPrimaryRecipeCategories = true;
    private boolean importRecipeNamePrefixAsPrimaryRecipeCategories;
    private long importRecipeCategoriesRecipeCountMin = IMPORT_RECIPE_CATEGORIES_COUNT_MIN;
    private long importRecipeCategoriesRecipeCountMax = IMPORT_RECIPE_CATEGORIES_COUNT_MAX;
    /**
     * On command step display the full text or just the verb action.
     */
    private boolean importStepCommandExecuteTextFull = true;

    private boolean importAddMeasuringCupReminderBeforeOperation; // Do not enable until detection of special conditions (Like do NOT add measuring cup)
    private boolean importAnalysisStrict;

    /**
     * Even more strict. Will crash if not every ingredient has a clear determined purpose.
     * At the moment many recipes will fail because parallel operations (pans/oven etc...) are not analyzed in detail
     * For dev analysis only.
     */
    private boolean importAnalysisStrictSuper;

    private boolean importOnStart = true;
    private boolean closeAfterImport = true;

    private boolean entityIngredientPreparationIgnore = true;

    private String dataDefaultAssetPath;
    private String dataDefaultAssetPathLocationPattern;


    @Bean
    public RecipeAnalyzer recipeAnalyzerCookiDe() {
        return new RecipeStandardAnalyzer(new RecipeAnalyzerCookiDeConfiguration());
    }

    @Bean
    public RecipeAnalyzer recipeAnalyzerCookiEn() {
        return new RecipeStandardAnalyzer(new RecipeAnalyzerCookiEnConfiguration());
    }

    @Bean
    public RecipeAnalyzer recipeAnalyzerCookiEs() {
        return new RecipeStandardAnalyzer(new RecipeAnalyzerCookiEsConfiguration());
    }

    @Bean
    public RecipeAnalyzer recipeAnalyzerCookiFr() {
        return new RecipeStandardAnalyzer(new RecipeAnalyzerCookiFrConfiguration());
    }

    @Bean
    public RecipeDefinitionStorageService recipeStorageService() {
        return new RecipeDefinitionStorageService(new ObjectMapperFactory().createInstance(true));
    }

    @Data
    public static class ImageDimensions {
        private int width;
        private int height;
    }
}
