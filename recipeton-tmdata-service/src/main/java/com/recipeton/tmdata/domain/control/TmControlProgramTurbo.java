package com.recipeton.tmdata.domain.control;

/*-
 * #%L
 * recipeton-tmdata-service
 * %%
 * Copyright (C) 2021 tincomisc
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import lombok.*;
import lombok.experimental.Accessors;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Table(name = "tmControlProgramTurbo")
@Data
@AllArgsConstructor
@NoArgsConstructor
@Accessors(chain = true)
@ToString(onlyExplicitlyIncluded = true)
public class TmControlProgramTurbo implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @Column(name = "tmControl_id", nullable = false) // bad col name
    private Long tmControlProgramId;

    @OneToOne
    @JoinColumn(name = "tmControl_id")
    @MapsId
    @EqualsAndHashCode.Exclude
    private TmControlProgram tmControlProgram;

    @ManyToOne
    @JoinColumn(name = "tmControlTurboType_id", nullable = false)
    @ToString.Include
    private TmControlTurboType tmControlTurboType;

    @Column(name = "impulseCount", nullable = false)
    @ToString.Include
    private Long impulseCount;
}
