package com.recipeton.tmdata.domain.nutrition;

/*-
 * #%L
 * recipeton-tmdata-service
 * %%
 * Copyright (C) 2021 tincomisc
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.recipeton.tmdata.domain.Localized;
import com.recipeton.tmdata.domain.nutrition.lang.RecipeNutritionLang;
import com.recipeton.tmdata.domain.recipe.Recipe;
import com.recipeton.tmdata.domain.unit.UnitNotation;
import lombok.*;
import lombok.experimental.Accessors;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Collection;

import static com.recipeton.shared.util.StreamUtil.toStream;
import static javax.persistence.FetchType.LAZY;

@Entity
@Table(name = "recipeNutritionalValue")
@Data
@AllArgsConstructor
@NoArgsConstructor
@Accessors(chain = true)
@ToString(onlyExplicitlyIncluded = true)
public class RecipeNutritionalValue implements Localized, Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", nullable = false)
    private Long id;

    @OneToOne
    @JoinColumn(name = "recipe_id", nullable = false)
    private Recipe recipe;

    @ManyToOne(fetch = LAZY)
    @JoinColumn(name = "servingUnitNotation_id", nullable = false)
    @ToString.Include
    private UnitNotation servingUnitNotation;

    @EqualsAndHashCode.Exclude
    @ToString.Include
    @OneToMany(mappedBy = "recipeNutritionalValue", cascade = CascadeType.ALL, orphanRemoval = true)
    private Collection<RecipeNutritionalValueData> recipeNutritionalValueDatas;

    @EqualsAndHashCode.Exclude
    @ToString.Include
    @OneToMany(mappedBy = "recipeNutritionalValue", cascade = CascadeType.ALL, orphanRemoval = true)
    private Collection<RecipeNutritionalValuePortionRange> recipeNutritionalValuePortionRanges;

    @EqualsAndHashCode.Exclude
    @ToString.Include
    @OneToMany(mappedBy = "recipeNutritionalValue", cascade = CascadeType.ALL, orphanRemoval = true)
    private Collection<RecipeNutritionLang> langs;

    @PrePersist
    public void prePersist() {
        toStream(langs).filter(e -> e.getRecipeNutritionalValue() == null).forEach(e -> e.setRecipeNutritionalValue(this));
        toStream(recipeNutritionalValueDatas).filter(e -> e.getRecipeNutritionalValue() == null).forEach(e -> e.setRecipeNutritionalValue(this));
        toStream(recipeNutritionalValuePortionRanges).filter(e -> e.getRecipeNutritionalValue() == null).forEach(e -> e.setRecipeNutritionalValue(this));
    }
}
