package com.recipeton.tmdata.domain.project;

/*-
 * #%L
 * recipeton-tmdata-service
 * %%
 * Copyright (C) 2021 tincomisc
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.recipeton.tmdata.domain.Localized;
import com.recipeton.tmdata.domain.misc.Company;
import com.recipeton.tmdata.domain.project.lang.ProjectLang;
import com.recipeton.util.jpa.LocalDateTimeLongSecondsConverter;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;
import lombok.experimental.Accessors;

import javax.persistence.*;
import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.Collection;
import java.util.List;

@Entity
@Data
@Table(name = "project")
@Accessors(chain = true)
public class Project implements Localized, Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @Column(name = "id", nullable = false)
    private Long id;

    @Column(name = "uid", nullable = false)
    private String uid;

    @Column(name = "description", nullable = false)
    private String description;

    @ManyToOne
    @JoinColumn(name = "projectType_id", nullable = false)
    private ProjectType projectType;

    @Column(name = "nofRecipesPlanned", nullable = false)
    private Long nofRecipesPlanned;

    @Column(name = "isInternal", nullable = false)
    private String internal;

    @Column(name = "isInternational", nullable = false)
    private String international;

    @Column(name = "isDeleted", nullable = false)
    private boolean deleted;

    @Convert(converter = LocalDateTimeLongSecondsConverter.class)
    @Column(name = "createdts", nullable = false)
    private LocalDateTime created;

    @Convert(converter = LocalDateTimeLongSecondsConverter.class)
    @Column(name = "lastchangedts", nullable = false)
    private LocalDateTime lastchanged;

    @EqualsAndHashCode.Exclude
    @ToString.Exclude
    @OneToMany(mappedBy = "project", cascade = CascadeType.ALL, orphanRemoval = true)
    private List<ProjectEntry> projectEntries;

    @EqualsAndHashCode.Exclude
    @ToString.Exclude
    @ManyToMany
    @JoinTable(name = "project_projectCategory",
            joinColumns = @JoinColumn(name = "project_id"),
            inverseJoinColumns = @JoinColumn(name = "projectCategory_id"))
    private Collection<ProjectCategory> projectCategories;

    @EqualsAndHashCode.Exclude
    @ToString.Exclude
    @ManyToMany
    @JoinTable(name = "project_company",
            joinColumns = @JoinColumn(name = "project_id"),
            inverseJoinColumns = @JoinColumn(name = "company_id"))
    private Collection<Company> projectCompanies;

    @EqualsAndHashCode.Exclude
    @ToString.Exclude
    @OneToMany(mappedBy = "project", cascade = CascadeType.ALL, orphanRemoval = true)
    private Collection<ProjectLang> langs;

    @EqualsAndHashCode.Exclude
    @ToString.Exclude
    @OneToMany(mappedBy = "project", cascade = CascadeType.ALL, orphanRemoval = true)
    @OrderBy("position")
    private List<ProjectMaterialObject> projectMaterialObjects;

}
