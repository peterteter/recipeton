package com.recipeton.tmdata.domain.ingredient.extra;

/*-
 * #%L
 * recipeton-tmdata-service
 * %%
 * Copyright (C) 2021 tincomisc
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import lombok.*;
import lombok.experimental.Accessors;

import javax.persistence.*;
import java.io.Serializable;
import java.util.List;

// Empty
@Entity
@Table(name = "ingredientFoodCategory")
@Data
@AllArgsConstructor
@NoArgsConstructor
@Accessors(chain = true)
public class IngredientFoodCategory implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @Column(name = "id", nullable = false)
    private Long id;

    @Column(name = "lft", nullable = false)
    private Long lft;

    @Column(name = "rgt", nullable = false)
    private Long rgt;

    @EqualsAndHashCode.Exclude
    @ToString.Exclude
    @OneToMany(mappedBy = "ingredientFoodCategory", cascade = CascadeType.ALL, orphanRemoval = true)
    private List<IngredientFoodCategoryLang> ingredientFoodCategoryLangs;

}
