package com.recipeton.tmdata.domain.recipe;

/*-
 * #%L
 * recipeton-tmdata-service
 * %%
 * Copyright (C) 2021 tincomisc
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;
import lombok.experimental.Accessors;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Collection;

import static com.recipeton.shared.util.StreamUtil.toStream;

@Entity
@Data
@AllArgsConstructor
@NoArgsConstructor
@Accessors(chain = true)
@Table(name = "recipeStep_recipeIngredient")
@ToString(onlyExplicitlyIncluded = true)
public class RecipeStepRecipeIngredient implements Serializable {

    private static final long serialVersionUID = 1L;

    @EmbeddedId
    private ID id = new ID();

    @ManyToOne
    @JoinColumn(name = "recipeStep_id")
    @MapsId("recipeStepId")
    private RecipeStep recipeStep;

    @ManyToOne
    @JoinColumn(name = "recipeIngredient_id")
    @MapsId("recipeIngredientId")
    @ToString.Include
    private RecipeIngredient recipeIngredient;

    @Column(name = "position", nullable = false)
    @ToString.Include
    private Long position;

    @OneToMany(cascade = CascadeType.ALL, mappedBy = "recipeStepRecipeIngredient")
    private Collection<RecipeStepRecipeIngredientRange> recipeStepRecipeIngredientRanges; // Supertotals??

    @PrePersist
    public void prePersist() {
        toStream(recipeStepRecipeIngredientRanges).forEach(r -> r.setRecipeStepRecipeIngredient(this));
    }

    @Data
    @Embeddable
    public static class ID implements Serializable {
        @Column(name = "recipeStep_id", nullable = false)
        private Long recipeStepId;

        @Column(name = "recipeIngredient_id", nullable = false)
        private Long recipeIngredientId;
    }


}
