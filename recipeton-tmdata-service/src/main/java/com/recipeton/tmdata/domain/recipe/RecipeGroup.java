package com.recipeton.tmdata.domain.recipe;

/*-
 * #%L
 * recipeton-tmdata-service
 * %%
 * Copyright (C) 2021 tincomisc
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.recipeton.tmdata.domain.Localized;
import com.recipeton.tmdata.domain.misc.Locale;
import com.recipeton.tmdata.domain.recipe.lang.RecipeGroupLang;
import lombok.*;
import lombok.experimental.Accessors;
import org.apache.commons.lang3.StringUtils;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Collection;
import java.util.List;

import static com.recipeton.shared.util.StreamUtil.toStream;

@Entity
@Table(name = "recipeGroup")
@Data
@AllArgsConstructor
@NoArgsConstructor
@Accessors(chain = true)
@ToString(onlyExplicitlyIncluded = true)
public class RecipeGroup implements Localized, Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", nullable = false)
    @ToString.Include
    private Long id;

    @OneToMany(mappedBy = "recipeGroup", cascade = CascadeType.ALL, orphanRemoval = true)
    private Collection<RecipeGroupLang> langs;

    // CUSTOM
    @ManyToMany(mappedBy = "recipeGroups")
    @EqualsAndHashCode.Exclude
    private Collection<Recipe> recipes;

    public static RecipeGroup of(String text, Locale locale) {
        return new RecipeGroup()
                .setLangs(StringUtils.isBlank(text) ? null : List.of(new RecipeGroupLang().setLocale(locale).setText(text)
                ));
    }

    @PrePersist
    public void prePersist() {
        toStream(langs).filter(n -> n.getRecipeGroup() == null).forEach(n -> n.setRecipeGroup(this));
    }
}
