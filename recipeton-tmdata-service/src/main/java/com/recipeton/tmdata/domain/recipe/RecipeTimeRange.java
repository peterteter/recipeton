package com.recipeton.tmdata.domain.recipe;

/*-
 * #%L
 * recipeton-tmdata-service
 * %%
 * Copyright (C) 2021 tincomisc
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.recipeton.tmdata.domain.misc.RangeType;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;
import lombok.experimental.Accessors;

import javax.persistence.*;
import java.io.Serializable;

import static javax.persistence.FetchType.LAZY;

@Entity
@Data
@AllArgsConstructor
@NoArgsConstructor
@Accessors(chain = true)
@Table(name = "recipeTimeRange")
@ToString(onlyExplicitlyIncluded = true)
public class RecipeTimeRange implements Serializable {

    private static final long serialVersionUID = 1L;

    @EmbeddedId
    private ID id = new ID();

    @ManyToOne
    @JoinColumn(name = "recipeTime_id")
    @MapsId("recipeTimeId")
    private RecipeTime recipeTime;

    @ManyToOne(fetch = LAZY)
    @JoinColumn(name = "rangeType_id")
    @MapsId("rangeTypeId")
    @ToString.Include
    private RangeType rangeType;

    @Column(name = "secondsValue", nullable = false)
    @ToString.Include
    private Long secondsValue;

    @Data
    public static class ID implements Serializable {
        @Column(name = "recipeTime_id", nullable = false)
        private Long recipeTimeId;

        @Column(name = "rangeType_id", nullable = false)
        private Long rangeTypeId;
    }
}
