package com.recipeton.tmdata.domain.misc;

/*-
 * #%L
 * recipeton-tmdata-service
 * %%
 * Copyright (C) 2021 tincomisc
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;
import lombok.experimental.Accessors;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.*;
import java.io.Serializable;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static javax.persistence.FetchType.LAZY;

@Entity
@Table(name = "locale")
@Cacheable
@org.hibernate.annotations.Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
@Data
@AllArgsConstructor
@NoArgsConstructor
@Accessors(chain = true)
@ToString(onlyExplicitlyIncluded = true, includeFieldNames = false)
public class Locale implements Serializable {

    public static final List<Locale> DEFAULT = Stream.of(Locale.Enum.values()).map(e -> e.data).collect(Collectors.toList());

    private static final long serialVersionUID = 1L;

    @Id
    @Column(name = "id", nullable = false)
    private Long id;

    @ManyToOne(fetch = LAZY)
    @JoinColumn(name = "language_id", nullable = false)
    @ToString.Include
    private Language language;

    @ManyToOne(fetch = LAZY)
    @JoinColumn(name = "country_id", nullable = false)
    @ToString.Include
    private Country country;

    @Column(name = "lft", nullable = false)
    private Long lft;

    @Column(name = "rgt", nullable = false)
    private Long rgt;

    public Locale(long id, Language.Enum language, Country.Enum country, long lft, long rgt) {
        this(id, Language.byId(language.getId()), Country.byId(country.getId()), lft, rgt);
    }

    // Complete this
    public enum Enum {
        en_VI(new Locale(1L, Language.Enum.EN, Country.Enum.VI, 1L, 2L)),
        de_DE(new Locale(2L, Language.Enum.DE, Country.Enum.DE, 2L, 3L)),
        fr_FR(new Locale(3L, Language.Enum.FR, Country.Enum.FR, 4L, 5L)),
        es_ES(new Locale(4L, Language.Enum.ES, Country.Enum.ES, 6L, 7L)),
        it_IT(new Locale(5L, Language.Enum.IT, Country.Enum.IT, 8L, 9L)),
        pt_PT(new Locale(6L, Language.Enum.PT_PT, Country.Enum.PT, 10L, 11L)),
        L7L(new Locale(7L, Language.byId(7L), Country.byId(10L), 12L, 13L)),
        L8L(new Locale(8L, Language.byId(8L), Country.byId(11L), 14L, 15L)),
        L9L(new Locale(9L, Language.byId(9L), Country.byId(12L), 16L, 17L)),
        L10L(new Locale(10L, Language.byId(16L), Country.byId(16L), 18L, 19L)),
        L11L(new Locale(11L, Language.byId(11L), Country.byId(13L), 20L, 21L)),
        L12L(new Locale(12L, Language.byId(12L), Country.byId(14L), 22L, 23L)),
        L13L(new Locale(13L, Language.byId(13L), Country.byId(15L), 24L, 25L)),
        L14L(new Locale(14L, Language.byId(14L), Country.byId(17L), 26L, 27L)),
        L15L(new Locale(15L, Language.byId(15L), Country.byId(18L), 28L, 29L)),
        en_UK(new Locale(16L, Language.Enum.EN, Country.Enum.UK, 30L, 31L)),
        L17L(new Locale(17L, Language.byId(17L), Country.byId(20L), 32L, 33L)),
        L18L(new Locale(18L, Language.byId(18L), Country.byId(21L), 34L, 35L)),
        L19L(new Locale(19L, Language.byId(19L), Country.byId(22L), 36L, 37L)),
        L20L(new Locale(20L, Language.byId(20L), Country.byId(23L), 38L, 39L)),
        L21L(new Locale(21L, Language.byId(21L), Country.byId(24L), 40L, 41L)),
        L22L(new Locale(22L, Language.byId(22L), Country.byId(25L), 42L, 43L)),
        L23L(new Locale(23L, Language.byId(23L), Country.byId(26L), 44L, 45L)),
        L24L(new Locale(24L, Language.byId(32L), Country.byId(27L), 46L, 47L)),
        L25L(new Locale(25L, Language.byId(24L), Country.byId(28L), 48L, 49L)),
        L26L(new Locale(26L, Language.byId(25L), Country.byId(6L), 50L, 51L)),
        L27L(new Locale(27L, Language.byId(26L), Country.byId(29L), 52L, 53L)),
        L28L(new Locale(28L, Language.byId(27L), Country.byId(30L), 54L, 55L)),
        L29L(new Locale(29L, Language.byId(28L), Country.byId(31L), 56L, 57L)),
        L30L(new Locale(30L, Language.Enum.EN, Country.Enum.KZ, 58L, 59L)),
        L31L(new Locale(31L, Language.Enum.EN, Country.Enum.LV, 60L, 61L)),
        L32L(new Locale(32L, Language.Enum.EN, Country.Enum.BE, 62L, 63L)),
        L33L(new Locale(33L, Language.Enum.EN, Country.Enum.LU, 64L, 65L)),
        L34L(new Locale(34L, Language.byId(29L), Country.byId(20L), 66L, 67L)),
        L35L(new Locale(35L, Language.byId(30L), Country.byId(36L), 68L, 69L)),
        L36L(new Locale(36L, Language.byId(31L), Country.byId(37L), 70L, 71L)),
        L37L(new Locale(37L, Language.Enum.EN, Country.Enum.EN_CA, 72L, 73L)),
        L38L(new Locale(38L, Language.Enum.EN, Country.Enum.EE, 74L, 75L)),
        L39L(new Locale(39L, Language.Enum.EN, Country.Enum.LT, 76L, 77L)),
        L40L(new Locale(40L, Language.Enum.EN, Country.Enum.DO, 78L, 79L)),
        L41L(new Locale(41L, Language.Enum.EN, Country.Enum.EC, 80L, 81L)),
        L42L(new Locale(42L, Language.Enum.EN, Country.Enum.HK, 82L, 83L)),
        L43L(new Locale(43L, Language.Enum.EN, Country.Enum.IL, 84L, 85L)),
        L44L(new Locale(44L, Language.Enum.EN, Country.Enum.MA, 86L, 87L)),
        L45L(new Locale(45L, Language.Enum.EN, Country.Enum.ZA, 88L, 89L)),
        L46L(new Locale(46L, Language.Enum.EN, Country.Enum.AE, 90L, 91L)),
        L47L(new Locale(47L, Language.Enum.EN, Country.Enum.BH, 92L, 93L)),
        L48L(new Locale(48L, Language.Enum.EN, Country.Enum.KW, 94L, 95L)),
        L49L(new Locale(49L, Language.Enum.EN, Country.Enum.OM, 96L, 97L)),
        L50L(new Locale(50L, Language.Enum.EN, Country.Enum.QA, 98L, 99L)),
        L51L(new Locale(51L, Language.Enum.EN, Country.Enum.NZ, 100L, 101L));

        private final Locale data;

        Enum(Locale locale) {
            data = locale;
        }

        public Long getId() {
            return data.getId();
        }
    }

}
