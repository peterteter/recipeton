package com.recipeton.tmdata.domain.control;

/*-
 * #%L
 * recipeton-tmdata-service
 * %%
 * Copyright (C) 2021 tincomisc
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;
import lombok.experimental.Accessors;

import javax.persistence.*;
import java.io.Serializable;

// Empty
@Entity
@Table(name = "tmControlProgramSoftBlendingSpeedRange")
@Data
@AllArgsConstructor
@NoArgsConstructor
@Accessors(chain = true)
@ToString(onlyExplicitlyIncluded = true)
public class TmControlProgramSoftBlendingSpeedRange implements Serializable {

    private static final long serialVersionUID = 1L;

    @EmbeddedId
    private ID id = new ID();

    @ManyToOne
    @JoinColumn(name = "tmControlProgramSoftBlending_id")
    @MapsId("tmControlProgramSoftBlendingId")
    private TmControlProgramSoftBlending tmControlProgramSoftBlending;

    @ManyToOne
    @JoinColumn(name = "tmControlProgramSoftBlendingSpeedRangeType_id")
    @MapsId("tmControlProgramSoftBlendingSpeedRangeTypeId")
    @ToString.Include
    private TmControlProgramSoftBlendingSpeedRangeType tmControlProgramSoftBlendingSpeedRangeType;

    @ManyToOne
    @JoinColumn(name = "tmControlSpeedType_id", nullable = false)
    @ToString.Include
    private TmControlSpeedType tmControlSpeedType;

    @Data
    public static class ID implements Serializable {
        @Column(name = "tmControlProgramSoftBlending_id", nullable = false)
        private Long tmControlProgramSoftBlendingId;

        @Column(name = "tmControlProgramSoftBlendingSpeedRangeType_id", nullable = false)
        private Long tmControlProgramSoftBlendingSpeedRangeTypeId;
    }

}
