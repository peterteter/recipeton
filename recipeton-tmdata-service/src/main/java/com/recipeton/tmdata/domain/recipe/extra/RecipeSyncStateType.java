package com.recipeton.tmdata.domain.recipe.extra;

/*-
 * #%L
 * recipeton-tmdata-service
 * %%
 * Copyright (C) 2021 tincomisc
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;
import lombok.experimental.Accessors;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.*;
import java.io.Serializable;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@Entity
@Table(name = "recipeSyncStateType")
@Cacheable
@org.hibernate.annotations.Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
@Data
@AllArgsConstructor
@NoArgsConstructor
@Accessors(chain = true)
@ToString(onlyExplicitlyIncluded = true, includeFieldNames = false)
public class RecipeSyncStateType implements Serializable {

    public static final List<RecipeSyncStateType> DEFAULT = Stream.of(RecipeSyncStateType.Enum.values()).map(e -> new RecipeSyncStateType(e.id, e.text)).collect(Collectors.toList());
    private static final long serialVersionUID = 1L;

    @Id
    @Column(name = "id", nullable = false)
    private Long id;

    @Column(name = "type", nullable = false)
    @ToString.Include
    private String type;

    public enum Enum {
        NEW_TOTAL(1L, "nof recipes total add"),
        NEW_CURRENT(2L, "nof recipes current add"),
        CHANGE_TOTAL(3L, "nof recipes total change"),
        CHANGE_CURRENT(4L, "nof recipes current change"),
        DELETE_TOTAL(5L, "nof recipes total delete"),
        DELETE_CURRENT(6L, "nof recipes current delete");

        private final long id;
        private final String text;

        Enum(Long id, String text) {
            this.id = id;
            this.text = text;
        }

        public Long getId() {
            return id;
        }
    }
}
