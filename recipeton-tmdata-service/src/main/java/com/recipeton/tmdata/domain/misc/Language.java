package com.recipeton.tmdata.domain.misc;

/*-
 * #%L
 * recipeton-tmdata-service
 * %%
 * Copyright (C) 2021 tincomisc
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.recipeton.tmdata.domain.Localized;
import com.recipeton.tmdata.domain.misc.lang.LanguageLang;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;
import lombok.experimental.Accessors;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.*;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@Entity
@Table(name = "language")
@Cacheable
@org.hibernate.annotations.Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
@Data
@AllArgsConstructor
@NoArgsConstructor
@Accessors(chain = true)
@ToString(onlyExplicitlyIncluded = true, includeFieldNames = false)
public class Language implements Localized, Serializable {

    public static final List<Language> DEFAULT = Stream.of(Language.Enum.values()).map(e -> new Language(e.id, e.text, new ArrayList<>())).collect(Collectors.toList());

    private static final long serialVersionUID = 1L;

    @Id
    @Column(name = "id", nullable = false)
    private Long id;

    @Column(name = "code", nullable = false)
    @ToString.Include
    private String code;

    @OneToMany(mappedBy = "language", cascade = CascadeType.ALL, orphanRemoval = true)
    private List<LanguageLang> langs;

    public static Language byId(long id) {
        return DEFAULT.stream()
                .filter(v -> v.id == id)
                .findFirst()
                .orElseThrow(() -> new IllegalArgumentException("Unknown Language Id=" + id));
    }

    public enum Enum {
        EN(1L, "en"),
        DE(2L, "de"),
        FR(3L, "fr"),
        ES(4L, "es"),
        IT(5L, "it"),
        PT_PT(6L, "pt_PT"),
        PL(7L, "pl"),
        ES_MX(8L, "es_MX"),
        ZH_TW(9L, "zh_tw"),
        RU(11L, "ru"),
        CS(12L, "cs"),
        EN_AU(13L, "en_AU"),
        EL_CY(14L, "el_CY"),
        NL(15L, "nl"),
        ZH_CN(16L, "zh_cn"),
        EN_MY(17L, "en_my"),
        ZH_MY(18L, "zh_my"),
        DE_AT(19L, "de_AT"),
        KO(20L, "ko"),
        PT_BR(21L, "pt_BR"),
        ES_CL(22L, "es_CL"),
        HU(23L, "hu"),
        PT(24L, "pt"),
        IT_CH(25L, "it_CH"),
        NO(26L, "no"),
        RO(27L, "ro"),
        DK(28L, "dk"),
        MS_MY(29L, "ms_MY"),
        EN_US(30L, "en_US"),
        FR_CA(31L, "fr_CA"),
        PT_AO(32L, "pt_AO");

        private final long id;
        private final String text;

        Enum(long id, String text) {
            this.id = id;
            this.text = text;
        }

        public long getId() {
            return id;
        }
    }
}
