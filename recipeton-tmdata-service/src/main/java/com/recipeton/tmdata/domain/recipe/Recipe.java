package com.recipeton.tmdata.domain.recipe;

/*-
 * #%L
 * recipeton-tmdata-service
 * %%
 * Copyright (C) 2021 tincomisc
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.recipeton.tmdata.domain.Localized;
import com.recipeton.tmdata.domain.misc.Company;
import com.recipeton.tmdata.domain.nutrition.RecipeNutritionalValue;
import com.recipeton.tmdata.domain.recipe.lang.RecipeLang;
import com.recipeton.tmdata.domain.unit.UnitNotation;
import com.recipeton.util.jpa.LocalDateTimeLongSecondsConverter;
import lombok.*;
import lombok.experimental.Accessors;
import lombok.extern.slf4j.Slf4j;

import javax.persistence.*;
import java.io.Serializable;
import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.Collection;
import java.util.List;

import static com.recipeton.shared.util.StreamUtil.toStream;
import static javax.persistence.FetchType.LAZY;

@Slf4j
@Entity
@Table(name = "recipe")
@Data
@AllArgsConstructor
@NoArgsConstructor
@Accessors(chain = true)
@ToString(onlyExplicitlyIncluded = true)
public class Recipe implements Localized, Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", nullable = false)
    @ToString.Include
    private Long id;

    @Column(name = "uid", nullable = false)
    @ToString.Include
    private String uid;

    @Column(name = "originalTitle", nullable = false)
    @ToString.Include
    private String originalTitle;

    @Column(name = "year", nullable = false)
    private Long year;

    @Column(name = "servingQuantity", nullable = false)
    private BigDecimal servingQuantity;

    @ManyToOne(fetch = LAZY)
    @JoinColumn(name = "servingUnitNotation_id")
    private UnitNotation servingUnitNotation;

    @ManyToOne(fetch = LAZY)
    @JoinColumn(name = "recipeDifficulty_id", nullable = false)
    private RecipeDifficulty recipeDifficulty;

    @ManyToOne(fetch = LAZY)
    @JoinColumn(name = "recipePrice_id", nullable = false)
    private RecipePrice recipePrice;

    @Column(name = "version", nullable = false)
    private BigDecimal version;

    @Convert(converter = LocalDateTimeLongSecondsConverter.class)
    @Column(name = "createdts", nullable = false)
    private LocalDateTime created;

    @Convert(converter = LocalDateTimeLongSecondsConverter.class)
    @Column(name = "lastchangedts", nullable = false)
    private LocalDateTime lastChanged;

    @Column(name = "isDeleted", nullable = false)
    private boolean deleted;

    @EqualsAndHashCode.Exclude
    @ManyToMany(cascade = CascadeType.ALL)
    @JoinTable(name = "recipe_recipeCategory", joinColumns = @JoinColumn(name = "recipe_id"), inverseJoinColumns = @JoinColumn(name = "recipeCategory_id"))
    private List<RecipeCategory> recipeCategories;

    @EqualsAndHashCode.Exclude
    @OneToMany(mappedBy = "recipe", cascade = CascadeType.ALL, orphanRemoval = true)
    @OrderBy("position")
//    @OrderColumn(name = "position") Needs to enforce in code. Its not continuous
    private List<RecipeIngredient> recipeIngredients;

    @OneToMany(mappedBy = "recipe")
//    @OneToMany(mappedBy = "recipe", cascade = CascadeType.ALL, orphanRemoval = true)
    @OrderBy("position")
    @EqualsAndHashCode.Exclude
//    @OrderColumn(name = "position") Needs to enforce in code. Its not continuous
    private List<RecipeStep> recipeSteps;

    // Change for O2O
    @OneToOne(mappedBy = "recipe", cascade = CascadeType.ALL, orphanRemoval = true)
    @EqualsAndHashCode.Exclude
    private RecipeNutritionalValue recipeNutritionalValue;

    @OneToMany(mappedBy = "recipe", cascade = CascadeType.ALL, orphanRemoval = true)
    @EqualsAndHashCode.Exclude
    private Collection<RecipeMaterialObject> recipeMaterialObject;

    @OneToMany(mappedBy = "recipe", cascade = CascadeType.ALL, orphanRemoval = true)
    @EqualsAndHashCode.Exclude
    @ToString.Include
    private Collection<RecipeLang> langs;

    @EqualsAndHashCode.Exclude
    @ManyToMany(cascade = CascadeType.ALL)
    @JoinTable(name = "recipe_company",
            joinColumns = @JoinColumn(name = "recipe_id"),
            inverseJoinColumns = @JoinColumn(name = "company_id"))
    private Collection<Company> companies;

    @EqualsAndHashCode.Exclude
    @OneToMany(mappedBy = "recipe", cascade = CascadeType.ALL, orphanRemoval = true)
    private Collection<RecipeUtensil> recipeUtensils;

    @EqualsAndHashCode.Exclude
    @OneToMany(mappedBy = "recipe", cascade = CascadeType.ALL, orphanRemoval = true)
    private Collection<RecipeTmVersion> recipeTmVersions;

    @EqualsAndHashCode.Exclude
    @OneToMany(mappedBy = "recipe", cascade = CascadeType.ALL, orphanRemoval = true)
    private Collection<RecipeTime> recipeTime;

    @EqualsAndHashCode.Exclude
    @ManyToMany(cascade = CascadeType.ALL)
    @JoinTable(name = "recipe_recipeGroup", joinColumns = @JoinColumn(name = "recipe_id"), inverseJoinColumns = @JoinColumn(name = "recipeGroup_id"))
    private List<RecipeGroup> recipeGroups;

    @PrePersist
    public void prePersist() {
        toStream(recipeTmVersions).filter(n -> n.getRecipe() == null).forEach(n -> n.setRecipe(this));
        toStream(recipeSteps).filter(n -> n.getRecipe() == null).forEach(n -> n.setRecipe(this));
        toStream(recipeIngredients).filter(n -> n.getRecipe() == null).forEach(n -> n.setRecipe(this));

        toStream(langs).filter(n -> n.getRecipe() == null).forEach(n -> n.setRecipe(this));
        toStream(recipeTime).filter(n -> n.getRecipe() == null).forEach(n -> n.setRecipe(this));
        toStream(recipeMaterialObject).filter(n -> n.getRecipe() == null).forEach(n -> n.setRecipe(this));
        toStream(recipeNutritionalValue).filter(n -> n.getRecipe() == null).forEach(n -> n.setRecipe(this));
        toStream(recipeUtensils).filter(n -> n.getRecipe() == null).forEach(n -> n.setRecipe(this));
        lastChanged = LocalDateTime.now();
        if (created == null) {
            created = lastChanged;
        }
    }

    @PreUpdate
    public void preUpdate() {
        lastChanged = LocalDateTime.now();
    }

}

