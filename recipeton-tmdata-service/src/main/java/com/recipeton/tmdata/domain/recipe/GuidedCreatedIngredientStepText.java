package com.recipeton.tmdata.domain.recipe;

/*-
 * #%L
 * recipeton-tmdata-service
 * %%
 * Copyright (C) 2021 tincomisc
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.recipeton.tmdata.domain.Localized;
import com.recipeton.tmdata.domain.recipe.lang.GuidedCreatedIngredientStepTextLang;
import lombok.*;
import lombok.experimental.Accessors;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Collection;

import static com.recipeton.shared.util.StreamUtil.toStream;

@Entity
@Table(name = "guidedCreatedIngredientStepText")
@Data
@AllArgsConstructor
@NoArgsConstructor
@Accessors(chain = true)
@ToString(onlyExplicitlyIncluded = true)
public class GuidedCreatedIngredientStepText implements Localized, Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", nullable = false)
    private Long id;

    @ManyToOne
    @JoinColumn(name = "guidedCreatedIngredientStepTextAttribute_id", nullable = false)
    @ToString.Include
    private GuidedCreatedIngredientStepTextAttribute attribute;

    @Column(name = "isPredefined", nullable = false)
    private boolean predefined;

    @OneToMany(mappedBy = "guidedCreatedIngredientStepText", cascade = CascadeType.ALL, orphanRemoval = true)
    @EqualsAndHashCode.Exclude
    @ToString.Include
    private Collection<GuidedCreatedIngredientStepTextLang> langs;

    @PrePersist
    public void prePersist() {
        toStream(langs).filter(n -> n.getGuidedCreatedIngredientStepText() == null).forEach(n -> n.setGuidedCreatedIngredientStepText(this));
    }
}
