package com.recipeton.tmdata.domain.recipe.extra;

/*-
 * #%L
 * recipeton-tmdata-service
 * %%
 * Copyright (C) 2021 tincomisc
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.recipeton.tmdata.domain.recipe.RecipeStep;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

import javax.persistence.*;
import java.io.Serializable;

// Empty
@Entity
@Table(name = "recipePathEntry")
@Data
@AllArgsConstructor
@NoArgsConstructor
@Accessors(chain = true)
public class RecipePathEntry implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @Column(name = "id", nullable = false)
    private Long id;

    @ManyToOne
    @JoinColumn(name = "recipePath_id", nullable = false)
    private RecipePath recipePath;

    @ManyToOne
    @JoinColumn(name = "recipeStep_id", nullable = false)
    private RecipeStep recipeStep;

    @Column(name = "position", nullable = false)
    private Long position;

    @Column(name = "displayNo", nullable = false)
    private Long displayNo;

    @Column(name = "isForceDisplay", nullable = false)
    private boolean forceDisplay;

}
