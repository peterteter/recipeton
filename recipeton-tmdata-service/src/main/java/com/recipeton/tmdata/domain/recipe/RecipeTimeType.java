package com.recipeton.tmdata.domain.recipe;

/*-
 * #%L
 * recipeton-tmdata-service
 * %%
 * Copyright (C) 2021 tincomisc
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.recipeton.tmdata.domain.recipe.lang.RecipeTimeTypeLang;
import lombok.*;
import lombok.experimental.Accessors;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.*;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@Entity
@Table(name = "recipeTimeType")
@Cacheable
@org.hibernate.annotations.Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
@Data
@AllArgsConstructor
@NoArgsConstructor
@Accessors(chain = true)
@ToString(onlyExplicitlyIncluded = true, includeFieldNames = false)
public class RecipeTimeType implements Serializable {

    public static final List<RecipeTimeType> DEFAULT = Stream.of(RecipeTimeType.Enum.values()).map(e -> new RecipeTimeType(e.id, e.text, new ArrayList<>())).collect(Collectors.toList());
    private static final long serialVersionUID = 1L;

    @Id
    @Column(name = "id", nullable = false)
    private Long id;

    @Column(name = "type", nullable = false)
    @ToString.Include
    private String type;

    @OneToMany(mappedBy = "recipeTimeType", cascade = CascadeType.ALL, orphanRemoval = true)
    @EqualsAndHashCode.Exclude
    private Collection<RecipeTimeTypeLang> langs;

    public enum Enum {
        PREP(1L, "timeprep"),
        TOTAL(2L, "timetotal"),
        WAIT(3L, "timewaiting"),
        BAKE(4L, "timebaking"),
        TM(5L, "timetm");

        private final long id;
        private final String text;

        Enum(Long id, String text) {
            this.id = id;
            this.text = text;
        }

        public Long getId() {
            return id;
        }
    }

}
