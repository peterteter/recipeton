package com.recipeton.tmdata.domain.recipe;

/*-
 * #%L
 * recipeton-tmdata-service
 * %%
 * Copyright (C) 2021 tincomisc
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */


import com.recipeton.shared.analysis.domain.RecipeAnalysisTag;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;
import lombok.experimental.Accessors;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.*;
import java.io.Serializable;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@Entity
@Table(name = "guidedUtensilActionType")
@Cacheable
@org.hibernate.annotations.Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
@Data
@AllArgsConstructor
@NoArgsConstructor
@Accessors(chain = true)
@ToString(onlyExplicitlyIncluded = true, includeFieldNames = false)
public class GuidedUtensilActionType implements Serializable {

    public static final List<GuidedUtensilActionType> DEFAULT = Stream.of(GuidedUtensilActionType.Enum.values()).map(e -> new GuidedUtensilActionType(e.id, e.text)).collect(Collectors.toList());
    private static final long serialVersionUID = 1L;

    @Id
    @Column(name = "id", nullable = false)
    private Long id;

    @Column(name = "value", nullable = false)
    @ToString.Include
    private String value;

    public enum Enum {
        SIMMERING_BASKET_PUT(1L, "insert simmering basket", RecipeAnalysisTag.SIMMERING_BASKET_PUT_INSIDE, "utensil_action_insert_simmering_basket.png"),
        SIMMERING_BASKET_REMOVE(2L, "remove simmering basket", RecipeAnalysisTag.SIMMERING_BASKET_REMOVE, "utensil_action_remove_simmering_basket.png"),
        STEAMER_TRAY_PUT(3L, "insert varoma tray", RecipeAnalysisTag.STEAMER_TRAY_PUT, "utensil_action_insert_varoma_tray.png"),
        STEAMER_TRAY_REMOVE(4L, "remove varoma tray", RecipeAnalysisTag.STEAMER_TRAY_REMOVE, "utensil_action_remove_varoma_tray.png"),
        STEAMER_PUT(5L, "place varoma into position", RecipeAnalysisTag.STEAMER_PUT, "utensil_action_place_varoma_into_position.png"),
        STEAMER_REMOVE(6L, "set varoma aside", RecipeAnalysisTag.STEAMER_REMOVE, "utensil_action_set_varoma_aside.png"),
        SIMMERING_BASKET_AS_LID_PUT(7L, "place simmering basket instead of measuring cup onto lid", RecipeAnalysisTag.SIMMERING_BASKET_AS_LID_PUT, "utensil_action_place_simmering_basket_instead_of_measuring_cup_onto_lid.png"),
        SIMMERING_BASKET_REMOVE_MEASURING_CUP_PUT(8L, "remove simmering basket insert measuring cup", RecipeAnalysisTag.SIMMERING_BASKET_REMOVE_MEASURING_CUP_PUT, "utensil_action_remove_simmering_basket_insert_measuring_cup.png"),
        MIXING_BOWL_REMOVE(9L, "remove mixing bowl", RecipeAnalysisTag.MIXING_BOWL_REMOVE, "utensil_action_remove_mixing_bowl.png"),
        MIXING_BOWL_PUT(10L, "place mixing bowl into position", RecipeAnalysisTag.MIXING_BOWL_PUT, "utensil_action_place_mixing_bowl_into_position.png"),
        TRANSFER_TO_BOWL(11L, "transfer into a bowl and set aside", RecipeAnalysisTag.TRANSFER_TO_BOWL, "utensil_action_transfer_into_a_bowl_and_set_aside.png"),
        MIXING_BOWL_EMPTY(12L, "empty mixing bowl", RecipeAnalysisTag.MIXING_BOWL_EMPTY, "utensil_action_empty_mixing_bowl.png"),
        BUTTERFLY_PUT(13L, "insert butterfly whisk", RecipeAnalysisTag.WHISK_PUT, "utensil_action_insert_butterfly_whisk.png"),
        BUTTERFLY_REMOVE(14L, "remove butterfly whisk", RecipeAnalysisTag.WHISK_REMOVE, "utensil_action_remove_butterfly_whisk.png"),
        MIXING_BOWL_CLEAN(15L, "clean mixing bowl", RecipeAnalysisTag.MIXING_BOWL_CLEAN, "utensil_action_clean_mixing_bowl.png"),

        MEASURING_CUP_NOTUSE(16L, "without measuring cup", RecipeAnalysisTag.MEASURING_CUP_NOTUSE, "utensil_action_without_measuring_cup.png"),
        MEASURING_CUP_REMOVE(17L, "remove measuring cup", RecipeAnalysisTag.MEASURING_CUP_REMOVE, "utensil_action_remove_measuring_cup.png"),
        MEASURING_CUP_PUT(18L, "insert measuring cup", RecipeAnalysisTag.MEASURING_CUP_PUT, "utensil_action_insert_measuring_cup.png"),

        SPATULA_MIX(19L, "mix with aid of spatula", RecipeAnalysisTag.SPATULA_MIX, "utensil_action_mix_with_aid_of_spatula.png"),
        SPATULA_SCRAP_SIDE(20L, "scrape down sides of mixing bowl with spatula", RecipeAnalysisTag.SPATULA_SCRAP_SIDE, "utensil_action_scrape_down_sides_of_mixing_bowl_with_spatula.png"),
        MEASURING_CUP_HOLD_VIBRATION(21L, "hold measuring cup to prevent it from vibrating", RecipeAnalysisTag.MEASURING_CUP_HOLD_VIBRATION, "utensil_action_hold_measuring_cup_to_prevent_it_from_vibrating.png"),
        OVEN_PREHEAT(22L, "preheat oven", RecipeAnalysisTag.OVEN_OPERATION, "utensil_action_preheat_oven.png"),
        REFRIGERATE(23L, "refrigerate", RecipeAnalysisTag.REFRIGERATE, "utensil_action_refrigerate.png"),
        FREEZE(24L, "freeze", RecipeAnalysisTag.FREEZE, "utensil_action_freeze.png"),
        STEAMER(25L, "varoma", RecipeAnalysisTag.STEAMER, "utensil_action_varoma.png"),
        STEAMER_DISH_REMOVE(26L, "take varoma dish", RecipeAnalysisTag.STEAMER_DISH_REMOVE, "utensil_action_take_varoma_dish.png"),
        SIMMERING_BASKET_AS_LID_REMOVE(27L, "remove simmering basket from mixing bowl lid", RecipeAnalysisTag.SIMMERING_BASKET_AS_LID_REMOVE, "utensil_action_remove_simmering_basket_from_mixing_bowl_lid.png"),
        SUMMERING_BASKET(28L, "simmering basket", RecipeAnalysisTag.SUMMERING_BASKET, "utensil_action_simmering_basket.png"),
        BUTTERFLY(29L, "butterfly whisk", RecipeAnalysisTag.WHISK, "utensil_action_butterfly_whisk.png"),
        MEASURING_CUP(30L, "measuring cup", RecipeAnalysisTag.MEASURING_CUP, "utensil_action_measuring_cup.png"),
        SPATULA(31L, "spatula", RecipeAnalysisTag.SPATULA, "utensil_action_spatula.png"),
        SPATULA_MIX_WELL(32L, "mix well with spatula", RecipeAnalysisTag.SPATULA_MIX_WELL, "utensil_action_mix_well_with_spatula.png"),
        MIXING_BOWL(33L, "mixing bowl", RecipeAnalysisTag.MIXING_BOWL, "utensil_action_mixing_bowl.png"),
        THERMOMIX_PARTS(34L, "thermomix parts", RecipeAnalysisTag.OTHER_OPERATION, "utensil_action_thermomix_parts.png"),
        USEFUL_ITEMS(35L, "useful items", RecipeAnalysisTag.USEFUL_ITEMS, "utensil_action_useful_items.png"),
        KITCHEN_EQUIPMENT(36L, "kitchen equipment", RecipeAnalysisTag.KITCHEN_EQUIPMENT, "utensil_action_kitchen_equipment.png"),
        STEAMER_DISH_PUT(37L, "place varoma dish into position", RecipeAnalysisTag.STEAMER_DISH_PUT, "utensil_action_place_varoma_dish_into_position.png");

        private final long id;
        private final String text;
        private final RecipeAnalysisTag recipeAnalysisTag;
        private final String defaultIcon;

        Enum(Long id, String text, RecipeAnalysisTag recipeAnalysisTag, String defaultIcon) {
            this.id = id;
            this.text = text;
            this.recipeAnalysisTag = recipeAnalysisTag;
            this.defaultIcon = defaultIcon;
        }

        public static Enum by(RecipeAnalysisTag recipeAnalysisTag) {
            if (recipeAnalysisTag == null) {
                return null;
            }
            return Stream.of(values()).filter(v -> v.recipeAnalysisTag == recipeAnalysisTag).findFirst().orElseThrow(() -> new IllegalArgumentException("No valid action found with value=" + recipeAnalysisTag + "."));
        }

        public String getDefaultIcon() {
            return defaultIcon;
        }

        public Long getId() {
            return id;
        }

        public String getText() {
            return text;
        }
    }


}
