package com.recipeton.tmdata.domain.recipe;

/*-
 * #%L
 * recipeton-tmdata-service
 * %%
 * Copyright (C) 2021 tincomisc
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.recipeton.tmdata.domain.misc.RangeType;
import lombok.Data;
import lombok.experimental.Accessors;

import javax.persistence.*;
import java.io.Serializable;
import java.math.BigDecimal;

@Entity
@Data
@Table(name = "recipeStep_recipeIngredientRange")
@Accessors(chain = true)
public class RecipeStepRecipeIngredientRange implements Serializable {

    private static final long serialVersionUID = 1L;

    @EmbeddedId
    private ID id = new ID();

    @ManyToOne(optional = false)
    @MapsId("recipeStepRecipeIngredientId")
    @JoinColumns({
            @JoinColumn(name = "recipeStep_id", referencedColumnName = "recipeStep_id"),
            @JoinColumn(name = "recipeIngredient_id", referencedColumnName = "recipeIngredient_id"),
    })
    private RecipeStepRecipeIngredient recipeStepRecipeIngredient;

    @ManyToOne
    @JoinColumn(name = "rangeType_id")
    @MapsId("rangeTypeId")
    private RangeType rangeType;

    @Column(name = "ingredientAmountValue", nullable = false)
    private BigDecimal ingredientAmountValue;

    @Data
    public static class ID implements Serializable {
        private RecipeStepRecipeIngredient.ID recipeStepRecipeIngredientId;

        @Column(name = "rangeType_id", nullable = false)
        private Long rangeTypeId;
    }
}
