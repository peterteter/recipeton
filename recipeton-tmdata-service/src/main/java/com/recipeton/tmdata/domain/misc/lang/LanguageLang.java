package com.recipeton.tmdata.domain.misc.lang;

/*-
 * #%L
 * recipeton-tmdata-service
 * %%
 * Copyright (C) 2021 tincomisc
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.recipeton.tmdata.domain.Localization;
import com.recipeton.tmdata.domain.misc.Language;
import com.recipeton.tmdata.domain.misc.Locale;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;
import lombok.experimental.Accessors;

import javax.persistence.*;
import java.io.Serializable;

import static javax.persistence.FetchType.LAZY;

@Entity
@Table(name = "languageLang")
@Data
@AllArgsConstructor
@NoArgsConstructor
@Accessors(chain = true)
@ToString(onlyExplicitlyIncluded = true, includeFieldNames = false)
public class LanguageLang implements Localization<LanguageLang>, Serializable {

    private static final long serialVersionUID = 1L;

    @EmbeddedId
    private ID id = new ID();

    @ManyToOne
    @JoinColumn(name = "language_id")
    @MapsId("languageId")
    private Language language;
    @ManyToOne(fetch = LAZY)
    @JoinColumn(name = "locale_id")
    @MapsId("localeId")
    @ToString.Include
    private Locale locale;

    @Column(name = "text", nullable = false)
    @ToString.Include
    private String text;

    @Column(name = "sortKey", nullable = false)
    private String sortKey;

    @Override
    @PrePersist
    public void prePersist() {
        setSortKey(toSortKey());
    }

    @Data
    public static class ID implements Serializable {

        @Column(name = "language_id", nullable = false)
        private Long languageId;

        @Column(name = "locale_id", nullable = false)
        private Long localeId;
    }
}
