package com.recipeton.tmdata.domain.ingredient;

/*-
 * #%L
 * recipeton-tmdata-service
 * %%
 * Copyright (C) 2021 tincomisc
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.recipeton.tmdata.domain.Localized;
import com.recipeton.tmdata.domain.ingredient.lang.ShoppingCategoryLang;
import com.recipeton.util.jpa.IdentityFallbackGenerator;
import lombok.*;
import lombok.experimental.Accessors;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Collection;

import static com.recipeton.shared.util.StreamUtil.toStream;

@Entity
@Table(name = "shoppingCategory")
@Cacheable
@org.hibernate.annotations.Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
@Data
@AllArgsConstructor
@NoArgsConstructor
@Accessors(chain = true)
@ToString(onlyExplicitlyIncluded = true)
public class ShoppingCategory implements Localized, Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GenericGenerator(name = IdentityFallbackGenerator.NAME, strategy = IdentityFallbackGenerator.ID)
    @GeneratedValue(generator = IdentityFallbackGenerator.NAME)
    @Column(name = "id", nullable = false)
    @ToString.Include
    private Long id;

    @Column(name = "position", nullable = false)
    private Long position;

    @EqualsAndHashCode.Exclude
    @OneToMany(mappedBy = "shoppingCategory", cascade = CascadeType.ALL, orphanRemoval = true)
//    @ToString.Include
    private Collection<ShoppingCategoryLang> langs;

    @PrePersist
    public void prePersist() {
        toStream(langs).filter(n -> n.getShoppingCategory() == null).forEach(n -> n.setShoppingCategory(this));
    }

}
