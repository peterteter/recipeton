package com.recipeton.tmdata.domain.material;

/*-
 * #%L
 * recipeton-tmdata-service
 * %%
 * Copyright (C) 2021 tincomisc
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.recipeton.tmdata.domain.material.lang.MaterialObjectLang;
import lombok.*;
import lombok.experimental.Accessors;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Collection;

@Entity
@Table(name = "materialObject")
@Cacheable
@org.hibernate.annotations.Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
@Data
@AllArgsConstructor
@NoArgsConstructor
@Accessors(chain = true)
@ToString(onlyExplicitlyIncluded = true)
public class MaterialObject implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", nullable = false)
    private Long id;

    @ManyToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "material_id", nullable = false)
    @ToString.Include
    private Material material;

    @EqualsAndHashCode.Exclude
    @ManyToMany
    @JoinTable(name = "materialObject_materialObjectFlag",
            joinColumns = @JoinColumn(name = "materialObject_id"),
            inverseJoinColumns = @JoinColumn(name = "materialObjectFlag_id"))
    @ToString.Include
    private Collection<MaterialObjectFlag> materialObjectFlags;

    @EqualsAndHashCode.Exclude
    @OneToMany(mappedBy = "materialObject", cascade = CascadeType.ALL, orphanRemoval = true)
    private Collection<MaterialValue> materialValues;

    @EqualsAndHashCode.Exclude
    @OneToMany(mappedBy = "materialObject", cascade = CascadeType.ALL, orphanRemoval = true)
    private Collection<MaterialObjectLang> materialObjectLangs;

}
