package com.recipeton.tmdata.domain.nutrition;

/*-
 * #%L
 * recipeton-tmdata-service
 * %%
 * Copyright (C) 2021 tincomisc
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.recipeton.tmdata.domain.unit.UnitNotation;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;
import lombok.experimental.Accessors;

import javax.persistence.*;
import java.io.Serializable;
import java.math.BigDecimal;

@Table(name = "recipeNutritionalValueData")
@Entity
@Data
@AllArgsConstructor
@NoArgsConstructor
@Accessors(chain = true)
@ToString(onlyExplicitlyIncluded = true)
public class RecipeNutritionalValueData implements Serializable {

    private static final long serialVersionUID = 1L;

    @EmbeddedId
    private ID id = new ID();

    @ManyToOne
    @JoinColumn(name = "recipeNutritionalValue_id")
    @MapsId("recipeNutritionalValueId")
    private RecipeNutritionalValue recipeNutritionalValue;

    @ManyToOne
    @JoinColumn(name = "recipeNutritionalValueType_id")
    @MapsId("recipeNutritionalValueTypeId")
    @ToString.Include
    private RecipeNutritionalValueType recipeNutritionalValueType;


    @Column(name = "nutritionalValue", nullable = false)
    @ToString.Include
    private BigDecimal nutritionalValue;

    @ManyToOne
    @JoinColumn(name = "unitNotation_id", nullable = false)
    @ToString.Include
    private UnitNotation unitNotation;

    @Data
    public static class ID implements Serializable {
        @Column(name = "recipeNutritionalValue_id", nullable = false)
        private Long recipeNutritionalValueId;

        @Column(name = "recipeNutritionalValueType_id", nullable = false)
        private Long recipeNutritionalValueTypeId;
    }

}
