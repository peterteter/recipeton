package com.recipeton.tmdata.domain.recipe;

/*-
 * #%L
 * recipeton-tmdata-service
 * %%
 * Copyright (C) 2021 tincomisc
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.recipeton.tmdata.domain.Localized;
import com.recipeton.tmdata.domain.control.TmControl;
import com.recipeton.tmdata.domain.recipe.lang.GuidedTmSettingStepLang;
import lombok.*;
import lombok.experimental.Accessors;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Collection;

import static com.recipeton.shared.util.StreamUtil.toStream;

@Entity
@Table(name = "guidedTmSettingStep")
@Data
@AllArgsConstructor
@NoArgsConstructor
@Accessors(chain = true)
@ToString(onlyExplicitlyIncluded = true)
public class GuidedTmSettingStep implements Localized, Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @Column(name = "guidedStep_id", nullable = false)
    private Long guidedStepId;

    @OneToOne
    @JoinColumn(name = "guidedStep_id")
    @MapsId
    @EqualsAndHashCode.Exclude
    private GuidedStep guidedStep;

    @ManyToOne
    @JoinColumn(name = "recipeStep_id", nullable = false)
    private RecipeStep recipeStep;

    @ManyToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "tmControl_id", nullable = false)
    @ToString.Include
    private TmControl tmControl;

    @OneToMany(mappedBy = "guidedTmSettingStep", cascade = CascadeType.ALL, orphanRemoval = true)
    @ToString.Include
    @EqualsAndHashCode.Include
    private Collection<GuidedTmSettingStepLang> langs;

    @PrePersist
    public void prePersist() {
        toStream(langs).filter(e -> e.getGuidedTmSettingStep() == null).forEach(e -> e.setGuidedTmSettingStep(this));
    }


}
