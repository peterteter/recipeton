package com.recipeton.tmdata.domain.ingredient;

/*-
 * #%L
 * recipeton-tmdata-service
 * %%
 * Copyright (C) 2021 tincomisc
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.recipeton.tmdata.domain.Localized;
import com.recipeton.tmdata.domain.ingredient.lang.IngredientNotationLang;
import com.recipeton.tmdata.domain.ingredient.lang.IngredientNotationLangAttribute;
import com.recipeton.tmdata.domain.misc.Locale;
import lombok.*;
import lombok.experimental.Accessors;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Collection;

import static com.recipeton.shared.util.StreamUtil.toStream;
import static javax.persistence.FetchType.LAZY;

@Entity
@Table(name = "ingredientNotation")
@Cacheable
@org.hibernate.annotations.Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
@Data
@AllArgsConstructor
@NoArgsConstructor
@Accessors(chain = true)
@ToString(onlyExplicitlyIncluded = true)
public class IngredientNotation implements Localized, Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", nullable = false)
    private Long id;

    @ManyToOne
    @JoinColumn(name = "ingredient_id", nullable = false)
    private Ingredient ingredient;

    @ManyToOne(fetch = LAZY)
    @JoinColumn(name = "ingredientNotationPriority_id", nullable = false)
    @ToString.Include
    private IngredientNotationPriority ingredientNotationPriority;

    @EqualsAndHashCode.Exclude
    @ToString.Include
    @OneToMany(mappedBy = "ingredientNotation", cascade = CascadeType.ALL, orphanRemoval = true)
    private Collection<IngredientNotationLang> langs;

    public String getLang(IngredientNotationLangAttribute.Enum attribute, Locale locale) {
        return getLangs().stream()
                .filter(l -> locale.equals(l.getLocale()))
                .filter(l -> attribute.getId() == l.getAttribute().getId())
                .map(IngredientNotationLang::getText)
                .findFirst()
                .orElse(null);
    }

    @PrePersist
    public void prePersist() {
        toStream(langs).filter(n -> n.getIngredientNotation() == null).forEach(n -> n.setIngredientNotation(this));
    }
}
