package com.recipeton.tmdata.domain.recipe;

/*-
 * #%L
 * recipeton-tmdata-service
 * %%
 * Copyright (C) 2021 tincomisc
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.recipeton.tmdata.domain.recipe.lang.GuidedFreetextStepLang;
import lombok.*;
import lombok.experimental.Accessors;

import javax.persistence.*;
import java.io.Serializable;
import java.util.List;

import static com.recipeton.shared.util.StreamUtil.toStream;

@Entity
@Table(name = "guidedFreetextStep")
@Data
@AllArgsConstructor
@NoArgsConstructor
@Accessors(chain = true)
@ToString(onlyExplicitlyIncluded = true)
public class GuidedFreetextStep implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @Column(name = "guidedStep_id", nullable = false)
    private Long guidedStepId;

    @OneToOne
    @JoinColumn(name = "guidedStep_id")
    @MapsId
    @EqualsAndHashCode.Exclude
    private GuidedStep guidedStep;

    @ManyToOne
    @JoinColumn(name = "guidedFreetextType_id", nullable = false)
    private GuidedFreetextType guidedFreetextType;

    @OneToMany(mappedBy = "guidedFreetextStep", cascade = CascadeType.ALL, orphanRemoval = true)
    @ToString.Include
    @EqualsAndHashCode.Exclude
    private List<GuidedFreetextStepLang> langs;

    @PrePersist
    public void prePersist() {
        toStream(langs).filter(n -> n.getGuidedFreetextStep() == null).forEach(n -> n.setGuidedFreetextStep(this));
    }

}
