package com.recipeton.tmdata.domain.ingredient.lang;

/*-
 * #%L
 * recipeton-tmdata-service
 * %%
 * Copyright (C) 2021 tincomisc
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.recipeton.tmdata.domain.Localization;
import com.recipeton.tmdata.domain.ingredient.ShoppingCategory;
import com.recipeton.tmdata.domain.misc.Locale;
import lombok.Data;
import lombok.ToString;
import lombok.experimental.Accessors;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.*;
import java.io.Serializable;

import static javax.persistence.FetchType.LAZY;

@Entity
@Table(name = "shoppingCategoryLang")
@Cacheable
@org.hibernate.annotations.Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
@Data
@Accessors(chain = true)
@ToString(onlyExplicitlyIncluded = true, includeFieldNames = false)
public class ShoppingCategoryLang implements Localization<ShoppingCategoryLang>, Serializable {

    private static final long serialVersionUID = 1L;

    @EmbeddedId
    private ID id = new ID();

    @ManyToOne
    @JoinColumn(name = "shoppingCategory_id")
    @MapsId("shoppingCategoryId")
    private ShoppingCategory shoppingCategory;

    @ManyToOne(fetch = LAZY)
    @JoinColumn(name = "shoppingCategoryLangAttribute_id")
    @MapsId("shoppingCategoryLangAttributeId")
    @ToString.Include
    private ShoppingCategoryLangAttribute attribute;

    @ManyToOne(fetch = LAZY)
    @JoinColumn(name = "locale_id")
    @MapsId("localeId")
    @ToString.Include
    private Locale locale;

    @Column(name = "text", nullable = false)
    @ToString.Include
    private String text;

    @Column(name = "sortKey", nullable = false)
    private String sortKey;

    @Override
    @PrePersist
    public void prePersist() {
        setSortKey(toSortKey());
    }

    @Data
    public static class ID implements Serializable {
        @Column(name = "shoppingCategory_id", nullable = false)
        private Long shoppingCategoryId;

        @Column(name = "shoppingCategoryLangAttribute_id", nullable = false)
        private Long shoppingCategoryLangAttributeId;

        @Column(name = "locale_id", nullable = false)
        private Long localeId;
    }

}
