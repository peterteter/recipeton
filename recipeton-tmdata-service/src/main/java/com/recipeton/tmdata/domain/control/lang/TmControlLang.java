package com.recipeton.tmdata.domain.control.lang;

/*-
 * #%L
 * recipeton-tmdata-service
 * %%
 * Copyright (C) 2021 tincomisc
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.recipeton.tmdata.domain.Localization;
import com.recipeton.tmdata.domain.control.TmControl;
import com.recipeton.tmdata.domain.misc.Locale;
import lombok.Data;
import lombok.ToString;
import lombok.experimental.Accessors;

import javax.persistence.*;
import java.io.Serializable;

import static javax.persistence.FetchType.LAZY;

@Entity
@Data
@Table(name = "tmControlLang")
@Accessors(chain = true)
@ToString(onlyExplicitlyIncluded = true, includeFieldNames = false)
public class TmControlLang implements Localization<TmControlLang>, Serializable {

    @EmbeddedId
    private ID id = new ID();

    @ManyToOne
    @JoinColumn(name = "tmControl_id")
    @MapsId("tmControlId")
    private TmControl tmControl;

    @ManyToOne
    @JoinColumn(name = "tmControlLangAttribute_id")
    @MapsId("tmControlLangAttributeId")
    @ToString.Include
    private TmControlLangAttribute attribute;
    @ManyToOne(fetch = LAZY)
    @JoinColumn(name = "locale_id")
    @MapsId("localeId")
    @ToString.Include
    private Locale locale;

    @Column(name = "text", nullable = false)
    @ToString.Include
    private String text;

    @Column(name = "sortKey", nullable = false)
    private String sortKey;

    @Override
    @PrePersist
    public void prePersist() {
        setSortKey(toSortKey());
    }

    @Data
    public static class ID implements Serializable {
        @Column(name = "tmControl_id", nullable = false)
        private Long tmControlId;

        @Column(name = "tmControlLangAttribute_id", nullable = false)
        private Long tmControlLangAttributeId;

        @Column(name = "locale_id", nullable = false)
        private Long localeId;
    }

}
