package com.recipeton.tmdata.domain.recipe;

/*-
 * #%L
 * recipeton-tmdata-service
 * %%
 * Copyright (C) 2021 tincomisc
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import lombok.*;
import lombok.experimental.Accessors;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Collection;

import static com.recipeton.shared.util.StreamUtil.toStream;

@Entity
@Table(name = "guidedStep")
@Data
@AllArgsConstructor
@NoArgsConstructor
@Accessors(chain = true)
@ToString(onlyExplicitlyIncluded = true)
public class GuidedStep implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", nullable = false)
    private Long id;

    @ManyToOne
    @JoinColumn(name = "recipeStep_id", nullable = false)
    private RecipeStep recipeStep;

    @Column(name = "position", nullable = false)
    @ToString.Include
    private Long position;

    @ManyToOne
    @JoinColumn(name = "guidedStepType_id", nullable = false)
    @ToString.Include
    private GuidedStepType guidedStepType;

    @OneToMany(mappedBy = "guidedStep", cascade = CascadeType.ALL, orphanRemoval = true)
    @EqualsAndHashCode.Exclude
    private Collection<GuidedStepMaterialObject> guidedStepMaterialObject;

    @OneToOne(mappedBy = "guidedStep", cascade = CascadeType.ALL, orphanRemoval = true)
    @PrimaryKeyJoinColumn
    private GuidedIngredientStep guidedIngredientStep;

    @OneToOne(mappedBy = "guidedStep", cascade = CascadeType.ALL, orphanRemoval = true)
    @PrimaryKeyJoinColumn
    private GuidedCreatedIngredientStep guidedCreatedIngredientStep;

    @OneToOne(mappedBy = "guidedStep", cascade = CascadeType.ALL, orphanRemoval = true)
    @PrimaryKeyJoinColumn
    private GuidedFreetextStep guidedFreetextStep;

    @OneToOne(mappedBy = "guidedStep", cascade = CascadeType.ALL, orphanRemoval = true)
    @PrimaryKeyJoinColumn
    private GuidedTmSettingStep guidedTmSettingStep;

    @OneToOne(mappedBy = "guidedStep", cascade = CascadeType.ALL, orphanRemoval = true)
    @PrimaryKeyJoinColumn
    private GuidedUtensilStep guidedUtensilStep;

    @PrePersist
    public void prePersist() {
        toStream(guidedStepMaterialObject).filter(n -> n.getGuidedStep() == null).forEach(n -> n.setGuidedStep(this));

        if (guidedCreatedIngredientStep != null) {
            guidedCreatedIngredientStep.setGuidedStep(this);
        }
        if (guidedFreetextStep != null) {
            guidedFreetextStep.setGuidedStep(this);
        }
        if (guidedIngredientStep != null) {
            guidedIngredientStep.setGuidedStep(this);
        }
        if (guidedTmSettingStep != null) {
            guidedTmSettingStep.setGuidedStep(this);
        }
        if (guidedUtensilStep != null) {
            guidedUtensilStep.setGuidedStep(this);
        }
    }
}
