package com.recipeton.tmdata.domain.recipe;

/*-
 * #%L
 * recipeton-tmdata-service
 * %%
 * Copyright (C) 2021 tincomisc
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.recipeton.tmdata.domain.Localized;
import com.recipeton.tmdata.domain.recipe.lang.RecipeTimeLang;
import lombok.*;
import lombok.experimental.Accessors;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Collection;

import static com.recipeton.shared.util.StreamUtil.toStream;

@Entity
@Table(name = "recipeTime")
@Data
@AllArgsConstructor
@NoArgsConstructor
@Accessors(chain = true)
@ToString(onlyExplicitlyIncluded = true)
public class RecipeTime implements Localized, Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", nullable = false)
    private Long id;

    @ManyToOne
    @JoinColumn(name = "recipe_id", nullable = false)
    private Recipe recipe;

    @ManyToOne
    @JoinColumn(name = "recipeTimeType_id", nullable = false)
    @ToString.Include
    private RecipeTimeType recipeTimeType;

    @EqualsAndHashCode.Exclude
    @ToString.Include
    @OneToMany(mappedBy = "recipeTime", cascade = CascadeType.ALL, orphanRemoval = true)
    private Collection<RecipeTimeRange> recipeTimeRanges;

    @EqualsAndHashCode.Exclude
    @ToString.Include
    @OneToMany(mappedBy = "recipeTime", cascade = CascadeType.ALL, orphanRemoval = true)
    private Collection<RecipeTimeLang> langs;

    @PrePersist
    public void prePersist() {
        toStream(langs).filter(n -> n.getRecipeTime() == null).forEach(n -> n.setRecipeTime(this));
        toStream(recipeTimeRanges).filter(n -> n.getRecipeTime() == null).forEach(n -> n.setRecipeTime(this));
    }

}
