package com.recipeton.tmdata.domain.recipe;

/*-
 * #%L
 * recipeton-tmdata-service
 * %%
 * Copyright (C) 2021 tincomisc
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.recipeton.shared.analysis.domain.RecipeDifficultyDefinition;
import com.recipeton.tmdata.domain.Localized;
import com.recipeton.tmdata.domain.recipe.lang.RecipeDifficultyLang;
import lombok.*;
import lombok.experimental.Accessors;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.*;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@Entity
@Table(name = "recipeDifficulty")
@Cacheable
@org.hibernate.annotations.Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
@Data
@AllArgsConstructor
@NoArgsConstructor
@ToString(onlyExplicitlyIncluded = true, includeFieldNames = false)
@Accessors(chain = true)
public class RecipeDifficulty implements Localized, Serializable {

    public static final List<RecipeDifficulty> DEFAULT = Stream.of(RecipeDifficulty.Enum.values()).map(e -> new RecipeDifficulty(e.id, e.text, new ArrayList<>())).collect(Collectors.toList());

    private static final long serialVersionUID = 1L;
    @Id
    @Column(name = "id", nullable = false)
    private Long id;

    @Column(name = "difficulty", nullable = false)
    @ToString.Include
    private String difficulty;

    @EqualsAndHashCode.Exclude
    @OneToMany(mappedBy = "recipeDifficulty", cascade = CascadeType.ALL, orphanRemoval = true)
    private Collection<RecipeDifficultyLang> langs;

    public enum Enum {
        EASY(1L, "easy", RecipeDifficultyDefinition.EASY),
        MEDIUM(2L, "medium", RecipeDifficultyDefinition.MEDIUM),
        ADVANCED(3L, "advanced", RecipeDifficultyDefinition.ADVANCED),
        UNKNOWN(4L, "no information", RecipeDifficultyDefinition.UNKNOWN);

        private final long id;
        private final String text;
        private final RecipeDifficultyDefinition recipeDifficultyDefinition;

        Enum(Long id, String text, RecipeDifficultyDefinition recipeDifficultyDefinition) {
            this.id = id;
            this.text = text;
            this.recipeDifficultyDefinition = recipeDifficultyDefinition;
        }

        public static Enum by(RecipeDifficultyDefinition recipeDifficultyDefinition) {
            if (recipeDifficultyDefinition == null) {
                return null;
            }
            return Stream.of(values()).filter(v -> v.recipeDifficultyDefinition == recipeDifficultyDefinition).findFirst().orElseThrow(() -> new IllegalArgumentException("No valid found with value=" + recipeDifficultyDefinition + "."));
        }

        public Long getId() {
            return id;
        }

    }

}
