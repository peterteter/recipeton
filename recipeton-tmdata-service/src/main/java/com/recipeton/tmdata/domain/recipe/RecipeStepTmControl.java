package com.recipeton.tmdata.domain.recipe;

/*-
 * #%L
 * recipeton-tmdata-service
 * %%
 * Copyright (C) 2021 tincomisc
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.recipeton.tmdata.domain.control.TmControl;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Data
@AllArgsConstructor
@NoArgsConstructor
@Accessors(chain = true)
@Table(name = "recipeStep_tmControl")
public class RecipeStepTmControl implements Serializable {

    @EmbeddedId
    private ID id = new ID();

    @ManyToOne
    @JoinColumn(name = "recipeStep_id")
    @MapsId("recipeStepId")
    private RecipeStep recipeStep;

    @ManyToOne
    @JoinColumn(name = "tmControl_id")
    @MapsId("tmControlId")
    private TmControl tmControl;

    @ManyToOne
    @JoinColumns({
            @JoinColumn(name = "recipe_id", referencedColumnName = "recipe_id"),
            @JoinColumn(name = "tmVersion_id", referencedColumnName = "tmVersion_id")
    })
    private RecipeTmVersion recipeTmVersion;

    @Data
    public static class ID implements Serializable {
        @Column(name = "recipeStep_id", nullable = false)
        private Long recipeStepId;

        @Column(name = "tmControl_id", nullable = false)
        private Long tmControlId;
    }

}
