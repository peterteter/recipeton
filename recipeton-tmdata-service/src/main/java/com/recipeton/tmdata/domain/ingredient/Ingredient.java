package com.recipeton.tmdata.domain.ingredient;

/*-
 * #%L
 * recipeton-tmdata-service
 * %%
 * Copyright (C) 2021 tincomisc
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.recipeton.tmdata.domain.Localized;
import com.recipeton.tmdata.domain.ingredient.extra.IngredientFoodCategory;
import com.recipeton.tmdata.domain.ingredient.lang.IngredientLang;
import com.recipeton.tmdata.domain.misc.Locale;
import com.recipeton.util.jpa.LocalDateTimeLongSecondsConverter;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;
import lombok.experimental.Accessors;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.*;
import java.io.Serializable;
import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.Collection;
import java.util.List;

import static com.recipeton.shared.util.StreamUtil.toStream;
import static javax.persistence.FetchType.LAZY;

@Entity
@Data
@Table(name = "ingredient")
@Cacheable
@org.hibernate.annotations.Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
@Accessors(chain = true)
@ToString(onlyExplicitlyIncluded = true)
public class Ingredient implements Localized, Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", nullable = false)
    private Long id;

    // Unique too?
    @Column(name = "uid", nullable = false)
    @ToString.Include
    private String uid;

    @ManyToOne(fetch = LAZY)
    @JoinColumn(name = "shoppingCategory_id", nullable = false)
    @ToString.Include
    private ShoppingCategory shoppingCategory;

    @Column(name = "version", nullable = false)
    private BigDecimal version = BigDecimal.ZERO;

    @Column(name = "isDeleted", nullable = false)
    private boolean deleted;

    @Convert(converter = LocalDateTimeLongSecondsConverter.class)
    @Column(name = "createdts", nullable = false)
    private LocalDateTime created;

    @EqualsAndHashCode.Exclude
    @ManyToMany(cascade = CascadeType.ALL)
    @JoinTable(name = "ingredient_ingredientFoodCategory",
            joinColumns = @JoinColumn(name = "ingredient_id"),
            inverseJoinColumns = @JoinColumn(name = "ingredientFoodCategory_id"))
    private Collection<IngredientFoodCategory> ingredientFoodCategories;

    @EqualsAndHashCode.Exclude
    @OneToMany(mappedBy = "ingredient", cascade = CascadeType.ALL, orphanRemoval = true)
    private Collection<IngredientCountryIngredientFlag> ingredientCountryIngredientFlags;

    @EqualsAndHashCode.Exclude
    @OneToMany(mappedBy = "ingredient", cascade = CascadeType.ALL, orphanRemoval = true)
    @OrderBy("position")
    private List<IngredientMaterialObject> ingredientMaterialObjects;

    @EqualsAndHashCode.Exclude
    @OneToMany(mappedBy = "ingredient", cascade = CascadeType.ALL, orphanRemoval = true)
    private Collection<IngredientNotation> ingredientNotations;

    @EqualsAndHashCode.Exclude
    @OneToMany(mappedBy = "ingredient", cascade = CascadeType.ALL, orphanRemoval = true)
    private Collection<IngredientPreparation> ingredientPreparations;

    @EqualsAndHashCode.Exclude
    @OneToMany(mappedBy = "ingredient", cascade = CascadeType.ALL, orphanRemoval = true)
    @ToString.Include
    private Collection<IngredientLang> langs;

    public IngredientNotation getIngredientNotationByText(String ingredientName, Locale locale) {
        if (ingredientNotations == null) {
            return null;
        }
        return ingredientNotations.stream()
                .filter(in -> ingredientName.equals(in.getTextByLocale(locale)))
                .findFirst()
                .orElse(null);
    }

    public IngredientPreparation getIngredientPreparationByText(String preparationText, Locale locale) {
        if (ingredientPreparations == null) {
            return null;
        }

        if (preparationText == null) {
            return null;
        }
        return ingredientPreparations.stream()
                .filter(in -> preparationText.equals(in.getTextByLocale(locale)))
                .findFirst()
                .orElse(null);
    }

    @PrePersist
    public void prePersist() {
        if (created == null) {
            created = LocalDateTime.now();
        }
        toStream(langs).filter(n -> n.getIngredient() == null).forEach(n -> n.setIngredient(this));
        toStream(ingredientNotations).filter(n -> n.getIngredient() == null).forEach(n -> n.setIngredient(this));
        toStream(ingredientPreparations).filter(n -> n.getIngredient() == null).forEach(n -> n.setIngredient(this));
        toStream(ingredientMaterialObjects).filter(n -> n.getIngredient() == null).forEach(n -> n.setIngredient(this));
        toStream(ingredientCountryIngredientFlags).filter(n -> n.getIngredient() == null).forEach(n -> n.setIngredient(this));
    }

}
