package com.recipeton.tmdata.domain.unit;

/*-
 * #%L
 * recipeton-tmdata-service
 * %%
 * Copyright (C) 2021 tincomisc
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.recipeton.tmdata.domain.Localized;
import com.recipeton.tmdata.domain.unit.lang.UnitNotationLang;
import lombok.*;
import lombok.experimental.Accessors;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.*;
import java.io.Serializable;
import java.util.List;

import static com.recipeton.shared.util.StreamUtil.toStream;
import static javax.persistence.FetchType.LAZY;

@Entity
@Table(name = "unitNotation")
@Cacheable
@org.hibernate.annotations.Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
@Data
@AllArgsConstructor
@NoArgsConstructor
@Accessors(chain = true)
@ToString(onlyExplicitlyIncluded = true)
public class UnitNotation implements Localized, Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", nullable = false)
    private Long id;

    @ManyToOne(fetch = LAZY)
    @JoinColumn(name = "unit_id", nullable = false)
    @ToString.Include
    private Unit unit;

    @ManyToOne(fetch = LAZY)
    @JoinColumn(name = "unitNotationPriority_id", nullable = false)
    @ToString.Include
    private UnitNotationPriority unitNotationPriority;

    @EqualsAndHashCode.Exclude
    @ToString.Include
    @OneToMany(mappedBy = "unitNotation", cascade = CascadeType.ALL, orphanRemoval = true)
    private List<UnitNotationLang> langs;

    @PrePersist
    public void prePersist() {
        toStream(langs).filter(e -> e.getUnitNotation() == null).forEach(e -> e.setUnitNotation(this));
    }


}
