package com.recipeton.tmdata.domain.recipe;

/*-
 * #%L
 * recipeton-tmdata-service
 * %%
 * Copyright (C) 2021 tincomisc
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.recipeton.shared.analysis.domain.RecipePriceDefinition;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;
import lombok.experimental.Accessors;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.*;
import java.io.Serializable;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@Entity
@Table(name = "recipePrice")
@Cacheable
@org.hibernate.annotations.Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
@Data
@AllArgsConstructor
@NoArgsConstructor
@Accessors(chain = true)
@ToString(onlyExplicitlyIncluded = true, includeFieldNames = false)
public class RecipePrice implements Serializable {

    public static final List<RecipePrice> DEFAULT = Stream.of(RecipePrice.Enum.values()).map(e -> new RecipePrice(e.id, e.text)).collect(Collectors.toList());

    private static final long serialVersionUID = 1L;

    @Id
    @Column(name = "id", nullable = false)
    private Long id;

    @Column(name = "pricelevel", nullable = false)
    @ToString.Include
    private String priceLevel;

    public enum Enum {
        LOW(1L, "low", RecipePriceDefinition.LOW),
        MEDIUM(2L, "medium", RecipePriceDefinition.MEDIUM),
        HIGH(3L, "high", RecipePriceDefinition.HIGH),
        NO_INFORMATION(4L, "no information", RecipePriceDefinition.NO_INFORMATION);

        private final long id;
        private final String text;
        private final RecipePriceDefinition recipePriceDefinition;

        Enum(Long id, String text, RecipePriceDefinition recipePriceDefinition) {
            this.id = id;
            this.text = text;
            this.recipePriceDefinition = recipePriceDefinition;
        }

        public static Enum by(RecipePriceDefinition recipePriceDefinition) {
            if (recipePriceDefinition == null) {
                return null;
            }
            return Stream.of(values()).filter(v -> v.recipePriceDefinition == recipePriceDefinition).findFirst().orElseThrow(() -> new IllegalArgumentException("No valid price found with value=" + recipePriceDefinition + "."));
        }

        public Long getId() {
            return id;
        }


    }

}
