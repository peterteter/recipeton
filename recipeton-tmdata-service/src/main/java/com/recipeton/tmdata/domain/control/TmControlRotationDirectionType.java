package com.recipeton.tmdata.domain.control;

/*-
 * #%L
 * recipeton-tmdata-service
 * %%
 * Copyright (C) 2021 tincomisc
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.recipeton.shared.analysis.domain.RecipeCommandRotation;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;
import lombok.experimental.Accessors;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.*;
import java.io.Serializable;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@Entity
@Table(name = "tmControlRotationDirectionType")
@Cacheable
@org.hibernate.annotations.Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
@Data
@AllArgsConstructor
@NoArgsConstructor
@Accessors(chain = true)
@ToString(onlyExplicitlyIncluded = true, includeFieldNames = false)
public class TmControlRotationDirectionType implements Serializable {

    public static final List<TmControlRotationDirectionType> DEFAULT = Stream.of(TmControlRotationDirectionType.Enum.values()).map(e -> new TmControlRotationDirectionType(e.id, e.text)).collect(Collectors.toList());

    private static final long serialVersionUID = 1L;

    @Id
    @Column(name = "id", nullable = false)
    private Long id;

    @Column(name = "value", nullable = false)
    @ToString.Include
    private String value;

    public enum Enum {
        CLOCKWISE(1L, "clockwise"),
        COUNTER_CLOCKWISE(2L, "counterclockwise"),
        DISABLED_COUNTERCLOCKWISE(3L, "disable counterclockwise"); // Is this disabled??

        private final long id;
        private final String text;

        Enum(long id, String text) {
            this.id = id;
            this.text = text;
        }

        public static Enum by(RecipeCommandRotation rotation) {
            if (rotation == null) {
                return TmControlRotationDirectionType.Enum.CLOCKWISE;
            }
            switch (rotation) {
                case REVERSE:
                    return TmControlRotationDirectionType.Enum.COUNTER_CLOCKWISE;
                case NONE:
                    return TmControlRotationDirectionType.Enum.DISABLED_COUNTERCLOCKWISE;
                case FORWARD:
                default:
                    return TmControlRotationDirectionType.Enum.CLOCKWISE;
            }
        }

        public long getId() {
            return id;
        }

    }

}
