package com.recipeton.tmdata.domain.control;

/*-
 * #%L
 * recipeton-tmdata-service
 * %%
 * Copyright (C) 2021 tincomisc
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;
import lombok.experimental.Accessors;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Data
@Table(name = "tmControlProgram")
@Accessors(chain = true)
@ToString(onlyExplicitlyIncluded = true)
public class TmControlProgram implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @Column(name = "id", nullable = false) // bad name
    private Long tmControlId;

    @OneToOne
    @JoinColumn(name = "id") // bad name
    @MapsId
    @EqualsAndHashCode.Exclude
    private TmControl tmControl;

    @ManyToOne
    @JoinColumn(name = "tmControlProgramType_id", nullable = false)
    @ToString.Include
    private TmControlProgramType tmControlProgramType;

    @OneToOne(mappedBy = "tmControlProgram", cascade = CascadeType.ALL)
    @PrimaryKeyJoinColumn
    @ToString.Include
    private TmControlProgramSoftBlending tmControlProgramSoftBlending;

    @OneToOne(mappedBy = "tmControlProgram", cascade = CascadeType.ALL)
    @PrimaryKeyJoinColumn
    @ToString.Include
    private TmControlProgramTurbo tmControlProgramTurbo;

    @PrePersist
    public void prePersist() {
        if (tmControlProgramTurbo != null) {
            tmControlProgramTurbo.setTmControlProgram(this);
        }
        if (tmControlProgramSoftBlending != null) {
            tmControlProgramSoftBlending.setTmControlProgram(this);
        }
    }
}
