package com.recipeton.tmdata.domain.recipe;

/*-
 * #%L
 * recipeton-tmdata-service
 * %%
 * Copyright (C) 2021 tincomisc
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.recipeton.tmdata.domain.Localized;
import com.recipeton.tmdata.domain.recipe.lang.RecipeStepLang;
import lombok.*;
import lombok.experimental.Accessors;
import lombok.extern.slf4j.Slf4j;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Collection;
import java.util.List;

import static com.recipeton.shared.util.StreamUtil.toStream;

@Slf4j
@Table(name = "recipeStep")
@Entity
@Data
@AllArgsConstructor
@NoArgsConstructor
@Accessors(chain = true)
@ToString(onlyExplicitlyIncluded = true)
public class RecipeStep implements Localized, Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", nullable = false)
    private Long id;

    @ManyToOne
    @JoinColumn(name = "recipe_id", nullable = false)
    private Recipe recipe;

    @ManyToOne
    @JoinColumn(name = "recipeGroup_id")
    private RecipeGroup recipeGroup;

    @Column(name = "position", nullable = false)
    @ToString.Include
    private Long position;

    @Column(name = "displayNo", nullable = false)
    @ToString.Include
    private Long displayNo;

    @EqualsAndHashCode.Exclude
    @OneToMany(mappedBy = "recipeStep", cascade = CascadeType.ALL, orphanRemoval = true)
    @OrderBy("position")
//    @OrderColumn(name = "position") Needs to enforce in code. Its not continuous
    private List<GuidedStep> guidedSteps;

    @EqualsAndHashCode.Exclude
    @OneToMany(mappedBy = "recipeStep")
    @OrderBy("position")
    private List<RecipeStepUtensil> recipeStepUtensils;

    @EqualsAndHashCode.Exclude
    @OneToMany(mappedBy = "recipeStep", cascade = CascadeType.ALL, orphanRemoval = true)
    @OrderBy("position")
    private List<RecipeStepMaterialObject> recipeStepMaterialObjects;

    @EqualsAndHashCode.Exclude
    @OneToMany(mappedBy = "recipeStep", cascade = CascadeType.ALL, orphanRemoval = true)
    private Collection<RecipeStepLang> langs;

    @PrePersist
    public void prePersist() {
        toStream(recipeStepUtensils).filter(n -> n.getRecipeStep() == null).forEach(n -> n.setRecipeStep(this));
        toStream(recipeStepMaterialObjects).filter(n -> n.getRecipeStep() == null).forEach(n -> n.setRecipeStep(this));
        toStream(langs).filter(n -> n.getRecipeStep() == null).forEach(n -> n.setRecipeStep(this));
    }

}
