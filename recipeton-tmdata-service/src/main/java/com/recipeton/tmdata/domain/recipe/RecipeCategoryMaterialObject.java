package com.recipeton.tmdata.domain.recipe;

/*-
 * #%L
 * recipeton-tmdata-service
 * %%
 * Copyright (C) 2021 tincomisc
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.recipeton.tmdata.domain.material.MaterialObject;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;
import lombok.experimental.Accessors;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.*;
import java.io.Serializable;

import static javax.persistence.FetchType.LAZY;

@Entity
@Table(name = "recipeCategory_materialObject")
@Cacheable
@org.hibernate.annotations.Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
@Data
@AllArgsConstructor
@NoArgsConstructor
@Accessors(chain = true)
@ToString(onlyExplicitlyIncluded = true)
public class RecipeCategoryMaterialObject implements Serializable {

    private static final long serialVersionUID = 1L;

    @EmbeddedId
    private ID id = new ID();

    @ManyToOne
    @JoinColumn(name = "recipeCategory_id")
    @MapsId("recipeCategoryId")
    private RecipeCategory recipeCategory;

    @ManyToOne(fetch = LAZY, cascade = CascadeType.ALL)
    @MapsId("materialObjectId")
    @JoinColumn(name = "materialObject_id")
    @ToString.Include
    private MaterialObject materialObject;

    @Column(name = "position", nullable = false)
    private Long position;

    @Column(name = "isPrimary", nullable = false)
    private boolean primary;

    @Data
    public static class ID implements Serializable {
        @Column(name = "recipeCategory_id", nullable = false)
        private Long recipeCategoryId;

        @Column(name = "materialObject_id", nullable = false)
        private Long materialObjectId;
    }
}
