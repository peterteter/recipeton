package com.recipeton.tmdata.domain.control;

/*-
 * #%L
 * recipeton-tmdata-service
 * %%
 * Copyright (C) 2021 tincomisc
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;
import lombok.experimental.Accessors;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@Entity
@Table(name = "tmControlSpeedType")
@Data
@AllArgsConstructor
@NoArgsConstructor
@Accessors(chain = true)
@ToString(onlyExplicitlyIncluded = true, includeFieldNames = false)
public class TmControlSpeedType implements Serializable {

    public static final List<TmControlSpeedType> DEFAULT = Stream.of(TmControlSpeedType.Enum.values()).map(e -> new TmControlSpeedType(e.id, e.value, e.text)).collect(Collectors.toList());

    private static final long serialVersionUID = 1L;

    @Id
    @Column(name = "id", nullable = false)
    private Long id;

    @Column(name = "value", nullable = false)
    private BigDecimal value;

    @Column(name = "description", nullable = false)
    @ToString.Include
    private String description;

    public enum Enum {
        SOFT1(1L, new BigDecimal("0.1"), "Soft1"),
        SOFT2(2L, new BigDecimal("0.2"), "Soft2"),
        SOFT3(3L, new BigDecimal("0.3"), "Soft3"),
        NORMAL1(4L, BigDecimal.ONE, "Normal1"),
        NORMAL2(5L, new BigDecimal("2"), "Normal2"),
        NORMAL3(6L, new BigDecimal("3"), "Normal3"),
        NORMAL4(7L, new BigDecimal("4"), "Normal4"),
        NORMAL5(8L, new BigDecimal("5"), "Normal5"),
        NORMAL6(9L, new BigDecimal("6"), "Normal6"),
        NORMAL7(10L, new BigDecimal("7"), "Normal7"),
        NORMAL8(11L, new BigDecimal("8"), "Normal8"),
        NORMAL9(12L, new BigDecimal("9"), "Normal9"),
        NORMAL10(13L, BigDecimal.TEN, "Normal10"),
        NORMAL0_5(14L, new BigDecimal("0.5"), "Normal0.5"),
        NORMAL1_5(15L, new BigDecimal("1.5"), "Normal1.5"),
        NORMAL2_5(16L, new BigDecimal("2.5"), "Normal2.5"),
        NORMAL3_5(17L, new BigDecimal("3.5"), "Normal3.5"),
        NORMAL4_5(18L, new BigDecimal("4.5"), "Normal4.5"),
        NORMAL5_5(19L, new BigDecimal("5.5"), "Normal5.5"),
        NORMAL6_5(20L, new BigDecimal("6.5"), "Normal6.5"),
        NORMAL7_5(21L, new BigDecimal("7.5"), "Normal7.5"),
        NORMAL8_5(22L, new BigDecimal("8.5"), "Normal8.5"),
        NORMAL9_5(23L, new BigDecimal("9.5"), "Normal9.5");

        private final long id;
        private final BigDecimal value;
        private final String text;

        Enum(long id, BigDecimal value, String text) {
            this.id = id;
            this.value = value;
            this.text = text;
        }

        public static Enum byValue(BigDecimal value) {
            if (value == null) {
                return null;
            }
            // Could do closest...
            return Stream.of(values()).filter(v -> v.value.equals(value)).findFirst().orElseThrow(() -> new IllegalArgumentException("No valid speed found with value=" + value + "."));
        }


        public long getId() {
            return id;
        }
    }
}
