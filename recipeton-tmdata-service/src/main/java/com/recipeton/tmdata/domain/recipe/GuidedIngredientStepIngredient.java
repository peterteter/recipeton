package com.recipeton.tmdata.domain.recipe;

/*-
 * #%L
 * recipeton-tmdata-service
 * %%
 * Copyright (C) 2021 tincomisc
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.recipeton.tmdata.domain.ingredient.IngredientNotation;
import com.recipeton.tmdata.domain.unit.UnitNotation;
import lombok.Data;
import lombok.ToString;
import lombok.experimental.Accessors;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Collection;

import static com.recipeton.shared.util.StreamUtil.toStream;

@Entity
@Data
@Table(name = "guidedIngredientStepIngredient")
@Accessors(chain = true)
@ToString(onlyExplicitlyIncluded = true)
public class GuidedIngredientStepIngredient implements Serializable {

    private static final long serialVersionUID = 1L;

    @EmbeddedId
    private ID id = new ID();

    @ManyToOne
    @JoinColumn(name = "guidedIngredientStep_id")
    @MapsId("guidedIngredientStepId")
    private GuidedIngredientStep guidedIngredientStep;

    @ManyToOne
    @MapsId("recipeStepRecipeIngredientId")
    @JoinColumns({
            @JoinColumn(name = "recipeStep_id", referencedColumnName = "recipeStep_id"),
            @JoinColumn(name = "recipeIngredient_id", referencedColumnName = "recipeIngredient_id")
    })
    private RecipeStepRecipeIngredient recipeStepRecipeIngredient;

    @ManyToOne
    @JoinColumn(name = "ingredientNotation_id", nullable = false)
    private IngredientNotation ingredientNotation;


    @OneToMany(cascade = CascadeType.ALL, mappedBy = "guidedIngredientStepIngredient")
    private Collection<GuidedIngredientStepIngredientAmountRange> guidedIngredientStepIngredientAmountRanges;

    /**
     * Optional
     */
    @ManyToOne
    @JoinColumn(name = "unitNotation_id")
    private UnitNotation unitNotation;

    @ManyToOne
    @JoinColumn(name = "guidedIngredientStepWeighingAttribute_id", nullable = false)
    private GuidedIngredientStepWeighingAttribute weighingAttribute;

    /**
     * Mandatory. Should create on demand
     */
    @ManyToOne
//            (cascade = CascadeType.ALL)
    @JoinColumn(name = "guidedIngredientStepText_id", nullable = false)
    private GuidedIngredientStepText guidedIngredientStepText;

    @PrePersist
    public void prePersist() {
        toStream(guidedIngredientStepIngredientAmountRanges).forEach(e -> e.setGuidedIngredientStepIngredient(this));
    }

    @Data
    @Embeddable
    public static class ID implements Serializable {
        @Column(name = "guidedIngredientStep_id", nullable = false)
        private Long guidedIngredientStepId;

        private RecipeStepRecipeIngredient.ID recipeStepRecipeIngredientId;
    }
}
