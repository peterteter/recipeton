package com.recipeton.tmdata.domain.misc;

/*-
 * #%L
 * recipeton-tmdata-service
 * %%
 * Copyright (C) 2021 tincomisc
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.recipeton.tmdata.domain.Localized;
import com.recipeton.tmdata.domain.misc.lang.CountryLang;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;
import lombok.experimental.Accessors;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.*;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@Entity
@Table(name = "country")
@Cacheable
@org.hibernate.annotations.Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
@Data
@AllArgsConstructor
@NoArgsConstructor
@Accessors(chain = true)
@ToString(onlyExplicitlyIncluded = true, includeFieldNames = false)
public class Country implements Localized, Serializable {

    public static final List<Country> DEFAULT = Stream.of(Enum.values()).map(e -> new Country(e.id, e.text, new ArrayList<>())).collect(Collectors.toList());

    private static final long serialVersionUID = 1L;

    @Id
    @Column(name = "id", nullable = false)
    private Long id;

    @Column(name = "code", nullable = false)
    @ToString.Include
    private String code;

    @OneToMany(mappedBy = "country", cascade = CascadeType.ALL, orphanRemoval = true)
    private List<CountryLang> langs;

    public static Country byId(long id) {
        return DEFAULT.stream()
                .filter(v -> v.getId() == id)
                .findFirst()
                .orElseThrow(() -> new IllegalArgumentException("Unknown Country Id=" + id));
    }

    public enum Enum {
        GB(1L, "GB"),
        DE(2L, "DE"),
        FR(3L, "FR"),
        ES(4L, "ES"),
        IT(5L, "IT"),
        CH(6L, "CH"),
        US(7L, "US"),
        VI(8L, "VI"),
        PT(9L, "PT"),
        PL(10L, "PL"),
        MX(11L, "MX"),
        TW(12L, "TW"),
        RU(13L, "RU"),
        CZ(14L, "CZ"),
        AU(15L, "AU"),
        CN(16L, "CN"),
        CY(17L, "CY"),
        NL(18L, "NL"),
        UK(19L, "UK"),
        EN_MY(20L, "en_MY"),
        ZH_MY(21L, "zh_MY"),
        AT(22L, "AT"),
        KR(23L, "KR"),
        BR(24L, "BR"),
        CL(25L, "CL"),
        HU(26L, "HU"),
        AO(27L, "AO"),
        MO(28L, "MO"),
        NO(29L, "NO"),
        RO(30L, "RO"),
        DK(31L, "DK"),
        KZ(32L, "KZ"),
        LV(33L, "LV"),
        BE(34L, "BE"),
        LU(35L, "LU"),
        US2(36L, "US"),
        FR_CA(37L, "fr_CA"),
        EN_CA(38L, "en_CA"),
        EE(39L, "EE"),
        LT(40L, "LT"),
        DO(41L, "DO"),
        EC(42L, "EC"),
        HK(43L, "HK"),
        IL(44L, "IL"),
        MA(45L, "MA"),
        ZA(46L, "ZA"),
        AE(47L, "AE"),
        BH(48L, "BH"),
        KW(49L, "KW"),
        OM(50L, "OM"),
        QA(51L, "QA"),
        NZ(52L, "NZ");

        private final long id;
        private final String text;

        Enum(long id, String text) {
            this.id = id;
            this.text = text;
        }

        public long getId() {
            return id;
        }
    }
}
