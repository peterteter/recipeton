package com.recipeton.tmdata.domain.recipe;

/*-
 * #%L
 * recipeton-tmdata-service
 * %%
 * Copyright (C) 2021 tincomisc
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import lombok.*;
import lombok.experimental.Accessors;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Collection;

import static com.recipeton.shared.util.StreamUtil.toStream;
import static javax.persistence.CascadeType.ALL;

@Entity
@Table(name = "guidedIngredientStep")
@Data
@AllArgsConstructor
@NoArgsConstructor
@Accessors(chain = true)
@ToString(onlyExplicitlyIncluded = true)
public class GuidedIngredientStep implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @Column(name = "guidedStep_id", nullable = false)
    private Long guidedStepId;

    @OneToOne
    @JoinColumn(name = "guidedStep_id")
    @MapsId
    @EqualsAndHashCode.Exclude
    private GuidedStep guidedStep;

    @OneToMany(mappedBy = "guidedIngredientStep", cascade = ALL)
    @EqualsAndHashCode.Exclude
    private Collection<GuidedIngredientStepIngredient> guidedIngredientStepIngredients;

    @PrePersist
    public void prePersist() {
        toStream(guidedIngredientStepIngredients).filter(e -> e.getGuidedIngredientStep() == null)
                .forEach(e -> {
                    e.setGuidedIngredientStep(this);
                    e.getRecipeStepRecipeIngredient().setRecipeStep(this.getGuidedStep().getRecipeStep()); // Check if not needed
                });
    }
}
