package com.recipeton.tmdata.service.recipe;

/*-
 * #%L
 * recipeton-tmdata-service
 * %%
 * Copyright (C) 2021 tincomisc
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.recipeton.tmdata.domain.misc.Locale;
import com.recipeton.tmdata.domain.misc.RangeType;
import com.recipeton.tmdata.domain.recipe.*;
import com.recipeton.tmdata.domain.recipe.lang.GuidedIngredientStepTextLang;
import com.recipeton.tmdata.service.misc.RangeTypeRepository;
import com.recipeton.tmdata.service.recipe.lang.GuidedIngredientStepTextLangRepository;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static java.util.List.of;

@Slf4j
@Service
@AllArgsConstructor
public class GuidedIngredientStepService {

    private final GuidedIngredientStepTextAttributeRepository guidedIngredientStepTextAttributeRepository;
    private final GuidedIngredientStepTextLangRepository guidedIngredientStepTextLangRepository;
    private final GuidedIngredientStepTextRepository guidedIngredientStepTextRepository;
    private final GuidedIngredientStepWeighingAttributeRepository guidedIngredientStepWeighingAttributeRepository;
    private final GuidedStepTypeRepository guidedStepTypeRepository;
    private final RangeTypeRepository rangeTypeRepository;

    public GuidedIngredientStepText createGuidedIngredientStepText(Map<Locale, String> localization, GuidedIngredientStepTextAttribute.Enum attribute, boolean predefined) {
        return new GuidedIngredientStepText()
                .setPredefined(predefined)
                .setAttribute(guidedIngredientStepTextAttributeRepository.getOne(attribute))
                .setLangs(localization.entrySet().stream()
                        .map(e -> new GuidedIngredientStepTextLang()
                                .setText(e.getValue())
                                .setLocale(e.getKey())
                        ).collect(Collectors.toList())
                );
    }

    public GuidedStep createGuidedStepIngredient(RecipeIngredient recipeIngredient, BigDecimal magnitudeFrom, BigDecimal magnitudeTo, GuidedIngredientStepText guidedIngredientStepText, GuidedIngredientStepWeighingAttribute.Enum weighingAttribute) {
        List<GuidedIngredientStepIngredientAmountRange> ranges = Stream.of(
                createRange(magnitudeFrom, RangeType.Enum.FROM),
                createRange(magnitudeTo, RangeType.Enum.TO))
                .filter(Objects::nonNull)
                .collect(Collectors.toList());


        List<RecipeStepRecipeIngredientRange> recipeStepRanges = ranges.stream().map(s ->
                        new RecipeStepRecipeIngredientRange()
//                                                            .setRecipeStep(recipeStep)
//                                .setRecipeIngredient(recipeIngredient)
                                .setIngredientAmountValue(s.getAmount())
                                .setRangeType(s.getRangeType())

        ).collect(Collectors.toList());


        return new GuidedStep()
                .setPosition(-1L) // Assigned by recipe.organize
                .setGuidedIngredientStep(new GuidedIngredientStep()
                        .setGuidedIngredientStepIngredients(of(
                                new GuidedIngredientStepIngredient()

                                        .setRecipeStepRecipeIngredient(
                                                new RecipeStepRecipeIngredient()
                                                        .setPosition(1L)
                                                        .setRecipeIngredient(recipeIngredient)
                                                        .setRecipeStepRecipeIngredientRanges(recipeStepRanges)
                                        )
                                        .setUnitNotation(recipeIngredient.getUnitNotation())
                                        .setIngredientNotation(recipeIngredient.getIngredientNotation())
                                        .setWeighingAttribute(guidedIngredientStepWeighingAttributeRepository.getOne(weighingAttribute))
                                        .setGuidedIngredientStepText(guidedIngredientStepText)
                                        .setGuidedIngredientStepIngredientAmountRanges(ranges)
                        )))
                .setGuidedStepType(guidedStepTypeRepository.getOne(GuidedStepType.Enum.INGREDIENT));
    }

    private GuidedIngredientStepIngredientAmountRange createRange(BigDecimal magnitude, RangeType.Enum rangeType) {
        return magnitude == null ? null : new GuidedIngredientStepIngredientAmountRange()
                .setAmount(magnitude)
                .setRangeType(rangeTypeRepository.getOne(rangeType));
    }

    public GuidedIngredientStepText getPersistedGuidedIngredientStepText(GuidedIngredientStepTextAttribute.Enum attribute) {
        return guidedIngredientStepTextRepository.getOneByAttributeAndPredefined(guidedIngredientStepTextAttributeRepository.getOne(attribute), true);
    }

    public GuidedIngredientStepText getPersistedGuidedIngredientStepText(GuidedIngredientStepTextAttribute.Enum attribute, boolean predefined, String text, Locale locale) {
        return guidedIngredientStepTextLangRepository.findFirstByTextAndLocale(text, locale)
                .map(GuidedIngredientStepTextLang::getGuidedIngredientStepText)
                .orElseGet(() -> guidedIngredientStepTextRepository.save(createGuidedIngredientStepText(Map.of(locale, text), attribute, predefined)));
    }
}
