package com.recipeton.tmdata.service.ingredient;

/*-
 * #%L
 * recipeton-tmdata-service
 * %%
 * Copyright (C) 2021 tincomisc
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.recipeton.tmdata.config.ApplicationConfiguration;
import com.recipeton.tmdata.domain.ingredient.ShoppingCategory;
import com.recipeton.tmdata.domain.ingredient.lang.ShoppingCategoryLang;
import com.recipeton.tmdata.domain.ingredient.lang.ShoppingCategoryLangAttribute;
import com.recipeton.tmdata.domain.misc.Locale;
import com.recipeton.tmdata.service.DataInitializer;
import com.recipeton.tmdata.service.JsonResourceReaderService;
import com.recipeton.tmdata.service.ingredient.lang.ShoppingCategoryLangAttributeRepository;
import com.recipeton.tmdata.service.ingredient.lang.ShoppingCategoryLangRepository;
import com.recipeton.tmdata.service.misc.LocaleRepository;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.io.IOException;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import static java.util.Arrays.asList;

@Slf4j
@Service
@AllArgsConstructor
public class ShoppingCategoryService implements DataInitializer {

    public static final long POSITION = 99L;
    private final ApplicationConfiguration applicationConfiguration;
    private final JsonResourceReaderService jsonResourceReaderService;
    private final LocaleRepository localeRepository;
    private final ShoppingCategoryLangAttributeRepository shoppingCategoryLangAttributeRepository;
    private final ShoppingCategoryLangRepository shoppingCategoryLangRepository;
    private final ShoppingCategoryRepository shoppingCategoryRepository;

    @Transactional
    public void doDataInitialize() throws IOException {

        List<ShoppingCategoryForm> shoppingCategoryForms = jsonResourceReaderService.readCollection(applicationConfiguration.getDataDefaultShoppingCategories(), ShoppingCategoryForm.class);
        List<ShoppingCategoryLangForm> shoppingCategoryLangForms = jsonResourceReaderService.readCollection(applicationConfiguration.getDataDefaultShoppingCategoryLangs(), ShoppingCategoryLangForm.class);

        for (ShoppingCategoryService.ShoppingCategoryForm scf : shoppingCategoryForms) {
            Map<Locale, String> lMap = shoppingCategoryLangForms.stream()
                    .filter(l -> l.getShoppingCategoryId().equals(scf.getId()))
                    .collect(Collectors.toMap(l -> localeRepository.getOne(l.getLocaleId()), l -> l.text));
            getPersisted(scf.getId(), scf.position, lMap);
        }
    }

    @Transactional
    public ShoppingCategory getPersisted(String text, Locale locale) {
        return shoppingCategoryLangRepository
                .findByTextAndLocale(text, locale).map(ShoppingCategoryLang::getShoppingCategory).orElseGet(() -> {
                    ShoppingCategoryLangAttribute langAttribute = shoppingCategoryLangAttributeRepository.getOne(ShoppingCategoryLangAttribute.Enum.CATEGORY.getId());
                    return shoppingCategoryRepository.save(new ShoppingCategory()
                            .setPosition(POSITION)
                            .setLangs(asList(new ShoppingCategoryLang()
                                    .setAttribute(langAttribute)
                                    .setText(text)
                                    .setLocale(locale)))
                    );
                });
    }

    @Transactional
    public ShoppingCategory getPersisted(Long id, Long position, Map<Locale, String> names) {
        ShoppingCategory storedShoppingCategory = shoppingCategoryRepository.findById(id).orElse(null);
        if (storedShoppingCategory != null) {
            return storedShoppingCategory;
        }

        ShoppingCategoryLangAttribute langAttribute = shoppingCategoryLangAttributeRepository.getOne(ShoppingCategoryLangAttribute.Enum.CATEGORY.getId());

        ShoppingCategory entity = new ShoppingCategory()
                .setId(id)
                .setPosition(position)
                .setLangs(names.entrySet().stream()
                        .map(e -> new ShoppingCategoryLang()
                                .setAttribute(langAttribute)
                                .setText(e.getValue())
                                .setLocale(e.getKey())).collect(Collectors.toList()));
        // TODO findout why when not generated Id then does not apply to OneToMany automatically
        entity.prePersist();
        log.debug("initialize entity={}", entity);
        return shoppingCategoryRepository.save(entity);
    }

    @Data
    private static class ShoppingCategoryForm {
        private long id;
        private long position;
    }

    @Data
    private static class ShoppingCategoryLangForm {
        private Long localeId;
        private Long shoppingCategoryId;
        private String text;
    }


}
