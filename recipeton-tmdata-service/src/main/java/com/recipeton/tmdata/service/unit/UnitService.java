package com.recipeton.tmdata.service.unit;

/*-
 * #%L
 * recipeton-tmdata-service
 * %%
 * Copyright (C) 2021 tincomisc
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.recipeton.shared.analysis.domain.RecipeUnitDefinition;
import com.recipeton.tmdata.domain.misc.Locale;
import com.recipeton.tmdata.domain.unit.Unit;
import com.recipeton.tmdata.domain.unit.UnitNotation;
import com.recipeton.tmdata.domain.unit.UnitNotationPriority;
import com.recipeton.tmdata.domain.unit.UnitType;
import com.recipeton.tmdata.domain.unit.lang.UnitNotationLang;
import com.recipeton.tmdata.domain.unit.lang.UnitNotationLangAttribute;
import com.recipeton.tmdata.service.DataInitializer;
import com.recipeton.tmdata.service.misc.LocaleService;
import com.recipeton.tmdata.service.unit.lang.UnitNotationLangAttributeRepository;
import com.recipeton.tmdata.service.unit.lang.UnitNotationLangRepository;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

import static java.util.List.of;

@Slf4j
@Service
@AllArgsConstructor
public class UnitService implements DataInitializer {

    private final UnitRepository unitRepository;
    private final UnitNotationRepository unitNotationRepository;
    private final UnitNotationPriorityRepository unitNotationPriorityRepository;
    private final UnitNotationLangRepository unitNotationLangRepository;
    private final UnitNotationLangAttributeRepository unitNotationLangAttributeRepository;
    private final UnitTypeRepository unitTypeRepository;

    private final LocaleService localeService;

    @Transactional
    @Override
    public void doDataInitialize() {
        DataInitializer.getPersisted(unitNotationLangAttributeRepository, UnitNotationLangAttribute.DEFAULT);
        DataInitializer.getPersisted(unitNotationPriorityRepository, UnitNotationPriority.DEFAULT);
        DataInitializer.getPersisted(unitTypeRepository, UnitType.DEFAULT);

        Locale locale = localeService.getLocaleByDefault(); // Should probably create in all supported locales
        getPersisted(RecipeUnitDefinition.UNIT_KJ, locale, UnitNotationPriority.Enum.PRIMARY, UnitType.Enum.PORTION);
        getPersisted(RecipeUnitDefinition.UNIT_GRAM, locale, UnitNotationPriority.Enum.PRIMARY, UnitType.Enum.PORTION);
        getPersisted(RecipeUnitDefinition.UNIT_MGRAM, locale, UnitNotationPriority.Enum.PRIMARY, UnitType.Enum.PORTION);
        getPersisted(RecipeUnitDefinition.UNIT_KCAL, locale, UnitNotationPriority.Enum.PRIMARY, UnitType.Enum.PORTION);
    }

    @Transactional
    public UnitNotation getPersisted(String text, Locale locale, UnitNotationPriority.Enum priority, UnitType.Enum unitType) {
        List<UnitNotationLang> unitNotationLangs = unitNotationLangRepository.findByTextAndLocale(text, locale);
        if (!unitNotationLangs.isEmpty()) {
            UnitNotation unitNotation = unitNotationLangs.stream()
                    .map(UnitNotationLang::getUnitNotation)
                    .filter(un -> priority.getId() == un.getUnitNotationPriority().getId() && un.getUnit().isUnitType(unitType))
                    .findFirst().orElse(null);
            if (unitNotation != null) {
                return unitNotation;
            }
        }
        // Could potentially share unit notations with table of translations. Set this in Analyzer!
        Unit unit = unitRepository.save(new Unit()
                .setUnitType(unitTypeRepository.getOne(unitType))
                .setVersion(0L)
                .setPosition(1L));

        UnitNotation unitNotation = new UnitNotation()
                .setUnitNotationPriority(unitNotationPriorityRepository.getOne(priority))
                .setUnit(unit)
                .setLangs(of(new UnitNotationLang()
                        .setLocale(locale)
                        .setAttribute(unitNotationLangAttributeRepository.getOne(UnitNotationLangAttribute.Enum.NAME))
                        .setText(text)));
        unitNotation.prePersist(); // NOTE: Under some circumstances (to findout) its not called!
        return unitNotationRepository.save(unitNotation);
    }


}
