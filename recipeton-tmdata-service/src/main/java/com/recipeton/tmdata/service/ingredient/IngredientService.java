package com.recipeton.tmdata.service.ingredient;

/*-
 * #%L
 * recipeton-tmdata-service
 * %%
 * Copyright (C) 2021 tincomisc
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.recipeton.tmdata.config.ApplicationConfiguration;
import com.recipeton.tmdata.domain.ingredient.*;
import com.recipeton.tmdata.domain.ingredient.lang.*;
import com.recipeton.tmdata.domain.misc.Locale;
import com.recipeton.tmdata.service.DataInitializer;
import com.recipeton.tmdata.service.ingredient.lang.*;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.tuple.Pair;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static java.util.List.of;

@Slf4j
@Service
@AllArgsConstructor
public class IngredientService implements DataInitializer {

    private final ApplicationConfiguration applicationConfiguration;

    private final IngredientRepository ingredientRepository;
    private final ShoppingCategoryRepository shoppingCategoryRepository;
    private final IngredientNotationPriorityRepository ingredientNotationPriorityRepository;
    private final IngredientNotationRepository ingredientNotationRepository;
    private final IngredientNotationLangRepository ingredientNotationLangRepository;
    private final IngredientNotationLangAttributeRepository ingredientNotationLangAttributeRepository;
    private final IngredientPreparationRepository ingredientPreparationRepository;
    private final IngredientPreparationLangAttributeRepository ingredientPreparationLangAttributeRepository;
    private final IngredientFlagRepository ingredientFlagRepository;
    private final IngredientLangAttributeRepository ingredientLangAttributeRepository;
    private final ShoppingCategoryLangAttributeRepository shoppingCategoryLangAttributeRepository;

    /**
     * Only one reserved text should be included. Otherwise ingredient duplication will show.
     *
     * @param ingredient
     * @param notationText
     * @param reservedAText Looks used for indications between parentheses. Not clear if there is any difference. Maybe between software versions?
     * @param reservedBText
     * @param locale
     * @return
     */
    private IngredientNotation createIngredientNotation(Ingredient ingredient, String notationText, String reservedAText, String reservedBText, Locale locale) {
        return new IngredientNotation()
                .setIngredient(ingredient)
                .setIngredientNotationPriority(ingredientNotationPriorityRepository.getOne(IngredientNotationPriority.Enum.RECIPE_SPECIFIC.getId()))
                .setLangs(Stream.of(
                        createIngredientNotationLang(notationText, IngredientNotationLangAttribute.Enum.NAME, locale),
                        reservedAText == null ? null : createIngredientNotationLang(reservedAText, IngredientNotationLangAttribute.Enum.RESERVED, locale),
                        reservedBText == null ? null : createIngredientNotationLang(reservedBText, IngredientNotationLangAttribute.Enum.RESERVED_B, locale)
                        ).filter(Objects::nonNull)
                                .collect(Collectors.toList())
                );
    }

    private IngredientNotationLang createIngredientNotationLang(String text, IngredientNotationLangAttribute.Enum atttribute, Locale locale) {
        return new IngredientNotationLang()
                .setAttribute(ingredientNotationLangAttributeRepository.getOne(atttribute))
                .setText(text)
                .setLocale(locale);
    }

    @Override
    @Transactional
    public void doDataInitialize() {
        DataInitializer.getPersisted(ingredientFlagRepository, IngredientFlag.DEFAULT);
        DataInitializer.getPersisted(ingredientLangAttributeRepository, IngredientLangAttribute.DEFAULT);
        DataInitializer.getPersisted(ingredientNotationLangAttributeRepository, IngredientNotationLangAttribute.DEFAULT);
        DataInitializer.getPersisted(ingredientNotationPriorityRepository, IngredientNotationPriority.DEFAULT);
        DataInitializer.getPersisted(ingredientPreparationLangAttributeRepository, IngredientPreparationLangAttribute.DEFAULT);
        DataInitializer.getPersisted(shoppingCategoryLangAttributeRepository, ShoppingCategoryLangAttribute.DEFAULT);
    }

    private Ingredient getIngredientByTexts(String notationText, String preparationText, Locale locale) {
        List<IngredientNotationLang> ingredientNotationLangs = ingredientNotationLangRepository.findByTextAndLocaleAndAttribute(notationText, locale, ingredientNotationLangAttributeRepository.getOne(IngredientNotationLangAttribute.Enum.NAME));
        if (!ingredientNotationLangs.isEmpty()) {
            IngredientNotationLang ingredientNotationLang = ingredientNotationLangs.get(0); //
            IngredientNotation ingredientNotation = ingredientNotationLang.getIngredientNotation();

            if (isSame(ingredientNotation, notationText, null, preparationText, locale)) {
                return ingredientNotation.getIngredient();
            }
        }
        return null;
    }

    @Transactional
    public Pair<IngredientNotation, IngredientPreparation> getPersisted(String uid, String notationText, String preparationText, Locale locale) { // NOPMD complex

        Ingredient ingredient = ingredientRepository.findByUid(uid).orElse(null);

        if (ingredient == null) {
            ingredient = getIngredientByTexts(notationText, preparationText, locale);
        }

        if (ingredient == null) {
            ShoppingCategory shoppingCategory = shoppingCategoryRepository.findFirst(); // NOTE: No ingredient taxonomy at the moment so any wil do
            IngredientNotation ingredientNotation = createIngredientNotation(ingredient, notationText, null, preparationText, locale);
            IngredientPreparation ingredientPreparation = null;
            if (!applicationConfiguration.isEntityIngredientPreparationIgnore() && StringUtils.isNotBlank(preparationText)) {
                ingredientPreparation = new IngredientPreparation()
                        .setIngredient(ingredient)
                        .setLangs(of(
                                new IngredientPreparationLang()
                                        .setAttribute(ingredientPreparationLangAttributeRepository.getOne(IngredientPreparationLangAttribute.Enum.TEXT.getId()))
                                        .setText(preparationText)
                                        .setLocale(locale)
                        ));
            }
            ingredient = new Ingredient()
                    .setUid(uid)
                    .setShoppingCategory(shoppingCategory)
                    .setIngredientNotations(of(ingredientNotation))
                    .setIngredientPreparations(ingredientPreparation == null ? new ArrayList<>() : of(ingredientPreparation));
            ingredientRepository.save(ingredient);
//            log.debug("initialize entity=" + ingredient);
            return Pair.of(ingredientNotation, ingredientPreparation);
        }

        IngredientPreparation ingredientPreparation = null;
        if (!applicationConfiguration.isEntityIngredientPreparationIgnore() && StringUtils.isNotBlank(preparationText)) {
            ingredientPreparation = ingredient.getIngredientPreparationByText(preparationText, locale);
            if (ingredientPreparation == null) {
                ingredientPreparation = ingredientPreparationRepository.save(new IngredientPreparation()
                        .setIngredient(ingredient)
                        .setLangs(of(
                                new IngredientPreparationLang()
                                        .setAttribute(ingredientPreparationLangAttributeRepository.getOne(IngredientPreparationLangAttribute.Enum.TEXT.getId()))
                                        .setText(preparationText)
                                        .setLocale(locale)
                        )));
                log.debug("initialize entity={}", ingredientPreparation);
            }
        }

        // Ingredient found by uid but no notation
        IngredientNotation ingredientNotation = ingredient.getIngredientNotationByText(notationText, locale);
        if (ingredientNotation == null) {
            ingredientNotation = ingredientNotationRepository.save(createIngredientNotation(ingredient, notationText, null, preparationText, locale));
            log.debug("initialize entity={}", ingredientNotation);
        }

        return Pair.of(ingredientNotation, ingredientPreparation);
    }

    private boolean isSame(IngredientNotation ingredientNotation, String notationText, String reserved1, String reserved2, Locale locale) {
        if (!StringUtils.equals(notationText, ingredientNotation.getLang(IngredientNotationLangAttribute.Enum.NAME, locale))) {
            return false;
        }
        if (!StringUtils.equals(reserved1, ingredientNotation.getLang(IngredientNotationLangAttribute.Enum.RESERVED, locale))) {
            return false;
        }
        return StringUtils.equals(reserved2, ingredientNotation.getLang(IngredientNotationLangAttribute.Enum.RESERVED_B, locale));
    }

}
