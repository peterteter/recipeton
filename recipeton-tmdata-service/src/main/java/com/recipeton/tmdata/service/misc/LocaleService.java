package com.recipeton.tmdata.service.misc;

/*-
 * #%L
 * recipeton-tmdata-service
 * %%
 * Copyright (C) 2021 tincomisc
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.recipeton.tmdata.config.ApplicationConfiguration;
import com.recipeton.tmdata.domain.misc.Country;
import com.recipeton.tmdata.domain.misc.Language;
import com.recipeton.tmdata.domain.misc.Locale;
import com.recipeton.tmdata.domain.misc.lang.CountryLang;
import com.recipeton.tmdata.domain.misc.lang.LanguageLang;
import com.recipeton.tmdata.service.DataInitializer;
import com.recipeton.tmdata.service.JsonResourceReaderService;
import com.recipeton.tmdata.service.misc.lang.CountryLangRepository;
import com.recipeton.tmdata.service.misc.lang.LanguageLangRepository;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.io.IOException;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@Slf4j
@Service
@AllArgsConstructor
public class LocaleService implements DataInitializer {

    private static final List<Long> LOCALE_IDS = Stream.of(Locale.Enum.en_VI, Locale.Enum.es_ES, Locale.Enum.fr_FR, Locale.Enum.de_DE, Locale.Enum.en_UK)
            .map(Locale.Enum::getId).collect(Collectors.toList());

    private final ApplicationConfiguration applicationConfiguration;
    private final CountryLangRepository countryLangRepository;
    private final CountryRepository countryRepository;
    private final JsonResourceReaderService jsonResourceReaderService;
    private final LanguageLangRepository languageLangRepository;
    private final LanguageRepository languageRepository;
    private final LocaleRepository localeRepository;

    @Override
    @Transactional
    public void doDataInitialize() throws IOException {
        boolean updated = false;

        if (countryRepository.count() < Country.DEFAULT.size()) {
            Country.DEFAULT.forEach(c -> {
                if (countryRepository.existsById(c.getId())) {
                    return;
                }
                log.debug("initialize entity={}", c);
                countryRepository.save(c);
            });
            updated = true;
        }
        if (languageRepository.count() < Language.DEFAULT.size()) {
            Language.DEFAULT.forEach(l -> {
                if (languageRepository.existsById(l.getId())) {
                    return;
                }
                log.debug("initialize entity={}", l);
                languageRepository.save(l);
            });
            updated = true;
        }

        if (localeRepository.count() < Locale.DEFAULT.size()) {
            Locale.DEFAULT.forEach(l -> {
                if (localeRepository.existsById(l.getId())) {
                    return;
                }
                Country attachedCountry = countryRepository.getOne(l.getCountry().getId());
                Language attachedLanguage = languageRepository.getOne(l.getLanguage().getId());
                log.debug("initialize entity={}", l);
                localeRepository.save(l.setCountry(attachedCountry).setLanguage(attachedLanguage));
            });
            updated = true;
        }

        if (updated) {
            countryLangRepository.deleteAll();
            jsonResourceReaderService.readCollection(applicationConfiguration.getDataDefaultCountryLangs(), CountryLangForm.class).stream()
                    .collect(Collectors.groupingBy(
                            f -> f.countryId,
                            Collectors.mapping(f -> new CountryLang()
                                    .setText(f.getText())
                                    .setLocale(localeRepository.getOne(f.localeId)), Collectors.toList())))
                    .forEach((countryId, langs) -> {
                        Country attachedCountry = countryRepository.getOne(countryId);
                        langs.forEach(l -> l.setCountry(attachedCountry));
                        countryLangRepository.saveAll(langs);
                    });

            languageLangRepository.deleteAll();
            jsonResourceReaderService.readCollection(applicationConfiguration.getDataDefaultLanguageLangs(), LanguageLangForm.class).stream()
                    .collect(Collectors.groupingBy(
                            f -> f.languageId,
                            Collectors.mapping(f -> new LanguageLang()
                                    .setText(f.getText())
                                    .setLocale(localeRepository.getOne(f.localeId)), Collectors.toList())))
                    .forEach((languageId, langs) -> {
                        Language attachedLanguage = languageRepository.getOne(languageId);
                        langs.forEach(l -> l.setLanguage(attachedLanguage));
                        languageLangRepository.saveAll(langs);
                    });
        }
    }

    public Locale getLocaleByDefault() {
        return localeRepository.getOneByLanguageAndCountry(languageRepository.getOne(applicationConfiguration.getBookLanguage()), countryRepository.getOne(applicationConfiguration.getBookCountry()));
    }

    public Locale getLocaleByLabel(String label) {
        String[] parts;
        if (label.contains("_")) {
            parts = label.split("_");
        } else if (label.contains("-")) {
            parts = label.split("-");
        } else {
            throw new IllegalArgumentException("Invalid locale label=" + label);
        }
        String languageCode = parts[0];
        String countryCode = parts[1];
        return localeRepository.getOneByLanguageAndCountry(languageRepository.getOneByCode(languageCode), countryRepository.getOneByCode(countryCode));
    }

    public List<Locale> getLocalesByDefault() {
        return localeRepository.findAllById(LOCALE_IDS);
    }

    @Data
    private static class CountryLangForm {
        private Long countryId;
        private Long localeId;
        private String text;
    }

    @Data
    private static class LanguageLangForm {
        private Long languageId;
        private Long localeId;
        private String text;
    }

}
