package com.recipeton.tmdata.service.recipe;

/*-
 * #%L
 * recipeton-tmdata-service
 * %%
 * Copyright (C) 2021 tincomisc
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.recipeton.tmdata.config.ApplicationConfiguration;
import com.recipeton.tmdata.domain.nutrition.RecipeNutritionalValueType;
import com.recipeton.tmdata.domain.nutrition.lang.RecipeNutritionLangAttribute;
import com.recipeton.tmdata.domain.nutrition.lang.RecipeNutritionalValueTypeLang;
import com.recipeton.tmdata.domain.recipe.*;
import com.recipeton.tmdata.domain.recipe.extra.RecipePathType;
import com.recipeton.tmdata.domain.recipe.extra.RecipeRecentlySyncedType;
import com.recipeton.tmdata.domain.recipe.extra.RecipeSyncStateType;
import com.recipeton.tmdata.domain.recipe.lang.*;
import com.recipeton.tmdata.service.DataInitializer;
import com.recipeton.tmdata.service.JsonResourceReaderService;
import com.recipeton.tmdata.service.misc.LocaleRepository;
import com.recipeton.tmdata.service.nutrition.RecipeNutritionalValueTypeRepository;
import com.recipeton.tmdata.service.nutrition.lang.RecipeNutritionLangAttributeRepository;
import com.recipeton.tmdata.service.nutrition.lang.RecipeNutritionalValueTypeLangRepository;
import com.recipeton.tmdata.service.recipe.extra.RecipePathTypeRepository;
import com.recipeton.tmdata.service.recipe.extra.RecipeRecentlySyncedTypeRepository;
import com.recipeton.tmdata.service.recipe.extra.RecipeSyncStateTypeRepository;
import com.recipeton.tmdata.service.recipe.lang.*;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.io.IOException;
import java.util.stream.Collectors;

@Slf4j
@Service
@AllArgsConstructor
public class RecipeDataInitializeService implements DataInitializer {

    private final ApplicationConfiguration applicationConfiguration;
    private final JsonResourceReaderService jsonResourceReaderService;
    private final LocaleRepository localeRepository;
    private final RecipeDifficultyLangRepository recipeDifficultyLangRepository;
    private final RecipeDifficultyRepository recipeDifficultyRepository;
    private final RecipeLangAttributeRepository recipeLangAttributeRepository;
    private final RecipeLinkTypeRepository recipeLinkTypeRepository;
    private final RecipeNutritionLangAttributeRepository recipeNutritionLangAttributeRepository;
    private final RecipeNutritionalValueTypeRepository recipeNutritionalValueTypeRepository;
    private final RecipeNutritionalValueTypeLangRepository recipeNutritionalValueTypeLangRepository;
    private final RecipePathTypeRepository recipePathTypeRepository;
    private final RecipePriceRepository recipePriceRepository;
    private final RecipePriceLangRepository recipePriceLangRepository;
    private final RecipeRecentlySyncedTypeRepository recipeRecentlySyncedTypeRepository;
    private final RecipeSignatureTypeRepository recipeSignatureTypeRepository;
    private final RecipeStepLangAttributeRepository recipeStepLangAttributeRepository;
    private final RecipeSyncStateTypeRepository recipeSyncStateTypeRepository;
    private final RecipeTimeTypeLangRepository recipeTimeTypeLangRepository;
    private final RecipeTimeTypeRepository recipeTimeTypeRepository;


    @Override
    @Transactional
    public void doDataInitialize() throws IOException {
        if (recipeDifficultyRepository.count() < RecipeDifficulty.DEFAULT.size()) {

            RecipeDifficulty.DEFAULT.forEach(d -> {
                if (recipeDifficultyRepository.existsById(d.getId())) {
                    return;
                }
                recipeDifficultyRepository.save(d);
            });

            recipeDifficultyLangRepository.deleteAll();
            jsonResourceReaderService.readCollection(applicationConfiguration.getDataDefaultRecipeDifficultyLangs(), RecipeDifficultyLangForm.class).stream()
                    .collect(Collectors.groupingBy(
                            f -> f.recipeDifficultyId,
                            Collectors.mapping(f -> new RecipeDifficultyLang()
                                    .setText(f.getText())
                                    .setLocale(localeRepository.getOne(f.localeId)), Collectors.toList())))
                    .forEach((recipeDifficultyId, langs) -> {
                        RecipeDifficulty recipeDifficulty = recipeDifficultyRepository.getOne(recipeDifficultyId);
                        langs.forEach(l -> l.setRecipeDifficulty(recipeDifficulty));
                        recipeDifficultyLangRepository.saveAll(langs);
                    });
        }

        if (recipePriceRepository.count() < RecipePrice.DEFAULT.size()) {

            RecipePrice.DEFAULT.forEach(d -> {
                if (recipePriceRepository.existsById(d.getId())) {
                    return;
                }
                recipePriceRepository.save(d);
            });

            recipePriceLangRepository.deleteAll();
            jsonResourceReaderService.readCollection(applicationConfiguration.getDataDefaultRecipePriceLangs(), RecipePriceLangForm.class).stream()
                    .collect(Collectors.groupingBy(
                            f -> f.recipePriceId,
                            Collectors.mapping(f -> new RecipePriceLang()
                                    .setText(f.getText())
                                    .setLocale(localeRepository.getOne(f.localeId)), Collectors.toList())))
                    .forEach((recipePriceId, langs) -> {
                        RecipePrice recipePrice = recipePriceRepository.getOne(recipePriceId);
                        langs.forEach(l -> l.setRecipePrice(recipePrice));
                        recipePriceLangRepository.saveAll(langs);
                    });
        }


        if (recipeTimeTypeRepository.count() < RecipeTimeType.DEFAULT.size()) {
            RecipeTimeType.DEFAULT.forEach(d -> {
                if (recipeTimeTypeRepository.existsById(d.getId())) {
                    return;
                }
                recipeTimeTypeRepository.save(d);
            });

            recipeTimeTypeLangRepository.deleteAll();
            jsonResourceReaderService.readCollection(applicationConfiguration.getDataDefaultRecipeTimeTypeLangs(), RecipeTimeTypeLangForm.class).stream()
                    .collect(Collectors.groupingBy(
                            f -> f.recipeTimeTypeId,
                            Collectors.mapping(f -> new RecipeTimeTypeLang()
                                    .setText(f.getText())
                                    .setLocale(localeRepository.getOne(f.localeId)), Collectors.toList())))
                    .forEach((recipeTimeTypeId, langs) -> {
                        RecipeTimeType recipeTimeType = recipeTimeTypeRepository.getOne(recipeTimeTypeId);
                        langs.forEach(l -> l.setRecipeTimeType(recipeTimeType));
                        recipeTimeTypeLangRepository.saveAll(langs);
                    });
        }

        DataInitializer.getPersisted(recipeLangAttributeRepository, RecipeLangAttribute.DEFAULT);
        DataInitializer.getPersisted(recipeStepLangAttributeRepository, RecipeStepLangAttribute.DEFAULT);

        DataInitializer.getPersisted(recipeNutritionLangAttributeRepository, RecipeNutritionLangAttribute.DEFAULT);
        if (recipeNutritionalValueTypeRepository.count() < RecipeNutritionalValueType.DEFAULT.size()) {
            RecipeNutritionalValueType.DEFAULT.forEach(d -> {
                if (recipeNutritionalValueTypeRepository.existsById(d.getId())) {
                    return;
                }
                recipeNutritionalValueTypeRepository.save(d);
            });

            recipeNutritionalValueTypeLangRepository.deleteAll();
            jsonResourceReaderService.readCollection(applicationConfiguration.getDataDefaultRecipeNutritionalValueTypeLangs(), RecipeNutritionalValueTypeLangForm.class).stream()
                    .collect(Collectors.groupingBy(
                            f -> f.recipeNutritionalValueTypeId,
                            Collectors.mapping(f -> new RecipeNutritionalValueTypeLang()
                                    .setText(f.getText())
                                    .setLocale(localeRepository.getOne(f.localeId)), Collectors.toList())))
                    .forEach((recipeNutritionalValueTypeId, langs) -> {
                        RecipeNutritionalValueType recipeNutritionalValueType = recipeNutritionalValueTypeRepository.getOne(recipeNutritionalValueTypeId);
                        langs.forEach(l -> l.setRecipeNutritionalValueType(recipeNutritionalValueType));
                        recipeNutritionalValueTypeLangRepository.saveAll(langs);
                    });
        }


        DataInitializer.getPersisted(recipeLinkTypeRepository, RecipeLinkType.DEFAULT);
        DataInitializer.getPersisted(recipePathTypeRepository, RecipePathType.DEFAULT);
        DataInitializer.getPersisted(recipeRecentlySyncedTypeRepository, RecipeRecentlySyncedType.DEFAULT);
        DataInitializer.getPersisted(recipeSignatureTypeRepository, RecipeSignatureType.DEFAULT);
        DataInitializer.getPersisted(recipeSyncStateTypeRepository, RecipeSyncStateType.DEFAULT);
    }


    @Data
    private static class RecipeNutritionalValueTypeLangForm {
        private Long recipeNutritionalValueTypeId;
        private Long localeId;
        private String text;
    }


    @Data
    private static class RecipeDifficultyLangForm {
        private Long recipeDifficultyId;
        private Long localeId;
        private String text;
    }


    @Data
    private static class RecipePriceLangForm {
        private Long recipePriceId;
        private Long localeId;
        private String text;
    }

    @Data
    private static class RecipeTimeTypeLangForm {
        private Long recipeTimeTypeId;
        private Long localeId;
        private String text;
    }
}
