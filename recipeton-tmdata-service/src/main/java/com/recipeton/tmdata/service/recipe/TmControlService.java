package com.recipeton.tmdata.service.recipe;

/*-
 * #%L
 * recipeton-tmdata-service
 * %%
 * Copyright (C) 2021 tincomisc
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.recipeton.tmdata.domain.control.*;
import com.recipeton.tmdata.domain.control.lang.TmControlLangAttribute;
import com.recipeton.tmdata.domain.misc.RangeType;
import com.recipeton.tmdata.service.DataInitializer;
import com.recipeton.tmdata.service.control.*;
import com.recipeton.tmdata.service.control.lang.TmControlLangAttributeRepository;
import com.recipeton.tmdata.service.misc.RangeTypeRepository;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.Duration;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static java.util.Arrays.asList;

@Slf4j
@Service
@AllArgsConstructor
public class TmControlService implements DataInitializer {

    private final RangeTypeRepository rangeTypeRepository;

    private final TmControlLangAttributeRepository tmControlLangAttributeRepository;
    private final TmControlProgramSoftBlendingSpeedRangeTypeRepository tmControlProgramSoftBlendingSpeedRangeTypeRepository;
    private final TmControlProgramTypeRepository tmControlProgramTypeRepository;
    private final TmControlRotationDirectionTypeRepository tmControlRotationDirectionTypeRepository;
    private final TmControlSpeedTypeRepository tmControlSpeedTypeRepository;
    private final TmControlTemperatureTypeRepository tmControlTemperatureTypeRepository;
    private final TmControlTimeTypeRepository tmControlTimeTypeRepository;
    private final TmControlTurboTypeRepository tmControlTurboTypeRepository;

    public TmControl createTmControl(Duration from, Duration to, TmControlTemperatureType.Enum temperature, TmControlSpeedType.Enum speed, TmControlRotationDirectionType.Enum direction) {
        return new TmControl()
                .setPosition(1L)
                .setTmControlTime(createTmControlTimeByRange(from, to))
                .setTmControlTemperatureType(tmControlTemperatureTypeRepository.getOne(temperature))
                .setTmControlSpeed(speed == null ? null : createTmControlSpeed(speed, direction));
    }

    public TmControl createTmControlByDough(Duration from, Duration to, TmControlTemperatureType.Enum temperatureType) {
        return new TmControl()
                .setPosition(1L)
                .setTmControlTime(createTmControlTimeByRange(from, to))
                .setTmControlTemperatureType(tmControlTemperatureTypeRepository.getOne(temperatureType))
                .setTmControlProgram(new TmControlProgram()
                        .setTmControlProgramType(tmControlProgramTypeRepository.getOne(TmControlProgramType.Enum.DOUGH.getId())));
    }

    public TmControl createTmControlBySoftBlending(Duration duration, TmControlSpeedType.Enum startSpeed, TmControlSpeedType.Enum viaSpeed, TmControlSpeedType.Enum finalSpeed, TmControlTemperatureType.Enum temperatureType) {
        return new TmControl()
                .setPosition(1L)
                .setTmControlTime(createTmControlTimeByFixedTime(duration))
                .setTmControlTemperatureType(tmControlTemperatureTypeRepository.getOne(temperatureType))
                .setTmControlProgram(new TmControlProgram()
                        .setTmControlProgramType(tmControlProgramTypeRepository.getOne(TmControlProgramType.Enum.SOFT_BLENDING.getId()))
                        .setTmControlProgramSoftBlending(
                                new TmControlProgramSoftBlending()
                                        .setDuration(duration.getSeconds())
                                        .setTmControlProgramSoftBlendingSpeedRanges(
                                                asList(
                                                        new TmControlProgramSoftBlendingSpeedRange()
                                                                .setTmControlProgramSoftBlendingSpeedRangeType(tmControlProgramSoftBlendingSpeedRangeTypeRepository.getOne(TmControlProgramSoftBlendingSpeedRangeType.Enum.START_SPEED))
                                                                .setTmControlSpeedType(tmControlSpeedTypeRepository.getOne(startSpeed.getId())),
                                                        new TmControlProgramSoftBlendingSpeedRange()
                                                                .setTmControlProgramSoftBlendingSpeedRangeType(tmControlProgramSoftBlendingSpeedRangeTypeRepository.getOne(TmControlProgramSoftBlendingSpeedRangeType.Enum.VIA_SPEED))
                                                                .setTmControlSpeedType(tmControlSpeedTypeRepository.getOne(viaSpeed.getId())),
                                                        new TmControlProgramSoftBlendingSpeedRange()
                                                                .setTmControlProgramSoftBlendingSpeedRangeType(tmControlProgramSoftBlendingSpeedRangeTypeRepository.getOne(TmControlProgramSoftBlendingSpeedRangeType.Enum.FINAL_SPEED))
                                                                .setTmControlSpeedType(tmControlSpeedTypeRepository.getOne(finalSpeed.getId()))
                                                )
                                        )
                        ));
    }

    public TmControl createTmControlByTurbo(TmControlTurboType.Enum turboType, int impulseCount, TmControlTemperatureType.Enum temperatureType) {
        return new TmControl()
                .setPosition(1L)
                .setTmControlTemperatureType(tmControlTemperatureTypeRepository.getOne(temperatureType))
                .setTmControlProgram(new TmControlProgram()
                        .setTmControlProgramType(tmControlProgramTypeRepository.getOne(TmControlProgramType.Enum.TURBO))
                        .setTmControlProgramTurbo(
                                new TmControlProgramTurbo()
                                        .setTmControlTurboType(tmControlTurboTypeRepository.getOne(turboType))
                                        .setImpulseCount((long) impulseCount)
                        ));
    }

    public TmControlSpeed createTmControlSpeed(TmControlSpeedType.Enum speed, TmControlRotationDirectionType.Enum direction) {
        return new TmControlSpeed()
                .setTmControlSpeedRanges(List.of(
                        new TmControlSpeedRange()
                                .setRangeType(rangeTypeRepository.getOne(RangeType.Enum.FROM))
                                .setTmControlSpeedType(tmControlSpeedTypeRepository.getOne(speed))
                ))
                .setTmControlRotationDirectionType(tmControlRotationDirectionTypeRepository.getOne(direction));
    }

    public TmControlSpeed createTmControlSpeed(TmControlSpeedType.Enum speedFrom, TmControlSpeedType.Enum speedTo, TmControlRotationDirectionType.Enum direction) {
        return new TmControlSpeed()
                .setTmControlSpeedRanges(List.of(
                        new TmControlSpeedRange()
                                .setRangeType(rangeTypeRepository.getOne(RangeType.Enum.FROM))
                                .setTmControlSpeedType(tmControlSpeedTypeRepository.getOne(speedFrom)),
                        new TmControlSpeedRange()
                                .setRangeType(rangeTypeRepository.getOne(RangeType.Enum.TO))
                                .setTmControlSpeedType(tmControlSpeedTypeRepository.getOne(speedTo))
                ))
                .setTmControlRotationDirectionType(tmControlRotationDirectionTypeRepository.getOne(direction));
    }

    public TmControlTime createTmControlTimeByFixedTime(Duration duration) {
        return createTmControlTimeByRange(duration, null);
    }

    public TmControlTime createTmControlTimeByRange(Duration from, Duration to) {
        return new TmControlTime()
                .setTmControlTimeType(tmControlTimeTypeRepository.getOne(from == null ? TmControlTimeType.Enum.USER_MANUAL : TmControlTimeType.Enum.RECIPE_DEFINED)) // tmControlTimeTypeRepository.getOne(TmControlTimeType.Enum.USER_DEFINED) :
                .setTmControlTimeRanges(Stream.of(
                        from == null ? null : new TmControlTimeRange()
                                .setRangeType(rangeTypeRepository.getOne(RangeType.Enum.FROM))
                                .setTime(from.getSeconds()),
                        to == null ? null : new TmControlTimeRange()
                                .setRangeType(rangeTypeRepository.getOne(RangeType.Enum.TO))
                                .setTime(to.getSeconds())
                        ).filter(Objects::nonNull)
                                .collect(Collectors.toList())
                );
    }

    public TmControlTime createTmControlTimeByUserDefined() {
        return new TmControlTime().setTmControlTimeType(tmControlTimeTypeRepository.getOne(TmControlTimeType.Enum.USER_DEFINED));
    }

    public TmControlTime createTmControlTimeByUserManually() {
        return new TmControlTime().setTmControlTimeType(tmControlTimeTypeRepository.getOne(TmControlTimeType.Enum.USER_MANUAL));
    }

    @Override
    @Transactional
    public void doDataInitialize() {
        DataInitializer.getPersisted(tmControlLangAttributeRepository, TmControlLangAttribute.DEFAULT);
        DataInitializer.getPersisted(tmControlProgramSoftBlendingSpeedRangeTypeRepository, TmControlProgramSoftBlendingSpeedRangeType.DEFAULT);
        DataInitializer.getPersisted(tmControlProgramTypeRepository, TmControlProgramType.DEFAULT);
        DataInitializer.getPersisted(tmControlRotationDirectionTypeRepository, TmControlRotationDirectionType.DEFAULT);
        DataInitializer.getPersisted(tmControlSpeedTypeRepository, TmControlSpeedType.DEFAULT);
        DataInitializer.getPersisted(tmControlTemperatureTypeRepository, TmControlTemperatureType.DEFAULT);
        DataInitializer.getPersisted(tmControlTimeTypeRepository, TmControlTimeType.DEFAULT);
        DataInitializer.getPersisted(tmControlTurboTypeRepository, TmControlTurboType.DEFAULT);
    }
}
