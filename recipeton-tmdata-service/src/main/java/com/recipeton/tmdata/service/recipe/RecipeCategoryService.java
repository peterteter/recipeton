package com.recipeton.tmdata.service.recipe;

/*-
 * #%L
 * recipeton-tmdata-service
 * %%
 * Copyright (C) 2021 tincomisc
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.recipeton.shared.util.MapUtil;
import com.recipeton.tmdata.config.ApplicationConfiguration;
import com.recipeton.tmdata.domain.material.MaterialFlag;
import com.recipeton.tmdata.domain.material.MaterialObject;
import com.recipeton.tmdata.domain.material.MaterialObjectFlag;
import com.recipeton.tmdata.domain.misc.Locale;
import com.recipeton.tmdata.domain.recipe.RecipeCategory;
import com.recipeton.tmdata.domain.recipe.RecipeCategoryMaterialObject;
import com.recipeton.tmdata.domain.recipe.lang.RecipeCategoryLang;
import com.recipeton.tmdata.domain.recipe.lang.RecipeCategoryLangAttribute;
import com.recipeton.tmdata.service.material.MaterialObjectFlagRepository;
import com.recipeton.tmdata.service.material.MaterialService;
import com.recipeton.tmdata.service.recipe.lang.RecipeCategoryLangAttributeRepository;
import com.recipeton.tmdata.service.recipe.lang.RecipeCategoryLangRepository;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.Collection;
import java.util.List;
import java.util.Optional;
import java.util.function.Predicate;
import java.util.stream.Collectors;

import static com.recipeton.shared.util.ListUtil.toListMutable;

@Slf4j
@Service
@AllArgsConstructor
public class RecipeCategoryService {

    private final ApplicationConfiguration applicationConfiguration;

    private final MaterialService materialService;
    private final MaterialObjectFlagRepository materialObjectFlagRepository;

    private final RecipeCategoryRepository recipeCategoryRepository;
    private final RecipeCategoryLangRepository recipeCategoryLangRepository;
    private final RecipeCategoryLangAttributeRepository recipeCategoryLangAttributeRepository;

    private Collection<RecipeCategoryMaterialObject> createRecipeCategoryMaterialObject(boolean primary, String fileName) throws IOException {
        if (primary && StringUtils.isBlank(fileName)) {
            throw new IllegalArgumentException("Cant create primary recipe category without material file name");
        }

        Path filePath = null;
        if (fileName != null) {
            filePath = materialService.getOrCreateMaterialSmallResource(fileName, false);
            if (!Files.exists(filePath)) {
                Path defaultCategoryIconFilePath = materialService.getOrCreateMaterialSmallResource(applicationConfiguration.getBookRecipeCategoryDefaultIconFileName(), true);
                Files.copy(defaultCategoryIconFilePath, filePath);
            }
        }

        return filePath == null ? null : toListMutable(new RecipeCategoryMaterialObject()
                .setPosition(1L)
                .setPrimary(primary)
                .setMaterialObject(new MaterialObject()
                        .setMaterialObjectFlags(toListMutable(materialObjectFlagRepository.getOne(MaterialObjectFlag.Enum.CATEGORY)))
                        .setMaterial(materialService.createMaterial(filePath, null, primary ? MaterialFlag.Enum.INTERNAL : MaterialFlag.Enum.USER_DEFINED))));
    }

    @Transactional
    public RecipeCategory getPersisted(String uid, String text, Locale locale, String fileName, boolean primary) throws IOException {
        if (uid != null) {
            Optional<RecipeCategory> recipeCategory = recipeCategoryRepository.findByUid(uid);
            if (recipeCategory.isPresent()) {
                return recipeCategory.get();
            }
        }

        RecipeCategoryLang recipeCategoryLang = recipeCategoryLangRepository.findByTextAndLocale(text, locale).orElse(null);
        if (recipeCategoryLang != null) {
            return recipeCategoryLang.getRecipeCategory();
        }

        long categoryPosition = 1L;
        long lft = categoryPosition * 2 + 1;
        long rgt = categoryPosition * 2 + 2;

        RecipeCategory recipeCategory = new RecipeCategory()
                .setUid(uid)
                .setPrimary(primary)
                .setPosition(categoryPosition)
                .setLft(lft)
                .setRgt(rgt)
                .setLangs(MapUtil.toMap(locale, text).entrySet().stream()
                        .map(e -> new RecipeCategoryLang()
                                .setAttribute(recipeCategoryLangAttributeRepository.getOne(RecipeCategoryLangAttribute.Enum.NAME))
                                .setText(e.getValue())
                                .setLocale(e.getKey()))
                        .collect(Collectors.toList()))
                .setRecipeCategoryMaterialObjects(createRecipeCategoryMaterialObject(primary, fileName));

        log.debug("saving category={}", recipeCategory);
        return recipeCategoryRepository.save(recipeCategory);
    }

    @Transactional
    public void removeRecipeCategoriesByRecipeCountGreaterThan(long count, Predicate<RecipeCategory> filter) {
        List<RecipeCategory> recipeCategories = recipeCategoryRepository.findByRecipeCountIsGreaterThan(count);
        recipeCategoryRepository.deleteAll(filter == null ? recipeCategories : recipeCategories.stream().filter(filter).collect(Collectors.toList()));
    }

    @Transactional
    public void removeRecipeCategoriesByRecipeCountLessThan(long count, Predicate<RecipeCategory> filter) {
        List<RecipeCategory> recipeCategories = recipeCategoryRepository.findByRecipeCountIsLessThan(count);
        recipeCategoryRepository.deleteAll(filter == null ? recipeCategories : recipeCategories.stream().filter(filter).collect(Collectors.toList()));
    }

    //    @Transactional
    public void updateRecipeCategoryPositions() {
        List<RecipeCategory> recipeCategories = recipeCategoryRepository.findByPrimaryOrderByUid(true);
        Long position = 1L;
        for (RecipeCategory recipeCategory : recipeCategories) {
            if (!position.equals(recipeCategory.getPosition())) {
                recipeCategory
                        .setPosition(position)
                        .setLft(position * 2 + 1)
                        .setRgt(position * 2 + 2);
                recipeCategoryRepository.save(recipeCategory);
            }
            position++;
        }
    }


}
