package com.recipeton.tmdata.service.recipe;

/*-
 * #%L
 * recipeton-tmdata-service
 * %%
 * Copyright (C) 2021 tincomisc
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.recipeton.tmdata.domain.control.*;
import com.recipeton.tmdata.domain.misc.Locale;
import com.recipeton.tmdata.domain.recipe.GuidedStep;
import com.recipeton.tmdata.domain.recipe.GuidedStepType;
import com.recipeton.tmdata.domain.recipe.GuidedTmSettingStep;
import com.recipeton.tmdata.domain.recipe.lang.GuidedTmSettingStepLang;
import com.recipeton.tmdata.domain.recipe.lang.GuidedTmSettingStepLangAttribute;
import com.recipeton.tmdata.service.recipe.lang.GuidedTmSettingStepLangAttributeRepository;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Service;

import java.time.Duration;
import java.util.Optional;

import static java.util.Arrays.asList;

@Slf4j
@Service
@AllArgsConstructor
public class GuidedTmStepService {

    private final GuidedStepTypeRepository guidedStepTypeRepository;
    private final GuidedTmSettingStepLangAttributeRepository guidedTmSettingStepLangAttributeRepository;
    private final TmControlService tmControlService;

    public GuidedStep createGuidedStepTmControl(Duration duration, TmControlTemperatureType.Enum temperature, TmControlSpeedType.Enum speed, TmControlRotationDirectionType.Enum direction, String explanationText, String executingText, Locale locale) {
        return createGuidedStepTmSetting(
                createGuidedTmSettingStep(
                        tmControlService.createTmControl(
                                duration,
                                null,
                                temperature,
                                speed,
                                direction
                        ),
                        explanationText, executingText, locale));
    }

    public GuidedStep createGuidedStepTmDough(Duration duration, String explanationText, String executingText, Locale locale) {
        return createGuidedStepTmSetting(
                createGuidedTmSettingStep(
                        tmControlService.createTmControlByDough(duration, null, TmControlTemperatureType.Enum.T0), // Possible with temp???
                        explanationText, executingText, locale));
    }

    public GuidedStep createGuidedStepTmSetting(GuidedTmSettingStep guidedTmSettingStep) {
        return new GuidedStep()
                .setPosition(-1L) // Set by organize
                .setGuidedTmSettingStep(guidedTmSettingStep)
                .setGuidedStepType(guidedStepTypeRepository.getOne(GuidedStepType.Enum.TM_SETTING));
    }

    public GuidedStep createGuidedStepTmTurbo(Duration duration, Integer repetition, String explanationText, String executingText, Locale locale) {
        return createGuidedStepTmSetting(
                createGuidedTmSettingStep(
                        tmControlService.createTmControlByTurbo(TmControlTurboType.Enum.by(duration),
                                Optional.ofNullable(repetition).orElse(1),
                                TmControlTemperatureType.Enum.T0), // Possible with temp???
                        explanationText, executingText, locale));
    }

    public GuidedTmSettingStep createGuidedTmSettingStep(TmControl tmControl, String explanationText, String executingText, Locale locale) {
        return new GuidedTmSettingStep()
                .setLangs(asList(
                        createGuidedTmSettingStepLangByBeforeMotor(explanationText, locale),
                        createGuidedTmSettingStepLangByDuring(StringUtils.isNotBlank(executingText) ? executingText : explanationText, locale)
                ))
                .setTmControl(tmControl);
    }

    // NOTE: make this getPersisted. Many messages repeat
    public GuidedTmSettingStepLang createGuidedTmSettingStepLang(GuidedTmSettingStepLangAttribute.Enum attribute, String text, Locale locale) {
        return new GuidedTmSettingStepLang()
                .setAttribute(guidedTmSettingStepLangAttributeRepository.getOne(attribute))
                .setLocale(locale)
                .setText(text);
    }

    public GuidedTmSettingStepLang createGuidedTmSettingStepLangByBeforeMotor(String text, Locale locale) {
        return createGuidedTmSettingStepLang(GuidedTmSettingStepLangAttribute.Enum.BEFORE_MOTOR_RUNNING, text, locale);
    }

    public GuidedTmSettingStepLang createGuidedTmSettingStepLangByDuring(String text, Locale locale) {
        return createGuidedTmSettingStepLang(GuidedTmSettingStepLangAttribute.Enum.MOTOR_RUNNING, text, locale);
    }
}
