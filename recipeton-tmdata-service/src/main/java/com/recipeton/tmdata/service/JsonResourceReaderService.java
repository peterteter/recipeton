package com.recipeton.tmdata.service;

/*-
 * #%L
 * recipeton-tmdata-service
 * %%
 * Copyright (C) 2021 tincomisc
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.fasterxml.jackson.databind.ObjectMapper;
import com.recipeton.shared.util.ObjectMapperFactory;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.core.io.Resource;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.io.InputStream;
import java.util.List;

@Slf4j
@Service
@AllArgsConstructor
public class JsonResourceReaderService {

    private final ObjectMapper jsonObjectMapper;

    public JsonResourceReaderService() {
        jsonObjectMapper = new ObjectMapperFactory().createInstance(false);
    }

    public <T> List<T> readCollection(Resource resource, Class<T> clazz) throws IOException {
        try (InputStream is = resource.getInputStream()) {
            return jsonObjectMapper.readValue(is, jsonObjectMapper.getTypeFactory().constructCollectionType(List.class, clazz));
        }
    }
}
