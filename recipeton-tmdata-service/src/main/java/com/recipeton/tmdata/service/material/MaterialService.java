package com.recipeton.tmdata.service.material;

/*-
 * #%L
 * recipeton-tmdata-service
 * %%
 * Copyright (C) 2021 tincomisc
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.recipeton.tmdata.config.ApplicationConfiguration;
import com.recipeton.tmdata.domain.material.*;
import com.recipeton.tmdata.domain.material.lang.MaterialObjectLangAttribute;
import com.recipeton.tmdata.domain.recipe.GuidedUtensilActionType;
import com.recipeton.tmdata.service.DataInitializer;
import com.recipeton.tmdata.service.material.lang.MaterialObjectLangAttributeRepository;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.io.FilenameUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.core.io.Resource;
import org.springframework.core.io.ResourceLoader;
import org.springframework.core.io.support.PathMatchingResourcePatternResolver;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;

import static com.recipeton.shared.util.ListUtil.toListMutable;

@Slf4j
@Service
@AllArgsConstructor
public class MaterialService implements DataInitializer {

    private final ApplicationConfiguration applicationConfiguration;
    private final MaterialFlagRepository materialFlagRepository;
    private final MaterialObjectFlagRepository materialObjectFlagRepository;
    private final MaterialObjectLangAttributeRepository materialObjectLangAttributeRepository;
    private final MaterialStorageLocationRepository materialStorageLocationRepository;
    private final MaterialStorageLocationTypeRepository materialStorageLocationTypeRepository;
    private final MaterialTypeRepository materialTypeRepository;
    private final MaterialValueTypeRepository materialValueTypeRepository;
    private final MaterialObjectRepository materialObjectRepository;
    private final ResourceLoader resourceLoader;

    private Path createDirectories(Path path) throws IOException {
        if (!Files.exists(path)) {
            Files.createDirectories(path);
        }
        return path;
    }

    public Material createMaterial(Path filePath, Long mdbId, MaterialFlag.Enum flags) {
        return new Material()
                .setMaterialtype(materialTypeRepository.getOne(MaterialType.Enum.PHOTO))
                .setMaterialFlags(flags == null ? null : materialFlagRepository.getAllById(flags))
                .setMdbId(mdbId)
                .setMaterialStorageLocations(toListMutable(getPersistedMaterialStorageLocation(filePath.getFileName().toString())));
    }

    @Override
    @Transactional
    public void doDataInitialize() throws IOException {
        DataInitializer.getPersisted(materialFlagRepository, MaterialFlag.DEFAULT);
        DataInitializer.getPersisted(materialObjectFlagRepository, MaterialObjectFlag.DEFAULT);
        DataInitializer.getPersisted(materialObjectLangAttributeRepository, MaterialObjectLangAttribute.DEFAULT);
        DataInitializer.getPersisted(materialStorageLocationTypeRepository, MaterialStorageLocationType.DEFAULT);
        DataInitializer.getPersisted(materialTypeRepository, MaterialType.DEFAULT);
        DataInitializer.getPersisted(materialValueTypeRepository, MaterialValueType.DEFAULT);

        MaterialType photoMaterialType = materialTypeRepository.getOne(MaterialType.Enum.PHOTO);
        if (photoMaterialType.getMaterialValueTypes().isEmpty()) {
            photoMaterialType.getMaterialValueTypes().add(materialValueTypeRepository.getOne(MaterialValueType.Enum.CROP_LEFT));
            photoMaterialType.getMaterialValueTypes().add(materialValueTypeRepository.getOne(MaterialValueType.Enum.CROP_TOP));
            photoMaterialType.getMaterialValueTypes().add(materialValueTypeRepository.getOne(MaterialValueType.Enum.CROP_HEIGHT));
            photoMaterialType.getMaterialValueTypes().add(materialValueTypeRepository.getOne(MaterialValueType.Enum.CROP_WIDTH));
        }

        createDirectories(applicationConfiguration.getBookAssetsPath());
        createDirectories(applicationConfiguration.getBookAssetsThemeBrightPath());

        for (Resource resource : new PathMatchingResourcePatternResolver(resourceLoader).getResources(applicationConfiguration.getDataDefaultAssetPathLocationPattern())) {
            Path resourceAbsoluteSubPath = Path.of(StringUtils.substringAfter(resource.getURI().toString(), applicationConfiguration.getDataDefaultAssetPath()));
            Path targetFile = applicationConfiguration.getBookAssetsPath().resolve(resourceAbsoluteSubPath.getRoot().relativize(resourceAbsoluteSubPath));
            if (!FilenameUtils.getExtension(targetFile.getFileName().toString()).isEmpty() && !Files.exists(targetFile)) {
                Files.createDirectories(targetFile.getParent());
                Files.copy(resource.getInputStream(), targetFile);
            }
        }

//        Path defaultLogoSplash = getOrCreateMaterialLargeResource(applicationConfiguration.getBookSplashDefaultLogoFileName(), applicationConfiguration.isFailOnBookAssetUnresolved());
//        log.debug("Material default logo path={}", defaultLogoSplash);
        Path defaultBookSplash = getOrCreateMaterialLargeResource(applicationConfiguration.getBookSplashDefaultFileName(), applicationConfiguration.isFailOnBookAssetUnresolved());
        log.debug("Material default book path={}", defaultBookSplash);
    }

    public Path getOrCreateMaterialLargeResource(String fileName, boolean verifyExists) throws IOException {
        return getOrCreateMaterialResource(fileName, applicationConfiguration.getBookImageLargeDimensions(), verifyExists);
    }

    public Path getOrCreateMaterialResource(String fileName, ApplicationConfiguration.ImageDimensions imageDimensions, boolean verifyExists) throws IOException {
        Path resolve = applicationConfiguration.getBookAssetsPath().resolve(fileName);
        if (verifyExists && !Files.exists(resolve)) {
            throw new IOException("Expect file to exist at " + resolve + ". NOTE: LOOKUP IN IMPORT AND COPY RESIZING!! " + imageDimensions);
        }
        return resolve;
    }

    public Path getOrCreateMaterialSmallResource(String fileName, boolean verifyExists) throws IOException {
        return getOrCreateMaterialResource(fileName, applicationConfiguration.getBookImageSmallDimensions(), verifyExists);
    }

    public MaterialObject getPersisted(GuidedUtensilActionType.Enum type) {
        return materialObjectRepository.findFirstByMaterialStorageLocationSource(type.getDefaultIcon()).orElseGet(
                () -> materialObjectRepository.save(new MaterialObject().setMaterial(createMaterial(Path.of(type.getDefaultIcon()), null, null)))
        );
    }

    public MaterialStorageLocation getPersistedMaterialStorageLocation(String source) {
        return materialStorageLocationRepository.findFirstBySource(source)
                .orElseGet(
                        () -> materialStorageLocationRepository.save(new MaterialStorageLocation()
                                .setMaterialStorageLocationType(materialStorageLocationTypeRepository.getOne(MaterialStorageLocationType.Enum.FILENAME))
                                .setSource(source)
                        ));
    }
}
