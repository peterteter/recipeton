package com.recipeton.tmdata.service.recipe;

/*-
 * #%L
 * recipeton-tmdata-service
 * %%
 * Copyright (C) 2021 tincomisc
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.recipeton.shared.analysis.domain.*;
import com.recipeton.shared.analysis.service.RecipeAnalyzer;
import com.recipeton.shared.domain.RecipeDefinition;
import com.recipeton.shared.service.RecipeCategoryDefinitionUtil;
import com.recipeton.shared.service.RecipeDefinitionStorageService;
import com.recipeton.tmdata.config.ApplicationConfiguration;
import com.recipeton.tmdata.domain.control.TmControlRotationDirectionType;
import com.recipeton.tmdata.domain.control.TmControlSpeedType;
import com.recipeton.tmdata.domain.control.TmControlTemperatureType;
import com.recipeton.tmdata.domain.misc.Locale;
import com.recipeton.tmdata.domain.misc.TmVersion;
import com.recipeton.tmdata.domain.recipe.*;
import com.recipeton.tmdata.domain.recipe.lang.RecipeLang;
import com.recipeton.tmdata.domain.recipe.lang.RecipeLangAttribute;
import com.recipeton.tmdata.domain.unit.UnitNotation;
import com.recipeton.tmdata.domain.unit.UnitNotationPriority;
import com.recipeton.tmdata.domain.unit.UnitType;
import com.recipeton.tmdata.domain.utensil.UtensilType;
import com.recipeton.tmdata.service.ImageService;
import com.recipeton.tmdata.service.misc.LocaleService;
import com.recipeton.tmdata.service.misc.TmVersionRepository;
import com.recipeton.tmdata.service.nutrition.RecipeNutritionService;
import com.recipeton.tmdata.service.recipe.lang.RecipeLangAttributeRepository;
import com.recipeton.tmdata.service.unit.UnitService;
import com.recipeton.tmdata.service.utensil.UtensilService;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.io.FilenameUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.tuple.Pair;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.io.IOException;
import java.math.BigDecimal;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static com.recipeton.shared.analysis.domain.RecipeAnalysisTag.*;
import static com.recipeton.shared.analysis.domain.RecipeUnitDefinition.UNIT_KCAL;
import static com.recipeton.shared.service.RecipeCategoryDefinitionUtil.RECIPE_CATEGORY_EXTRA_PREFIX;
import static com.recipeton.shared.util.StreamUtil.toStream;
import static com.recipeton.shared.util.ThrowingHelper.uFunction;
import static java.util.List.of;

@Slf4j
@Service
@AllArgsConstructor
public class RecipeImportService {

    public static final double CAL_TO_KJ = 4.184; // Move to unit
    public static final double KJ_TO_CAL = 0.239;
    public static final long YEAR = 2015L;

    private final ApplicationConfiguration applicationConfiguration;
    private final GuidedFreetextStepService guidedFreetextStepService;
    private final GuidedIngredientStepService guidedIngredientStepService;
    private final GuidedTmStepService guidedTmStepService;
    private final GuidedUtensilStepService guidedUtensilStepService;
    private final ImageService imageService;
    private final List<RecipeAnalyzer> recipeAnalyzers;
    private final LocaleService localeService;
    private final RecipeCategoryService recipeCategoryService;
    private final RecipeDifficultyRepository recipeDifficultyRepository;
    private final RecipeGroupRepository recipeGroupRepository;
    private final RecipeIngredientService recipeIngredientService;
    private final RecipeLangAttributeRepository recipeLangAttributeRepository;
    private final RecipeNutritionService recipeNutritionService;
    private final RecipePriceRepository recipePriceRepository;
    private final RecipeRepository recipeRepository;
    private final RecipeService recipeService;
    private final TmVersionRepository tmVersionRepository;
    private final UnitService unitService;
    private final UtensilService utensilService;
    private final RecipeDefinitionStorageService recipeDefinitionStorageService;

    private void addPlaceMeasuringCupStep(RecipeAnalyzer recipeAnalyzer, Locale locale, List<GuidedStep> guidedSteps) {
        if (applicationConfiguration.isImportAddMeasuringCupReminderBeforeOperation()) {
//            log.warn("Disabled for now. Needs to check if Do not place measuring cup kind of message has happened in current or maybe prev step.
//            Should place this in analyzer and tag the command step accordingly ");
            guidedSteps.add(guidedUtensilStepService.createGuidedStepUtensil(GuidedUtensilActionType.Enum.MEASURING_CUP_PUT, recipeAnalyzer.getMeasuringCupPutMessage(), locale));
        }
    }

    private void addRecipeCategoryProblem(Recipe recipe, RecipeAnalyzer recipeAnalyzer, Locale locale) {
        try {
            Pair<String, String> zzzProblem = recipeAnalyzer.getRecipeCategoryUidNamePair("Zzz Problems", true);
            if (recipe.getRecipeCategories().stream().noneMatch(rc -> zzzProblem.getLeft().equals(rc.getUid()))) {
                recipe.getRecipeCategories().add(recipeCategoryService.getPersisted(zzzProblem.getLeft(), zzzProblem.getRight(), locale, applicationConfiguration.getBookRecipeCategoryDefaultIconFileName(), true));
            }
        } catch (IOException e) {
            throw new RecipeImportUncheckedException("Adding warn category failed!", e);
        }
    }

    @Transactional
    public Pair<ImportRecipeResult, Long> doImportRecipe(Path recipePath, Path picturePath) throws IOException {
        log.info("Importing recipe path={}, picture={}", recipePath, picturePath);
        RecipeDefinition recipeDefinition = recipeDefinitionStorageService.read(recipePath);

        if (applicationConfiguration.getImportDeviceNameFilter() != null && !recipeDefinition.getDevices().contains(applicationConfiguration.getImportDeviceNameFilter())) {
            log.info("Skipping. Not supported device.path={}", recipePath);
            return Pair.of(ImportRecipeResult.SKIP, null);
        }

        String recipeId = FilenameUtils.getBaseName(recipePath.getFileName().toString());
        String uid = recipeId;
        recipeRepository.findFirstByUid(uid).ifPresent(
                r -> {
                    log.info("Found existing recipe with same uid. Replacing uid={}, path={}", uid, recipePath);
                    recipeRepository.delete(r);
                }
        );

        RecipeAnalyzer recipeAnalyzer = recipeAnalyzers.stream()
                .filter(ra -> ra.isHandlerOf(recipeDefinition))
                .findFirst()
                .orElseThrow(() -> new RecipeImportUncheckedException("Could not find valid analyzer for recipe={} in analyzers={},", recipeDefinition, recipeAnalyzers));

        Locale locale = localeService.getLocaleByLabel(recipeAnalyzer.analyzeLocale(recipeDefinition));

        RecipeMeasurement recipeYield = recipeAnalyzer.analyzeRecipeMeasurement(recipeDefinition.getRecipeYield());
        UnitNotation servingUnitNotation = toRecipeServingUnitNotation(recipeYield, locale);

        long mdbId = System.nanoTime(); // whatever for now

        RecipeMeasurement caloriesMeasurement = recipeAnalyzer.analyzeRecipeMeasurement(recipeDefinition.getNutrition().getCalories());
        BigDecimal calories;
        BigDecimal caloriesKcal;
        if (caloriesMeasurement == null || caloriesMeasurement.getMagnitudeValue() == null) {
            calories = null;
            caloriesKcal = null;
        } else {
            if (UNIT_KCAL.equalsIgnoreCase(caloriesMeasurement.getUnit())) {
                caloriesKcal = caloriesMeasurement.getMagnitudeValue();
                calories = new BigDecimal((int) (caloriesMeasurement.getMagnitudeValue().intValue() * CAL_TO_KJ));
            } else {
                calories = caloriesMeasurement.getMagnitudeValue();
                caloriesKcal = new BigDecimal((int) (caloriesMeasurement.getMagnitudeValue().intValue() * KJ_TO_CAL));
            }
        }

        log.info("Importing recipe name={}, configId={}", recipeDefinition.getName(), recipeAnalyzer.getConfiguration().getId());

        Recipe recipe = new Recipe()
                .setUid(uid)
                .setOriginalTitle(StringUtils.trim(recipeDefinition.getName()))
                .setRecipeCategories(toRecipeCategories(recipeDefinition, recipeAnalyzer, locale))
                .setLangs(toRecipeLangs(uid, recipeDefinition, recipeAnalyzer, locale))
                .setRecipeMaterialObject(recipeService.createRecipeMaterialObjects(picturePath, mdbId))
                .setRecipeIngredients(new ArrayList<>())
                .setRecipeSteps(new ArrayList<>())
                .setRecipePrice(recipePriceRepository.getOne(RecipePrice.Enum.by(recipeAnalyzer.analyzeRecipePrice(recipeDefinition))))
                .setRecipeDifficulty(recipeDifficultyRepository.getOne(RecipeDifficulty.Enum.by(recipeAnalyzer.analyzeRecipeDifficulty(recipeDefinition))))
                .setRecipeUtensils(toRecipeUtensils(recipeDefinition.getUtensils(), recipeAnalyzer, locale))
                .setRecipeTime(Stream.of(
                        recipeService.createRecipeTime(RecipeTimeType.Enum.TOTAL, recipeDefinition.getTotalTime(), null, locale),
                        recipeService.createRecipeTime(RecipeTimeType.Enum.BAKE, recipeDefinition.getCookTime(), null, locale),
                        recipeService.createRecipeTime(RecipeTimeType.Enum.PREP, recipeDefinition.getPrepTime(), null, locale)
                ).filter(Objects::nonNull).collect(Collectors.toSet()))
                .setRecipeTmVersions(toRecipeTmVersions(recipeDefinition.getDevices()))
                .setServingQuantity(Optional.ofNullable(recipeYield.getMagnitudeValue()).orElse(BigDecimal.ONE))
                .setServingUnitNotation(servingUnitNotation)
                .setRecipeNutritionalValue(recipeNutritionService.createRecipeNutritionalValue(
                        calories,
                        recipeAnalyzer.analyzeRecipeMeasurementValue(recipeDefinition.getNutrition().getProteinContent()),
                        recipeAnalyzer.analyzeRecipeMeasurementValue(recipeDefinition.getNutrition().getCarbohydrateContent()),
                        recipeAnalyzer.analyzeRecipeMeasurementValue(recipeDefinition.getNutrition().getFatContent()),
                        recipeAnalyzer.analyzeRecipeMeasurementValue(recipeDefinition.getNutrition().getCholesterolContent()),
                        recipeAnalyzer.analyzeRecipeMeasurementValue(recipeDefinition.getNutrition().getFibreContent()),
                        caloriesKcal,
                        servingUnitNotation, locale))
                .setRecipeGroups(new ArrayList<>())
                .setVersion(BigDecimal.ZERO) //
                .setYear(YEAR) // NOTE: Use analyzer to extract
                .setDeleted(false);

        recipe.setRecipeSteps(recipeDefinition.getRecipeSections().stream()
                .flatMap(rs -> toRecipeSteps(rs, recipe, recipeAnalyzer, locale).stream())
                .collect(Collectors.toList()));

        log.debug("Importing recipe. prepare picture. uid={}", uid);
        if (Files.exists(picturePath)) {
            ApplicationConfiguration.ImageDimensions targetImageDimensions = applicationConfiguration.getBookImageSmallDimensions();
            imageService.resize(picturePath, applicationConfiguration.getBookAssetsPath().resolve(picturePath.getFileName()), "jpg", targetImageDimensions.getWidth(), targetImageDimensions.getHeight());
        }
        log.debug("Importing recipe. Saving. uid={}", uid);
        return Pair.of(ImportRecipeResult.SUCCESS, recipeService.prepareAndSave(recipe, locale).getId());
    }

    public List<RecipeCategory> toRecipeCategories(RecipeDefinition recipeDefinition, RecipeAnalyzer recipeAnalyzer, Locale locale) {
        Map<String, String> categoryUidNameMap = new HashMap<>();

        toStream(recipeDefinition.getRecipeCategory())
                .map(proposedName -> recipeAnalyzer.getRecipeCategoryUidNamePair(proposedName, true))
                .filter(Objects::nonNull)
                .forEach(p -> {
                    String uid = Optional.of(p.getLeft()).filter(RecipeCategoryDefinitionUtil::isRecipeCategoryUidPrincipal)
                            .orElseGet(() -> RecipeCategoryDefinitionUtil.createRecipeCategoryUidByPrincipal(p.getLeft()));
                    String name = p.getRight();
                    if (StringUtils.isNotBlank(uid) && !categoryUidNameMap.containsKey(uid)) {
                        categoryUidNameMap.put(uid, name);
                    }
                });

        if (applicationConfiguration.isImportRecipeNamePrefixAsPrimaryRecipeCategories()) {
            String title = recipeAnalyzer.analyzeTitle(recipeDefinition);
            String firstChars = title.substring(0, 2).trim();
            categoryUidNameMap.put("pr_" + firstChars, RECIPE_CATEGORY_EXTRA_PREFIX + firstChars);
        }

        if (applicationConfiguration.isImportRecipeKeywordsAsPrimaryRecipeCategories()) {
            toStream(recipeDefinition.getKeywords())
                    .map(proposedName -> recipeAnalyzer.getRecipeCategoryUidNamePair(proposedName, false))
                    .filter(Objects::nonNull)
                    .forEach(p -> {
                        String uid = p.getLeft();
                        String name = p.getRight();
                        if (StringUtils.isNotBlank(uid) && !categoryUidNameMap.containsKey(uid)) {
                            categoryUidNameMap.put(uid, name);
                        }
                    });
        }

        return categoryUidNameMap.entrySet().stream()
                .map(uFunction(e -> recipeCategoryService.getPersisted(e.getKey(), e.getValue(), locale, "CATEGORY_" + e.getKey() + ".png", true)))
                .distinct()
                .collect(Collectors.toList());
    }

    private RecipeIngredient toRecipeIngredient(RecipeIngredientAnalysis recipeIngredientAnalysis, RecipeGroup recipeGroup, Locale locale) {
        return recipeIngredientService.createRecipeIngredient(
                recipeIngredientAnalysis.getUid(),
                recipeIngredientAnalysis.getIngredientNotation(),
                recipeIngredientAnalysis.getPreparation(),
                recipeIngredientAnalysis.getMagnitudeFromValue(),
                recipeIngredientAnalysis.getMagnitudeToValue(),
                recipeIngredientAnalysis.getUnit(), locale, recipeGroup, recipeIngredientAnalysis.isOptional());
    }

    public List<RecipeLang> toRecipeLangs(String uid, RecipeDefinition recipeDefinition, RecipeAnalyzer recipeAnalyzer, Locale locale) {
        String title = recipeAnalyzer.analyzeTitle(recipeDefinition);
        List<String> hints = recipeDefinition.getHints();
        CharSequence backGround = recipeAnalyzer.toBackgroundReport(uid, recipeDefinition);
        return Stream.of(
                new RecipeLang()
                        .setAttribute(recipeLangAttributeRepository.getOne(RecipeLangAttribute.Enum.TITLE))
                        .setText(title)
                        .setLocale(locale),
                hints == null ? null : new RecipeLang()
                        .setAttribute(recipeLangAttributeRepository.getOne(RecipeLangAttribute.Enum.TIP))
                        .setText(String.join(", ", hints))
                        .setLocale(locale),
                backGround.length() == 0 ? null : new RecipeLang()
                        .setAttribute(recipeLangAttributeRepository.getOne(RecipeLangAttribute.Enum.BACKGROUND))
                        .setText(backGround.toString())
                        .setLocale(locale)
        ).filter(Objects::nonNull)
                .collect(Collectors.toList());
    }

    public UnitNotation toRecipeServingUnitNotation(RecipeMeasurement recipeMeasurement, Locale locale) {
        return unitService.getPersisted(recipeMeasurement.getUnit(), locale, UnitNotationPriority.Enum.RECIPE_SPECIFIC, UnitType.Enum.PORTION);
    }

    private List<RecipeStep> toRecipeSteps(RecipeDefinition.RecipeSection recipeSection, Recipe recipe, RecipeAnalyzer recipeAnalyzer, Locale locale) {  // NOPMD Clear view
        log.debug("Importing recipeSection name={}", recipeSection.getName());

        RecipeGroup recipeGroup = RecipeGroup.of(recipeAnalyzer.getRecipeInstructionAnalyzer().cleanInstruction(recipeSection.getName()), locale); // Each section is a group
        recipe.getRecipeGroups().add(recipeGroup);

        List<RecipeIngredientAnalysis> recipeIngredientAnalyses = recipeAnalyzer.getRecipeIngredientAnalyzer().toRecipeIngredientAnalyses(recipeSection.getIngredients());
        List<RecipeInstructionAnalysis> recipeInstructionAnalyses = recipeAnalyzer.getRecipeInstructionAnalyzer().analyze(recipeSection.getInstructions(), recipeIngredientAnalyses, applicationConfiguration.isImportAnalysisStrictSuper());

        List<RecipeStep> recipeSteps = new ArrayList<>();

        List<RecipeIngredientAnalysis> ingredientsByUnmatched = recipeAnalyzer.findIngredientsByUnmatched(recipeIngredientAnalyses, recipeInstructionAnalyses);
        boolean ingredientsUnmatchedPresent = !ingredientsByUnmatched.isEmpty();
        if (ingredientsUnmatchedPresent) {
            if (applicationConfiguration.isImportAnalysisStrict()) {
                throw new RecipeImportUncheckedException("Could not find essential ingredient in analysis. missingIngredients={}", ingredientsByUnmatched);
            }
            log.warn("Could not find essential ingredient in analysis. missingIngredients={}", ingredientsByUnmatched);
            addRecipeCategoryProblem(recipe, recipeAnalyzer, locale);

            Pair<String, String> report = recipeAnalyzer.toRecipeIngredientUnmatchedReport(ingredientsByUnmatched);
            recipeSteps.add(recipeService.createRecipeStep(report.getLeft(), recipeGroup, locale,
                    of(guidedFreetextStepService.createGuidedStepFreetext(GuidedFreetextType.Enum.DEFAULT, report.getRight(), locale))));
        }

        Set<RecipeIngredientAnalysis> unusedRecipeIngredientAnalyses = new HashSet<>(recipeIngredientAnalyses);
        for (RecipeInstructionAnalysis ria : recipeInstructionAnalyses) {
            log.debug("Importing recipeSectionStep ria={}", ria);
            if (ria.isTagPresent(PARALLEL) && ria.isTagPresentInPrev(COMMAND)) {
                log.debug("Importing recipeSectionStep PARALLEL should have been handled by previous COMMAND step.");
                continue;
            }

            String stepText = ria.getText();
            String explanationText = recipeAnalyzer.getRecipeInstructionAnalyzer().getTextAsHumanReadable(ria);
            List<GuidedStep> guidedSteps = new ArrayList<>(); // NOPMD
            for (RecipeTerm recipeTerm : recipeAnalyzer.getRecipeInstructionAnalyzer().getTermsSorted(ria)) {

                if (recipeTerm instanceof RecipeIngredientTerm) {
                    // When multi term will only consider one of the matches for the same ingredient
                    RecipeIngredientTerm recipeIngredientTerm = (RecipeIngredientTerm) recipeTerm;
                    RecipeIngredientAnalysis recipeIngredientAnalysis = recipeIngredientTerm.getIngredient();

                    if (ria.isTagsAllPresent(REF_IN_MAIN_COMPARTMENT, ADD)) { // recipeIngredientAnalysis.getAlternative() Find out how to declare alternative!
                        log.debug("Importing recipeSectionSubStep ingredientAdd term={}", recipeIngredientTerm);
                        GuidedIngredientStepWeighingAttribute.Enum weighingAttribute = recipeIngredientAnalysis.isWeighted() ? GuidedIngredientStepWeighingAttribute.Enum.SHOW_SCALE_AND_MIXING_BOWL : GuidedIngredientStepWeighingAttribute.Enum.DO_NOT_SHOW_SCALE;
                        guidedSteps.add(guidedIngredientStepService.createGuidedStepIngredient(toRecipeIngredient(recipeIngredientAnalysis, recipeGroup, locale), recipeIngredientAnalysis.getMagnitudeFromValue(), recipeIngredientAnalysis.getMagnitudeToValue(), guidedIngredientStepService.getPersistedGuidedIngredientStepText(GuidedIngredientStepTextAttribute.Enum.ADD), weighingAttribute));
                        unusedRecipeIngredientAnalyses.remove(recipeIngredientAnalysis);
                    } else if (ria.isTagsAllPresent(REF_IN_MAIN_COMPARTMENT, PLACE)) {
                        log.debug("Importing recipeSectionSubStep ingredientPlace term={}", recipeIngredientTerm);
                        GuidedIngredientStepWeighingAttribute.Enum weighingAttribute = recipeIngredientAnalysis.isWeighted() ? GuidedIngredientStepWeighingAttribute.Enum.SHOW_SCALE_AND_MIXING_BOWL : GuidedIngredientStepWeighingAttribute.Enum.DO_NOT_SHOW_SCALE;
                        guidedSteps.add(guidedIngredientStepService.createGuidedStepIngredient(toRecipeIngredient(recipeIngredientAnalysis, recipeGroup, locale), recipeIngredientAnalysis.getMagnitudeFromValue(), recipeIngredientAnalysis.getMagnitudeToValue(), guidedIngredientStepService.getPersistedGuidedIngredientStepText(GuidedIngredientStepTextAttribute.Enum.PLACE), weighingAttribute));
                        unusedRecipeIngredientAnalyses.remove(recipeIngredientAnalysis);
                    } else if (ria.isTagsAllPresent(REF_IN_MAIN_COMPARTMENT, WEIGH)) {
                        log.debug("Importing recipeSectionSubStep ingredientWeighInMain term={}", recipeIngredientTerm);
                        GuidedIngredientStepWeighingAttribute.Enum weighingAttribute = recipeIngredientAnalysis.isWeighted() ? GuidedIngredientStepWeighingAttribute.Enum.SHOW_SCALE_AND_MIXING_BOWL : GuidedIngredientStepWeighingAttribute.Enum.DO_NOT_SHOW_SCALE;
                        guidedSteps.add(guidedIngredientStepService.createGuidedStepIngredient(toRecipeIngredient(recipeIngredientAnalysis, recipeGroup, locale), recipeIngredientAnalysis.getMagnitudeFromValue(), recipeIngredientAnalysis.getMagnitudeToValue(), guidedIngredientStepService.getPersistedGuidedIngredientStepText(GuidedIngredientStepTextAttribute.Enum.WEIGHT_OUTSIDE, false, explanationText, locale), weighingAttribute));
                        unusedRecipeIngredientAnalyses.remove(recipeIngredientAnalysis); // Ingredient used
                    } else if (ria.isTagsAllPresent(REF_TOP_OF_MAIN_COMPARTMENT, WEIGH)) {
                        log.debug("Importing recipeSectionSubStep ingredientWeighOnTop term={}", recipeIngredientTerm);
                        GuidedIngredientStepWeighingAttribute.Enum weighingAttribute = recipeIngredientAnalysis.isWeighted() ? GuidedIngredientStepWeighingAttribute.Enum.SHOW_SCALE : GuidedIngredientStepWeighingAttribute.Enum.DO_NOT_SHOW_SCALE;
                        guidedSteps.add(guidedIngredientStepService.createGuidedStepIngredient(toRecipeIngredient(recipeIngredientAnalysis, recipeGroup, locale), recipeIngredientAnalysis.getMagnitudeFromValue(), recipeIngredientAnalysis.getMagnitudeToValue(), guidedIngredientStepService.getPersistedGuidedIngredientStepText(GuidedIngredientStepTextAttribute.Enum.WEIGHT_OUTSIDE, false, explanationText, locale), weighingAttribute));
                        unusedRecipeIngredientAnalyses.remove(recipeIngredientAnalysis); // Ingredient used
                    }
                } else if (recipeTerm instanceof RecipeCommandTerm) {
                    RecipeCommandTerm recipeCommandTerm = (RecipeCommandTerm) recipeTerm;
                    String executingText = applicationConfiguration.isImportStepCommandExecuteTextFull() ? explanationText : StringUtils.substringBefore(explanationText, recipeCommandTerm.getText()).trim();
                    if (recipeCommandTerm.getProgram() == null) {
                        log.debug("Importing recipeSectionSubStep commandStandard term={}", recipeCommandTerm);
                        addPlaceMeasuringCupStep(recipeAnalyzer, locale, guidedSteps);
                        guidedSteps.add(guidedTmStepService.createGuidedStepTmControl(recipeCommandTerm.getDuration(),
                                TmControlTemperatureType.Enum.byValue(recipeCommandTerm.getTemperature()),
                                TmControlSpeedType.Enum.byValue(recipeCommandTerm.getSpeed()),
                                TmControlRotationDirectionType.Enum.by(recipeCommandTerm.getRotation()),
                                explanationText, executingText, locale));
                    } else {
                        switch (recipeCommandTerm.getProgram()) {
                            case TURBO:
                                log.debug("Importing recipeSectionSubStep commandTurbo term={}", recipeCommandTerm);
                                addPlaceMeasuringCupStep(recipeAnalyzer, locale, guidedSteps);
                                guidedSteps.add(guidedTmStepService.createGuidedStepTmTurbo(recipeCommandTerm.getDuration(), recipeCommandTerm.getRepetitionFrom(), explanationText, executingText, locale));
                                break;
                            case DOUGH:
                                log.debug("Importing recipeSectionSubStep commandDough term={}", recipeCommandTerm);
                                addPlaceMeasuringCupStep(recipeAnalyzer, locale, guidedSteps);
                                guidedSteps.add(guidedTmStepService.createGuidedStepTmDough(recipeCommandTerm.getDuration(), explanationText, executingText, locale));
                                break;
                            case SOUS_VIDE:
                            case FERMENT:
                            case HEAT_ONLY:
                            default:
                                log.warn("Importing recipeSectionSubStep command not managed. Ignoring. term={}", recipeCommandTerm);
                                break;
                        }
                    }

                    boolean commandAdded = !guidedSteps.isEmpty();
                    if (commandAdded && ria.isTagPresentInNext(PARALLEL)) {
                        RecipeInstructionAnalysis nextRia = ria.getNext();
                        stepText = stepText + ". " + nextRia.getText(); // NOPMD
                        String nextExplanationText = recipeAnalyzer.getRecipeInstructionAnalyzer().getTextAsHumanReadable(nextRia);
                        guidedSteps.add(guidedFreetextStepService.createGuidedStepFreetext(GuidedFreetextType.Enum.PARALLEL, nextExplanationText, locale));
                    }

                } else if (recipeTerm instanceof RecipeUtensilActionTerm) {
                    RecipeUtensilActionTerm recipeUtensilActionTerm = (RecipeUtensilActionTerm) recipeTerm;
                    log.debug("Importing recipeSectionSubStep utensil term={}", recipeUtensilActionTerm);
                    guidedSteps.add(guidedUtensilStepService.createGuidedStepUtensil(GuidedUtensilActionType.Enum.by(recipeUtensilActionTerm.getTag()), explanationText, locale));
                    continue;
                }
            }

            if (guidedSteps.isEmpty() && ria.isTagPresent(PARALLEL)) {
                log.warn("Importing recipeSectionSubStep textParallel. No previous command present that handled this. Maybe bad splitting or command detection?. ria={}", ria);
                guidedSteps.add(guidedFreetextStepService.createGuidedStepFreetext(GuidedFreetextType.Enum.PARALLEL, explanationText, locale));
            }

            if (guidedSteps.isEmpty() && ria.isTagPresent(SERVING)) {
                log.debug("Importing recipeSectionSubStep textServing");
                guidedSteps.add(guidedFreetextStepService.createGuidedStepFreetext(GuidedFreetextType.Enum.SERVING, explanationText, locale));
            }

            if (guidedSteps.isEmpty()) {
                log.debug("Importing recipeSectionSubStep textOther");
                guidedSteps.add(guidedFreetextStepService.createGuidedStepFreetext(GuidedFreetextType.Enum.DEFAULT, explanationText, locale));
            }

            recipeSteps.add(recipeService.createRecipeStep(stepText, recipeGroup, locale, guidedSteps));
        }

        for (RecipeIngredientAnalysis unusedRecipeIngredientAnalysis : unusedRecipeIngredientAnalyses) {
            RecipeIngredient recipeIngredient = toRecipeIngredient(unusedRecipeIngredientAnalysis, recipeGroup, locale);
            recipe.getRecipeIngredients().add(recipeIngredient);
        }

        return recipeSteps;
    }

    public List<RecipeTmVersion> toRecipeTmVersions(List<String> devices) {
        return of(
                new RecipeTmVersion().setTmVersion(tmVersionRepository.getOne(TmVersion.Enum.APP))
//                tmVersionRepository.getOne(TmVersion.Enum.TM31),
        );
    }

    public List<RecipeUtensil> toRecipeUtensils(List<String> utensils, RecipeAnalyzer recipeAnalyzer, Locale locale) {
        if (utensils == null) {
            return null;
        }
        return utensils.stream()
                .map(recipeAnalyzer::analyzeUtensil)
                .filter(Objects::nonNull)
                .map(u -> new RecipeUtensil()
                        .setPosition(1L)
                        .setUtensil(utensilService.getPersisted(u.getUid(), u.getText(), null, UtensilType.Enum.KITCHEN_EQUIPMENT, locale))
                )
                .distinct()
                .collect(Collectors.toList());
    }

    public enum ImportRecipeResult {
        SUCCESS, SKIP
    }

}
