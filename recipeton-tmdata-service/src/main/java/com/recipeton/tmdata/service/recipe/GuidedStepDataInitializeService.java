package com.recipeton.tmdata.service.recipe;

/*-
 * #%L
 * recipeton-tmdata-service
 * %%
 * Copyright (C) 2021 tincomisc
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.recipeton.tmdata.config.ApplicationConfiguration;
import com.recipeton.tmdata.domain.misc.Locale;
import com.recipeton.tmdata.domain.recipe.*;
import com.recipeton.tmdata.domain.recipe.lang.GuidedTmSettingStepLangAttribute;
import com.recipeton.tmdata.service.DataInitializer;
import com.recipeton.tmdata.service.JsonResourceReaderService;
import com.recipeton.tmdata.service.material.MaterialService;
import com.recipeton.tmdata.service.misc.LocaleRepository;
import com.recipeton.tmdata.service.recipe.lang.GuidedTmSettingStepLangAttributeRepository;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.io.IOException;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@Slf4j
@Service
@AllArgsConstructor
public class GuidedStepDataInitializeService implements DataInitializer {

    private final ApplicationConfiguration applicationConfiguration;
    private final GuidedCreatedIngredientStepTextAttributeRepository guidedCreatedIngredientStepTextAttributeRepository;
    private final GuidedCreatedIngredientStepTextRepository guidedCreatedIngredientStepTextRepository;
    private final GuidedFreetextTypeRepository guidedFreetextTypeRepository;
    private final GuidedIngredientStepTextAttributeRepository guidedIngredientStepTextAttributeRepository;
    private final GuidedIngredientStepTextRepository guidedIngredientStepTextRepository;
    private final GuidedIngredientStepWeighingAttributeRepository guidedIngredientStepWeighingAttributeRepository;
    private final GuidedStepMaterialObjectTypeRepository guidedStepMaterialObjectTypeRepository;
    private final GuidedIngredientStepService guidedIngredientStepService;
    private final GuidedCreatedIngredientStepService guidedCreatedIngredientStepService;
    private final GuidedStepTypeRepository guidedStepTypeRepository;
    private final GuidedTmSettingStepLangAttributeRepository guidedTmSettingStepLangAttributeRepository;
    private final GuidedUtensilActionTypeRepository guidedUtensilActionTypeRepository;
    private final JsonResourceReaderService jsonResourceReaderService;
    private final LocaleRepository localeRepository;
    private final GuidedStepMaterialObjectRepository guidedStepMaterialObjectRepository;
    private final MaterialService materialService;


    @Override
    @Transactional
    public void doDataInitialize() throws IOException {
        DataInitializer.getPersisted(guidedStepTypeRepository, GuidedStepType.DEFAULT);
        DataInitializer.getPersisted(guidedStepMaterialObjectTypeRepository, GuidedStepMaterialObjectType.DEFAULT);

        DataInitializer.getPersisted(guidedFreetextTypeRepository, GuidedFreetextType.DEFAULT);
        DataInitializer.getPersisted(guidedTmSettingStepLangAttributeRepository, GuidedTmSettingStepLangAttribute.DEFAULT);

        DataInitializer.getPersisted(guidedCreatedIngredientStepTextAttributeRepository, GuidedCreatedIngredientStepTextAttribute.DEFAULT);
        if (guidedCreatedIngredientStepTextRepository.count() == 0) {
            List<GuidedCreatedIngredientStepTextLangForm> forms = jsonResourceReaderService.readCollection(applicationConfiguration.getDataDefaultGuidedCreatedIngredientStepTextLangs(), GuidedCreatedIngredientStepTextLangForm.class);

            Stream.of(GuidedCreatedIngredientStepTextAttribute.Enum.values())
                    .forEach(a -> {
                        Map<Locale, String> localization = forms.stream()
                                .filter(f -> f.getGuidedCreatedIngredientStepTextAttributeId() == a.getId())
                                .collect(Collectors.toMap(f -> localeRepository.getOne(f.getLocaleId()), GuidedCreatedIngredientStepTextLangForm::getText));
                        guidedCreatedIngredientStepTextRepository.save(guidedCreatedIngredientStepService.createGuidedCreatedIngredientStepText(localization, a, true));
                    });
        }


        DataInitializer.getPersisted(guidedIngredientStepWeighingAttributeRepository, GuidedIngredientStepWeighingAttribute.DEFAULT);
        DataInitializer.getPersisted(guidedIngredientStepTextAttributeRepository, GuidedIngredientStepTextAttribute.DEFAULT);
        if (guidedIngredientStepTextRepository.count() == 0) {
            List<GuidedIngredientStepTextLangForm> forms = jsonResourceReaderService.readCollection(applicationConfiguration.getDataDefaultGuidedIngredientStepTextLangs(), GuidedIngredientStepTextLangForm.class);

            Stream.of(GuidedIngredientStepTextAttribute.Enum.values())
                    .forEach(a -> {
                        Map<Locale, String> localization = forms.stream()
                                .filter(f -> f.getGuidedIngredientStepTextAttributeId() == a.getId())
                                .collect(Collectors.toMap(f -> localeRepository.getOne(f.getLocaleId()), GuidedIngredientStepTextLangForm::getText));
                        guidedIngredientStepTextRepository.save(guidedIngredientStepService.createGuidedIngredientStepText(localization, a, true));
                    });
        }


        DataInitializer.getPersisted(guidedUtensilActionTypeRepository, GuidedUtensilActionType.DEFAULT);
        if (guidedStepMaterialObjectRepository.count() == 0) {
            for (GuidedUtensilActionType.Enum type : GuidedUtensilActionType.Enum.values()) {
                materialService.getPersisted(type);
            }
        }
    }

    @Data
    private static class GuidedIngredientStepTextLangForm {
        private long guidedIngredientStepTextAttributeId;
        private long localeId;
        private String text;
    }


    @Data
    private static class GuidedCreatedIngredientStepTextLangForm {
        private long guidedCreatedIngredientStepTextAttributeId;
        private long localeId;
        private String text;
    }


}
