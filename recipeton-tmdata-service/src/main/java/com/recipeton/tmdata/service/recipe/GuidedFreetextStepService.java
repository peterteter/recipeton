package com.recipeton.tmdata.service.recipe;

/*-
 * #%L
 * recipeton-tmdata-service
 * %%
 * Copyright (C) 2021 tincomisc
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.recipeton.tmdata.domain.misc.Locale;
import com.recipeton.tmdata.domain.recipe.GuidedFreetextStep;
import com.recipeton.tmdata.domain.recipe.GuidedFreetextType;
import com.recipeton.tmdata.domain.recipe.GuidedStep;
import com.recipeton.tmdata.domain.recipe.GuidedStepType;
import com.recipeton.tmdata.domain.recipe.lang.GuidedFreetextStepLang;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import static java.util.List.of;

@Slf4j
@Service
@AllArgsConstructor
public class GuidedFreetextStepService {

    private final GuidedFreetextTypeRepository guidedFreetextTypeRepository;
    private final GuidedStepTypeRepository guidedStepTypeRepository;

    public GuidedStep createGuidedStepFreetext(GuidedFreetextType.Enum type, String text, Locale locale) {
        return new GuidedStep()
                .setPosition(-1L)
                .setGuidedFreetextStep(new GuidedFreetextStep()
                        .setGuidedFreetextType(guidedFreetextTypeRepository.getOne(type.getId()))
                        .setLangs(of(
                                new GuidedFreetextStepLang().setText(text).setLocale(locale)
                        )))
                .setGuidedStepType(guidedStepTypeRepository.getOne(GuidedStepType.Enum.FREETEXT));
    }
}
