package com.recipeton.util.jpa;

/*-
 * #%L
 * recipeton-tmdata-service
 * %%
 * Copyright (C) 2021 tincomisc
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import org.springframework.lang.Nullable;

import javax.persistence.AttributeConverter;
import java.time.LocalDateTime;
import java.time.ZoneOffset;

public class LocalDateTimeLongSecondsConverter implements AttributeConverter<LocalDateTime, Long> {

    @Nullable
    public Long convertToDatabaseColumn(LocalDateTime date) {
        return date == null ? null : date.toEpochSecond(ZoneOffset.UTC);
    }

    @Nullable
    public LocalDateTime convertToEntityAttribute(Long date) {
        return date == null ? null : LocalDateTime.ofEpochSecond(date, 0, ZoneOffset.UTC);
    }
}
