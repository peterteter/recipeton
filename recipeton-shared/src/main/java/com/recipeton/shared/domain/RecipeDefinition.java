package com.recipeton.shared.domain;

/*-
 * #%L
 * recipeton-shared
 * %%
 * Copyright (C) 2021 tincomisc
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;
import lombok.experimental.Accessors;

import java.time.Duration;
import java.util.List;

@Data
@Accessors(chain = true)
public class RecipeDefinition {

    private String name;

    @JsonFormat(shape = JsonFormat.Shape.STRING)
    private Duration totalTime;
    @JsonFormat(shape = JsonFormat.Shape.STRING)
    private Duration cookTime;
    @JsonFormat(shape = JsonFormat.Shape.STRING)
    private Duration prepTime;

    private String recipeYield;

    private List<String> recipeCategory;
    private List<RecipeSection> recipeSections;
    private List<String> hints;
    private List<String> utensils;
    private Nutrition nutrition;
    private List<String> keywords;
    private List<String> collections;
    private List<String> devices;

    private String locale;

    private String source;
    private String author;
    private String difficulty;
    private String rating; // Not set yet

    private String documentVersion;

    public boolean isKeywordPresent(String keyword) {
        return keywords != null && keywords.contains(keyword);
    }

    @Data
    @Accessors(chain = true)
    public static class RecipeSection {
        private String name;
        private List<String> ingredients;
        private List<String> instructions;
    }

    @Data
    @Accessors(chain = true)
    public static class Nutrition {
        private String calories;
        private String carbohydrateContent;
        private String fatContent;
        private String proteinContent;
        private String fibreContent;
        private String cholesterolContent;
    }


}
