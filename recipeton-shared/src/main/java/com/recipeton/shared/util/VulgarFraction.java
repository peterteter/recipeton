package com.recipeton.shared.util;

/*-
 * #%L
 * recipeton-shared
 * %%
 * Copyright (C) 2021 tincomisc
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import java.math.BigDecimal;
import java.util.regex.Pattern;
import java.util.stream.Stream;

public enum VulgarFraction {

    ONE_QUARTER('¼', new BigDecimal("0.25")),
    ONE_HALF('½', new BigDecimal("0.5")),
    THREE_QUARTER('¾', new BigDecimal("0.75")),
    ONE_EIGHT('⅛', new BigDecimal("0.125")),
    TWO_THIRDS('⅔', new BigDecimal("0.66"));


    public static final Pattern PATTERN = Pattern.compile("[¼-¾⅐-⅞]");

    private final char character;
    private final BigDecimal value;

    VulgarFraction(char character, BigDecimal value) {
        this.character = character;
        this.value = value;
    }

    public static VulgarFraction by(char character) {
        return Stream.of(values()).filter(v -> v.character == character).findFirst().orElseThrow(() -> new IllegalArgumentException("No fraction for code=" + character));
    }

    public static VulgarFraction by(String text) {
        return by(text.charAt(0));
    }

    public BigDecimal getValue() {
        return value;
    }
}
