package com.recipeton.shared.util;

/*-
 * #%L
 * recipeton-shared
 * %%
 * Copyright (C) 2021 tincomisc
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import lombok.Data;
import lombok.extern.slf4j.Slf4j;

import java.util.HashMap;
import java.util.Map;

@Slf4j
public class StringMatchTraverser<T> {
    private final Node matchRoot;
    private final Map<String, Node> transitionsMap = new HashMap<>();
    private int nodeIdGenerator;
    private Node pointer;

    // Optional. Keep stack of matched nodes to be able to continue on submatches (if ever needed)

    public StringMatchTraverser(Map<String, T> replacements) {
        this.matchRoot = init(replacements);
    }

    public T getMatchReplacement() {
        return pointer.matchReplacement;
    }

    public String getMatchedSequence() {
        return pointer.matchSequence;
    }

    private Node init(Map<String, T> tokenSequences) {
        Node root = new Node(null);

        tokenSequences.entrySet().forEach(e -> {
            Node node = root;
            String matchString = e.getKey();
            for (int i = 0; i < matchString.length(); i++) {
                char character = matchString.charAt(i);
                Node next = node.findNext(character);
                if (next == null) {
                    next = new Node((i == matchString.length() - 1) ? e : null); // NOPMD
                    node.setNext(character, next);
                }
                node = next;
            }
        });

        return root;
    }

    public boolean isMatch() {
        return pointer != null && pointer.matchReplacement != null;
    }

    public boolean nextChar(char c) {
        if (pointer == null) { // No sequence
            pointer = matchRoot.findNext(c);
        } else { // Following sequence
            pointer = pointer.findNext(c);
            if (pointer == null) { // Sequence interrupted
                pointer = matchRoot.findNext(c); // Check if new sequence beginning
            }
        }
        return isMatch();
    }

    public void reset() {
        pointer = null;
    }

    @Data
    private final class Node {
        private final int id;

        private String matchSequence;
        private T matchReplacement;

        private Node(Map.Entry<String, T> replacement) {
            this.id = nodeIdGenerator++;
            if (replacement == null) {
                matchSequence = null;
                matchReplacement = null;
            } else {
                this.matchSequence = replacement.getKey();
                this.matchReplacement = replacement.getValue();
            }
        }

        private Node findNext(char c) {
            return transitionsMap.get(id + "_" + c);
        }

        private Node setNext(char character, Node next) {
            return transitionsMap.put(id + "_" + character, next);
        }
    }
}
