package com.recipeton.shared.util;

/*-
 * #%L
 * recipeton-shared
 * %%
 * Copyright (C) 2021 tincomisc
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import java.util.AbstractList;
import java.util.ArrayList;
import java.util.List;

public final class Partition<T> extends AbstractList<List<T>> {
    private final List<T> list;
    private final int chunkSize;

    public Partition(List<T> list, int chunkSize) {
        super();
        this.list = new ArrayList(list);
        this.chunkSize = chunkSize;
    }

    public static <T> Partition<T> ofSize(List<T> list, int chunkSize) {
        assert chunkSize > 0;

        return new Partition(list, chunkSize);
    }

    public List<T> get(int index) {
        int start = index * this.chunkSize;
        int end = Math.min(start + this.chunkSize, this.list.size());
        if (start > end) {
            throw new IndexOutOfBoundsException("Index " + index + " is out of the list range <0," + (this.size() - 1) + ">");
        } else {
            return new ArrayList(this.list.subList(start, end));
        }
    }

    public int size() {
        return (int) Math.ceil((double) this.list.size() / (double) this.chunkSize);
    }
}
