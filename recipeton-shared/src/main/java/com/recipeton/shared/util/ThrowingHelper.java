package com.recipeton.shared.util;

/*-
 * #%L
 * recipeton-shared
 * %%
 * Copyright (C) 2021 tincomisc
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import java.util.function.Consumer;
import java.util.function.Function;
import java.util.function.Supplier;

public class ThrowingHelper {

    public static <T, E extends Throwable> Consumer<T> uConsumer(ThrowingConsumer<T, E> consumer) {
        return ThrowingConsumer.unchecked(consumer);
    }

    public static <T, R, E extends Throwable> Function<T, R> uFunction(ThrowingFunction<T, R, E> function) {
        return ThrowingFunction.unchecked(function);
    }

    public static <R, E extends Throwable> Supplier<R> uSupplier(ThrowingSupplier<R, E> supplier) {
        return ThrowingSupplier.unchecked(supplier);
    }

    @FunctionalInterface
    public interface ThrowingConsumer<T, E extends Throwable> {
        static <T, E extends Throwable> Consumer<T> unchecked(ThrowingConsumer<T, E> consumer) {
            return (t) -> {
                try {
                    consumer.accept(t);
                } catch (Throwable e) { // NOPMD this is the whole purpose
                    throw new RuntimeException(e); // NOPMD this is the whole purpose
                }
            };
        }

        void accept(T t) throws E;
    }

    @FunctionalInterface
    public interface ThrowingFunction<T, R, E extends Throwable> {
        static <T, R, E extends Throwable> Function<T, R> unchecked(ThrowingFunction<T, R, E> f) {
            return t -> {
                try {
                    return f.apply(t);
                } catch (Throwable e) { // NOPMD this is the whole purpose
                    throw new RuntimeException(e); // NOPMD this is the whole purpose
                }
            };
        }

        R apply(T t) throws E;
    }

    @FunctionalInterface
    public interface ThrowingSupplier<R, E extends Throwable> {
        static <R, E extends Throwable> Supplier<R> unchecked(ThrowingSupplier<R, E> f) {
            return () -> {
                try {
                    return f.get();
                } catch (Throwable e) { // NOPMD this is the whole purpose
                    throw new RuntimeException(e); // NOPMD this is the whole purpose
                }
            };
        }

        R get() throws E;
    }
}
