package com.recipeton.shared.util;

/*-
 * #%L
 * recipeton-shared
 * %%
 * Copyright (C) 2021 tincomisc
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import java.util.Arrays;
import java.util.Collection;
import java.util.Iterator;
import java.util.stream.Stream;
import java.util.stream.StreamSupport;

public class StreamUtil {

    public static <T> Stream<T> concat(Stream<T> stream, Stream<T>... streams) {
        if (streams == null) {
            return stream;
        }
        boolean onlyTwo = streams.length == 1;
        if (onlyTwo) {
            return Stream.concat(stream, streams[0]);
        }
        return Stream.concat(stream, concat(streams[0], Arrays.copyOfRange(streams, 1, streams.length)));
    }

    public static <T> Stream<T> toStream(Iterator<T> iterator, boolean parallel) {
        if (iterator == null) {
            return Stream.empty();
        }
        return StreamSupport.stream(((Iterable<T>) () -> iterator).spliterator(), parallel);
    }

    public static <T> Stream<T> toStream(Iterator<T> iterator) {
        return toStream(iterator, false);
    }

    public static <T> Stream<T> toStream(Iterable<T> iterable) {
        if (iterable == null) {
            return Stream.empty();
        }
        return StreamSupport.stream(iterable.spliterator(), false);
    }

    public static <T> Stream<T> toStream(Collection<T> collection) {
        if (collection == null) {
            return Stream.empty();
        }
        return collection.stream();
    }

    public static <T> Stream<T> toStream(T... array) {
        if (array == null) {
            return Stream.empty();
        }
        return Arrays.stream(array);
    }
}
