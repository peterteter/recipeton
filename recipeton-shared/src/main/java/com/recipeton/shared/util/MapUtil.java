package com.recipeton.shared.util;

/*-
 * #%L
 * recipeton-shared
 * %%
 * Copyright (C) 2021 tincomisc
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import org.apache.commons.lang3.tuple.Pair;

import java.util.Map;
import java.util.stream.Collectors;
import java.util.stream.IntStream;
import java.util.stream.Stream;

public class MapUtil {

    public static <T, U> Map<T, U> toMap(T k, U v) {
        return toMapP(Pair.of(k, v));
    }

    public static <T, U> Map<T, U> toMap(T k1, U v1, T k2, U v2) {
        return toMapP(Pair.of(k1, v1), Pair.of(k2, v2));
    }

    public static <T, U> Map<T, U> toMap(T k1, U v1, T k2, U v2, T k3, U v3) {
        return toMapP(Pair.of(k1, v1), Pair.of(k2, v2), Pair.of(k3, v3));
    }

    public static <T, U> Map<T, U> toMap(T k1, U v1, T k2, U v2, T k3, U v3, T k4, U v4) {
        return toMapP(Pair.of(k1, v1), Pair.of(k2, v2), Pair.of(k3, v3), Pair.of(k4, v4));
    }

    public static <T, U> Map<T, U> toMap(T k1, U v1, T k2, U v2, T k3, U v3, T k4, U v4, T k5, U v5) {
        return toMapP(Pair.of(k1, v1), Pair.of(k2, v2), Pair.of(k3, v3), Pair.of(k4, v4), Pair.of(k5, v5));
    }

    public static <T, U> Map<T, U> toMap(T k1, U v1, T k2, U v2, T k3, U v3, T k4, U v4, T k5, U v5, T k6, U v6) {
        return toMapP(Pair.of(k1, v1), Pair.of(k2, v2), Pair.of(k3, v3), Pair.of(k4, v4), Pair.of(k5, v5), Pair.of(k6, v6));
    }

    public static <T, U> Map<T, U> toMap(T k1, U v1, T k2, U v2, T k3, U v3, T k4, U v4, T k5, U v5, T k6, U v6, T k7, U v7) {
        return toMapP(Pair.of(k1, v1), Pair.of(k2, v2), Pair.of(k3, v3), Pair.of(k4, v4), Pair.of(k5, v5), Pair.of(k6, v6), Pair.of(k7, v7));
    }

    public static <T, U> Map<T, U> toMap(T k1, U v1, T k2, U v2, T k3, U v3, T k4, U v4, T k5, U v5, T k6, U v6, T k7, U v7, T k8, U v8) {
        return toMapP(Pair.of(k1, v1), Pair.of(k2, v2), Pair.of(k3, v3), Pair.of(k4, v4), Pair.of(k5, v5), Pair.of(k6, v6), Pair.of(k7, v7), Pair.of(k8, v8));
    }

    public static <T, U> Map<T, U> toMap(T k1, U v1, T k2, U v2, T k3, U v3, T k4, U v4, T k5, U v5, T k6, U v6, T k7, U v7, T k8, U v8, T k9, U v9) {
        return toMapP(Pair.of(k1, v1), Pair.of(k2, v2), Pair.of(k3, v3), Pair.of(k4, v4), Pair.of(k5, v5), Pair.of(k6, v6), Pair.of(k7, v7), Pair.of(k8, v8), Pair.of(k9, v9));
    }

    public static <T, U> Map<T, U> toMap(T k1, U v1, T k2, U v2, T k3, U v3, T k4, U v4, T k5, U v5, T k6, U v6, T k7, U v7, T k8, U v8, T k9, U v9, T k10, U v10) {
        return toMapP(Pair.of(k1, v1), Pair.of(k2, v2), Pair.of(k3, v3), Pair.of(k4, v4), Pair.of(k5, v5), Pair.of(k6, v6), Pair.of(k7, v7), Pair.of(k8, v8), Pair.of(k9, v9), Pair.of(k10, v10));
    }

    public static <T, U> Map<T, U> toMap(T k1, U v1, T k2, U v2, T k3, U v3, T k4, U v4, T k5, U v5, T k6, U v6, T k7, U v7, T k8, U v8, T k9, U v9, T k10, U v10, T k11, U v11) {
        return toMapP(Pair.of(k1, v1), Pair.of(k2, v2), Pair.of(k3, v3), Pair.of(k4, v4), Pair.of(k5, v5), Pair.of(k6, v6), Pair.of(k7, v7), Pair.of(k8, v8), Pair.of(k9, v9), Pair.of(k10, v10), Pair.of(k11, v11));
    }

    public static <T, U> Map<T, U> toMap(T k1, U v1, T k2, U v2, T k3, U v3, T k4, U v4, T k5, U v5, T k6, U v6, T k7, U v7, T k8, U v8, T k9, U v9, T k10, U v10, T k11, U v11, T k12, U v12) {
        return toMapP(Pair.of(k1, v1), Pair.of(k2, v2), Pair.of(k3, v3), Pair.of(k4, v4), Pair.of(k5, v5), Pair.of(k6, v6), Pair.of(k7, v7), Pair.of(k8, v8), Pair.of(k9, v9), Pair.of(k10, v10), Pair.of(k11, v11), Pair.of(k12, v12));
    }

    public static Map<String, String> toMap(String... keyValues) {
        if (keyValues == null) {
            return null;
        }
        return IntStream.range(0, keyValues.length / 2).boxed().collect(Collectors.toMap(i -> keyValues[i * 2], i -> keyValues[i * 2 + 1]));
    }

    public static <T, U> Map<T, U> toMapP(Pair<T, U>... pairs) {
        return Stream.of(pairs).filter(p -> p.getRight() != null).collect(Collectors.toMap(Pair::getLeft, Pair::getRight));
    }
}
