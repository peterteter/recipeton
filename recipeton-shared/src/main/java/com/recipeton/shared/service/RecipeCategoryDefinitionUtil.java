package com.recipeton.shared.service;

/*-
 * #%L
 * recipeton-shared
 * %%
 * Copyright (C) 2021 tincomisc
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import org.apache.commons.lang3.StringUtils;

public final class RecipeCategoryDefinitionUtil {

    public static final String RECIPE_CATEGORY_PRINCIPAL_UID_PREFIX = "0_";
    public static final String RECIPE_CATEGORY_EXTRA_PREFIX = "#";

    private RecipeCategoryDefinitionUtil() {
    }

    public static String createRecipeCategoryUidByPrincipal(String uidPart) {
        return RECIPE_CATEGORY_PRINCIPAL_UID_PREFIX + uidPart;
    }

    public static boolean isRecipeCategoryUidExtra(String uid) {
        return StringUtils.startsWith(uid, RECIPE_CATEGORY_EXTRA_PREFIX);
    }

    public static boolean isRecipeCategoryUidPrincipal(String uid) {
        return StringUtils.startsWith(uid, RECIPE_CATEGORY_PRINCIPAL_UID_PREFIX);
    }

}
