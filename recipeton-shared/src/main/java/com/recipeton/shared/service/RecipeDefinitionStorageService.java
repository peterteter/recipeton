package com.recipeton.shared.service;

/*-
 * #%L
 * recipeton-scrapper
 * %%
 * Copyright (C) 2021 tincomisc
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.fasterxml.jackson.databind.ObjectMapper;
import com.recipeton.shared.domain.RecipeDefinition;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.Optional;

@Slf4j
@RequiredArgsConstructor
public class RecipeDefinitionStorageService {

    private final ObjectMapper objectMapper;

    public Boolean isRecipe(Path path) {
        return Optional.of(path.getFileName().toString()).map(n -> n.endsWith("yaml") || n.endsWith("yml")).orElse(false);
    }

    public RecipeDefinition read(Path path) throws IOException {
        try (InputStream is = Files.newInputStream(path)) {
            return read(is);
        }
    }

    public RecipeDefinition read(InputStream is) throws IOException {
        return objectMapper.readValue(is, RecipeDefinition.class);
    }

    public void write(RecipeDefinition recipe, Path path) throws IOException {
        try (OutputStream os = Files.newOutputStream(path)) {
            write(recipe, os);
        }
    }

    public void write(RecipeDefinition recipe, OutputStream os) throws IOException {
        objectMapper.writeValue(os, recipe);
    }

}
