package com.recipeton.shared.util;

/*-
 * #%L
 * recipeton-shared
 * %%
 * Copyright (C) 2021 tincomisc
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import org.junit.jupiter.api.Test;

import java.util.regex.Pattern;

import static org.assertj.core.api.AssertionsForInterfaceTypes.assertThat;

class TextUtilTest {

    @Test
    void testNormalizeAsciiGivenStringWithNonAsciiCharactersThenReplacesOnlyFirstCharacterWithAsciiEquivalent() {
        assertThat(TextUtil.normalizeAsciiFirstChar("àèìòùÀÈÌÒÙáéíóúýÁÉÍÓÚÝâêîôûÂÊÎÔÛãñõÃÑÕ")).isEqualTo("aèìòùÀÈÌÒÙáéíóúýÁÉÍÓÚÝâêîôûÂÊÎÔÛãñõÃÑÕ");
    }

    @Test
    void testNormalizeAsciiGivenStringWithNonAsciiCharactersThenReplacesWithAsciiEquivalent() {
        assertThat(TextUtil.normalizeAscii("àèìòùÀÈÌÒÙáéíóúýÁÉÍÓÚÝâêîôûÂÊÎÔÛãñõÃÑÕ")).isEqualTo("aeiouAEIOUaeiouyAEIOUYaeiouAEIOUanoANO");
    }

    @Test
    void testNormalizeAsciiGivenStringWithNumbersThenNumbersArePreserved() {
        assertThat(TextUtil.normalizeAsciiFirstChar("0123456789")).isEqualTo("0123456789");
    }

    @Test
    void testRemoveAllGivenTextAnAndMatchingTemplateThenReturnsTextWithoutMatchedTextParts() {
        Pattern p = Pattern.compile("<br>|<br />|<strong>|</strong>");
        assertThat(TextUtil.removeAll("a<br><br>b", p)).isEqualTo("ab");
        assertThat(TextUtil.removeAll("<br>a<br>b<br>", p)).isEqualTo("ab");
        assertThat(TextUtil.removeAll("<strong>a<br><br /><br>b</strong>", p)).isEqualTo("ab");
    }
}
