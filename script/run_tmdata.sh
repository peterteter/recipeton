#!/bin/sh
# run tmdata-service for every cookbook with non-empty import dir
TMDATA=recipeton-tmdata-service-0.0.2-SNAPSHOT.jar
DIR=~/recipeton/tmdata
for recipe_dir in $DIR/* ; do
	name=${recipe_dir##*/}
    if [ "$name" != "shared" ]; then
    	[ "$(ls -A $recipe_dir/import)" ] && java -jar $TMDATA --recipeton.bookName=$name
    fi
done
