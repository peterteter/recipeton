#!/bin/sh
# move crawled recipes to process them with tmdata-service
DIR=~/recipeton/tmdata
for recipe_dir in $DIR/* ; do
    name=${recipe_dir##*/}
    if [ "$name" != "shared" ]; then
	    if [ ! -d "$recipe_dir/import" ]; then
	  		mkdir "$recipe_dir/import"
	  		for recipe in $recipe_dir/* ; do
	  			mv "$recipe" "$recipe_dir/import/"
	  		done
		fi
    fi
done
