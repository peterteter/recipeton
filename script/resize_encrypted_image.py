import sys
import os

if len(sys.argv) < 3:
	print("use like: python resize.py image.iso 1234")
	sys.exit(0)

path = sys.argv[1]
try:
	future_size = int(sys.argv[2])
except Exception as e:
	print("size must be int!")
	sys.exit(0)
print("reading {}".format(path))

current_size = os.stat(path).st_size
print("file size: {}".format(current_size))

diff= future_size - current_size
if diff <= 0:
	print("file is already big enough")
	sys.exit(0)

print("size difference: {}".format(diff))

with open(path, 'a') as f:
  f.write('\0'*diff)

new_size = os.stat(path).st_size
print("new file size: {}".format(new_size))
