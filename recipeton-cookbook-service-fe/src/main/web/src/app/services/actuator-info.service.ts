/*-
 * #%L
 * recipeton-cookbook-service-fe
 * %%
 * Copyright (C) 2021 tincomisc
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */
import {HttpClient} from '@angular/common/http';
import {Injectable} from '@angular/core';
import {Observable, of} from 'rxjs';
import {catchError} from 'rxjs/operators';
import {AppConfig} from '../app.config';

export class ActuatorInfo {
  constructor(
    public build: ActuatorInfoBuild) {
  }
}

export class ActuatorInfoBuild {
  constructor(
    public version: string,
    public group: string,
    public artifact: string) {
  }
}

@Injectable({
  providedIn: 'root'
})
export class ActuatorInfoService {
  endpoint: string;

  constructor(private http: HttpClient) {
    this.endpoint = AppConfig.endpoints.actuator + '/info';
  }

  get(): Observable<ActuatorInfo> {
    return this.http.get<ActuatorInfo>(this.endpoint)
      .pipe(
        catchError((error: any): Observable<ActuatorInfo> => {
          // Should eventually: send the error to remote logging infrastructure
          console.error(error); // log to console instead
          if (error.status >= 400) {
            throw error;
          }

          return of();
        })
      );
  }
}
