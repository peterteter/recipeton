/*-
 * #%L
 * recipeton-cookbook-service-fe
 * %%
 * Copyright (C) 2021 tincomisc
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */
import {HttpClient} from '@angular/common/http';
import {Injectable} from '@angular/core';
import {Observable} from 'rxjs';

import {AppConfig} from '../app.config';
import {RecipeCollection} from '../models/recipeCollection.model';
import {RecipeCollectionContents} from '../models/recipeCollectionContents.model';
import {RecipePublishRequest} from '../models/recipePublishRequest.model';

@Injectable({
  providedIn: 'root'
})
export class RecipeCollectionService {
  private endpoint: string;

  constructor(private http: HttpClient) {
    this.endpoint = AppConfig.endpoints.recipetonRecipeCollectionApi;
  }

  getAll(): Observable<RecipeCollection[]> {
    return this.http.get<RecipeCollection[]>(this.endpoint);
  }

  get(id: string): Observable<RecipeCollection> {
    // @ts-ignore
    return this.http.get(`${this.endpoint}/${id}`);
  }

  getContents(id: string): Observable<RecipeCollectionContents> {
    // @ts-ignore
    return this.http.get(`${this.endpoint}/${id}/content`);
  }

  put(data: RecipeCollection): Observable<any> {
    return this.http.put(`${this.endpoint}/${data.id}`, data);
  }

  postPublishRequest(recipePublishRequest: RecipePublishRequest): Observable<any> {
    return this.http.post(`${this.endpoint}/${recipePublishRequest.id}/publish`, recipePublishRequest);
  }
}
