/*-
 * #%L
 * recipeton-cookbook-service-fe
 * %%
 * Copyright (C) 2021 tincomisc
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */
import {InjectionToken} from '@angular/core';
import {environment} from '../environments/environment';

export let APP_CONFIG = new InjectionToken('app.config');

export const AppConfig: any = {
  defaultLanguage: 'en',
  routes: {
    app: 'app',
    recipeton: 'recipeton',
  },
  endpoints: {
    actuator: environment.apiRoot + '/actuator',
    recipetonAuthApi: environment.apiRoot + '/api/auth',
    recipetonRecipeCollectionApi: environment.apiRoot + '/api/recipeCollection',
  },
  snackBarDuration: 1000
};
