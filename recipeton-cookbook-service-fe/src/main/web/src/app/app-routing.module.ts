/*-
 * #%L
 * recipeton-cookbook-service-fe
 * %%
 * Copyright (C) 2021 tincomisc
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */
import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {AppConfig} from './app.config';
import {Error404Component} from './components/error404/error404.component';
import {RecipeCollectionsComponent} from './components/recipeCollections/recipe-collections.component';

const routes: Routes = [
  {path: AppConfig.routes.recipeton, component: RecipeCollectionsComponent},
  {path: AppConfig.routes.recipeton + "/collections", component: RecipeCollectionsComponent},
  {path: AppConfig.routes.app + '/404', component: Error404Component},

  // On home go to...
  {path: '', pathMatch: 'prefix', redirectTo: '/' + AppConfig.routes.recipeton},
  // otherwise redirect to 404
  {path: '**', redirectTo: '/' + AppConfig.routes.app + '/404'}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule {
}
