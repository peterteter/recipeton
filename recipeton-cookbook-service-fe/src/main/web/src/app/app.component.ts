/*-
 * #%L
 * recipeton-cookbook-service-fe
 * %%
 * Copyright (C) 2021 tincomisc
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */
import {AfterViewInit, Component, OnDestroy, OnInit} from '@angular/core';
import {Title} from '@angular/platform-browser';
import {StorageMap} from '@ngx-pwa/local-storage';
import {TranslateService} from '@ngx-translate/core';
import {AppConfig} from './app.config';

declare const require;

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit, OnDestroy, AfterViewInit {

  isOnline: boolean;

  constructor(public title: Title,
              private translateService: TranslateService,
              private storage: StorageMap) {
    this.isOnline = navigator.onLine;
    storage.get("language").subscribe(l => {
      this.onLanguageRetrieved(<string>l);
    });
  }

  doReload() {
    window.location.reload();
  }

  ngAfterViewInit(): void {
    // this.getUser();
  }

  ngOnDestroy() {
    // this.webSocketService.disconnect();
  }

  ngOnInit(): void {
    console.log('ngOnInit');
    this.translateService.setDefaultLang(AppConfig.defaultLanguage);
    this.translateService.setTranslation('en', require('../assets/i18n/en.json'));
    this.translateService.setTranslation('es', require('../assets/i18n/es.json'));
    this.title.setTitle(this.translateService.instant('app'));
  }

  private onLanguageRetrieved(language: string) {
    this.translateService.use(language);
  }
}
