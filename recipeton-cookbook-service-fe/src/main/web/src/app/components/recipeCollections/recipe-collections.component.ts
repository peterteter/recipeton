/*-
 * #%L
 * recipeton-cookbook-service-fe
 * %%
 * Copyright (C) 2021 tincomisc
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */
import {animate, state, style, transition, trigger} from '@angular/animations';
import {AfterViewInit, Component, ElementRef, OnInit, ViewChild} from '@angular/core';
import {MatDialog} from '@angular/material/dialog';
import {MatSnackBar} from '@angular/material/snack-bar';
import {StorageMap} from '@ngx-pwa/local-storage';
import {TranslateService} from '@ngx-translate/core';
import {fromEvent} from 'rxjs';
import {debounceTime, distinctUntilChanged, tap} from 'rxjs/operators';
import {AppConfig} from '../../app.config';
import {RecipeCollection} from '../../models/recipeCollection.model';
import {RecipeCollectionService} from '../../services/recipeCollection.service';
import {AboutComponent} from '../about/about.component';

const STORAGE_PREFIX = "RC_";

@Component({
  selector: 'app-recipe-collections',
  templateUrl: './recipe-collections.component.html',
  styleUrls: ['./recipe-collections.component.scss'],
  animations: [
    trigger('detailExpand', [
      state('collapsed', style({height: '0px', minHeight: '0'})),
      state('expanded', style({height: '*'})),
      transition('expanded <=> collapsed', animate('225ms cubic-bezier(0.4, 0.0, 0.2, 1)')),
    ]),
  ],
})
export class RecipeCollectionsComponent implements OnInit, AfterViewInit {
  displayedColumns: string[] = ['recipeCollection', 'actions'];

  recipeCollections?: RecipeCollection[];
  recipeCollectionsExpanded = new Set<string>();

  searchRecipeCollections?: RecipeCollection[];
  searchBarActive: boolean;

  error: Error;
  busy: boolean;

  @ViewChild('queryInput', {static: true}) queryInput: ElementRef;

  private searchTerms: string;
  private selectedRecipeCollectionId: string;

  constructor(private recipeCollectionService: RecipeCollectionService,
              private matSnackBar: MatSnackBar,
              private dialog: MatDialog,
              private translateService: TranslateService,
              private storage: StorageMap) {
  }

  doPublish(recipeCollectionId: string) {
    this.busy = true
    this.recipeCollectionService.postPublishRequest({id: recipeCollectionId}).subscribe(
      d => {
        console.log(d);
        this.reload();

        this.translateService.get('Cookbook published').subscribe(t => {
          this.matSnackBar.open(t, 'OK', {duration: AppConfig.snackBarDuration});
        });
      },
      e => {
        this.busy = false;
        this.error = e;
        console.log("error", e);
      });
  }

  async fetchRecipeCollections() {
    this.busy = true;
    this.recipeCollectionService.getAll()
      .pipe(
        tap(recipeCollections => {
          // console.log("tap", recipeCollections)
          let expectedStorageRecipeCollectionIds = recipeCollections.map(rc => STORAGE_PREFIX + rc.id);

          this.storage.keys().subscribe(key => {
            if (key.startsWith(STORAGE_PREFIX)) {
              if (expectedStorageRecipeCollectionIds.indexOf(key) == -1) {
                this.storage.delete(key).subscribe(() => {
                });
              }
            }
          });

          recipeCollections.forEach(rc => {
            this.storage.get(STORAGE_PREFIX + rc.id).subscribe(rcc => {
              if (rcc === undefined) {
                this.recipeCollectionService.getContents(rc.id).subscribe(
                  d => {
                    rc.titles = d.titles;
                    this.onDataReceived();
                    this.storage.set(STORAGE_PREFIX + rc.id, d).subscribe(() => {
                    });
                  },
                  e => {
                    this.error = e;
                    console.log("error", e);
                  });
              } else {
                // @ts-ignore
                rc.titles = rcc.titles;
                this.onDataReceived();
              }
            });
          });
        })
      )
      .subscribe(
        d => {
          this.recipeCollections = d;
          this.onDataReceived();
        },
        e => {
          console.log(e);
          this.error = e;
          this.busy = false;
        });
  }

  getRecipeCollectionTitlesByRecipeCollection(recipeCollection: RecipeCollection) {
    if (!this.isExpanded(recipeCollection)) {
      return [];
    }
    let titles = recipeCollection.titles;
    if (titles === undefined) {
      return [];
    }
    return titles;
  }

  getRecipeCollections() {
    if (this.isSearchMode()) {
      return this.searchRecipeCollections;
    }
    return this.recipeCollections;
  }

  isExpanded(recipeCollection: RecipeCollection) {
    if (this.isSearchMode()) {
      return recipeCollection.titles !== undefined;
    }
    return this.recipeCollectionsExpanded.has(recipeCollection.id);
  }

  isPublishDisabled() {
    return this.busy || this.selectedRecipeCollectionId === undefined;
  }

  isSearchMode() {
    return this.searchRecipeCollections !== undefined;
  }

  isSelected(recipeCollection: RecipeCollection) {
    return recipeCollection.id == this.selectedRecipeCollectionId;
  }

  ngAfterViewInit() {
    fromEvent(this.queryInput.nativeElement, 'keyup')
      .pipe(
        debounceTime(1000),
        distinctUntilChanged(),
        tap(() => {
          this.doSearch(this.getSearchTermsFieldValue(), this.recipeCollections);
        })
      ).subscribe();
  }

  ngOnInit(): void {
    this.fetchRecipeCollections();
  }

  onAboutClicked(): void {
    this.dialog.open(AboutComponent);
  }

  onExpandClicked(recipeCollection: RecipeCollection) {
    this.toggleExpand(recipeCollection);
    this.setSelected(recipeCollection);
  }

  onPublishClicked() {
    if (this.selectedRecipeCollectionId === undefined) {
      this.translateService.get('Select cookbook').subscribe(t => {
        this.matSnackBar.open(t, 'OK', {duration: AppConfig.snackBarDuration});
      });
      return;
    }

    this.doPublish(this.selectedRecipeCollectionId);
  }

  onRowClicked(recipeCollection: RecipeCollection) {
    console.log("onRowClicked", recipeCollection);
    if (this.isSelected(recipeCollection)) {
      this.toggleExpand(recipeCollection);
      return;
    }
    this.setSelected(recipeCollection);
  }

  onSearchButtonClicked() {
    if (this.searchBarActive) {
      this.queryInput.nativeElement.value = '';
      this.doSearch(undefined, this.recipeCollections);
      this.searchBarActive = false;
    } else {
      this.searchBarActive = true;
      this.queryInput.nativeElement.focus();
    }
  }

  reload(): void {
    this.fetchRecipeCollections();
  }

  private async doSearch(searchTerms: string, recipeCollections: RecipeCollection[]) {
    console.log("computeSearch", searchTerms);

    if (recipeCollections === undefined) {
      console.log("No data to search");
      return;
    }
    if (searchTerms === undefined || searchTerms.length < 3) {
      if (this.searchTerms !== undefined) {
        console.log("Search terms clear");
        this.searchTerms = undefined;
        this.searchRecipeCollections = undefined;
        console.log("Search terms cleared");
      }
      return
    }

    let searchTermsFormatted = searchTerms.toLowerCase();
    this.searchTerms = searchTermsFormatted;

    this.busy = true;
    let promise = new Promise<RecipeCollection[]>((resolve, reject) => {
      let searchRecipeCollections = [];
      recipeCollections.forEach(rc => {
        if (rc.titles != undefined) {
          let filteredTitles = rc.titles.filter(t => t.toLowerCase().includes(searchTermsFormatted));
          if (filteredTitles.length > 0) {
            searchRecipeCollections.push({
              id: rc.id,
              name: rc.name,
              published: rc.published,
              titles: filteredTitles
            });
          }
        } else {
          console.log("searching NO TITLES!!!")
        }
      });
      resolve(searchRecipeCollections);
    });
    await promise.then(r => {
      this.selectedRecipeCollectionId = undefined;
      this.searchRecipeCollections = r;
      this.busy = false;
    });
  }

  private getSearchTermsFieldValue() {
    return this.queryInput.nativeElement.value ? this.queryInput.nativeElement.value : undefined;
  }

  private onDataReceived() {
    let undefinedCount = this.recipeCollections.map(rc => rc.titles).filter(t => t === undefined).length;
    // console.log("onDataReceived ", this.recipeCollections.length, undefinedCount);
    if (undefinedCount == 0) {
      this.onDataReceivedComplete();
    }
  }

  private onDataReceivedComplete() {
    console.log("onDataReceivedComplete sm=", this.isSearchMode());
    this.busy = false;
    if (this.isSearchMode()) {
      this.doSearch(this.getSearchTermsFieldValue(), this.recipeCollections);
    }

  }

  private setSelected(recipeCollection: RecipeCollection) {
    this.selectedRecipeCollectionId = recipeCollection.id;
  }

  private toggleExpand(recipeCollection: RecipeCollection) {
    if (this.isExpanded(recipeCollection)) {
      this.recipeCollectionsExpanded.delete(recipeCollection.id);
    } else {
      this.recipeCollectionsExpanded.add(recipeCollection.id);
    }
  }
}

