/*-
 * #%L
 * recipeton-cookbook-service-fe
 * %%
 * Copyright (C) 2021 tincomisc
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */
import {Component, OnInit} from '@angular/core';
import {ActuatorInfo, ActuatorInfoService} from '../../services/actuator-info.service';

@Component({
  selector: 'app-about',
  templateUrl: './about.component.html',
})
export class AboutComponent implements OnInit {

  actuatorInfo: ActuatorInfo;

  constructor(private actuatorInfoService: ActuatorInfoService) {
  }

  isBusy(): boolean {
    return !this.actuatorInfo;
  }

  ngOnInit() {
    this.actuatorInfoService.get().subscribe(i => {
      this.actuatorInfo = i;
    });
  }
}
