#!/bin/sh -e
#
printf "Removing gmasstorage module..."
/sbin/rmmod -v g_mass_storage
printf "gmasstorage module removed"
exit 0
