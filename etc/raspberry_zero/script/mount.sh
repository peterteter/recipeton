#!/bin/sh -e
#
printf "Loading module gmasstorage. file=$1"
/sbin/modprobe -v g_mass_storage file=$1 stall=0 idVendor=0x090c idProduct=0x1000 iManufacturer="General" iProduct="USB Flash Disk" iSerialNumber="0 0;exit 0" bcdDevice=0x1100 cdrom=y ro=y
printf "gmasstorage module loaded"

exit 0
